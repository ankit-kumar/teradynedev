package com.teradyne.pricing.model;

import java.util.ArrayList;

import org.w3c.dom.Document;

public class PricingLines {

	private String organizationCode;
	private Document storage;
	
	private ArrayList<PricingLine> priceLines = new ArrayList<PricingLine>();

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public ArrayList<PricingLine> getPriceLines() {
		return priceLines;
	}

	public void setPriceLines(ArrayList<PricingLine> priceLines) {
		this.priceLines = priceLines;
	}

	public Document getStorage() {
		return storage;
	}

	public void setStorage(Document storage) {
		this.storage = storage;
	}
	
	
}
