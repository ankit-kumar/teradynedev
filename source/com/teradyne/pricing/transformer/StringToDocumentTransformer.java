package com.teradyne.pricing.transformer;

import java.io.IOException;
import java.io.StringBufferInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class StringToDocumentTransformer {

	public Document transform(String xml) throws TransformerException
	{
		DocumentBuilderFactory docBuilderFactory =DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new TransformerException();	
		}
		try {
			return docBuilder.parse(new StringBufferInputStream(xml));
		} catch (SAXException e) {
			throw new TransformerException();	
		} catch (IOException e) {
			throw new TransformerException();	
		}
		
		
		
	}
	
}
