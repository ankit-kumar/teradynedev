package com.teradyne.pricing.transformer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.teradyne.pricing.model.PricingHeader;

public class PricingHeaderToPricingAssignmentDocumentTransformer {

	
	
//	<PricelistAssignment EnterpriseCode="Matrix" CustomerID="23PS" PricelistHeaderKey="PL_EAR_MICROFLEX">
//	<PricelistHeader PricelistHeaderKey="PL_EAR_MICROFLEX" 	PricelistName="PL_EAR_MICROFLEX" /> 
//	</PricelistAssignment>

	
	public Document transform(PricingHeader header)
	{
		try
		{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		// Populating Root <PricelistAssignment> Element
		Element rootElement = doc.createElement("PricelistAssignment");
		rootElement.setAttribute("EnterpriseCode", header.getOrganizationCode());
		rootElement.setAttribute("CustomerID", header.getOriginalAssignedCustomerId());
		rootElement.setAttribute("PricelistHeaderKey", header.getKey());
		
		
		Element pricelistHeaderElem = doc.createElement("PricelistHeader");
		pricelistHeaderElem.setAttribute("PricelistHeaderKey", header.getKey());
		pricelistHeaderElem.setAttribute("PricelistName", header.getListName());
		
		rootElement.appendChild(pricelistHeaderElem);
		doc.appendChild(rootElement);
		System.out.println("finished transforming");
		return doc;
	} catch (Exception e) {
		//throw new TransformerException();
	}
		return null;
	}
	
}
