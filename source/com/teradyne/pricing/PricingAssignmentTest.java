package com.teradyne.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.junit.Test;
import org.w3c.dom.Document;

import com.teradyne.pricing.model.PricingHeader;
import com.teradyne.pricing.transformer.DocumentToPricingHeaderTransformer;
import com.teradyne.pricing.transformer.DocumentToStringTransformer;
import com.teradyne.pricing.transformer.PricingHeaderToPricingAssignmentDocumentTransformer;
import com.teradyne.pricing.transformer.StringToDocumentTransformer;

public class PricingAssignmentTest {

	
	@Test
	public void testPricingAssignmentTransformer() throws Exception
	{
		BufferedReader br = null;
		String xml = "";
		try {
 
			String sCurrentLine;
			
 
			InputStream in = PricingHeaderTest.class.getResourceAsStream("/MultiApi.xml");
			Reader r = new InputStreamReader(in, "UTF-8");
			
			br = new BufferedReader(r);
 
			while ((sCurrentLine = br.readLine()) != null) {
				xml +=sCurrentLine;
			}
			
			//System.out.println(xml);
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		
		StringToDocumentTransformer stringTransformer = new StringToDocumentTransformer();
		
		Document inXml = stringTransformer.transform(xml);
		
		DocumentToPricingHeaderTransformer pricingTransformer = new DocumentToPricingHeaderTransformer();
		PricingHeader header = pricingTransformer.transform(inXml);
		
		
		PricingHeaderToPricingAssignmentDocumentTransformer assignmentTransformer = new PricingHeaderToPricingAssignmentDocumentTransformer();
		
		Document outDoc = assignmentTransformer.transform(header);
		
		DocumentToStringTransformer documentTransformer = new DocumentToStringTransformer();
		
		System.out.println(documentTransformer.transform(outDoc));
		
	}
}
