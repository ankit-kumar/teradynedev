package com.teradyne.sterling.service;

import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import com.teradyne.pricing.model.PricingHeader;
import com.teradyne.pricing.model.PricingLine;
import com.teradyne.pricing.model.PricingLines;
import com.teradyne.pricing.transformer.DocumentToPricingHeaderTransformer;
import com.teradyne.pricing.transformer.DocumentToPricingLinesTransformer;
import com.teradyne.pricing.transformer.PricingHeaderToPricingAssignmentDocumentTransformer;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class PricingMaster {
	public Document manageNodePriority(YFSEnvironment env,Document inDocument) throws RemoteException,ParserConfigurationException,YFSException{
		try {
			YIFApi api = YIFClientFactory.getInstance().getLocalApi();

			//1st step from Algorithm

			//1)	Invoke managePricelistHeader

			//this information is predefined, dont have to do anything 
			//Create price list header as PL_<Service_Type>_<System_Type> 
			//OR
			// Create price list header as PL_<Service_Type>_<Zone> 


			DocumentToPricingHeaderTransformer pricingHeaderTransformer = new DocumentToPricingHeaderTransformer();
			PricingHeader header = pricingHeaderTransformer.transform(inDocument);
			api.managePricelistHeader(env,header.getStorage());

			//2nd step from Alogrithm

			//2)	Invoke managePriceListLine.
			//If the PriceListHeader doesn�t match with the existing price list header then, throw error message.
			//Else, add �item� and �list price� to the price header 
			DocumentToPricingLinesTransformer pricingListTransformer = new DocumentToPricingLinesTransformer();
			PricingLines pricingLines = pricingListTransformer.transform(inDocument);

			for(PricingLine pricingLine:pricingLines.getPriceLines())
			{
				if(!pricingLine.getHeaderKey().equals(header.getKey()))
				{
					throw new YFSException("pricingLine header key does not match header key from Document");
				}
			}

			api.managePricelistLine(env, pricingLines.getStorage());

			//3rd step of algorithm
			//3)	Invoke following Sterling APIs managePricelistAssignment:
			//	a.	Check if IsCustomerSpecificPL=Y then   ,
			//	�	Invoke managePricelistAssignment  with customerId attribute
			//or else dont add the customer id attribute

			if(header.getIsCustomerSpecific().equals("Y")){
				PricingHeaderToPricingAssignmentDocumentTransformer assignmentTransformer = new PricingHeaderToPricingAssignmentDocumentTransformer();
				Document outDoc = assignmentTransformer.transform(header);
				api.managePricelistAssignment(env, outDoc);
			}
		} catch (YIFClientCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return inDocument;
	}

}
