package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class CreateSCFromTemplate {
	
	public Document createFromTemplate(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		//fetch template details
		YFCDocument sctOutDoc = fetchSCTemplateDetails(env, inDoc);
		//System.out.println("Sctemplate output"+sctOutDoc.toString());
		formOrderLinesFromTemplate(inDoc,sctOutDoc);
		String templateStr = "<Order OrderHeaderKey=\"\" />";
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.CREATE_ORDER, inDoc, YFCDocument.parse(templateStr));
		
		return outDoc.getDocument();
	}
	
	private YFCDocument fetchSCTemplateDetails(YFSEnvironment env, YFCDocument inDoc) throws YFCException, SAXException, IOException, Exception{
		YFCElement extnEle = inDoc.getDocumentElement().getChildElement(XMLConstants.EXTN);
		String scTemplateIDStr = extnEle.getAttribute(XMLConstants.SC_TEMPLATE_ID);
		String scTemplateKeyStr = extnEle.getAttribute(XMLConstants.TER_SCT_KEY);
		String scTypeStr = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_TYPE);
		YFCDocument sctInDoc = YFCDocument.createDocument(XMLConstants.TER_SC_TEMPLATE);
		sctInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_ID, scTemplateIDStr);
		sctInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SER_CONT_TYPE, scTypeStr);
		sctInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_KEY, scTemplateKeyStr);
		YFCDocument outDoc = getTemplate(env, sctInDoc);
		return outDoc;
	}
	
	private void formOrderLinesFromTemplate(YFCDocument inDoc, YFCDocument sctOutDoc) throws ParserConfigurationException, TransformerException{
		YFCNodeList<YFCElement> serItemsNL = sctOutDoc.getDocumentElement().getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		YFCElement orderLinesEle = inDoc.createElement(XMLConstants.ORDER_LINES);
		inDoc.getDocumentElement().appendChild(orderLinesEle);
		ArrayList<String> tranAL = new ArrayList<String>();
		for(Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();){
			YFCElement serItemEle = iter.next();
			YFCElement yfsItemEle = serItemEle.getChildElement(XMLConstants.YFS_ITEM);
			String kitCodeStr = yfsItemEle.getChildElement(XMLConstants.PRIMARY_INFORMATION).getAttribute(XMLConstants.KIT_CODE);
			String parentSIKeyStr = serItemEle.getAttribute(XMLConstants.TER_SI_PARENT_KEY);
			
			YFCElement orderLineEle = inDoc.createElement(XMLConstants.ORDER_LINE);
			orderLineEle.setAttribute(XMLConstants.ORDERED_QTY, "1");
			orderLineEle.setAttribute(XMLConstants.ACTION, XMLConstants.CREATE);
			
			YFCElement itemEle = inDoc.createElement(XMLConstants.ITEM);
			orderLineEle.appendChild(itemEle);
			itemEle.setAttribute(XMLConstants.ITEM_ID, yfsItemEle.getAttribute(XMLConstants.ITEM_ID));
			
			YFCElement olTranQtyEle = inDoc.createElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
			orderLineEle.appendChild(olTranQtyEle);
			olTranQtyEle.setAttribute(XMLConstants.TRANSACTIONAL_UOM, yfsItemEle.getAttribute(XMLConstants.UNIT_OF_MEASURE));
						
			YFCElement extnEle = inDoc.createElement(XMLConstants.EXTN);
			orderLineEle.appendChild(extnEle);
			
			extnEle.setAttribute(XMLConstants.NONREPAIR_PARTS_COVERED, serItemEle.getAttribute(XMLConstants.TER_NON_REP_PARTS_COVERED));
			extnEle.setAttribute(XMLConstants.REPAIRABLE_PARTS_COVERED, serItemEle.getAttribute(XMLConstants.TER_REP_PARTS_COVERED));
			extnEle.setAttribute(XMLConstants.STO_REPAIR_LIST_DISCOUNT, serItemEle.getAttribute(XMLConstants.TER_SI_REPAIR_LIST_DISCOUNT));
			extnEle.setAttribute(XMLConstants.STO_EXPEDITE_LIST_DISCOUNT, serItemEle.getAttribute(XMLConstants.TER_SI_EXPEDITE_DISCOUNT));
			extnEle.setAttribute(XMLConstants.STO_ADDITIONAL_DISCOUNT, serItemEle.getAttribute(XMLConstants.TER_SI_ADDITIONAL_DISCOUNT));
			
			if(kitCodeStr.equals("BUNDLE")){
				String siKey = serItemEle.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
				tranAL.add(siKey);
				orderLineEle.setAttribute(XMLConstants.TRANSACTIONAL_LINE_ID, Integer.toString(tranAL.indexOf(siKey)));
			}else if(!StringUtil.isEmpty(parentSIKeyStr)){
				orderLineEle.setAttribute(XMLConstants.TER_SI_PARENT_KEY, parentSIKeyStr);
			}		
			
			orderLinesEle.appendChild(orderLineEle);
		}
		YFCNodeList<YFCElement> orderlinesNL = orderLinesEle.getElementsByTagName(XMLConstants.ORDER_LINE);
		for(Iterator<YFCElement> iter = orderlinesNL.iterator(); iter.hasNext();){
			YFCElement orderLineEle = iter.next();
			String parentSIKeyStr = orderLineEle.getAttribute(XMLConstants.TER_SI_PARENT_KEY);
			if(!StringUtil.isEmpty(parentSIKeyStr))
			{
				int tranLineId = tranAL.indexOf(parentSIKeyStr);
				YFCElement olBundleParentEle = inDoc.createElement(XMLConstants.BUNDLE_PARENT_LINE);
				orderLineEle.appendChild(olBundleParentEle);
				olBundleParentEle.setAttribute(XMLConstants.TRANSACTIONAL_LINE_ID, Integer.toString(tranLineId));
			}
		}
		return;
	}
	
	private YFCDocument getTemplate(YFSEnvironment env, YFCDocument inDoc) throws YFCException,SAXException, IOException,Exception {
		
		String sctKeyStr = inDoc.getDocumentElement().getAttribute(XMLConstants.TER_SCT_KEY);
		YFCDocument scTemplateOut = SterlingUtil.callService(env, Constants.GET_SC_TEMPLATE, inDoc, "");
		YFCDocument serviceItemsInDoc = YFCDocument.createDocument(XMLConstants.TER_SERVICE_ITEM);
		serviceItemsInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument serviceItemsList = SterlingUtil.callService(env, Constants.GET_SERVICE_ITEM_LIST, serviceItemsInDoc, "");
		
		
		YFCElement serItemsListEle = scTemplateOut.createElement(XMLConstants.TER_SERVICE_ITEM_LIST);
		serItemsListEle = scTemplateOut.importNode(serviceItemsList.getDocumentElement(), true);
		scTemplateOut.getDocumentElement().appendChild(serItemsListEle);
		return scTemplateOut; 
	}
}
