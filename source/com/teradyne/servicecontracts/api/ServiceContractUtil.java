package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ServiceContractUtil {
	
	public YFCDocument changeOrderStatus(YFSEnvironment env, String orderHeaderKey, String baseDropStatus, String transactionID) throws RemoteException, YIFClientCreationException, SAXException, IOException{
		YFCDocument inDoc = YFCDocument.createDocument(XMLConstants.ORDER_STATUS_CHANGE);
		inDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, orderHeaderKey);
		inDoc.getDocumentElement().setAttribute(XMLConstants.BASE_DROP_STATUS, baseDropStatus);
		inDoc.getDocumentElement().setAttribute(XMLConstants.CHANGE_FOR_ALL_AVAILABLE_QTY, Constants.YES);
		inDoc.getDocumentElement().setAttribute(XMLConstants.TRANSACTION_ID,transactionID);
		String templateStr = "<OrderStatusChange OrderHeaderKey=\"\" />";
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_STATUS, inDoc,YFCDocument.parse(templateStr));
		return outDoc;	
	}
	
	private YFCDocument getIBMapping(YFSEnvironment env, YFCDocument doc, String level) throws YIFClientCreationException, YFSException, RemoteException{
		
		YFCDocument inDoc = YFCDocument.createDocument("TerSCIBMapping");
		if(level.equals("Order")){
			String dataKey = doc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY); 
			inDoc.getDocumentElement().setAttribute("TerSCOHKey", dataKey);
		}else {
			String dataKey = doc.getDocumentElement().getAttribute(XMLConstants.ORDER_LINE_KEY);
			inDoc.getDocumentElement().setAttribute("TerSCOLKey", dataKey);
		}
		
		YIFApi api = YIFClientFactory.getInstance().getApi();
		Document outDoc = api.executeFlow(env, "GetIBForLine", inDoc.getDocument());
		return YFCDocument.getDocumentFor(outDoc);
		
	}
	
	public void multiApiForSCIBMapping(YFSEnvironment env, YFCDocument inDoc, String operation, String level) throws YFSException, RemoteException, YIFClientCreationException {
		
		YFCDocument mappingDoc = getIBMapping(env, inDoc, level);
		
		YFCDocument multiInDoc = YFCDocument.createDocument(XMLConstants.MULTI_API_ELE);
		YFCNodeList<YFCElement> mappingListEle = mappingDoc.getDocumentElement().getElementsByTagName("TerSCIBMapping");
		
		for(Iterator<YFCElement> iter = mappingListEle.iterator(); iter.hasNext();){
			YFCElement mappingEle = iter.next();
			
			YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			if(operation.equals("Delete")){
				apiEle.setAttribute(XMLConstants.NAME, "deleteTerSCIBMapping");
			}
			
			YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			
			YFCElement newMappingEle = multiInDoc.createElement("TerSCIBMapping");
			newMappingEle = multiInDoc.importNode(mappingEle, false);
			inputEle.appendChild(newMappingEle);
			
			multiInDoc.getDocumentElement().appendChild(apiEle);
			
		}
		
		//System.out.println("Input to MultiApi"+multiInDoc.getString());
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");
	}
	
	public void setPriceAttributes(YFSEnvironment env, YFCDocument outDoc) throws RemoteException, YIFClientCreationException, SAXException, IOException{
		String templateStr = "<Order OrderHeaderKey=\"\" />";
		String ohKey = outDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		String grandTotalStr = outDoc.getDocumentElement().getChildElement("OverallTotals").getAttribute("GrandTotal");
		String lineSubTotalStr = outDoc.getDocumentElement().getChildElement("OverallTotals").getAttribute("LineSubTotal");
	
		YFCDocument orderDoc = YFCDocument.createDocument(XMLConstants.ORDER);
		orderDoc.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, ohKey);
		YFCElement extnEle = orderDoc.getDocumentElement().createChild(XMLConstants.EXTN);
		extnEle.setAttribute("TotalSellingPrice", grandTotalStr);
		if(Double.parseDouble(lineSubTotalStr) == 0){
			extnEle.setAttribute("AgreementDiscount", "0");
		}else {

			double discount = ((Double.parseDouble(lineSubTotalStr) - Double.parseDouble(grandTotalStr))*100)/Double.parseDouble(lineSubTotalStr);
			String disStr = new BigDecimal(discount).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
			extnEle.setAttribute("AgreementDiscount", disStr);
		}
		outDoc = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, orderDoc, YFCDocument.parse(templateStr));
	}
}
