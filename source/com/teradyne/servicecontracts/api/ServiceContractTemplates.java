package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ServiceContractTemplates {
	
	public Document modifyTemplate(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		//System.out.println("Input::"+inDoc.getString());
		String scOutTemplate = "<TerSerContTemplate TerSCTKey=\"\" />";
		YFCDocument scTemplateOut = inDoc;
		if(checkModifyTemplateNeeded(inDoc)){
			scTemplateOut = SterlingUtil.callService(env, Constants.MANAGE_SC_TEMPLATE, inDoc, YFCDocument.parse(scOutTemplate));
		}
		
		addServiceItemsToTemplate(env, inDoc);
		
		return scTemplateOut.getDocument();
	}
	private boolean checkModifyTemplateNeeded(YFCDocument inDoc){
		YFCElement inEle = inDoc.getDocumentElement();
		if(!StringUtil.isEmpty(inEle.getAttribute("TerSCTName")) || !StringUtil.isEmpty(inEle.getAttribute("TerSCTPriority")) 
		|| !StringUtil.isEmpty(inEle.getAttribute("TerIBAssoReqd")) || !StringUtil.isEmpty(inEle.getAttribute("TerSCTStatus"))
		|| !StringUtil.isEmpty(inEle.getAttribute("TerSCTEffectiveDate"))
		|| !StringUtil.isEmpty(inEle.getAttribute("TerSCTDurationDays")) || !StringUtil.isEmpty("TerSCTDraft")){
			return true;
		}
		return false;
	}
	
	
	private boolean checkServiceItemsAPICallNeeded(YFCElement inEle){
		if(!StringUtil.isEmpty(inEle.getAttribute("TerSIExpediteDiscount")) ||
			!StringUtil.isEmpty(inEle.getAttribute("TerSIRepairListDiscount")) ||
			!StringUtil.isEmpty(inEle.getAttribute("TerSIAdditionalDiscount")) || 
			!StringUtil.isEmpty(inEle.getAttribute("TerRepPartsCovered")) ||
			!StringUtil.isEmpty(inEle.getAttribute("TerNonRepPartsCovered"))){
			return true;
		}
		return false;
	}
	
	public Document getTemplate(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		
		String sctKeyStr = inDoc.getDocumentElement().getAttribute(XMLConstants.TER_SCT_KEY);
		YFCDocument scTemplateOut = SterlingUtil.callService(env, Constants.GET_SC_TEMPLATE, inDoc, "");
		YFCDocument serviceItemsInDoc = YFCDocument.createDocument(XMLConstants.TER_SERVICE_ITEM);
		serviceItemsInDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument serviceItemsList = SterlingUtil.callService(env, Constants.GET_SERVICE_ITEM_LIST, serviceItemsInDoc, "");
		YFCNodeList<YFCElement> serItemNL = serviceItemsList.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		if(serItemNL.getLength()!=0){
			scTemplateOut.getDocumentElement().setAttribute("HasChildren", Constants.YES);
		}
		
		YFCElement serItemListOutEle = scTemplateOut.createElement(XMLConstants.TER_SERVICE_ITEM_LIST);
		for(Iterator<YFCElement> iter= serItemNL.iterator();iter.hasNext();){
			YFCElement serItem = iter.next();
			String bundleKeyStr = serItem.getAttribute(XMLConstants.TER_SI_PARENT_KEY);
			if(StringUtil.isEmpty(bundleKeyStr)){
				YFCElement serItemOutEle = scTemplateOut.createElement(XMLConstants.TER_SERVICE_ITEM);
				serItemOutEle = scTemplateOut.importNode(serItem, true); 
				serItemListOutEle.appendChild(serItemOutEle);		
			}
		}
		scTemplateOut.getDocumentElement().appendChild(serItemListOutEle);
		
		return scTemplateOut.getDocument();
	}
	
	
	public void addServiceItemsToTemplate(YFSEnvironment env, YFCDocument inDoc) throws YIFClientCreationException, SAXException, IOException{
		YFCElement rootEle = inDoc.getDocumentElement();
		String reasonCodeStr = rootEle.getAttribute("TerSCTReasonCode");
		String reasonTextStr = rootEle.getAttribute("TerSCTReasonText");
		YFCNodeList<YFCElement> serItemsNL = rootEle.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		String sctKeyStr = rootEle.getAttribute(XMLConstants.TER_SCT_KEY);
		
		YFCDocument multiInDoc = YFCDocument.createDocument(XMLConstants.MULTI_API_ELE);
		
		for(Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();){
			YFCElement serItemEle = iter.next();
			String kitCodeStr = serItemEle.getAttribute(XMLConstants.KIT_CODE);
			String siKeyStr = serItemEle.getAttribute(XMLConstants.TER_SI_KEY);
			if(checkServiceItemsAPICallNeeded(serItemEle) || 
					(StringUtil.isEmpty(siKeyStr) && kitCodeStr.equals("BUNDLE"))){
				serItemEle.setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
				serItemEle.setAttribute("TerSIReasonCode", reasonCodeStr);
				serItemEle.setAttribute("TerSIReasonText", reasonTextStr);
				 
				if(!StringUtil.isEmpty(kitCodeStr) && kitCodeStr.equals("BUNDLE")){
					processBundleComponents(env, multiInDoc, serItemEle);
				}
				
				YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
				YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
				YFCElement serItemInEle = inputEle.createChild(XMLConstants.TER_SERVICE_ITEM);
					
				YFCElement templateEle = apiEle.createChild(XMLConstants.TEMPLATE_ELE);
				YFCElement siTempEle = templateEle.createChild(XMLConstants.TER_SERVICE_ITEM);
				siTempEle.setAttribute(XMLConstants.TER_SI_KEY, "");
					
				apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
				if(StringUtil.isEmpty(siKeyStr)){
					apiEle.setAttribute(XMLConstants.NAME, Constants.CREATE_SERVICE_ITEM);
				}else{
					apiEle.setAttribute(XMLConstants.NAME, Constants.MODIFY_SERVICE_ITEM);
				}
				serItemInEle.setAttributes(serItemEle.getAttributes());
				multiInDoc.getDocumentElement().appendChild(apiEle);
				
			}
		}
		YFCDocument outDoc =  SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");	
		//System.out.println("MultiAPI output::"+outDoc.getString());
		
	}
	
	private void processBundleComponents(YFSEnvironment env, YFCDocument multiInDoc,YFCElement serItemEle) throws RemoteException, YIFClientCreationException, SAXException, IOException{
		String itemKeyStr = serItemEle.getAttribute(XMLConstants.TER_SI_ITEM_KEY);
		String sctKeyStr = serItemEle.getAttribute(XMLConstants.TER_SCT_KEY);
		String reasonCodeStr = serItemEle.getAttribute("TerSCTReasonCode");
		String reasonTextStr = serItemEle.getAttribute("TerSCTReasonText");
		YFCDocument getItemDetails = YFCDocument.createDocument(XMLConstants.ITEM);
		getItemDetails.getDocumentElement().setAttribute(XMLConstants.ITEM_KEY, itemKeyStr);
		
		String getItemDetTempStr = "<Item ItemKey=\"\" ><Components><Component ComponentItemKey=\"\" /></Components></Item>";
		YFCDocument getItemDetOut = SterlingUtil.callAPI(env, Constants.GET_ITEM_DETAILS_API, getItemDetails, YFCDocument.parse(getItemDetTempStr));
		
		YFCNodeList<YFCElement> compNL = getItemDetOut.getElementsByTagName(XMLConstants.COMPONENT);
		for(Iterator<YFCElement> iter = compNL.iterator(); iter.hasNext();){
			String siItemKeyStr = iter.next().getAttribute("ComponentItemKey");
			
			YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			apiEle.setAttribute(XMLConstants.NAME, Constants.CREATE_SERVICE_ITEM);
			YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			YFCElement serItemInEle = inputEle.createChild(XMLConstants.TER_SERVICE_ITEM);
			serItemInEle.setAttribute(XMLConstants.TER_SI_ITEM_KEY, siItemKeyStr);
			serItemInEle.setAttribute(XMLConstants.TER_SI_PARENT_KEY, itemKeyStr);
			serItemInEle.setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
			serItemInEle.setAttribute("TerSIReasonCode", reasonCodeStr);
			serItemInEle.setAttribute("TerSIReasonText", reasonTextStr);
			
			YFCElement templateEle = apiEle.createChild(XMLConstants.TEMPLATE_ELE);
			YFCElement siTempEle = templateEle.createChild(XMLConstants.TER_SERVICE_ITEM);
			siTempEle.setAttribute(XMLConstants.TER_SI_KEY, "");
			
			multiInDoc.getDocumentElement().appendChild(apiEle);
		}
		
		
	}
	
	
	public Document deleteTemplate(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String sctKeyStr =  inDoc.getDocumentElement().getAttribute(XMLConstants.TER_SCT_KEY);
		String siKeyStr = inDoc.getDocumentElement().getAttribute(XMLConstants.TER_SI_KEY);
		
		YFCDocument getServiceItemsDoc = YFCDocument.createDocument(XMLConstants.TER_SERVICE_ITEM);
		getServiceItemsDoc.getDocumentElement().setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
		YFCDocument getSerItemsOut = SterlingUtil.callService(env, Constants.GET_SERVICE_ITEM_LIST, getServiceItemsDoc, "");
		
		YFCNodeList<YFCElement> serItemsNL = getSerItemsOut.getElementsByTagName(XMLConstants.TER_SERVICE_ITEM);
		
		YFCDocument multiInDoc = YFCDocument.createDocument(XMLConstants.MULTI_API_ELE);
		YFCElement apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
		apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
		apiEle.setAttribute(XMLConstants.NAME, Constants.DEL_SC_TEMPLATE_API);
		
		YFCElement inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
		YFCElement serItemInEle = inputEle.createChild(XMLConstants.TER_SC_TEMPLATE);
		serItemInEle.setAttribute(XMLConstants.TER_SCT_KEY, sctKeyStr);
		multiInDoc.getDocumentElement().appendChild(apiEle);
		
		for(Iterator<YFCElement> iter = serItemsNL.iterator(); iter.hasNext();){
			YFCElement serItemEle = iter.next();
			
			String kitCodeStr = serItemEle.getAttribute(XMLConstants.KIT_CODE);
			if(!StringUtil.isEmpty(kitCodeStr) && kitCodeStr.equals("BUNDLE")){
				RemoveServiceItems rsi = new RemoveServiceItems();
				rsi.removeBundleComponents(env,serItemEle);
			}
			else{
				apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
				apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
				apiEle.setAttribute(XMLConstants.NAME, Constants.DEL_SER_ITEM_API);
			
				inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
				serItemInEle = inputEle.createChild(XMLConstants.TER_SERVICE_ITEM);
				serItemInEle.setAttribute(XMLConstants.TER_SI_KEY, serItemEle.getAttribute(XMLConstants.TER_SI_KEY));
				
				multiInDoc.getDocumentElement().appendChild(apiEle);
			}
			
		}
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");
	
		
		return inputDoc;
	}
}
