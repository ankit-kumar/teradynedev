package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class ManageServiceContract {
	
	public Document manage(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String ohKey = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		//System.out.println("Input Doc::"+inDoc.getString());
		//check if order is not yet active, only then call the method
		setServiceStartDateForLines(inDoc);
		String templateStr = "<Order OrderHeaderKey=\"\" ><OverallTotals LineSubTotal=\"\" GrandTotal=\"\" /></Order>";
		//System.out.println("Input Doc to changeOrder::"+inDoc.getString());
		YFCDocument outDoc = SterlingUtil.callAPI(env, Constants.CHANGE_ORDER_API, inDoc, YFCDocument.parse(templateStr));
		
		ServiceContractUtil scUtil = new ServiceContractUtil();
		scUtil.setPriceAttributes(env, outDoc);
		return outDoc.getDocument();
}

	

	private void setServiceStartDateForLines(YFCDocument inDoc) {
		String effectiveDateStr = inDoc.getDocumentElement().getAttribute(XMLConstants.EFFECTIVE_DATE);
		YFCNodeList<YFCElement> orderLinesEle = inDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE);
		for(Iterator<YFCElement> iter = orderLinesEle.iterator(); iter.hasNext();){
			YFCElement orderLineEle = iter.next();
			YFCElement extnEle =  orderLineEle.getChildElement(XMLConstants.EXTN);
			String ssdStr = extnEle.getAttribute("ServiceStartDateTime");
			if(StringUtil.isEmpty(ssdStr)){
				extnEle.setAttribute("ServiceStartDateTime", effectiveDateStr);
			}
			
			
			YFCNodeList<YFCElement> linePriceInfoNL = orderLineEle.getElementsByTagName(XMLConstants.LINE_PRICE_INFO);
			if(linePriceInfoNL.getLength()>0){
				YFCElement linePriceInfo = linePriceInfoNL.item(0);
				double unitPrice = linePriceInfo.getDoubleAttribute("UnitPrice", 0);
				double discount = linePriceInfo.getDoubleAttribute("DiscountPercentage", 0);
				double qty = orderLineEle.getDoubleAttribute(XMLConstants.ORDERED_QTY);
				
				if(discount != 0){
					double amount = (unitPrice * qty * discount)/100;
					YFCElement lineChargesEle = orderLineEle.createChild("LineCharges");
					YFCElement lineChargeEle = lineChargesEle.createChild("LineCharge");
					lineChargeEle.setAttribute("ChargeCategory", "SCDiscount");
					lineChargeEle.setAttribute("ChargeName", "SCDiscount");
					String chargePerLine = new BigDecimal(amount).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
					lineChargeEle.setAttribute("ChargePerLine", chargePerLine);
				}
		
			}
					
		}
	}
	
		
}
