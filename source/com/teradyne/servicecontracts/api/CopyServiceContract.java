package com.teradyne.servicecontracts.api;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.SterlingUtil;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class CopyServiceContract {
	HashMap<String, String> olHashMap = new HashMap<String,String>();

	public Document copycontract(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		
		String oldOHKey = inDoc.getDocumentElement().getAttribute("CopyFromOrderHeaderKey");
		String oldOrderNo = inDoc.getDocumentElement().getAttribute("CopyFromOrderNo");
		inDoc.getDocumentElement().getChildElement(XMLConstants.EXTN).setAttribute("PriorContractNo", oldOrderNo);
	    
		String templateStr = "<Order OrderHeaderKey=\"\" OrderNo=\"\" ><OrderLines><OrderLine OrderLineKey=\"\" /></OrderLines></Order>";
		//System.out.println("Input Doc to copyOrder::"+inDoc.getString());
		YFCDocument outDoc = SterlingUtil.callAPI(env, "copyOrder", inDoc, YFCDocument.parse(templateStr));
		//System.out.println("Output from copyOrder::"+outDoc.getString());
		
		String changeOrderStr = "<Order OrderHeaderKey=\"\" OrderNo=\"\" />";
		YFCDocument changeOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		changeOrderIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, oldOHKey);
		YFCElement extnEle = changeOrderIn.getDocumentElement().createChild(XMLConstants.EXTN);
		extnEle.setAttribute("RenewalContractNo", outDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_NO));
		YFCDocument changeOrderOut = SterlingUtil.callAPI(env, "changeOrder", changeOrderIn, YFCDocument.parse(changeOrderStr));
		
		String orderStr = "<OrderList><Order OrderHeaderKey=\"\" OrderNo=\"\" ><OrderLines><OrderLine OrderLineKey=\"\" /></OrderLines></Order></OrderList>";
		YFCDocument orderDetailsIn = YFCDocument.createDocument(XMLConstants.ORDER);
		orderDetailsIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, oldOHKey);
		YFCDocument orderDetailsOut = SterlingUtil.callAPI(env, "getOrderList", orderDetailsIn, YFCDocument.parse(orderStr));
		
		//System.out.println("Output from getOrder::"+orderDetailsOut.getString());
		
		copySCIBMapping(orderDetailsOut.getDocumentElement().getElementsByTagName(XMLConstants.ORDER).item(0), outDoc);
		makeMultiApiInput(env, inDoc, outDoc);
		
		ServiceContractUtil scUtil = new ServiceContractUtil();
		scUtil.changeOrderStatus(env, oldOHKey, "1100.300", "START_RENEWAL.0018.ex.ex");
		
		return outDoc.getDocument();
}

	private void makeMultiApiInput(YFSEnvironment env, YFCDocument inDoc,
			YFCDocument outDoc) throws YFSException, RemoteException, YIFClientCreationException {
		String oldOHKey = inDoc.getDocumentElement().getAttribute("CopyFromOrderHeaderKey");
		YFCDocument multiInDoc = YFCDocument.createDocument(XMLConstants.MULTI_API_ELE);
		YFCDocument mappingDoc = getIBMapping(env, oldOHKey);
		YFCNodeList<YFCElement> mappingListEle = mappingDoc.getDocumentElement().getElementsByTagName("TerSCIBMapping");
		YFCElement apiEle = null;
		YFCElement inputEle = null;
		for(Iterator<YFCElement> iter = mappingListEle.iterator(); iter.hasNext();){
			YFCElement mappingEle = iter.next();
			String oldOLKey = mappingEle.getAttribute("TerSCOLKey");
			
			apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
			apiEle.setAttribute(XMLConstants.IS_EXTENDED_DB_API, Constants.YES);
			apiEle.setAttribute(XMLConstants.NAME, "createTerSCIBMapping");
			
			inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
			
			YFCElement newMappingEle = multiInDoc.createElement("TerSCIBMapping");
			newMappingEle = multiInDoc.importNode(mappingEle, false);
			inputEle.appendChild(newMappingEle);
			
			newMappingEle.setAttribute("TerSCOLKey", olHashMap.get(oldOLKey));
			newMappingEle.setAttribute("TerSCOHKey", olHashMap.get(oldOHKey));
			newMappingEle.removeAttribute("TerSCIBKey");
			
			multiInDoc.getDocumentElement().appendChild(apiEle);
			
		}
		
		/*apiEle = multiInDoc.createElement(XMLConstants.API_ELE);
		apiEle.setAttribute(XMLConstants.NAME, "changeOrder");
		
		inputEle = apiEle.createChild(XMLConstants.INPUT_ELE);
		YFCElement orderEle = inputEle.createChild(XMLConstants.ORDER);
		orderEle.setAttribute(XMLConstants.ORDER_HEADER_KEY, oldOHKey);
		YFCElement extnEle = orderEle.createChild(XMLConstants.EXTN);
		extnEle.setAttribute("RenewalContractNo", outDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_NO));
		
		YFCElement templateEle = apiEle.createChild(XMLConstants.TEMPLATE_ELE);
		YFCElement orderOutEle = templateEle.createChild(XMLConstants.ORDER);
		orderOutEle.setAttribute(XMLConstants.ORDER_HEADER_KEY, "");
		
		multiInDoc.getDocumentElement().appendChild(apiEle);
		*/
		//System.out.println("Input to MultiApi"+multiInDoc.getString());
		SterlingUtil.callAPI(env, Constants.MULTI_API, multiInDoc, "");
	}

	private void copySCIBMapping(YFCElement inEle, YFCDocument outDoc) {
		String oldOHKey = inEle.getAttribute(XMLConstants.ORDER_HEADER_KEY); 
		String newOHKey = outDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		olHashMap.put(oldOHKey, newOHKey);
		YFCNodeList<YFCElement> oldOrderLinesEle = inEle.getElementsByTagName(XMLConstants.ORDER_LINE);
		YFCNodeList<YFCElement> newOrderLinesEle = outDoc.getDocumentElement().getElementsByTagName(XMLConstants.ORDER_LINE);
		for(int i=0; i < oldOrderLinesEle.getLength(); i++){
			YFCElement oldOrderLineEle = oldOrderLinesEle.item(i);
			YFCElement newOrderLineEle = newOrderLinesEle.item(i);
			String newOLKey = newOrderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
			String oldOLKey = oldOrderLineEle.getAttribute(XMLConstants.ORDER_LINE_KEY);
			olHashMap.put(oldOLKey, newOLKey);
		}
	}

	private YFCDocument getIBMapping(YFSEnvironment env, String orderHeaderKey) throws YIFClientCreationException, YFSException, RemoteException{
		
		YFCDocument inDoc = YFCDocument.createDocument("TerSCIBMapping");
		inDoc.getDocumentElement().setAttribute("TerSCOHKey", orderHeaderKey);
		
		YIFApi api = YIFClientFactory.getInstance().getApi();
		Document outDoc = api.executeFlow(env, "GetIBForLine", inDoc.getDocument());
		return YFCDocument.getDocumentFor(outDoc);
		
	}
	
	public Document copyLine(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		String orderHeaderKey = inDoc.getDocumentElement().getAttribute(XMLConstants.ORDER_HEADER_KEY);
		
		String orderStr = "<OrderLine OrderLineKey=\"\" ><OrderLineTranQuantity TransactionalUOM=\"\" OrderedQty=\"\"  />"
		+ "<Item ItemID=\"\" UnitOfMeasure=\"\" /><LinePriceInfo UnitPrice=\"\" DiscountPercentage=\"\" />"
		+ "<Extn CoveredProductFamily=\"\" CoveredInstallBaseStatus=\"\" NonrepairPartsCovered=\"\"  RepairablePartsCovered=\"\" "
		+ "STORepairListDiscount=\"\" STOExpediteListDiscount=\"\" STOAdditionalDiscount=\"\" ServiceCapQty=\"\" "
		+ "ServiceCapUOM=\"\" Percent_Cap_Used=\"\"  /><LineCharges><LineCharge ChargeCategory=\"\" ChargeName=\"\" ChargePerLine=\"\" "
		+ "/></LineCharges></OrderLine>";
		YFCDocument orderDetailsOut = SterlingUtil.callAPI(env, "getOrderLineDetails", inDoc, YFCDocument.parse(orderStr));
		//System.out.println("Output from orderlIne"+orderDetailsOut.getString());
		YFCElement oldOrderLineEle = orderDetailsOut.getDocumentElement();
		
		String changeOrderStr = "<Order OrderHeaderKey=\"\" ><OverallTotals LineSubTotal=\"\" GrandTotal=\"\" /></Order>";
		YFCDocument changeOrderIn = YFCDocument.createDocument(XMLConstants.ORDER);
		changeOrderIn.getDocumentElement().setAttribute(XMLConstants.ORDER_HEADER_KEY, orderHeaderKey);
		YFCElement orderLinesEle = changeOrderIn.getDocumentElement().createChild(XMLConstants.ORDER_LINES);
		
		YFCElement orderLineEle = changeOrderIn.createElement(XMLConstants.ORDER_LINE);
		orderLineEle = changeOrderIn.importNode(oldOrderLineEle, true);
		orderLinesEle.appendChild(orderLineEle);
		//System.out.println("OrderLineEle"+orderLinesEle.getString());
		
		orderLineEle.removeAttribute(XMLConstants.ORDER_LINE_KEY);
		orderLineEle.setAttribute("Action", "CREATE");
		//System.out.println("Input to changeOrder"+changeOrderIn.getString());
		YFCDocument changeOrderOut = SterlingUtil.callAPI(env, "changeOrder", changeOrderIn, YFCDocument.parse(changeOrderStr));
		
		ServiceContractUtil scUtil = new ServiceContractUtil();
		scUtil.setPriceAttributes(env, changeOrderOut);
		
		return changeOrderOut.getDocument();	
	}
}
