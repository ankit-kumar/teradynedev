package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GetOrderShipment implements YIFCustomApi {

	private YFCLogCategory logger;

	YFCDocument inputOrder = null;
	YFCDocument inputShipmentLineList =null;
	YFCDocument inputShipment = null;
	YFCDocument inputOrganization = null;
	YFCDocument inputOrg=null;
	YFCDocument OrderDetail=null;

	Document outputOrder = null;
	Document outputShipment = null;
	Document outputOrganization = null;
	Document outputOrg = null;

	YFCDocument finalOutput = null;
	YFCDocument TemplateOrderLine = null;
	YFCDocument TemplateShipmentLine = null;
	YFCDocument TemplateOrganization = null;
	YFCDocument TemplateOrg = null;
	YFCDocument TemplateOrder=null;

	YFCDocument outDoc = null;
	Document outDoc5 = null;
	Document outDoc6 = null;
	
	String ServiceTypeDescn="";String OrderLineTypeDesc="";
	String OrderLineListApi = "getOrderLineList";
	String ShipmentLineListApi = "getShipmentLineList";
	String getOrgHierarchyApi = "getOrganizationHierarchy";
	String getOrganizationHierarchyApi = "getOrganizationHierarchy";
	String getStatusListApi = "getStatusList";

	String LineStatusDerived = "";
	String Status = "";
	String ShipDate = "";
	String CustSiteNo = "";
	String QuantityOrdered = "";
	String OrderLineStatusCodeDesc = "";
	String TotalReceiptDueQuantity = "";
	String OrderType = "";
	String OrgCode = "";
	String OrderedQty = "";
	Double ReceivedQty = null;
	String OrderHeaderKey ="";
	Double TotalQty =null;
	String OrderNo = "";
	String CustomerPONo = "";
	String OrderDate = "";
	String OrderLineNo = "";
	String IsCreditHold = "";
	String IsServiceHold = "";
	String TotalQuantityOrdered = "";
	String QuantityReceived = "";
	String OrderSubLineNo = "";
	//Double ServiceType = null;
	//Double ServiceTypeDesc = null;
	String PartNo = "";
	String OrderLineStatusCode = "";
	Boolean Flag = false;
	String PartyNo = "";
	String BuyerOrg="";
	String ReqDeliveryDate = "";
	String PartyAddressNo = "";
	String SysSerialNo = "";
	String SerialNo = "";
	String ActualShipmentDate="";
	String EntitlementNo = "";
	String WayBillNo = "";
	String CustPOno = "";
	String OrderTypeCode = "";
	String OrderLineStatusCDDescn = "";
	String SysProgram = "";
	String ServiceType = "";
	String PartSerialNo = "";
	String BuyerOrganizationCode = "";
	String ReqShipDate = "";
	String PrimeLineNo = "";
	String SCAC = "";
	String SubLineNo = "";
	String Quantity = "";
	String BolNo = "";
	String ItemShortDesc = "";
	String ItemID = "";
	String RecvdQty = "";
	String SystemSerialNo = "";
	String AgreementNo = "";
	String WarrantyNo = "";
	String MktStatus = "";
	String NoteText = "";
	String ContactAttnTo = "";
	String ContactName = "";
	String DOAQty = "";
	String Itemtype = "";
	String OEMSerialNo = "";
	String RepairNCCode = "";
	String ReceivingNode = "";
	String SystemType = "";
	String TotalReceiptDueQty = "";
	String DueDateFromCustomer = "";
	String ShipmentType = "";
	String OrganizationName = "";
	String MinLineStatus = "";
	String Status1 = "";
	String stringc = "";
	String stringd = "";
	String stringa = "";
	String stringb = "";

	public GetOrderShipment() {
		logger = YFCLogCategory.instance(this.getClass());
	}

	/**
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws Exception
	 */
	public Document getOrderShipment(YFSEnvironment env, Document inDoc)
			throws Exception {
		try {
			YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
			YFCElement eleInput = inYFCDoc.getDocumentElement();
			outDoc = YFCDocument.createDocument("Output");
		
			//YFCElement eleGetOrderShipment = eleInput.getChildElement("GetOrderShipment");

			if (eleInput != null) {
			
				YFCIterable<YFCElement> allInputline = eleInput.getChildren();
				for (YFCElement inputEle : allInputline) {
				
				OrderNo = inputEle.getAttribute("OrderNo");
				PartyNo = inputEle.getAttribute("PartyNo");
				PartyAddressNo = inputEle.getAttribute("PartyAddressNo");
				SysSerialNo = inputEle.getAttribute("SystemSerialNo");
				WayBillNo = inputEle.getAttribute("WayBillNo");
				EntitlementNo = inputEle.getAttribute("EntitlementNo");
				PartNo = inputEle.getAttribute("PartNo");
				CustPOno = inputEle.getAttribute("CustomerPoNo");
				OrderTypeCode = inputEle.getAttribute("OrderTypeCode");
				OrderLineStatusCDDescn = inputEle.getAttribute("OrderLineStatusCDDesc");
				SysProgram = inputEle.getAttribute("SysProgram");
				ServiceType = inputEle.getAttribute("ServiceTypeCode");
				PartSerialNo = inputEle.getAttribute("PartSerialNo");
				if (!PartyNo.isEmpty() && !PartyAddressNo.isEmpty()){
					ReceivingNode = PartyNo + "-" + PartyAddressNo;
					BuyerOrg = "";}
				if(!PartyNo.isEmpty() && PartyAddressNo.isEmpty()){
					BuyerOrg= PartyNo;
				}
				if(PartyNo.isEmpty() && PartyAddressNo.isEmpty())
					ReceivingNode = "";
			
			if (OrderLineStatusCDDescn != null
					&& !(OrderLineStatusCDDescn.isEmpty())) {
				// System.out.println("status from cust " +
				// OrderLineStatusCDDescn);
				logger.verbose("status from cust " + OrderLineStatusCDDescn);

				YFCDocument docInputStatusList = YFCDocument
						.getDocumentFor("<Status ProcessTypeKey=\"ORDER_FULFILLMENT\" Description=\"\"/>");
				YFCElement eleStatus = docInputStatusList.getDocumentElement();
				eleStatus.setAttribute("Description", OrderLineStatusCDDescn);

				YFCDocument docGetStatusListTemplate = YFCDocument
						.getDocumentFor("<StatusList> <Status Status=\"\"/> </StatusList>");
				env.setApiTemplate(getStatusListApi,
						docGetStatusListTemplate.getDocument());

				try {

					logger.verbose("getStatusList output"
							+ docInputStatusList.toString());
					Document docGetStatusListOP = YIFClientFactory
							.getInstance()
							.getLocalApi()
							.invoke(env, getStatusListApi,
									docInputStatusList.getDocument());

					if (null != docGetStatusListOP) {
						YFCDocument docGetStatusList = YFCDocument
								.getDocumentFor(docGetStatusListOP);
						logger.verbose("getStatusList output"
								+ docGetStatusList);
						YFCElement eleStatusList = docGetStatusList
								.getDocumentElement();
						if (null != eleStatusList) {
							YFCElement eleStatusOP = eleStatusList
									.getChildElement("Status");
							if (null != eleStatusOP) {
								Status1 = eleStatusOP.getAttribute("Status");
							}

						}

						logger.verbose("Status from api " + Status);
					}
				} finally {
					env.clearApiTemplate(getStatusListApi);
				}

			}
			
			if(WayBillNo.isEmpty()){
						
			inputOrder = YFCDocument.createDocument("OrderLine");
			YFCElement inputOrderEle = inputOrder.getDocumentElement();

			inputOrderEle.setAttribute("CustomerPONo", CustPOno);
			if (Status1 != null)
				inputOrderEle.setAttribute("Status", Status1);
			else
				inputOrderEle.setAttribute("Status", "");
			inputOrderEle.setAttribute("SerialNo", PartSerialNo);
			inputOrderEle.setAttribute("ReceivingNode", ReceivingNode);

			YFCElement extnEle = inputOrderEle.createChild("Extn");
			extnEle.setAttribute("SystemSerialNo", SysSerialNo);
			extnEle.setAttribute("SysProgram", SysProgram);
			extnEle.setAttribute("ServiceType", ServiceType);
			extnEle.setAttribute("WarrantyNo", EntitlementNo);

			YFCElement itemEle = inputOrderEle.createChild("Item");
			itemEle.setAttribute("ItemID", PartNo);

			YFCElement orderEle = inputOrderEle.createChild("Order");
			orderEle.setAttribute("OrderNo", OrderNo);
			orderEle.setAttribute("OrderType", OrderTypeCode);
			orderEle.setAttribute("BuyerOrganizationCode", BuyerOrg);

			TemplateOrderLine = YFCDocument
					.getDocumentFor("<OrderLineList> <OrderLine CustomerPONo=\"\"> <Order  OrderNo=\"\" OrderType=\"\" OrderDate=\"\" BuyerOrganizationCode=\"\"> <Extn ContactAttnTo=\"\" ContactName=\"\"/> <OrderLines> <OrderLine ReceivingNode=\"\" SerialNo=\"\" MinLineStatus=\"\" ReqShipDate=\"\" PrimeLineNo=\"\" SubLineNo=\"\" OrderedQty=\"\" ReqDeliveryDate=\"\" SCAC=\"\" ConditionVariable1=\"\"  Status=\"\"> <Extn SystemSerialNo=\"\" ServiceType=\"\" RecvdQty=\"\" SystemType=\"\" WarrantyNo=\"\" AgreementNo=\"\" SysProgram=\"\"  MktStatus=\"\" OEMSerialNo=\"\" RepairNoChargeCode=\"\" ServiceTypeCode=\"\"/> <ItemDetails ItemID=\"\" > <PrimaryInformation ItemType=\"\" ShortDescription=\"\"/></ItemDetails> <Notes> <Note NoteText=\"\"/> </Notes> </OrderLine> </OrderLines>	</Order> </OrderLine> </OrderLineList>");

			env.setApiTemplate(OrderLineListApi,
					TemplateOrderLine.getDocument());
			outputOrder = YIFClientFactory.getInstance().getLocalApi()
					.invoke(env, OrderLineListApi, inputOrder.getDocument());
			env.clearApiTemplate(OrderLineListApi);
			System.out.println("orderLineListOutput"
					+ YFCDocument.getDocumentFor(outputOrder));

			if (outputOrder != null) {
				YFCElement OutputOrderElementList = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
				System.out.println("Inside orderlinelist API");
				// YFCElement eleOrderLine =
				// OutputOrderElementList.getChildElement("OrderLine");
				// System.out.println("Orderlist output--->" + eleOrderLine );

				if (OutputOrderElementList != null) {

					// for loop for each orderline
					YFCIterable<YFCElement> allOrderline = OutputOrderElementList.getChildren();
				
					for (YFCElement orderLineEle : allOrderline) {

						CustomerPONo = orderLineEle.getAttribute("CustomerPONo");
						YFCElement eleOrder = orderLineEle.getChildElement("Order");
						if (eleOrder != null) {
							OrderNo = eleOrder.getAttribute("OrderNo");
							OrderDate = eleOrder.getAttribute("OrderDate");
							OrderType = eleOrder.getAttribute("OrderType");
							if(OrderType !=null){
								String apiName1 = "getCommonCodeList";
								YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" DocumentType=\"\"/>");
								inputDoc.getDocumentElement().setAttribute("CodeValue", OrderType);
								inputDoc.getDocumentElement().setAttribute("CodeType", "ORDER_TYPE");
								inputDoc.getDocumentElement().setAttribute("DocumentType", "0001");
								outDoc6 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
								//env.clearApiTemplate(apiName1);
								
								if (outDoc6 !=null){
									YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc6).getDocumentElement();
									if (OutputCommList != null){
										YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
										if(eleComm !=null){
											OrderLineTypeDesc= eleComm.getAttribute("CodeLongDescription");
											System.out.println("OrderLineTypeDesc Desc@@@@@@@@@@ "+OrderLineTypeDesc);
										}
									}
								}
							
							}
							
							BuyerOrganizationCode = eleOrder.getAttribute("BuyerOrganizationCode");
							ContactAttnTo = eleOrder.getChildElement("Extn").getAttribute("ContactAttnTo");
							ContactName = eleOrder.getChildElement("Extn").getAttribute("ContactName");

							YFCElement eleOrderLines = eleOrder.getChildElement("OrderLines");
							if (eleOrderLines != null) {
								YFCElement eleOrderLn = eleOrderLines.getChildElement("OrderLine");
								if (null != eleOrderLn) {

									OrderLineNo = eleOrderLn.getAttribute("PrimeLineNo");
									ReceivingNode = eleOrderLn.getAttribute("ReceivingNode");
									SubLineNo = eleOrderLn.getAttribute("SubLineNo");
									ServiceType = eleOrderLn.getChildElement("Extn").getAttribute("ServiceType");
									if(ServiceType !=null){
										String apiName1 = "getCommonCodeList";
										YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\"/>");
										inputDoc.getDocumentElement().setAttribute("CodeValue", ServiceType);
										inputDoc.getDocumentElement().setAttribute("CodeType", "Service_Type");
										inputDoc.getDocumentElement().setAttribute("OrganizationCode", "CSO");
										outDoc5 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
										env.clearApiTemplate(apiName1);
										
										if (outDoc5 !=null){
											YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc5).getDocumentElement();
											if (OutputCommList != null){
												YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
												if(eleComm !=null){
													ServiceTypeDescn= eleComm.getAttribute("CodeShortDescription");
													System.out.println("Serv Desc@@@@@@@@@@ "+ServiceTypeDescn);
												}
											}
										}
									
									}
									
									ItemShortDesc = eleOrderLn.getChildElement("ItemDetails").getChildElement("PrimaryInformation").getAttribute("ShortDescription");
									ItemID = eleOrderLn.getChildElement("ItemDetails").getAttribute("ItemID");
									Status = eleOrderLn.getAttribute("Status");
									OrderedQty = eleOrderLn.getAttribute("OrderedQty");
									RecvdQty = eleOrderLn.getChildElement("Extn").getAttribute("RecvdQty");
									ReqDeliveryDate = eleOrderLn.getAttribute("ReqDeliveryDate");
									SystemType = eleOrderLn.getChildElement("Extn").getAttribute("SystemType");
									ReqShipDate = eleOrderLn.getAttribute("ReqShipDate");
									SystemSerialNo = eleOrderLn.getChildElement("Extn").getAttribute("SystemSerialNo");
									SCAC = eleOrderLn.getAttribute("SCAC");
									SerialNo = eleOrderLn.getAttribute("SerialNo");
									AgreementNo = eleOrderLn.getChildElement("Extn").getAttribute("WarrantyNo");
									// WarrantyNo=eleOrderLn.getChildElement("Extn").getAttribute("WarrantyNo");
									MinLineStatus = eleOrderLn.getAttribute("MinLineStatus");
									SysProgram = eleOrderLn.getChildElement("Extn").getAttribute("SysProgram");
									MktStatus = eleOrderLn.getChildElement("Extn").getAttribute("MktStatus");
									if (eleOrderLn.getChildElement("Notes")
											.getChildElement("Note") != null)
										NoteText = eleOrderLn.getChildElement("Notes").getChildElement("Note").getAttribute("NoteText");
									else
										NoteText = "";
									
									DOAQty = eleOrderLn.getAttribute("ConditionVariable1");
									/*if (eleOrderLn.getChildElement("ItemDetails").getChildElement("PrimaryInformation") != null)
										Itemtype = eleOrderLn.getChildElement("ItemDetails").getChildElement("PrimaryInformation")
												.getAttribute("ItemType");
									else
										Itemtype = "";*/
									OEMSerialNo = eleOrderLn.getChildElement("Extn").getAttribute("OEMSerialNo");
									RepairNCCode = eleOrderLn.getChildElement("Extn").getAttribute("RepairNoChargeCode");

								}
							}

						}
						System.out.println("Receiving node= " + ReceivingNode);
						// Need to get the organization_name for the receiving
						// node---->SHIP-TO CUSTOMER NAME
						OrganizationName = OrgName(env, ReceivingNode);
						
						System.out.println("Org name" + OrganizationName);

						inputShipment = YFCDocument.getDocumentFor("<ShipmentLine OrderNo=\"\"/>");
						System.out.println("#########OrderNo" + OrderNo);
						inputShipment.getDocumentElement().setAttribute("OrderNo", OrderNo);
						TemplateShipmentLine = YFCDocument.getDocumentFor("<ShipmentLines> <ShipmentLine  OrderNo=\"\" Quantity=\"\" ShortageQty=\"\"> <Shipment ActualShipmentDate=\"\" BolNo=\"\" ShipmentType=\"\" RequestedShipmentDate=\"\"/> </ShipmentLine> </ShipmentLines>");

						env.setApiTemplate(ShipmentLineListApi,TemplateShipmentLine.getDocument());
						outputShipment = YIFClientFactory.getInstance().getLocalApi().invoke(env, ShipmentLineListApi,inputShipment.getDocument());
						env.clearApiTemplate(ShipmentLineListApi);
						System.out.println("########Outputshipmane"	+ YFCDocument.getDocumentFor(outputShipment));
						YFCElement OutputShipmentElementList = YFCDocument.getDocumentFor(outputShipment).getDocumentElement();
						if (OutputShipmentElementList != null && OutputShipmentElementList.hasChildNodes() ) {

							YFCElement eleShipmentLine = OutputShipmentElementList.getChildElement("ShipmentLine");
							Quantity = eleShipmentLine.getAttribute("Quantity");
							TotalReceiptDueQty =eleShipmentLine.getAttribute("ShortageQty");
							
							YFCElement eleShipment = eleShipmentLine.getChildElement("Shipment");
							DueDateFromCustomer = eleShipment.getAttribute("RequestedShipmentDate");
							BolNo = eleShipment.getAttribute("BolNo");
							ShipmentType = eleShipment.getAttribute("ShipmentType");
							if(eleShipment.hasAttribute("ActualShipmentDate")){
							ActualShipmentDate= eleShipment.getAttribute("ActualShipmentDate");
							}
							System.out.println("Inside shipment Api");
						}

						Flag = DateComp(OrderDate, ReqShipDate, MinLineStatus,Status);
		
					
						if (Flag) {
							System.out.println("PrepAring the out doc.......");

							YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetOrderShipment");
							finalOutputEle.setAttribute("OrderNo", OrderNo);
							finalOutputEle.setAttribute("OrderLineNo",OrderLineNo);
							finalOutputEle.setAttribute("OrderSubLineNo",SubLineNo);
							
							String ordDate = modifyDateLayout(OrderDate);
							finalOutputEle.setAttribute("OrderDate", ordDate);
							finalOutputEle.setAttribute("ServiceType",ServiceType);
							finalOutputEle.setAttribute("ServiceTypeDesc",ServiceTypeDescn);
							finalOutputEle.setAttribute("PartNo", ItemID);
							finalOutputEle.setAttribute("OrderLineStatusCode",MinLineStatus);
							finalOutputEle.setAttribute("OrderLineStatusCodeDesc", Status);
							finalOutputEle.setAttribute("QuantityOrdered",OrderedQty);
							finalOutputEle.setAttribute("QtyTotalIssue",Quantity);
							finalOutputEle.setAttribute("QtyReceived", RecvdQty);
							finalOutputEle.setAttribute("TotalReceiptDueQty",TotalReceiptDueQty);
							//finalOutputEle.setAttribute("DueDate",ReqDeliveryDate);
							
							String reqShipDate = modifyDateLayout(ReqShipDate);
							finalOutputEle.setAttribute("DueDate",reqShipDate);
							finalOutputEle.setAttribute("SystemType",SystemType);
							
							finalOutputEle.setAttribute("DueDateFromCustomer",DueDateFromCustomer);
							
							String actualShipDate =modifyDateLayout(ActualShipmentDate);
							finalOutputEle.setAttribute("ShipDate",actualShipDate );
							
							//finalOutputEle.setAttribute("ShipDate", ActualShipmentDate);
							finalOutputEle.setAttribute("SystemSerialNo",SystemSerialNo);
							finalOutputEle.setAttribute("Carrier", SCAC);
							finalOutputEle.setAttribute("WayBill", BolNo);
							finalOutputEle.setAttribute("PartSerialNo",	SerialNo);
							finalOutputEle.setAttribute("AgreementNo",AgreementNo);
							finalOutputEle.setAttribute("WarrantyType",	OrderType);
							finalOutputEle.setAttribute("Program", SysProgram);
							finalOutputEle.setAttribute("SystemDown", MktStatus);
							finalOutputEle.setAttribute("PlannerNotes",	NoteText);
							finalOutputEle.setAttribute("OrderPoNo",CustomerPONo);
							finalOutputEle.setAttribute("OrderAttentionTo",	ContactAttnTo);
							finalOutputEle.setAttribute("OrderContact",	ContactName);
							finalOutputEle.setAttribute("QtyDOA", DOAQty);
							if ("DOA".equals(OrderType)) {
								finalOutputEle.setAttribute("DOAReceiptQty",
										OrderedQty);
								finalOutputEle.setAttribute(
										"QuoteConversionInd", RecvdQty);
							} else {
								finalOutputEle
										.setAttribute("DOAReceiptQty", "");
								finalOutputEle.setAttribute("QuoteConversionInd", "");
							}
							finalOutputEle.setAttribute("OrderLineTypeCode","ORDER_TYPE");
							finalOutputEle.setAttribute("OrderLineTypeDesc",OrderLineTypeDesc);	
							finalOutputEle.setAttribute("OEMSerialNo",OEMSerialNo);
							finalOutputEle.setAttribute("ShipmentTypeCode",	ShipmentType);
							finalOutputEle.setAttribute("RepairNCCode",	RepairNCCode);

							// to do substring function for receiving node
							String[] str_array1 = ReceivingNode.split("-");

							stringc = str_array1[0];
							stringd = str_array1[1];
							finalOutputEle.setAttribute("ShipToCustomerNo",	stringc);
							finalOutputEle.setAttribute("ShipToCustomerSiteNo",	stringd);

							finalOutputEle.setAttribute("ShipToCustomerName",OrganizationName);

							// to do substring for bill to org
							String BillTo= BillName(env,BuyerOrganizationCode);
							if(BillTo !=null){
							String[] str_array = BillTo.split("-");

							stringa = str_array[0];
							stringb = str_array[1];

							finalOutputEle.setAttribute("BillToCustomerNo",	stringa);
							finalOutputEle.setAttribute("BillToCustomerSiteNo",	stringb);
							}
							else{
								finalOutputEle.setAttribute("BillToCustomerNo",	"");
								finalOutputEle.setAttribute("BillToCustomerSiteNo","");
							}
							
							// outDoc.getDocumentElement().addXMLToNode(finalOutputEle.toString());
						}
					}

				}
			}
			} // end of if with WayBillNo check
			else{
				inputShipmentLineList = YFCDocument.createDocument("ShipmentLine");
				YFCElement inputShipEle = inputShipmentLineList.getDocumentElement();
				inputShipEle.setAttribute("ItemID", PartNo);
				
				YFCElement shipmentEle = inputShipEle.createChild("Shipment");
				shipmentEle.setAttribute("BolNo", WayBillNo);
				
				YFCElement ordEle = inputShipEle.createChild("Order");
				ordEle.setAttribute("OrderNo", OrderNo);
				ordEle.setAttribute("BuyerOrganizationCode", BuyerOrg);
				ordEle.setAttribute("CustomerPONo", CustPOno);
				ordEle.setAttribute("OrderType", OrderTypeCode);
				
				YFCElement ordlineEle = inputShipEle.createChild("OrderLine");
				ordlineEle.setAttribute("ReceivingNode", ReceivingNode);
				ordlineEle.setAttribute("SerialNo", PartSerialNo);
				if (Status1 != null)
					ordlineEle.setAttribute("Status", Status1);
				else
					ordlineEle.setAttribute("Status", "");
				
				YFCElement ordlineExtnEle = ordlineEle.createChild("Extn");
				ordlineExtnEle.setAttribute("SystemSerialNo", SysSerialNo);
				ordlineExtnEle.setAttribute("WarrantyNo", EntitlementNo);
				ordlineExtnEle.setAttribute("SysProgram", SysProgram);
				ordlineExtnEle.setAttribute("ServiceType", ServiceType);
				
				TemplateShipmentLine = YFCDocument.getDocumentFor("<ShipmentLines><ShipmentLine OrderNo=\"\" Quantity=\"\" ShortageQty=\"\" OrderHeaderKey=\"\" ItemID=\"\" ItemDesc=\"\" CustomerPONo=\"\"><Shipment ActualShipmentDate=\"\" BolNo=\"\" ShipmentType=\"\" RequestedShipmentDate=\"\" BuyerOrganizationCode=\"\"/>	<OrderLine PrimeLineNo=\"\" SubLineNo=\"\" Status=\"\" MinLineStatus=\"\" OrderedQty=\"\" ReqDeliveryDate=\"\" ReqShipDate=\"\" SCAC=\"\" SerialNo=\"\" ConditionVariable1=\"\" ReceivingNode=\"\" ><Extn ServiceTypeCode=\"\" RecvdQty=\"\" SystemType=\"\" SystemSerialNo=\"\" AgreementNo=\"\" MktStatus=\"\" SysProgram=\"\" OEMSerialNo=\"\" RepairNoChargeCode=\"\" /><ItemDetails>	<PrimaryInformation ItemType=\"\"/>	</ItemDetails>	</OrderLine></ShipmentLine></ShipmentLines>");

				env.setApiTemplate(ShipmentLineListApi,TemplateShipmentLine.getDocument());
				outputShipment = YIFClientFactory.getInstance().getLocalApi().invoke(env, ShipmentLineListApi, inputShipmentLineList.getDocument());
				env.clearApiTemplate(ShipmentLineListApi);
				
				YFCElement OutputShipmentElementList = YFCDocument.getDocumentFor(outputShipment).getDocumentElement();
				if (OutputShipmentElementList != null && OutputShipmentElementList.hasChildNodes() ) {
					
					YFCIterable<YFCElement> allShipmentline = OutputShipmentElementList.getChildren();
					
					for (YFCElement shipLineEle : allShipmentline) 	{

					
					Quantity = shipLineEle.getAttribute("Quantity");
					TotalReceiptDueQty =shipLineEle.getAttribute("ShortageQty");
					OrderNo = shipLineEle.getAttribute("OrderNo");
					OrderHeaderKey = shipLineEle.getAttribute("OrderHeaderKey");
					ItemShortDesc = shipLineEle.getAttribute("ItemDesc");
					ItemID = shipLineEle.getAttribute("ItemID");
					CustomerPONo = shipLineEle.getAttribute("CustomerPONo");
					
					YFCElement opShipment = shipLineEle.getChildElement("Shipment");
					if (opShipment != null) {
						if(opShipment.hasAttribute("ActualShipmentDate")){
							ActualShipmentDate=opShipment.getAttribute("ActualShipmentDate");
						}
						BolNo = opShipment.getAttribute("BolNo");
						ShipmentType = opShipment.getAttribute("ShipmentType");
						DueDateFromCustomer = opShipment.getAttribute("RequestedShipmentDate");
						BuyerOrganizationCode = opShipment.getAttribute("BuyerOrganizationCode");
					}
					
					YFCElement opOrdLine = shipLineEle.getChildElement("OrderLine");
					if(opOrdLine !=null){
						OrderLineNo = opOrdLine.getAttribute("PrimeLineNo");
						ReceivingNode = opOrdLine.getAttribute("ReceivingNode");
						SubLineNo = opOrdLine.getAttribute("SubLineNo");
						ServiceType = opOrdLine.getChildElement("Extn").getAttribute("ServiceType");
						
						if(ServiceType !=null){
							String apiName1 = "getCommonCodeList";
							YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\"/>");
							inputDoc.getDocumentElement().setAttribute("CodeValue", ServiceType);
							inputDoc.getDocumentElement().setAttribute("CodeType", "Service_Type");
							inputDoc.getDocumentElement().setAttribute("OrganizationCode", "CSO");
							outDoc5 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
							env.clearApiTemplate(apiName1);
							
							if (outDoc5 !=null){
								YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc5).getDocumentElement();
								if (OutputCommList != null){
									YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
									if(eleComm !=null){
										ServiceTypeDescn= eleComm.getAttribute("CodeShortDescription");
										System.out.println("Serv Desc@@@@@@@@@@ "+ServiceTypeDescn);
									}
								}
							}
						
						}
						
						Status = opOrdLine.getAttribute("Status");
						OrderedQty = opOrdLine.getAttribute("OrderedQty");
						RecvdQty = opOrdLine.getChildElement("Extn").getAttribute("RecvdQty");
						ReqDeliveryDate = opOrdLine.getAttribute("ReqDeliveryDate");
						SystemType = opOrdLine.getChildElement("Extn").getAttribute("SystemType");
						ReqShipDate = opOrdLine.getAttribute("ReqShipDate");
						SystemSerialNo = opOrdLine.getChildElement("Extn").getAttribute("SystemSerialNo");
						SCAC = opOrdLine.getAttribute("SCAC");
						SerialNo = opOrdLine.getAttribute("SerialNo");
						AgreementNo = opOrdLine.getChildElement("Extn").getAttribute("WarrantyNo");
						// WarrantyNo=eleOrderLn.getChildElement("Extn").getAttribute("WarrantyNo");
						MinLineStatus = opOrdLine.getAttribute("MinLineStatus");
						SysProgram = opOrdLine.getChildElement("Extn").getAttribute("SysProgram");
						MktStatus = opOrdLine.getChildElement("Extn").getAttribute("MktStatus");
						OEMSerialNo = opOrdLine.getChildElement("Extn").getAttribute("OEMSerialNo");
						RepairNCCode = opOrdLine.getChildElement("Extn").getAttribute("RepairNoChargeCode");
						DOAQty = opOrdLine.getAttribute("ConditionVariable1");
						
						/*if (opOrdLine.getChildElement("ItemDetails").getChildElement("PrimaryInformation") != null)
							Itemtype = opOrdLine.getChildElement("ItemDetails").getChildElement("PrimaryInformation")
									.getAttribute("ItemType");
						else
							Itemtype = "";*/
						
					}
					OrganizationName = OrgName(env,ReceivingNode);
					
					OrderDetail = YFCDocument.getDocumentFor("<Order OrderHeaderKey=\"\"/>");
					OrderDetail.getDocumentElement().setAttribute("OrderHeaderKey", OrderHeaderKey);
					System.out.println("#####order detail"	+ OrderDetail);
					TemplateOrder = YFCDocument.getDocumentFor("<Order OrderDate=\"\" OrderType=\"\"><Extn ContactAttnTo=\"\" ContactName=\"\"/><Notes>	<Note NoteText=\"\"/></Notes></Order>");
					env.setApiTemplate("getOrderDetails",TemplateOrder.getDocument());
					outputOrder = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getOrderDetails",OrderDetail.getDocument());
					if (outputOrder != null) {
						YFCElement OutputOrderele = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
						if(OutputOrderele!= null){
							OrderDate = OutputOrderele.getAttribute("OrderDate");
							OrderType = OutputOrderele.getAttribute("OrderType");
							
						}
						if(OrderType !=null){
							String apiName1 = "getCommonCodeList";
							YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" DocumentType=\"\"/>");
							inputDoc.getDocumentElement().setAttribute("CodeValue", OrderType);
							inputDoc.getDocumentElement().setAttribute("CodeType", "ORDER_TYPE");
							inputDoc.getDocumentElement().setAttribute("DocumentType", "0001");
							outDoc6 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
							//env.clearApiTemplate(apiName1);
							
							if (outDoc6 !=null){
								YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc6).getDocumentElement();
								if (OutputCommList != null){
									YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
									if(eleComm !=null){
										OrderLineTypeDesc= eleComm.getAttribute("CodeLongDescription");
										System.out.println("OrderLineTypeDesc Desc@@@@@@@@@@ "+OrderLineTypeDesc);
									}
								}
							}
						
						}
						YFCElement opOrdExtn = OutputOrderele.getChildElement("Extn");
						if(opOrdExtn != null){
						ContactAttnTo = opOrdExtn.getAttribute("ContactAttnTo");
						ContactName = opOrdExtn.getAttribute("ContactName");
						}
						YFCElement opOrdNotes = OutputOrderele.getChildElement("Notes");
						if (opOrdNotes.getChildElement("Note") != null)
							NoteText = opOrdNotes.getChildElement("Note").getAttribute("NoteText");
						else
							NoteText = "";
					}
					
					Flag = DateComp(OrderDate, ReqShipDate, MinLineStatus,Status);
					
					if(Flag){
						System.out.println("PrepAring the out doc from shipment.......");
						
						YFCElement finalOutputEle = outDoc.getDocumentElement().createChild("GetOrderShipment");
						finalOutputEle.setAttribute("OrderNo", OrderNo);
						finalOutputEle.setAttribute("OrderLineNo",OrderLineNo);
						finalOutputEle.setAttribute("OrderSubLineNo",SubLineNo);
						
						String orddate = modifyDateLayout(OrderDate);
						finalOutputEle.setAttribute("OrderDate", orddate);
						
						finalOutputEle.setAttribute("ServiceType",ServiceType);
						finalOutputEle.setAttribute("ServiceTypeDesc",ServiceTypeDescn);
						finalOutputEle.setAttribute("PartNo", ItemID);
						finalOutputEle.setAttribute("OrderLineStatusCode",MinLineStatus);
						finalOutputEle.setAttribute("OrderLineStatusCodeDesc", Status);
						finalOutputEle.setAttribute("QuantityOrdered",OrderedQty);
						finalOutputEle.setAttribute("QtyTotalIssue",Quantity);
						finalOutputEle.setAttribute("QtyReceived", RecvdQty);
						finalOutputEle.setAttribute("TotalReceiptDueQty",TotalReceiptDueQty);
						//finalOutputEle.setAttribute("DueDate",ReqDeliveryDate);
						
						String reqShipDate = modifyDateLayout(ReqShipDate);
						finalOutputEle.setAttribute("DueDate",reqShipDate);
						
						finalOutputEle.setAttribute("SystemType",SystemType);
					
						finalOutputEle.setAttribute("DueDateFromCustomer",DueDateFromCustomer);
						
						String actualShipDate =modifyDateLayout(ActualShipmentDate);
						finalOutputEle.setAttribute("ShipDate",actualShipDate );
						
						finalOutputEle.setAttribute("SystemSerialNo",SystemSerialNo);
						finalOutputEle.setAttribute("Carrier", SCAC);
						finalOutputEle.setAttribute("WayBill", BolNo);
						finalOutputEle.setAttribute("PartSerialNo",	SerialNo);
						finalOutputEle.setAttribute("AgreementNo",AgreementNo);
						finalOutputEle.setAttribute("WarrantyType",	OrderType);
						finalOutputEle.setAttribute("Program", SysProgram);
						finalOutputEle.setAttribute("SystemDown", MktStatus);
						finalOutputEle.setAttribute("PlannerNotes",	NoteText);
						finalOutputEle.setAttribute("OrderPoNo",CustomerPONo);
						finalOutputEle.setAttribute("OrderAttentionTo",	ContactAttnTo);
						finalOutputEle.setAttribute("OrderContact",	ContactName);
						finalOutputEle.setAttribute("QtyDOA", DOAQty);
						if ("DOA".equals(OrderType)) {
							finalOutputEle.setAttribute("DOAReceiptQty",
									OrderedQty);
							finalOutputEle.setAttribute(
									"QuoteConversionInd", RecvdQty);
						} else {
							finalOutputEle
									.setAttribute("DOAReceiptQty", "");
							finalOutputEle.setAttribute("QuoteConversionInd", "");
						}
						finalOutputEle.setAttribute("OrderLineTypeCode","ORDER_TYPE");
						finalOutputEle.setAttribute("OrderLineTypeDesc",Itemtype);	
						finalOutputEle.setAttribute("OEMSerialNo",OEMSerialNo);
						finalOutputEle.setAttribute("ShipmentTypeCode",	ShipmentType);
						finalOutputEle.setAttribute("RepairNCCode",	RepairNCCode);

						// to do substring function for receiving node
						String[] str_array1 = ReceivingNode.split("-");

						stringc = str_array1[0];
						stringd = str_array1[1];
						finalOutputEle.setAttribute("ShipToCustomerNo",	stringc);
						finalOutputEle.setAttribute("ShipToCustomerSiteNo",	stringd);

						finalOutputEle.setAttribute("ShipToCustomerName",OrganizationName);

						// to do substring for bill to org
						String BillTo= BillName(env,BuyerOrganizationCode);
						if(BillTo !=null){
						String[] str_array = BillTo.split("-");

						stringa = str_array[0];
						stringb = str_array[1];

						finalOutputEle.setAttribute("BillToCustomerNo",	stringa);
						finalOutputEle.setAttribute("BillToCustomerSiteNo",	stringb);
						}
						else{
							finalOutputEle.setAttribute("BillToCustomerNo",	"");
							finalOutputEle.setAttribute("BillToCustomerSiteNo","");
						}
						
						}

					}
				
					
				
			}
				
			}
			}
				}
			
			
		} catch (Exception e) {
			throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}
		
	
		return outDoc.getDocument();
	}
	
	public String BillName(YFSEnvironment env1,String S) throws YFSException, RemoteException, YIFClientCreationException{
	String TerBillingID=""; String TerDefaultFLag="";
	inputOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"/>");
	inputOrg.getDocumentElement().setAttribute("OrganizationCode", S);
	TemplateOrg = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\" ><Extn><TerCustBillingOrgList><TerCustBillingOrg TerBillingID=\"\" TerDefaultFLag=\"\"/></TerCustBillingOrgList></Extn></Organization>");

	env1.setApiTemplate(getOrgHierarchyApi,TemplateOrg.getDocument());
	outputOrg = YIFClientFactory.getInstance().getLocalApi().invoke(env1, getOrgHierarchyApi,inputOrg.getDocument());
	env1.clearApiTemplate(getOrgHierarchyApi);
	
	YFCElement OutputOrgElementList = YFCDocument.getDocumentFor(outputOrg).getDocumentElement();
	if(OutputOrgElementList != null){
		YFCElement extnEle = OutputOrgElementList.getChildElement("Extn");
		if(extnEle !=null){
			YFCElement terElelist = extnEle.getChildElement("TerCustBillingOrgList");
			
			YFCIterable<YFCElement> allOrglist = terElelist.getChildren();
			
			for (YFCElement terlineEle : allOrglist){
				TerBillingID = terlineEle.getAttribute("TerBillingID");
				TerDefaultFLag = terlineEle.getAttribute("TerDefaultFLag");
				if("Y".equalsIgnoreCase(TerDefaultFLag)){
					break;
				}
				
			}
		}
	}
	return TerBillingID;

	}
	public String OrgName(YFSEnvironment env1,String S) throws YFSException, RemoteException, YIFClientCreationException{
		
		
		String OrgName1=null;
		inputOrganization = YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"/>");
		inputOrganization.getDocumentElement().setAttribute("OrganizationCode", S);
		System.out.println("#####inputOrganization"	+ inputOrganization);
		TemplateOrganization = YFCDocument.getDocumentFor("<Organization OrganizationName=\"\"/>");
		env1.setApiTemplate(getOrganizationHierarchyApi,TemplateOrganization.getDocument());
		outputOrganization = YIFClientFactory.getInstance().getLocalApi().invoke(env1, getOrganizationHierarchyApi,inputOrganization.getDocument());
		if (outputOrganization != null) {
			System.out.println("###########"+ outputOrganization);
			YFCElement OutputOrganizationElementList = YFCDocument.getDocumentFor(outputOrganization).getDocumentElement();
			if (OutputOrganizationElementList != null)
				 OrgName1 = OutputOrganizationElementList.getAttribute("OrganizationName");

		}
		return OrgName1;
	}
	
	public Boolean DateComp(String OrderDate, String ReqShpDate, String MinSts, String Status) throws ParseException  {
		// Date comparision
		
		java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = java.util.Calendar.getInstance().getTime();
		currentDate = dateFormat.parse(dateFormat.format(currentDate));
		System.out.println("Order DAte ------>" + OrderDate);
		System.out.println("Req ship DAte ------>"+ ReqShpDate);
		if(OrderDate !=null && ReqShpDate!=null){
		Date OrdDate = dateFormat.parse(OrderDate.substring(0,10));
		Date ShpDate = dateFormat.parse(ReqShpDate.substring(0, 10));
		int statusId = Integer.parseInt(MinSts);
		long diffInMillisecOrd = 0;
		long diffInMillisecShp = 0;
		long diffInDaysOrd = 0;
		long diffInDaysShp = 0;
		diffInMillisecOrd = currentDate.getTime()- OrdDate.getTime();
		diffInDaysOrd = diffInMillisecOrd/ (24 * 60 * 60 * 1000);
		diffInMillisecShp = ShpDate.getTime()- currentDate.getTime();
		diffInDaysShp = diffInMillisecShp/ (24 * 60 * 60 * 1000);
		System.out.println("diff order date" + diffInDaysOrd);
		System.out.println("diff ship date" + diffInDaysShp);
		if ((diffInDaysOrd >= 60) && (diffInDaysShp >= 90)&& !(Status.equalsIgnoreCase("cancelled")) && statusId < 3900) {
			Flag = true;
			System.out.println("FLAG is set here");
		} else
			Flag = false;
		}
		else
			Flag = false;
		return Flag;
	}
	
	public String modifyDateLayout(String inputDate) throws Exception{
		String newDateString = "";
		System.out.println("DateInput to the method----------->" + inputDate);
		if(!inputDate.isEmpty() || inputDate != null){
		    String subString = inputDate.substring(0,10);
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date newFormatedDate = formatter.parse(subString);
			newDateString=dateFormat.format(newFormatedDate);
		}
		System.out.println("newDateString ----------->" + newDateString);
		return newDateString;
	}
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

}
