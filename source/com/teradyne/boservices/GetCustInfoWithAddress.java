package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.util.Properties;

import org.w3c.dom.Document;

//import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
//import com.yantra.pca.ycd.jasperreports.returnOrderSummaryReportScriptlet;
//import com.yantra.shared.wms.WMSLiterals;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GetCustInfoWithAddress implements YIFCustomApi {

	
	YFCDocument inYFCXML = null;
	String CustNo = null;
	String OrgCode = null;
	String CusType = null;
	String apiName1 = "getCustomerDetails";
	String apiName2 = "getLocaleList";
	Document outDoc1 = null;
	Document outDoc2 = null;
	String locale = null;
	YFCDocument outputDoc =null;
	String sCID=null;
	String LocaleDesc = null;
	String CustId=null;
	String Org=null;
	
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document getCustInfo(YFSEnvironment env, Document inxml){
		try {
		inYFCXML = YFCDocument.getDocumentFor(inxml);
		YFCElement NodeList = inYFCXML.getDocumentElement();
		YFCElement Node = NodeList.getChildElement("GetCustomerInfoWithAddress");
	//	YFCElement Node = inYFCXML.getElementById("GetCustomerInfoWithAddress");
		
		if(Node != null){
			if (Node.hasAttribute("CustomerNumber") && Node.hasAttribute("SiteID") && Node.hasAttribute("SiteType")){
					CustNo = Node.getAttribute("CustomerNumber");
					OrgCode = Node.getAttribute("SiteID");
					CusType = Node.getAttribute("SiteType");
					CustId= CustNo +"-"+ OrgCode;
					 Org="CSO";
			}
			if(Node.hasAttribute("CustomerNumber")&& Node.hasAttribute("SiteType")&& Node.getAttribute("SiteID").isEmpty()){
			CustId =Node.getAttribute("CustomerNumber");
			CusType = Node.getAttribute("SiteType");
			 Org="CSO";
			}
		}

		
		/* final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Customer CustomerID="+CustNo+" OrganizationCOde="
				+OrgCode+" CustomerType="+CusType+"/>"); */
		final YFCDocument inputDoc = YFCDocument.getDocumentFor("<Customer CustomerID=\"\" OrganizationCode=\"\" CustomerType=\"\" />");
		inputDoc.getDocumentElement().setAttribute("CustomerID", CustId);
		inputDoc.getDocumentElement().setAttribute("OrganizationCode", Org);
		inputDoc.getDocumentElement().setAttribute("CustomerType", CusType);
		
	/*	
		YFCDocument taskDoc = YFCDocument.createDocument("Customer");
		YFCElement docElement =  taskDoc.getDocumentElement();
		YFCElement Extn = docElement.createChild("Extn");
		Extn.setAttribute("SpecialCustInstr", value)
		
	*/	
		final YFCDocument templateDoc = YFCDocument
				.getDocumentFor("<Customer>"
						+ "<Extn SpecialCustInstr=\"\" SpecialInstrVisible=\"\" IsTransExceptionAllowed=\"\" InternalCustomerFlag=\"\" IsCreditHold=\"\" IsServiceHold=\"\" />"
						+ "<BuyerOrganization LocaleCode=\"\" OrganizationName=\"\">" 
						+ "<Extn CustomerSiteStatus=\"\" />"
						+ "<BillingPersonInfo />"
						+ "</BuyerOrganization>"
						+ "</Customer>");
				env.setApiTemplate(apiName1, templateDoc.getDocument());
			

				outDoc1 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName1,inputDoc.getDocument());
	
			if (outDoc1 != null){
			YFCElement Node2 = YFCDocument.getDocumentFor(outDoc1).getDocumentElement().getChildElement("BuyerOrganization");
			locale = Node2.getAttribute("LocaleCode");
			}
			
			final YFCDocument localeInput = YFCDocument.getDocumentFor("<Locale LocaleCode=\"\" />");
			localeInput.getDocumentElement().setAttribute("LocaleCode",locale);
			env.setApiTemplate(apiName2,"");


				outDoc2 = YIFClientFactory.getInstance().getLocalApi().invoke(env,apiName2,localeInput.getDocument());

			if (outDoc1 != null){
			LocaleDesc=YFCDocument.getDocumentFor(outDoc2).getDocumentElement().getChildElement("Locale").getAttribute("Timezone");
			YFCDocument.getDocumentFor(outDoc1).getDocumentElement().setAttribute("LocaleDescription", LocaleDesc);
			}
			
			outputDoc = YFCDocument
					.getDocumentFor("<Output>"
							+ "<GetCustomerInfoWithAddress TimeZoneOffsetFromGMT=\"\" InvoicePrompt=\"\" " +
							"InvoicePromptCustomerDisplayableFlag=\"\" PTExceptionFlag=\"\" " +
							"CustomerName=\"\" InternalCustomerFlag=\"\" CreditHoldFlag=\"\" ServiceHoldFlag=\"\"" +
							" AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" AddressCity=\"\" " +
							"AddressState=\"\" AddressZipCode=\"\" AddressCountry=\"\" CustomerNumber=\"\" CustomerSiteId=\"\" />							" 
							+ "</Output>");
			
		//	YFCElement getCustInfo = YFCDocument.getDocumentFor(outDoc1).getDocumentElement().getChildElement("GetCustomerInfoWithAddress");
			//YFCElement Extn = getCustInfo.getChildElement("Extn");
		//	YFCElement Buyer = getCustInfo.getChildElement("BuyerOrganization");
			//YFCElement Address = getCustInfo.getChildElement("BillingPersonInfo");
			
			YFCElement Extn=YFCDocument.getDocumentFor(outDoc1).getDocumentElement().getChildElement("Extn");
			YFCElement Buyer=YFCDocument.getDocumentFor(outDoc1).getDocumentElement().getChildElement("BuyerOrganization");
			YFCElement Address = Buyer.getChildElement("BillingPersonInfo");
			String Flag = Buyer.getChildElement("Extn").getAttribute("CustomerSiteStatus");
			if( Extn!=null && Buyer!=null && Address != null){
				if(Flag.equalsIgnoreCase("A")){			
				
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("TimeZoneOffsetFromGMT", LocaleDesc);
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("InvoicePrompt", Extn.getAttribute("SpecialCustInstr"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("InvoicePromptCustomerDisplayableFlag", Extn.getAttribute("SpecialInstrVisible"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("PTExceptionFlag", Extn.getAttribute("IsTransExceptionAllowed"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("CustomerName", Buyer.getAttribute("OrganizationName"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("InternalCustomerFlag", Extn.getAttribute("InternalCustomerFlag"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("CreditHoldFlag", Extn.getAttribute("IsCreditHold"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("ServiceHoldFlag", Extn.getAttribute("IsServiceHold"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressLine1", Address.getAttribute("AddressLine1"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressLine2", Address.getAttribute("AddressLine2"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressLine3", Address.getAttribute("AddressLine3"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressLine4", Address.getAttribute("AddressLine4"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressLine5", Address.getAttribute("AddressLine5"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressCity", Address.getAttribute("City"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressState", Address.getAttribute("State"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressZipCode", Address.getAttribute("ZipCode"));
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("AddressCountry", Address.getAttribute("Country"));
				
				String[] str_array = outDoc1.getDocumentElement().getAttribute("CustomerID").split("-");
				
				 String CID = str_array[0]; 
				String  sCID = str_array[1];
				//String CID = outDoc1.getDocumentElement().getAttribute("CustomerID");
				//sCID = CID.substring(CID.length()-2);
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("CustomerNumber", CID);
				outputDoc.getDocumentElement().getChildElement("GetCustomerInfoWithAddress").setAttribute("CustomerSiteId", sCID);

			}
		}	
		
		
		
		}catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (YFSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch(Exception e)
		 {
			throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		 }
		return outputDoc.getDocument();
	}
}
