package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ServOrderSearchShipmentStatus implements YIFCustomApi {

	YFCDocument inYFCDoc = null;
	YFCElement inEle = null;
	int NodeLength = 0;
	YFCDocument inputOrder = null;
	YFCDocument inputShipmentList = null;
	YFCDocument inputShipmentDetails = null;
	YFCDocument inputOrderDetails = null;
	
	Document outputShipmentList = null;
	Document outputShipmentDetails = null;
	Document outputOrderDetails = null;
	Document docGetStatusListOP = null;
	YFCDocument finalOutput = null;
	YFCDocument templateOrder = null;
	YFCDocument templateShipmentList = null;
	YFCDocument templateShipmentDetails = null;
	YFCDocument templateOrderDetails = null;
	YFCDocument outDoc = null;

	
	String serviceType = "";
	String serviceTypeDesc = "";
	String itemId = "";
	String status = "";
	String statusDesc = "";
	String linestatusDerived = null;
	Double OrderedQty = null;
	Double ShippedQty = null;
	Double ReceivedQty = null;
	Double TotalReceiptDueQuantity = null;
	String dueDateStart = "";
	String dueDateEnd = "";
	String systemType = "";
	String dueDateFromCust = "";
	String shipDateStart = "";
	String shipDateEnd = "";

	String requestedShipdate = "";
	String requestedDeliverydate = "";
	String shipmentType = "";

	String calenderDate = "";
	String reqDelDateStart = "";	
	String reqDelDateEnd = "";
	String shippingDate = "";

	
	String shipNode = "";
	String orderedQty = "";
	String primeLineNo = "";
	String itemShortDesc = "";
	String shortDescription = "";
	String receivingNode = "";

	private static YFCLogCategory logger = YFCLogCategory.instance(ServOrderSearchShipmentStatus.class);

	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub
	}
	public Document servOrderSearchShipmentStatus(YFSEnvironment env,Document inDoc) {

		/*inDoc template
		 * -------------
		<Input>
			<ServOrderSearchShipmentStatus OrderNumber="" PartyNumber="" PartyAddressNumber="" SystemSerialNumber="" WayBillNumber="" EntitlementNumber="" PartNumber="" CustomerPoNo="" OrderTypeCode="" OrderLineStatusCdDescn="" CalendarDateStart="" CalendarDateEnd="" ShipDateStart="" ShipDateEnd="" DueDateStart="" DueDateEnd="" DueBackFromCustomerStart="" DueBackFromCustomerEnd="" OrderDateStart="" OrderDateStart="" SysProgram="" ServiceTypeCode=""/>
		</Input>
		*/
			try {
				YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
				YFCElement inEle =inYFCDoc.getDocumentElement();
				
				YFCElement Node = inEle.getChildElement("ServOrderSearchShipmentStatus");
				finalOutput = YFCDocument.createDocument("Output");

				try {

					inputOrder = YFCDocument.getDocumentFor("<OrderLine CustomerPONo=\"\" Status=\"\" FromReqDeliveryDate=\"\" ToReqDeliveryDate=\"\" ReqDeliveryDateQryType=\"\" FromReqShipDate=\"\" ToReqShipDate=\"\" ReqShipDateQryType=\"\" >"
									+ "<Order OrderNo=\"\" OrderHeaderKey=\"\" EnterpriseCode=\"\" ReceivingNode=\"\" OrderType=\"\" FromOrderDate=\"\" ToOrderDate=\"\" OrderDateQryType=\"\" />"
									+ "<Item ItemID=\"\" /> "
									+ "<Extn WarrantyNo=\"\" SystemSerialNo=\"\" SysProgram=\"\" ServiceType=\"\" />"
									+ "</OrderLine>");
					YFCElement orderElement = inputOrder.getDocumentElement();

					if (Node != null) {

						String orderNo="";
						String PartyAddressNumber = "";
						String SystemSerialNumber = "";
						String PartyNumber = "";
						
						String OrderLineStatusCDDescn = "";
						String Status1="";
						

						if (Node.hasAttribute("OrderNumber")) {
							orderNo = Node.getAttribute("OrderNumber");
							orderElement.getChildElement("Order").setAttribute("OrderNo", orderNo);
						}
						
						//review
						if (Node.hasAttribute("PartyNumber") && Node.hasAttribute("PartyAddressNumber")) {
							PartyNumber = Node.getAttribute("PartyNumber");
							PartyAddressNumber = Node.getAttribute("PartyAddressNumber");
							if(!(PartyNumber.isEmpty() || PartyAddressNumber.isEmpty())){
								orderElement.getChildElement("Order").setAttribute("ReceivingNode",PartyNumber+"-"+PartyAddressNumber);
							}
						}
						if (Node.hasAttribute("SystemSerialNumber")) {
							SystemSerialNumber = Node.getAttribute("SystemSerialNumber");
							orderElement.getChildElement("Extn").setAttribute("SystemSerialNo",	SystemSerialNumber);					}
						
						if (Node.hasAttribute("EntitlementNumber")) {
							orderElement.getChildElement("Extn").setAttribute("WarrantyNo",Node.getAttribute("EntitlementNumber"));
						}
						if (Node.hasAttribute("PartNumber")) {
							orderElement.getChildElement("Item").setAttribute("ItemID", Node.getAttribute("PartNumber"));
						}
						if (Node.hasAttribute("CustomerPoNo")) {
							orderElement.setAttribute("CustomerPONo",Node.getAttribute("CustomerPoNo"));
						}
						
						if (Node.hasAttribute("OrderTypeCode")) {
							orderElement.getChildElement("Order").setAttribute("OrderType",Node.getAttribute("OrderTypeCode"));
						}
						
						if (Node.hasAttribute("OrderLineStatusCdDescn")) {
							OrderLineStatusCDDescn = Node.getAttribute("OrderLineStatusCdDescn");
							if (OrderLineStatusCDDescn != null 	&& !(OrderLineStatusCDDescn.isEmpty())) {
								
								logger.debug("status from customer: " + OrderLineStatusCDDescn);
		
								YFCDocument docInputStatusList = YFCDocument.getDocumentFor("<Status ProcessTypeKey=\"ORDER_FULFILLMENT\" Description=\"\"/>");
								YFCElement eleStatus = docInputStatusList.getDocumentElement();
								eleStatus.setAttribute("Description", OrderLineStatusCDDescn);
		
								YFCDocument docGetStatusListTemplate = YFCDocument.getDocumentFor("<StatusList> <Status Status=\"\"/> </StatusList>");
								env.setApiTemplate("getStatusList",docGetStatusListTemplate.getDocument());
		
								logger.debug("getStatusList input: "+ docInputStatusList.toString());
								logger.debug("########## Calling getStatusList API ###########");
								docGetStatusListOP = YIFClientFactory.getInstance().getApi().invoke(env, "getStatusList", docInputStatusList.getDocument());
								logger.debug("getStatusList output: "+ docGetStatusListOP.toString());
								if (null != docGetStatusListOP) {
									YFCDocument docGetStatusList = YFCDocument.getDocumentFor(docGetStatusListOP);
									logger.debug("getStatusList output"+ docGetStatusList);
									YFCElement eleStatusList = docGetStatusList.getDocumentElement();
									if (null != eleStatusList) {
										YFCElement eleStatusOP = eleStatusList.getChildElement("Status");
										if (null != eleStatusOP) {
											Status1 = eleStatusOP.getAttribute("Status");
										}
									}		
									logger.debug("Status from api " + Status1);
								}				
								env.clearApiTemplate("getStatusList");
							}
						}
						if (Status1 != null)
							orderElement.setAttribute("Status", Status1);
						else
							orderElement.setAttribute("Status", "");
						
						}
						
						/* CalendarDate shifted to getShipmentListForOrder method
						 * ------------------------------------------------------
						 * ShipDate shifted to getShipmentListForOrder method
						 * ------------------------------------------------------
						 * DueDate shifted to getShipmentListForOrder method
						 * ------------------------------------------------------
						if (Node.hasAttribute("DueDateStart") && Node.hasAttribute("DueDateEnd")) {
							dueDateStart = Node.getAttribute("DueDateStart");
							dueDateEnd = Node.getAttribute("DueDateEnd");
							if(!dueDateStart.isEmpty()){
								reqDelDateStart = modifyDateLayout(dueDateStart);}
							if(!dueDateEnd.isEmpty()){
								reqDelDateEnd = modifyDateLayout(dueDateEnd);}
							orderElement.setAttribute("FromReqDeliveryDate", reqDelDateStart);
							orderElement.setAttribute("ToReqDeliveryDate", reqDelDateStart);
							orderElement.setAttribute("ReqDeliveryDateQryType", "BETWEEN");
						}*/
						String dueBackFromCustomerStart = "";
						String dueBackFromCustomerEnd = "";
						if (Node.hasAttribute("DueBackFromCustomerStart") && Node.hasAttribute("DueBackFromCustomerEnd")) {
							dueBackFromCustomerStart = Node.getAttribute("DueBackFromCustomerStart");
							dueBackFromCustomerEnd = Node.getAttribute("DueBackFromCustomerEnd");
							if(!(dueBackFromCustomerStart.isEmpty() || dueBackFromCustomerEnd.isEmpty())){
								dueBackFromCustomerStart = modifyDateLayout(dueBackFromCustomerStart);
								dueBackFromCustomerEnd = modifyDateLayout(dueBackFromCustomerEnd);
								orderElement.setAttribute("FromReqDeliveryDate", dueBackFromCustomerStart);
								orderElement.setAttribute("ToReqDeliveryDate", dueBackFromCustomerEnd);
								orderElement.setAttribute("ReqDeliveryDateQryType", "BETWEEN");
							}
						}
						String orderDateStart = "";
						String orderDateEnd = "";
						if (Node.hasAttribute("OrderDateStart") && Node.hasAttribute("OrderDateEnd")) {
							orderDateStart = Node.getAttribute("OrderDateStart");
							orderDateEnd = Node.getAttribute("OrderDateEnd");
							if(!(orderDateStart.isEmpty() || orderDateEnd.isEmpty())){
								orderDateStart = modifyDateLayout(orderDateStart);
								orderDateEnd = modifyDateLayout(orderDateEnd);
								orderElement.getChildElement("Order").setAttribute("FromOrderDate", orderDateStart);						
								orderElement.getChildElement("Order").setAttribute("ToOrderDate", orderDateEnd);
								orderElement.getChildElement("Order").setAttribute("OrderDateQryType", "BETWEEN");
							}
						}
						String dueDateStart = "";
						String dueDateEnd = "";
						if (Node.hasAttribute("DueDateStart") && Node.hasAttribute("DueDateEnd")) {
							dueDateStart = Node.getAttribute("DueDateStart");
							dueDateEnd = Node.getAttribute("DueDateEnd");
							String dueDelDateStart="";
							String dueDelDateEnd="";
							if(!(dueDateStart.isEmpty() || dueDateEnd.isEmpty())){
								dueDelDateStart = modifyDateLayout(dueDateStart);
								dueDelDateEnd = modifyDateLayout(dueDateEnd);
								orderElement.setAttribute("FromReqShipDate", dueDelDateStart);
								orderElement.setAttribute("ToReqShipDate", dueDelDateEnd);
								orderElement.setAttribute("ReqShipDateQryType", "BETWEEN");
							}
						}
						if (Node.hasAttribute("SysProgram")) {
							orderElement.getChildElement("Extn").setAttribute("SysProgram", Node.getAttribute("SysProgram"));
						}
						if (Node.hasAttribute("ServiceTypeCode")) {
							orderElement.getChildElement("Extn").setAttribute("ServiceType",Node.getAttribute("ServiceTypeCode"));
						}
						logger.debug("Input orderLineList XML before checking for Shipments ----"+ inputOrder.toString());
							
						/* Fetching output from getOrderDetailsFromShipment method
						 * Iterate through each OrderHeaderKey in the output
						 */
						String sTrackingNo = "";
						String sCalendarDate = "";
						String sShipDate = "";
						String sDueDate = "";
						if(Node.hasAttribute("WayBillNumber")){ sTrackingNo = Node.getAttribute("WayBillNumber"); }
						if(Node.hasAttribute("CalendarDateStart")){ sCalendarDate = Node.getAttribute("CalendarDateStart"); }
						if(Node.hasAttribute("ShipDateStart")){ sShipDate = Node.getAttribute("ShipDateStart"); }
						if(Node.hasAttribute("DueDateStart")){ sDueDate = Node.getAttribute("DueDateStart"); }
						
						Document orderListFromShipment = null;
						if(!(sTrackingNo.isEmpty() && sCalendarDate.isEmpty() && sShipDate.isEmpty() && sDueDate.isEmpty())){
							logger.debug("******************** Fetching Shipment List ************************");
							orderListFromShipment = getOrderDetailsfromShipment(env, inDoc);
						}
						if(orderListFromShipment != null){
							YFCDocument orderListDoc = YFCDocument.getDocumentFor(orderListFromShipment);
							YFCElement orderListEle = orderListDoc.getDocumentElement();
							
							YFCNodeList<YFCElement> totalOrderList = orderListEle.getElementsByTagName("Shipment");
							if (totalOrderList.getLength() > 0) {
								for(int i=0; i<totalOrderList.getLength();i++){
									YFCElement shipElem = totalOrderList.item(i);
									String orderHKey=shipElem.getAttribute("OrderHeaderKey");
									if(!orderHKey.isEmpty()){
										orderElement.getChildElement("Order").setAttribute("OrderHeaderKey", orderHKey);
									}
									fetchAndPrepareOrderDetails(env, finalOutput, inputOrder);
								}
							}
						}
						else{
							fetchAndPrepareOrderDetails(env, finalOutput, inputOrder);	
						}
						logger.debug("Final Output of service =============== "+finalOutput.getString());
				} catch (YFSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (YIFClientCreationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO: handle exception
	 				e.printStackTrace();
				}
				if (finalOutput != null)
					return finalOutput.getDocument();
				else
					return null;
			} catch (Exception ex) {
				throw new YFSException("Exception Thrown", "E-01",
						"Exception Thrown");
			}
		}
	
	private Document getOrderDetailsfromShipment(YFSEnvironment env, Document shipDoc){
	
		Document getShpmtListForOrder = null; //declaring OutputDoc
		String getShipmentListApi = "getShipmentList";
		YFCDocument shipInDoc = YFCDocument.getDocumentFor(shipDoc);
		YFCElement shipInEle = shipInDoc.getDocumentElement();
		YFCElement shipNode = shipInEle.getChildElement("ServOrderSearchShipmentStatus");
		
		YFCDocument shipOutDoc = YFCDocument.getDocumentFor("<Shipment BolNo=\"\" ActualShipmentDateQryType=\"\" FromActualShipmentDate=\"\" ToActualShipmentDate=\"\" />");
	    YFCElement shipOutEle = shipOutDoc.getDocumentElement();
		
		YFCDocument templateOutDoc = YFCDocument.getDocumentFor("<Shipments><Shipment OrderHeaderKey=\"\" /></Shipments>");
		env.setApiTemplate(getShipmentListApi, templateOutDoc.getDocument());
		
		if (shipNode.hasAttribute("WayBillNumber")) {
			String shipWaybill = shipNode.getAttribute("WayBillNumber");
			shipOutEle.setAttribute("BolNo", shipWaybill);
		}
		try{
			String shipDelDateStart="";
			String shipDelDateEnd="";
			
			if (shipNode.hasAttribute("CalendarDateStart") && shipNode.hasAttribute("CalendarDateEnd")) {
				String shipDateStart = shipNode.getAttribute("CalendarDateStart");
				String shipDateEnd = shipNode.getAttribute("CalendarDateEnd");
				
				if(!(shipDateStart.isEmpty() || shipDateEnd.isEmpty())){
					shipDelDateStart = modifyDateLayout(shipDateStart);
					shipDelDateEnd = modifyDateLayout(shipDateEnd);
					shipOutEle.setAttribute("FromActualShipmentDate", shipDelDateStart);
					shipOutEle.setAttribute("ToActualShipmentDate", shipDelDateEnd);
					shipOutEle.setAttribute("ActualShipmentDateQryType", "BETWEEN");
				}
			}
			/* CalendarDate attribute is mapped to ActualShipmentDate 
			 * Ditto for ShipDate attribute
			 * Preference given to ShipDate variables. So if entered ShipDate attributes will be used to obtained Shipments.
			 */
			if (shipNode.hasAttribute("ShipDateStart") && shipNode.hasAttribute("ShipDateEnd")) {
				String shipDateStart = shipNode.getAttribute("ShipDateStart");
				String shipDateEnd = shipNode.getAttribute("ShipDateEnd");
			
				if(!(shipDateStart.isEmpty() || shipDateEnd.isEmpty())){
					shipDelDateStart = modifyDateLayout(shipDateStart);
					shipDelDateEnd = modifyDateLayout(shipDateEnd);
					shipOutEle.setAttribute("FromActualShipmentDate", shipDelDateStart);
					shipOutEle.setAttribute("ToActualShipmentDate", shipDelDateEnd);
					shipOutEle.setAttribute("ActualShipmentDateQryType", "BETWEEN");
				}
			}
			
		
			logger.debug("Input to getOrderListForShipment method ============ "+shipOutDoc.toString());
			
			getShpmtListForOrder = YIFClientFactory.getInstance().getApi().invoke(env, getShipmentListApi, shipOutDoc.getDocument());
			logger.debug("Output to getOrderListForShipment method ============ "+getShpmtListForOrder.toString());
		}catch(Exception e)
			{e.printStackTrace();
			logger.debug("$$$$$$$$$ ERROR in DateFormat $$$$$$$$$$");}
		return getShpmtListForOrder;
	}
	private void fetchAndPrepareOrderDetails(YFSEnvironment env, YFCDocument finalOutput, YFCDocument orderDoc){	
		try{
			Document outputOrder = null;
			String getOrderListApi = "getOrderLineList";
			YFCDocument templateOrder = YFCDocument
					.getDocumentFor("<OrderLineList>"
							+ "<OrderLine OrderHeaderKey=\"\" OrderLineKey=\"\" PrimeLineNo=\"\" SubLineNo=\"\" Status=\"\" MinLineStatus=\"\" OrderedQty=\"\" ReqDeliveryDate=\"\" ReqShipDate=\"\" SCAC=\"\" SerialNo=\"\" CustomerPONo=\"\" >"
							+ "<Order OrderNo=\"\" OrderDate=\"\" OrderType=\"\" >"
								+ "<Extn ContactAttnTo=\"\" ContactName=\"\" />"
							+ "</Order>"
							+ "<Item ItemShortDesc=\"\" ItemID=\"\" />"
							+ "<Notes> <Note NoteText=\"\" /> </Notes>"
							+ "<Extn RecvdQty=\"\" ServiceType=\"\" SystemType=\"\" SystemSerialNo=\"\" SysProgram=\"\" MktStatus=\"\" OEMSerialNo=\"\" WarrantyNo=\"\" />"
							+ "</OrderLine>" 
							+ "</OrderLineList>");	
			
			env.setApiTemplate(getOrderListApi, templateOrder.getDocument());
		    Document orderListInputDoc = orderDoc.getDocument();
			logger.debug("Input for getOrderLineList API ========= "+ orderListInputDoc.toString());
			logger.debug("######### Calling getOrderLineList API #########");
			outputOrder = YIFClientFactory.getInstance().getApi().invoke(env, getOrderListApi, orderListInputDoc);
			logger.debug("Output of getOrderLineList API ========= "+ outputOrder.toString());
			env.clearApiTemplate("getOrderLineList");
			YFCElement outputOrderEle = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();

				if (!YFCElement.isVoid(outputOrderEle)) {
				
				YFCNodeList<YFCElement> outputOrderList = outputOrderEle.getElementsByTagName("OrderLine");
				if (outputOrderList.getLength() > 0) {
					//YFCIterable<YFCElement> allOrderline = outputOrderEle.getChildren();
					for (int i=0; i<outputOrderList.getLength(); i++){
						String systemSerialNo = "";
						String scac = "";
						String partSerialNumber = "";
						String agreementNumber = "";
	
						String program = "";
						String systemDown = "";
						String orderNoPo = "";
						String orderAttentionTo = "";
						String orderContact = "";
						String doaQty = "";
						String doaReceiptQty = "";
						String quoteConversionInd = "";
						String orderLineTypeCode = "";
						String OrderLineTypeDesc = "";
						String oemSerialNo = "";
						String plannerNotes = "";
						String orderNo = "";
						String orderLineNo = "";
						String orderSublineNo = "";
						String orderDate = "";
						
						String reqDelDate="";
						String serviceType = "";
						String waybill = "";
						String status = "";
						String statusDesc = "";
						String orderHeaderKey="";
						String PartNumber="";
						String ShipmentKey="";
						String OrderTypeCode = "";
						String serviceTypeDescn = "";
						
						String expDelDate = "";
						String shipDate = "";
						String linestatusDerived = null;
						
						Double extnRcvdQty = null;
						Double actualQuantity = null;
						Double receiptDueQty = null;
	
						YFCElement orderLineEle = outputOrderList.item(i);
					
						orderHeaderKey =orderLineEle.getAttribute("OrderHeaderKey");
						orderLineNo = orderLineEle.getAttribute("PrimeLineNo");
						orderSublineNo = orderLineEle.getAttribute("SubLineNo");
						orderedQty = orderLineEle.getAttribute("OrderedQty");
						dueDateFromCust = orderLineEle.getAttribute("ReqDeliveryDate");
						//dueDateFromCust = orderLineEle.getAttribute("ReqShipDate");
						scac = orderLineEle.getAttribute("SCAC");
						partSerialNumber = orderLineEle.getAttribute("SerialNo");
						orderNoPo = orderLineEle.getAttribute("CustomerPONo");
						doaQty = orderLineEle.getAttribute("OrderedQty");
						status = orderLineEle.getAttribute("Status");
						
						String reqDeliveryDate ="";
						if(orderLineEle.hasAttribute("ReqDeliveryDate")){
							reqDeliveryDate = orderLineEle.getAttribute("ReqDeliveryDate");
							reqDelDate = modifyDateTerLayout(reqDeliveryDate);}
						
						String orderShipDate="";
						if(orderLineEle.getChildElement("Order").hasAttribute("OrderDate")){
							orderShipDate = orderLineEle.getChildElement("Order").getAttribute("OrderDate");
							orderDate = modifyDateTerLayout(orderShipDate);}
						statusDesc =orderLineEle.getAttribute("MinLineStatus");
						linestatusDerived = orderLineEle.getAttribute("Status");
													
						YFCElement eleOrderLn = orderLineEle.getChildElement("Order");
						if(eleOrderLn !=null){
							orderNo = eleOrderLn.getAttribute("OrderNo");
							if(eleOrderLn.hasAttribute("OrderDate")){
								orderDate = eleOrderLn.getAttribute("OrderDate");
								orderDate = modifyDateTerLayout(orderDate);
								//orderDate value in Order Level is overriding the value set at Order Line Level, if date is available
								OrderTypeCode = eleOrderLn.getAttribute("OrderType");
								program = eleOrderLn.getAttribute("OrderType");
								
								YFCElement eleExtnLn = eleOrderLn.getChildElement("Extn");
								if(eleExtnLn !=null){
									orderAttentionTo = eleExtnLn.getAttribute("ContactAttnTo");
									orderContact = eleExtnLn.getAttribute("ContactName");
									//quoteConversionInd = eleExtnLn.getAttribute("OrderType");
								}									
							}
						}
						orderLineTypeCode = "ORDER_TYPE";
						if(OrderTypeCode !=null){
						       Document outCommonDoc = null;
						       YFCDocument inCommonDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" DocumentType=\"\"/>");
						       inCommonDoc.getDocumentElement().setAttribute("CodeValue", OrderTypeCode);
						       inCommonDoc.getDocumentElement().setAttribute("CodeType", orderLineTypeCode);
						       inCommonDoc.getDocumentElement().setAttribute("DocumentType", "0001");
						       outCommonDoc = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getCommonCodeList", inCommonDoc.getDocument());
						       
						       if (outCommonDoc !=null){
							       YFCElement OutputCommList = YFCDocument.getDocumentFor(outCommonDoc).getDocumentElement();
							       if (OutputCommList != null){
								         YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
								         if(eleComm !=null){
									          OrderLineTypeDesc= eleComm.getAttribute("CodeLongDescription");
								         }
							        }
						       }		      
						}
						
						YFCElement eleItemLn = orderLineEle.getChildElement("Item");
						if(eleItemLn !=null){
							serviceTypeDesc = eleItemLn.getAttribute("ItemShortDesc");
							PartNumber = eleItemLn.getAttribute("ItemID");	
						}	
						
						YFCElement eleNotesLn = orderLineEle.getChildElement("Notes");
						if (eleNotesLn.getChildElement("Note") != null)
							plannerNotes = eleNotesLn.getChildElement("Note").getAttribute("NoteText");
						else
							plannerNotes = "";
						
						YFCElement eleExtnOrderLn = orderLineEle.getChildElement("Extn");	
						if(eleExtnOrderLn !=null){
							serviceType = eleExtnOrderLn.getAttribute("ServiceType");
							systemType = eleExtnOrderLn.getAttribute("SystemType");
							systemSerialNo = eleExtnOrderLn.getAttribute("SystemSerialNo");
							agreementNumber = eleExtnOrderLn.getAttribute("WarrantyNo");
							systemDown = eleExtnOrderLn.getAttribute("MktStatus");
							oemSerialNo =eleExtnOrderLn.getAttribute("OEMSerialNo");
							extnRcvdQty = Double.parseDouble(eleExtnOrderLn.getAttribute("RecvdQty"));
						}
						if(!serviceType.isEmpty()){
						  Document outDoc =null;	  
				          YFCDocument inputCommonCodeDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" />");
				          inputCommonCodeDoc.getDocumentElement().setAttribute("CodeValue", serviceType);
				          inputCommonCodeDoc.getDocumentElement().setAttribute("CodeType", "Service_Type");
				          outDoc = YIFClientFactory.getInstance().getLocalApi().invoke(env, "getCommonCodeList", inputCommonCodeDoc.getDocument());
				          
				          if (outDoc !=null){
				           YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc).getDocumentElement();
					           if (OutputCommList != null){
					            YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
						            if(eleComm !=null){						              
						             serviceTypeDescn= eleComm.getAttribute("CodeLongDescription");
						            }
					           }
				          }
					    }
						
				
						String getShipListApi = "getShipmentList";
						inputShipmentList = YFCDocument.getDocumentFor("<Shipment OrderHeaderKey=\"\"/>");
						inputShipmentList.getDocumentElement().setAttribute("OrderHeaderKey", orderHeaderKey);
						templateShipmentList = YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey=\"\" /></Shipments>");
						env.setApiTemplate(getShipListApi,templateShipmentList.getDocument());
	
						outputShipmentList = YIFClientFactory.getInstance().getLocalApi().invoke(env, getShipListApi,
										inputShipmentList.getDocument());
						YFCElement outputShipmentListEle = YFCDocument.getDocumentFor(outputShipmentList).getDocumentElement();
						if (!YFCElement.isVoid(outputShipmentListEle)) {							
							YFCElement outputShipmentEle = outputShipmentListEle.getChildElement("Shipment");
							//review
							if (!YFCElement.isVoid(outputShipmentEle)) {
								ShipmentKey = outputShipmentListEle.getChildElement("Shipment").getAttribute("ShipmentKey");
								String getShipmentDtlsApi = "getShipmentDetails";
								inputShipmentDetails = YFCDocument.getDocumentFor("<Shipment ShipmentKey=\"\"/>");
								templateShipmentDetails = YFCDocument.getDocumentFor("<Shipment BolNo=\"\" ExpectedDeliveryDate=\"\" ActualShipmentDate=\"\" Carrier=\"\" ShipmentType=\"\" Status=\"\" ShipDate=\"\">"
										+ "<ShipmentLines>"
										+ "<ShipmentLine ShortageQty=\"\" Quantity=\"\"/>"
										+ "</ShipmentLines>" + "</Shipment>");
								logger.debug("Input to getShipment details API : "+inputShipmentDetails.toString());
								logger.debug("######### Calling getShipmentDetails API #########");
							
								env.setApiTemplate(getShipmentDtlsApi,templateShipmentDetails.getDocument());
								inputShipmentDetails.getDocumentElement().setAttribute("ShipmentKey", ShipmentKey);
								outputShipmentDetails = YIFClientFactory.getInstance().getLocalApi().invoke(env, getShipmentDtlsApi,
										inputShipmentDetails.getDocument());
								logger.debug("Output of getShipment details API : "+outputShipmentDetails.toString());
								YFCElement outputShipmentDetailEle = YFCDocument.getDocumentFor(outputShipmentDetails).getDocumentElement();
								if (!YFCElement.isVoid(outputShipmentDetailEle)) {
									YFCElement outShipmentEle = outputShipmentDetailEle.getChildElement("Shipment");
									if (!YFCElement.isVoid(outShipmentEle)
									&& !outputShipmentDetailEle.getAttribute("Status").equals("9000")) {
										actualQuantity = Double.parseDouble(outputShipmentDetailEle.getChildElement("ShipmentLines")
												.getChildElement("ShipmentLine").getAttribute("Quantity"));
										//doaReceiptQty = outputShipmentDetailEle.getChildElement("ShipmentLines").getChildElement("ShipmentLine").getAttribute("Quantity");
										receiptDueQty = Double.parseDouble(outputShipmentDetailEle.getChildElement("ShipmentLines").getChildElement("ShipmentLine")
														.getAttribute("ShortageQty"));
										waybill = outputShipmentDetailEle.getAttribute("BolNo");										
									}
									shipmentType = outputShipmentDetailEle.getAttribute("ShipmentType");
									scac = outputShipmentDetailEle.getAttribute("Carrier");
									
									String expShipDelDate="";									
									if(outputShipmentDetailEle.hasAttribute("ExpectedDeliveryDate")){
										expShipDelDate = outputShipmentDetailEle.getAttribute("ExpectedDeliveryDate");
										expDelDate = modifyDateTerLayout(expShipDelDate);}
									
									String actShipDate="";
									if(outputShipmentDetailEle.hasAttribute("ActualShipmentDate")){
										actShipDate = outputShipmentDetailEle.getAttribute("ActualShipmentDate");
										shipDate = modifyDateTerLayout(actShipDate);}
								}
							}
						}
						
						
						/* Expected Service output template
						 * --------------------------------
						 <Output>
							<ServOrderSearchShipmentStatus AgreementNumber="" Carrier="" DOAReceiptQty="" DueDate="" DueDateFromCustomer="" 
							LineStatusDerived="" OemSerialNo="" OrderAttentionTo="" OrderContact="" OrderDate="" OrderLineNumber="" OrderLineStatusCode="" 
							OrderLineStatusCodeDescn="" OrderLineTypeCode="" OrderLineTypeDescn="" OrderNumber="" OrderSubLineNumber="" Order_Po_No="" 
							PartNumber="" PartSerialNumber="" PlannerNotes="" Program="" QtyDOA="" QtyReceived="" QtyTotalIssue="" QuantityOrdered="" 
							QuoteConversionInd="" ServiceType="" ServiceTypeDescn="" ShipDate="" ShipmentTypeCode="" SystemDown="" SystemSerialNumber="" 
							SystemType="" TotalReceiptDueQuantity="" WarrantyType="" WaybillNumber=""/>
						</Output>
						 */
			
						YFCElement output = finalOutput.getDocumentElement();
						YFCElement defReceiptDueBackToTer = output.createChild("ServOrderSearchShipmentStatus");
						defReceiptDueBackToTer.setAttribute("OrderNumber",orderNo);
						defReceiptDueBackToTer.setAttribute("OrderLineNumber",orderLineNo);
						defReceiptDueBackToTer.setAttribute("OrderSubLineNumber", orderSublineNo);
						defReceiptDueBackToTer.setAttribute("OrderDate",orderDate);
						defReceiptDueBackToTer.setAttribute("ServiceType",serviceType);
						defReceiptDueBackToTer.setAttribute("ServiceTypeDescn",	serviceTypeDescn);
						defReceiptDueBackToTer.setAttribute("PartNumber",PartNumber);
						defReceiptDueBackToTer.setAttribute("OrderLineStatusCode", statusDesc);
						defReceiptDueBackToTer.setAttribute("OrderLineStatusCodeDescn", status);
						defReceiptDueBackToTer.setAttribute("LineStatusDerived", linestatusDerived);
						defReceiptDueBackToTer.setAttribute("QuantityOrdered",orderedQty);
						defReceiptDueBackToTer.setAttribute("QtyTotalIssue",actualQuantity);
						defReceiptDueBackToTer.setAttribute("QtyReceived",extnRcvdQty);
						defReceiptDueBackToTer.setAttribute("TotalReceiptDueQuantity", receiptDueQty);
						defReceiptDueBackToTer.setAttribute("DueDate", expDelDate);
						defReceiptDueBackToTer.setAttribute("SystemType",systemType);
						defReceiptDueBackToTer.setAttribute("DueDateFromCustomer", reqDelDate);
						defReceiptDueBackToTer.setAttribute("ShipDate",shipDate);
						defReceiptDueBackToTer.setAttribute("SystemSerialNumber", systemSerialNo);
						defReceiptDueBackToTer.setAttribute("Carrier", scac);
						defReceiptDueBackToTer.setAttribute("Waybill", waybill);
						defReceiptDueBackToTer.setAttribute("PartSerialNumber",	partSerialNumber);
						defReceiptDueBackToTer.setAttribute("AgreementNumber",agreementNumber);
						defReceiptDueBackToTer.setAttribute("WarrantyType",OrderTypeCode);
						defReceiptDueBackToTer.setAttribute("Program", program);
						defReceiptDueBackToTer.setAttribute("SystemDown",systemDown);
						defReceiptDueBackToTer.setAttribute("PlannerNotes",	plannerNotes);
						defReceiptDueBackToTer.setAttribute("OrderPoNo",orderNoPo);	
						defReceiptDueBackToTer.setAttribute("OrderAttentionTo",	orderAttentionTo);
						defReceiptDueBackToTer.setAttribute("OrderContact",orderContact);
						if (OrderTypeCode.equals("DOA")) {
							defReceiptDueBackToTer.setAttribute("QtyDOA",doaQty);
						}
						if (OrderTypeCode.equals("DOA")) {
							defReceiptDueBackToTer.setAttribute("DOAReceiptQty", doaReceiptQty);
						}
						defReceiptDueBackToTer.setAttribute("QuoteConversionInd", quoteConversionInd);
						defReceiptDueBackToTer.setAttribute("OrderLineTypeCode", orderLineTypeCode);
						defReceiptDueBackToTer.setAttribute("OrderLineTypeDescn", OrderLineTypeDesc);
						defReceiptDueBackToTer.setAttribute("ShipmentTypeCode",	shipmentType);
						defReceiptDueBackToTer.setAttribute("OemSerialNo",oemSerialNo);
						}
					}
				}
		} catch (YFSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}		
	}
							
	
	private String modifyDateLayout(String inputDate) throws Exception{
	    String subString = inputDate.substring(0,10);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date newFormatedDate = formatter.parse(subString);
	    return dateFormat.format(newFormatedDate);
	}
	private static String modifyDateTerLayout(String inputDate) throws Exception{
	    String subString = inputDate.substring(0,10);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date newFormatedDate = formatter.parse(subString);
	    return dateFormat.format(newFormatedDate);
	}
}
