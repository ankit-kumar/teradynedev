package com.teradyne.boservices;

import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class ActiveSystemSerialNoServiceType {

	String sSystemSerialNo = null;
	String sExpDate = null;
	String apiName1 = "multiApi";
	java.text.DateFormat dateFormat = new java.text.SimpleDateFormat(
			"yyyy-MM-dd");

	Document outDoc1 = null;
	Document outDoc2 = null;
	YFCDocument outputDoc = null;
	YFCDocument finalOutDoc = null;
	String sMktStatus = null;
	String ExpFlag = null;
	String StatusDesc = null;
	//String sExpDate =null;
	int NodeLength = 0;

	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public Document activeSystemSerialNoServiceType(YFSEnvironment env,	Document inDoc) {
		Document outDoc = null;
		YIFApi yifc;
		try {
			yifc = YIFClientFactory.getInstance().getLocalApi();
			YFCDocument inputDoc = YFCDocument.getDocumentFor(inDoc);
			// YFCElement IterEle = null;
			System.out.println("Invoking API...MultiAPI");
			outDoc = yifc.invoke(env, apiName1, inputDoc.getDocument());

			YFCNodeList<YFCElement> NodeList = YFCDocument.getDocumentFor(outDoc).getElementsByTagName("OrderLine");
			for (int i = 0; i < NodeList.getLength(); i++) {
				System.out.println("Iteration: " + i);
				YFCNode cNode = NodeList.item(i);
				YFCElement element = (YFCElement) cNode;
				System.out.println(element.toString());
				YFCElement ordEle = element.getChildElement("Order");
				if(ordEle !=null){
					YFCElement extEle=ordEle.getChildElement("Extn");
						if(extEle !=null){
							
							sExpDate = extEle.getAttribute("ExpirationDate");
						}
				}
				
				
				
				
				System.out.println("sExpDate: " + sExpDate);
				if(sExpDate !=null){
				Date currentDate = java.util.Calendar.getInstance().getTime();
				Date curDate = dateFormat.parse(dateFormat.format(currentDate));;
				System.out.println("Current Date:" + currentDate) ;
				
				Date ExpDate = dateFormat.parse(sExpDate.substring(0, 10));

				System.out.println("ExpDate (after dateformat): " + ExpDate);
				
				System.out.println("Comparing: " + curDate.compareTo(ExpDate));
				
				if (!((curDate.compareTo(ExpDate) <= 0))) {
					System.out.println("Inside if condition");
					element.getParentNode().removeChild(cNode);
					i--;
				}
				}
				else{
					element.getParentNode().removeChild(cNode);
					i--;
				}
			}

			

		}

		catch (Exception exception) {
			throw new YFSException("Record not found for the given input","E-01", "Exception Thrown");
		}

		return outDoc;
	}
	

}
