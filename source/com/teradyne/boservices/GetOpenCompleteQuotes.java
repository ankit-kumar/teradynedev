package com.teradyne.boservices;

import java.rmi.RemoteException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class GetOpenCompleteQuotes implements YIFCustomApi {

	YFCDocument inputDoc = null;
	YFCDocument inputOrder=null;
	YFCDocument inputOrganization=null;
	YFCDocument inputCurrency=null;
	YFCDocument inputPrice=null;
	Document outputOrder=null;
	Document outputOrganization=null;
	Document outputCurrency=null;
	Document outputPrice=null;
	Document outDoc5=null;
	YFCDocument finalOutput=null;
	YFCDocument templateOrder=null;
	YFCDocument templateOrganization=null;
	YFCDocument templatePrice=null;
	YFCDocument outDoc=null;
	String apiName1="getOrderList";
	String apiName2="getOrganizationHierarchy";
	String apiName3="getCurrencyList";
	String apiName4="getPricelistLineList";
	String CustNo="";
	String ReceivingNode ="";
	String DraftFlag="";
	String CustSiteNo="";
	String OrderHold="";
	String OrderStatus="";
	String OrderNo="";
	String TotalAmount = "";
	String Currency = "";
	String DraftOrderFlag = "";
	String OrderType="";
	String OrgCode="";
	String ServiceTypeDescn="";
	Boolean Go=false;
	//Boolean Go2=false;
	Double OrderedQty=null;
	Double ReceivedQty=null;
	Double TotalQty=null;
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}
	public Document getOpenCompleteQuotes(YFSEnvironment env, Document inDoc){
		
		//YFCDocument inYFCDoc = YFCDocument.getDocumentFor(inDoc);
		YFCElement inEle =  YFCDocument.getDocumentFor(inDoc).getDocumentElement();
		YFCElement Node = inEle.getChildElement("GetOpenCompleteQuotes");
		finalOutput = YFCDocument.createDocument("Output");
		
		try {
			if(Node!=null){
				
				
					CustNo = inEle.getChildElement("GetOpenCompleteQuotes").getAttribute("ShipToCustomerNumber");
					CustSiteNo = inEle.getChildElement("GetOpenCompleteQuotes").getAttribute("ShipToCustomerSiteNumber");
					if (!CustNo.isEmpty() && !CustSiteNo.isEmpty())
					ReceivingNode= CustNo+"-"+CustSiteNo;
					else
					ReceivingNode=CustNo;
				
				
				
				inputOrder=YFCDocument.getDocumentFor("<Order ReceivingNodeQryType=\"FLIKE\" ReceivingNode=\"\" DocumentType=\"\" Status=\"\" />");
				inputOrder.getDocumentElement().setAttribute("ReceivingNode",ReceivingNode );
				inputOrder.getDocumentElement().setAttribute("DocumentType","0001" );
				inputOrder.getDocumentElement().setAttribute("Status","1100" );
				
				templateOrder=YFCDocument.getDocumentFor("<OrderList> <Order OrderNo=\"\" Status=\"\"  DraftOrderFlag=\"\" HoldFlag=\"\" EnterpriseCode=\"\" OrderType=\"\">" +
						"<PriceInfo Currency=\"\" TotalAmount=\"\" />" +
						"<OrderLines><OrderLine ReceivingNode=\"\" OrderedQty=\"\"  PrimeLineNo=\"\" > <Item ItemID=\"\" ItemShortDesc=\"\" />" +
						"<Extn ServiceType=\"\" SystemType=\"\" SystemSerialNo=\"\" LeadTime=\"\" />" +
						"</OrderLine></OrderLines>" +
						"</Order></OrderList>");
				
				env.setApiTemplate(apiName1, templateOrder.getDocument());
				
				outputOrder = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputOrder.getDocument());
				if(outputOrder != null){
					
					YFCElement outputOrderEle = YFCDocument.getDocumentFor(outputOrder).getDocumentElement();
					if(outputOrderEle.getChildElement("Order")!=null){
						
						//for loop
						YFCIterable<YFCElement> allOrder = outputOrderEle.getChildren();
						for(YFCElement orderEle : allOrder){
						
						OrderNo = orderEle.getAttribute("OrderNo");
						OrderType = orderEle.getAttribute("OrderType");
						OrgCode = orderEle.getAttribute("EnterpriseCode");
						OrderStatus = orderEle.getAttribute("Status");
						OrderHold = orderEle.getAttribute("HoldFlag");
						DraftOrderFlag = orderEle.getAttribute("DraftOrderFlag");
						Currency = orderEle.getChildElement("PriceInfo").getAttribute("Currency");
						TotalAmount = orderEle.getChildElement("PriceInfo").getAttribute("TotalAmount");
						
						if(OrderType.equals("Q") && !OrderStatus.equalsIgnoreCase("cancelled") && !OrderHold.equals("Y")){
							inputOrganization=YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\" />");
							inputOrganization.getDocumentElement().setAttribute("OrganizationCode", OrgCode);
								
							templateOrganization=YFCDocument.getDocumentFor("<Organization OrganizationCode=\"\"> <Extn CustomerSiteStatus=\"\"/></Organization>");
							env.setApiTemplate(apiName2, templateOrganization.getDocument());
							outputOrganization = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName2, inputOrganization.getDocument());
							if(YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Extn").hasAttribute("CustomerSiteStatus")){
								if(YFCDocument.getDocumentFor(outputOrganization).getDocumentElement().getChildElement("Extn").getAttribute("CustomerSiteStatus").equals("A")){
									Go=true;
								}
								else
									Go=false;
							
							
							}
						}
					
					
					if(Go){
						
					YFCNodeList<YFCElement> OrderNodes = orderEle.getChildElement("OrderLines").getElementsByTagName("OrderLine");
					int OrderLength= OrderNodes.getLength();
					for (int i=0;i<OrderLength;i++){

						YFCElement OrderNode = OrderNodes.item(i);
						String ServiceType=  "";
						String LeadTime=  "";
						String CurrencyDescription=  "";
						String UnitPriceRepair=  "";
						String UnitPriceExpediting = "";
						String ServiceTypeDescription =  "";
						String SystemType = "";
						String SystemSerialNo = "";
						String CutNo="";
						//String OrderNo= OrderNode.getAttribute("OrderNo");
					//	String DraftOrderFlag = OrderNode.getAttribute("DraftOrderFlag");
						CutNo = OrderNode.getAttribute("ReceivingNode");
						String OrderLineNumber = OrderNode.getAttribute("PrimeLineNo");
						String ItemID = OrderNode.getChildElement("Item").getAttribute("ItemID");
						String QuantityOrdered = OrderNode.getAttribute("OrderedQty");
						//ServiceTypeDescription = OrderNode.getChildElement("Item").getAttribute("ItemShortDesc");
					//	String Currency = OrderNode.getAttribute("Currency");
					//	String TotalPrice = OrderNode.getAttribute("TotalPrice");
						if(OrderNode.getChildElement("Extn")!=null){
							YFCElement Extn= OrderNode.getChildElement("Extn");
							
							ServiceType= Extn.getAttribute("ServiceType");
							if(ServiceType != null){
								String apiName1 = "getCommonCodeList";
								YFCDocument inputDoc = YFCDocument.getDocumentFor("<CommonCode CodeType=\"\" CodeValue=\"\" OrganizationCode=\"\"/>");
								inputDoc.getDocumentElement().setAttribute("CodeValue", ServiceType);
								inputDoc.getDocumentElement().setAttribute("CodeType", "Service_Type");
								inputDoc.getDocumentElement().setAttribute("OrganizationCode", OrgCode);
								outDoc5 = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName1, inputDoc.getDocument());
								env.clearApiTemplate(apiName1);
								
								if (outDoc5 !=null){
									YFCElement OutputCommList = YFCDocument.getDocumentFor(outDoc5).getDocumentElement();
									if (OutputCommList != null){
										YFCElement eleComm = OutputCommList.getChildElement("CommonCode");
										if(eleComm !=null){
											ServiceTypeDescription= eleComm.getAttribute("CodeShortDescription");
											System.out.println("Serv Desc@@@@@@@@@@ "+ServiceTypeDescn);
										}
									}
								}
							}
							
							SystemType = Extn.getAttribute("SystemType");
							SystemSerialNo = Extn.getAttribute("SystemSerialNo");
							LeadTime = Extn.getAttribute("LeadTime");
						}
						if (!Currency.isEmpty()){
						inputCurrency = YFCDocument.createDocument("Currency");
						inputCurrency.getDocumentElement().setAttribute("Currency", Currency);
						
						outputCurrency = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName3, inputCurrency.getDocument());
						if(YFCDocument.getDocumentFor(outputCurrency).getDocumentElement().getChildElement("Currency")!=null){
							CurrencyDescription = YFCDocument.getDocumentFor(outputCurrency).getDocumentElement().getChildElement("Currency").getAttribute("CurrencyDescription");
						}
						}
						
						
						inputPrice=YFCDocument.getDocumentFor("<PricelistLine ItemID=\"\"><Item OrganizationCode=\"\"/></PricelistLine>");
						inputPrice.getDocumentElement().setAttribute("ItemID", ItemID);
						inputPrice.getDocumentElement().getChildElement("Item").setAttribute("OrganizationCode",OrgCode);
						
						
						templatePrice=YFCDocument.getDocumentFor("<PricelistLineList><PricelistLine ListPrice=\"\" ><Extn ExpiditingPrice=\"\" /></PricelistLine></PricelistLineList>");
						env.setApiTemplate(apiName4, templatePrice.getDocument());
						outputPrice = YIFClientFactory.getInstance().getLocalApi().invoke(env, apiName4, inputPrice.getDocument());
						
						if(YFCDocument.getDocumentFor(outputPrice).getDocumentElement().getChildElement("PricelistLine").getAttribute("ListPrice")!=null){
							UnitPriceRepair = YFCDocument.getDocumentFor(outputPrice).getDocumentElement().getChildElement("PricelistLine").getAttribute("ListPrice");
						}
						if(YFCDocument.getDocumentFor(outputPrice).getDocumentElement().getChildElement("PricelistLine").getChildElement("Extn").getAttribute("ExpiditingPrice")!=null){
							UnitPriceExpediting = YFCDocument.getDocumentFor(outputPrice).getDocumentElement().getChildElement("PricelistLine").getChildElement("Extn").getAttribute("ExpiditingPrice");
						}

						YFCDocument Output = YFCDocument.createDocument("GetOpenCompleteQuotes");
						Output.getDocumentElement().setAttribute("OrderNumber", OrderNo);
						Output.getDocumentElement().setAttribute("OrderLineNumber", OrderLineNumber);
						Output.getDocumentElement().setAttribute("ServiceType", ServiceType);
						Output.getDocumentElement().setAttribute("ServiceTypeDescription", ServiceTypeDescription);
						Output.getDocumentElement().setAttribute("PartNumber", ItemID);
						Output.getDocumentElement().setAttribute("QuantityOrdered", QuantityOrdered);
						Output.getDocumentElement().setAttribute("SystemType", SystemType);
						Output.getDocumentElement().setAttribute("SystemSerialNumber", SystemSerialNo);
						Output.getDocumentElement().setAttribute("QuoteConversionIndicator", DraftOrderFlag);
						Output.getDocumentElement().setAttribute("LeadTime", LeadTime);
						Output.getDocumentElement().setAttribute("CurrencyCode", Currency);
						Output.getDocumentElement().setAttribute("CurrencyCodeDescription", CurrencyDescription);
						Output.getDocumentElement().setAttribute("TotalPrice", TotalAmount);
						Output.getDocumentElement().setAttribute("UnitPriceRepair", UnitPriceRepair);
						Output.getDocumentElement().setAttribute("UnitPriceExpediting", UnitPriceExpediting);
						String[] str_array1 = CutNo.split("-");

						String c = str_array1[0];
						String d = str_array1[1];
						Output.getDocumentElement().setAttribute("ShipToCustomerNumber", c);
						Output.getDocumentElement().setAttribute("ShipToCustomerSiteNumber", d);
						
						finalOutput.getDocumentElement().addXMLToNode(Output.getDocumentElement().toString());
					
					}
				}
			}
		}
	}
					
		}
	}catch (YFSException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (YIFClientCreationException e) {
			// TODO Auto-generated catch block
			throw new YFSException("Record not found for the given input", "E-01", "Exception Thrown");
		}
		return finalOutput.getDocument();
	}

}
