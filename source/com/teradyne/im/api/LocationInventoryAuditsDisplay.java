package com.teradyne.im.api;

import java.io.IOException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.CustomLogCategory;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;



public class LocationInventoryAuditsDisplay {
	private static CustomLogCategory logger = CustomLogCategory.getLogger(LocationInventoryAuditsDisplay.class.getName());
	private static final String strGetLocnInvAuditList = "<LocationInventoryAudits><LocationInventoryAudit AdjustmentType=\"\" "
			+ "AuditOperation=\"\" CaseId=\"\" Createts=\"\" Createuserid=\"\" EnterpriseCode=\"\" InventoryStatus=\"\" "
			+ "LocationId=\"\" LocnInventoryAuditKey=\"\" Modifyts=\"\" Modifyuserid=\"\" Node=\"\" OuterMostCaseId=\"\""
			+ " OuterMostPalletId=\"\" PalletId=\"\" SerialNo=\"\" SequenceNo=\"\" Quantity=\"\" ReasonCode=\"\">"
			+ "<TagDetail InventoryTagKey=\"\" />"
			+ "<InventoryItem ItemID=\"\" ProductClass=\"\" UnitOfMeasure=\"\" InventoryItemKey=\"\"><Item>"
			+ "<PrimaryInformation Description=\"\" ShortDescription=\"\"/></Item></InventoryItem>"
			+ "</LocationInventoryAudit></LocationInventoryAudits>";
	
	public Document listLocationInventoryAudits(YFSEnvironment env, Document inputDoc) throws YFCException,SAXException, IOException,Exception {
		
		YFCDocument getLocnInvAuditTemplate = YFCDocument.parse(strGetLocnInvAuditList);
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);
		logger.verbose("LocationInvAuditsDisplay: Input \n",inDoc);
		
		YFCDocument getLocnInvAuditsListOut = SterlingUtil.callAPI(env, Constants.GET_LOCATION_INVENTORY_AUDIT_LIST, 
				inDoc, getLocnInvAuditTemplate);
		
				
		Map<String,String> locnAuditsMap = new HashMap<String,String>();
		YFCNodeList<YFCElement> nlLocnInvAudits = getLocnInvAuditsListOut.getElementsByTagName(XMLConstants.LOCATION_INVENTORY_AUDIT);
		for(int i=0 ; i < nlLocnInvAudits.getLength(); i++){
			YFCElement eleLocnInvAudit = nlLocnInvAudits.item(i);
			String createtsStr = eleLocnInvAudit.getAttribute(XMLConstants.CREATETS);
			String qtyStr = eleLocnInvAudit.getAttribute(XMLConstants.QUANTITY);
			
			if(locnAuditsMap.containsKey(createtsStr)){
				double qty = Double.parseDouble(locnAuditsMap.get(createtsStr))+ Double.parseDouble(qtyStr);
				locnAuditsMap.put(createtsStr, (new BigDecimal(qty).setScale(2, BigDecimal.ROUND_HALF_UP)).toString());
				
			}else{
				locnAuditsMap.put(createtsStr, qtyStr);
			}
			
		}
		YFCDocument outDoc = YFCDocument.createDocument(XMLConstants.LOCATION_INVENTORY_AUDITS);
		for(int j=0; j < nlLocnInvAudits.getLength(); j++){
			YFCElement eleLocnInvAudit = nlLocnInvAudits.item(j);
			String createtsStr = eleLocnInvAudit.getAttribute(XMLConstants.CREATETS);
			String strSerialNo = eleLocnInvAudit.getAttribute(XMLConstants.SERIAL_NO);
			
			if(locnAuditsMap.containsKey(createtsStr)){
				YFCElement eleOutLocnInvAudit = outDoc.createElement(XMLConstants.LOCATION_INVENTORY_AUDIT);
				//XMLUtil.copyElement(outDoc, eleLocnInvAudit, eleOutLocnInvAudit);
				eleOutLocnInvAudit = outDoc.importNode(eleLocnInvAudit, true);
				eleOutLocnInvAudit.setAttribute(XMLConstants.QUANTITY, locnAuditsMap.get(createtsStr));
				if(!StringUtil.isEmpty(strSerialNo)){
					YFCElement eleSerialList = outDoc.createElement(XMLConstants.SERIAL_LIST);
					eleOutLocnInvAudit.appendChild(eleSerialList);
					YFCElement eleSerial = outDoc.createElement(XMLConstants.SERIAL);
					eleSerial.setAttribute(XMLConstants.SERIAL_NO, strSerialNo);
					eleSerialList.appendChild(eleSerial);
				}
				
				outDoc.getDocumentElement().appendChild(eleOutLocnInvAudit);
				
				locnAuditsMap.remove(createtsStr);
			} else{
				YFCNode eleSerialList = XPathUtil.getXpathNode(outDoc, "//LocationInventoryAudit[@Createts=\""+createtsStr+"\"]/SerialList");
				if(!StringUtil.isEmpty(strSerialNo)){
					YFCElement eleSerial = outDoc.createElement(XMLConstants.SERIAL);
					eleSerial.setAttribute(XMLConstants.SERIAL_NO, strSerialNo);
					eleSerialList.appendChild(eleSerial);
				}
			}
		}
		return outDoc.getDocument();
	}
    
}
