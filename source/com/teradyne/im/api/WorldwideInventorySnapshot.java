package com.teradyne.im.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.bridge.sterling.utils.StringUtil;
import com.bridge.sterling.utils.XPathUtil;

public class WorldwideInventorySnapshot {

	public static final String TAIWAN_NODE = "H2T";
	public static final String BAD = "DEFECTIVE";
	public static final String GOOD = "GOOD";
	private static final String XPATH_SHPNODE_INV_FIRST = "//Item/ShipNodes/ShipNode[@ShipNode=\"";
	private static final String XPATH_SHPNODE_INV_SUPPLY = "\"]/Supplies/InventorySupplyType[@SupplyType=\"";
	private static final String XPATH_SHPNODE_INV_DEMAND = "\"]/Demands/InventoryDemandType[@DemandType=\"";
	private static final String XPATH_SHPNODE_INV_QTY = "\"]/@Quantity";
	private static final String orderListTemplate = "<OrderList><Order OrderNo=\"\" ><OrderLines><OrderLine ShipNode=\"\"><Item ItemID=\"\" ProductClass=\"\" />"
			+ "</OrderLine></OrderLines></Order></OrderList>";
	

	public Document getInventoryDetails(YFSEnvironment env, Document inputDoc)
			throws YFCException, SAXException, IOException, Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inputDoc);

		String templateName = null;
		YFCDocument getShipNodeInvGoodOut = SterlingUtil.callAPI(env,
				Constants.GET_SHIP_NODE_INVENTORY,
				createInDocForShipNodeInv(inDoc, WorldwideInventorySnapshot.GOOD), templateName);
		
		YFCDocument getShipNodeInvBadOut = SterlingUtil.callAPI(env,
				Constants.GET_SHIP_NODE_INVENTORY,
				createInDocForShipNodeInv(inDoc, WorldwideInventorySnapshot.BAD), templateName);

		Document getPlanNoGoodDoc = new ShipNodeInventory()
				.getShipNodeInventory(env,
						createInDocForShipNodeInv(inDoc, WorldwideInventorySnapshot.GOOD).getDocument());
		YFCDocument getPlanNoGoodOut = YFCDocument.getDocumentFor(getPlanNoGoodDoc);
		
		Document getPlanNoBadDoc = new ShipNodeInventory()
				.getShipNodeInventory(env,
						createInDocForShipNodeInv(inDoc, WorldwideInventorySnapshot.BAD).getDocument());
		
		YFCDocument getPlanNoBadOut = YFCDocument.getDocumentFor(getPlanNoBadDoc);
		
		YFCDocument outDoc = YFCDocument.createDocument(XMLConstants.ITEM);
				
		YFCElement eleShipNodes = outDoc.createElement(XMLConstants.SHIP_NODES);
		outDoc.getDocumentElement().appendChild(eleShipNodes);

		outDoc.getDocumentElement().setAttribute(XMLConstants.ITEM_ID,
				inDoc.getDocumentElement().getAttribute(XMLConstants.ITEM_ID));
		outDoc.getDocumentElement().setAttribute(
				XMLConstants.UNIT_OF_MEASURE,
				inDoc.getDocumentElement().getAttribute(
						XMLConstants.UNIT_OF_MEASURE));
		outDoc.getDocumentElement().setAttribute(
				XMLConstants.ORGANIZATION_CODE,
				inDoc.getDocumentElement().getAttribute(
						XMLConstants.ORGANIZATION_CODE));

		YFCNodeList<YFCElement> nlGoodShipNodes = getShipNodeInvGoodOut
				.getElementsByTagName(XMLConstants.SHIP_NODE);
		YFCNodeList<YFCElement> nlBadShipNodes = getShipNodeInvBadOut
				.getElementsByTagName(XMLConstants.SHIP_NODE);
		HashSet<String> hsShipNodes = new HashSet<String>();

		for (int i = 0; i < nlGoodShipNodes.getLength(); i++) {
			YFCElement eleShipNode = (YFCElement) nlGoodShipNodes.item(i);
			hsShipNodes.add(eleShipNode.getAttribute(XMLConstants.SHIP_NODE));
		}

		for (int j = 0; j < nlBadShipNodes.getLength(); j++) {
			YFCElement eleShipNode = (YFCElement) nlBadShipNodes.item(j);
			hsShipNodes.add(eleShipNode.getAttribute(XMLConstants.SHIP_NODE));
		}

		Map<String, Integer> ordersMap = callGetOrderList(env, inDoc, false);
		Map<String, Integer> advancedExchangeMap = callGetOrderList(env, inDoc,
				true);

		for (Iterator<String> it = hsShipNodes.iterator(); it.hasNext();) {
			String strShipNode = it.next();
			YFCElement eleOutShipNode = outDoc
					.createElement(XMLConstants.SHIP_NODE);
			eleOutShipNode.setAttribute(XMLConstants.SHIP_NODE, strShipNode);

			eleOutShipNode.setAttribute(
					"GoodOnhandQty",
					getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
							Constants.ONHAND));
			eleOutShipNode.setAttribute(
					"BadOnhandQty",
					getSupplyForShipNode(getShipNodeInvBadOut, strShipNode,
							Constants.ONHAND));

			eleOutShipNode.setAttribute(
					"GoodInTransit",
					getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
							Constants.INTRANSIT));
			eleOutShipNode.setAttribute(
					"BadInTransit",
					getSupplyForShipNode(getShipNodeInvBadOut, strShipNode,
							Constants.INTRANSIT));
			eleOutShipNode.setAttribute(
					"GoodOutTransit",
					getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
							Constants.OUTBOUND));
			eleOutShipNode.setAttribute(
					"BadOutTransit",
					getSupplyForShipNode(getShipNodeInvGoodOut, strShipNode,
							Constants.OUTBOUND));

			eleOutShipNode.setAttribute(
					"POQty",
					getPOQtyForShipNode(getShipNodeInvGoodOut,
							getShipNodeInvBadOut, strShipNode));
			eleOutShipNode.setAttribute(
					"OpenWO",
					getSumQtyForShipNode(getShipNodeInvGoodOut,
							getShipNodeInvBadOut, strShipNode,
							Constants.WO_PLACED));

			eleOutShipNode.setAttribute(
					"PlanNo",
					getPlanNoForShipNode(getPlanNoGoodOut, getPlanNoBadOut,
							strShipNode));
			eleOutShipNode.setAttribute(
					"Allocated",
					getSumQtyForShipNode(getShipNodeInvGoodOut,
							getShipNodeInvBadOut, strShipNode,
							Constants.ALLOCATED));
			eleOutShipNode.setAttribute(
					"Backordered",
					getSumQtyForShipNode(getShipNodeInvGoodOut,
							getShipNodeInvBadOut, strShipNode,
							Constants.BACKORDERED));

			Integer usageQty = ordersMap.containsKey(strShipNode) ? ordersMap
					.get(strShipNode) : new Integer(0);
			Integer advExchangeQty = advancedExchangeMap
					.containsKey(strShipNode) ? advancedExchangeMap
					.get(strShipNode) : new Integer(0);

			eleOutShipNode.setAttribute("Usage", usageQty.toString());
			if (usageQty.intValue() == 0) {
				eleOutShipNode.setAttribute("EMSPercentage", "0");
			} else {
				Double emsPercentDbl = new Double(
						(advExchangeQty.intValue() / usageQty.intValue()) * 100);
				eleOutShipNode.setAttribute("EMSPercentage", new BigDecimal(
						emsPercentDbl).setScale(2, BigDecimal.ROUND_HALF_UP)
						.toString());
			}

			eleShipNodes.appendChild(eleOutShipNode);
		}
		
		YFCDocument returnOut = callOrderListForWR(env, inDoc,"ReturnOut");
		YFCDocument workOrderOut = callOrderListForWR(env,inDoc,"WorkOrderOut");
		outDoc.getDocumentElement().setAttribute("CPSOwned", returnOut.getDocumentElement().getAttribute("Total"));
		outDoc.getDocumentElement().setAttribute("CPSOwnedAtTaiwan", returnOut.getDocumentElement().getAttribute("Taiwan"));
		outDoc.getDocumentElement().setAttribute("CustomerOwned", workOrderOut.getDocumentElement().getAttribute("Total"));
		outDoc.getDocumentElement().setAttribute("CustomerOwnedAtTaiwan", workOrderOut.getDocumentElement().getAttribute("Taiwan"));
		
		return outDoc.getDocument();
	}

	private String getPlanNoForShipNode(YFCDocument getPlanNoGoodOut,
			YFCDocument getPlanNoBadOut, String strShipNode) throws ParserConfigurationException, TransformerException {
		String goodPlanNoStr = XPathUtil.getXpathAttribute(getPlanNoGoodOut,
				"/ShipNodeInventory/Item/ShipNodes/ShipNode[@ShipNode=\""
						+ strShipNode + "\"]/@ExtnPlanNo");
		String badPlanNoStr = XPathUtil.getXpathAttribute(getPlanNoBadOut,
				"/ShipNodeInventory/Item/ShipNodes/ShipNode[@ShipNode=\""
						+ strShipNode + "\"]/@ExtnPlanNo");
		if(StringUtil.isEmpty(goodPlanNoStr)){
			goodPlanNoStr = "0";
		}
		if(StringUtil.isEmpty(badPlanNoStr)){
			badPlanNoStr = "0";
		}
		int planNo = Integer.parseInt(goodPlanNoStr)
				+ Integer.parseInt(badPlanNoStr);
		return Integer.toString(planNo);
	}

	private YFCDocument createInDocForShipNodeInv(YFCDocument inDoc,
			String productClassStr) {
		inDoc.getDocumentElement().setAttribute(XMLConstants.SHIP_DATE,
				DateTimeUtil.getFutureDate(Constants.YYYY_MM_DD, 180));
		inDoc.getDocumentElement().setAttribute(XMLConstants.PRODUCT_CLASS,
				productClassStr);
		return inDoc;
	}

	private String getSumQtyForShipNode(YFCDocument inGoodDoc, YFCDocument inBadDoc,
			String strShipNode, String strDemandType)
			throws ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		double qty = Double.parseDouble(getDemandForShipNode(inGoodDoc,
				strShipNode, strDemandType))
				+ Double.parseDouble(getDemandForShipNode(inBadDoc,
						strShipNode, strDemandType));
		String strQty = new BigDecimal(qty).setScale(2,
				BigDecimal.ROUND_HALF_UP).toString();
		return strQty;
	}

	private String getPOQtyForShipNode(YFCDocument inGoodDoc, YFCDocument inBadDoc,
			String strShipNode) throws ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		Double poQty = getDoubleValueForPOSupply(inGoodDoc,	strShipNode, Constants.PO_PLACED)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_PLACED)
				+ getDoubleValueForPOSupply(inGoodDoc, strShipNode, Constants.PO_BACKORDER)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_BACKORDER)
				+ getDoubleValueForPOSupply(inGoodDoc, strShipNode, Constants.PO_SCHEDULED)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_SCHEDULED)
				+ getDoubleValueForPOSupply(inGoodDoc, strShipNode, Constants.PO_RELEASED)
				+ getDoubleValueForPOSupply(inBadDoc, strShipNode, Constants.PO_RELEASED);
		String strPOQty = new BigDecimal(poQty).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		return strPOQty;

	}

	private String getDemandForShipNode(YFCDocument inDoc, String strShipNode,
			String strDemandType) throws ParserConfigurationException, TransformerException {

		String xpathDemandQty = XPATH_SHPNODE_INV_FIRST + strShipNode
				+ XPATH_SHPNODE_INV_DEMAND + strDemandType
				+ XPATH_SHPNODE_INV_QTY;
		return XPathUtil.getXpathAttributeWithDefaultValue(inDoc,
				xpathDemandQty, Constants.ZERO);

	}

	private String getSupplyForShipNode(YFCDocument inDoc, String strShipNode,
			String strSupplyType) throws ParserConfigurationException, TransformerException {
		String xpathSupplyQty = XPATH_SHPNODE_INV_FIRST + strShipNode
				+ XPATH_SHPNODE_INV_SUPPLY + strSupplyType
				+ XPATH_SHPNODE_INV_QTY;
		return XPathUtil.getXpathAttributeWithDefaultValue(inDoc,
				xpathSupplyQty, Constants.ZERO);

	}
	
	private Double getDoubleValueForPOSupply(YFCDocument inDoc, String strShipNode,
			String strSupplyType) throws ParserConfigurationException, TransformerException {
		String xpathSupplyQty = XPATH_SHPNODE_INV_FIRST + strShipNode
				+ XPATH_SHPNODE_INV_SUPPLY + strSupplyType
				+ XPATH_SHPNODE_INV_QTY;
		return Double.parseDouble(XPathUtil.getXpathAttributeWithDefaultValue(inDoc,
				xpathSupplyQty, Constants.ZERO));

	}
	
	private YFCDocument formInputForGetOrderList(String documentType, String fromStatus, String toStatus, String itemID, String uomStr, String enterpriseCode){
		YFCDocument getOrderListIn = YFCDocument.createDocument(XMLConstants.ORDER);
		
		YFCElement orderEle = getOrderListIn.getDocumentElement();
		orderEle.setAttribute(XMLConstants.ENTERPRISE_CODE, enterpriseCode);
		orderEle.setAttribute(XMLConstants.DOCUMENT_TYPE, documentType);
		orderEle.setAttribute(XMLConstants.MAXIMUM_RECORDS, "1000");
		orderEle.setAttribute(XMLConstants.DRAFT_ORDER_FLAG, Constants.NO);
		orderEle.setAttribute(XMLConstants.ORDER_DATE_QRY_TYPE, Constants.GE);
		orderEle.setAttribute(XMLConstants.ORDER_DATE,
				DateTimeUtil.getPreviousDate(Constants.YYYY_MM_DD, 270));
		orderEle.setAttribute("FromStatus", fromStatus);
		orderEle.setAttribute("ToStatus", toStatus);
		
		YFCElement orderLineEle = getOrderListIn
				.createElement(XMLConstants.ORDER_LINE);
		orderEle.appendChild(orderLineEle);
		orderLineEle.setAttribute(XMLConstants.ORDERING_UOM, uomStr);

		YFCElement itemEle = getOrderListIn.createElement(XMLConstants.ITEM);
		orderLineEle.appendChild(itemEle);
		itemEle.setAttribute(XMLConstants.ITEM_ID, itemID);
		
		
		return getOrderListIn;
	}
	
	private YFCDocument callOrderListForWR(YFSEnvironment env,YFCDocument inDoc, String docName) throws Exception{
		YFCDocument outDoc = YFCDocument.createDocument(docName);
		String docType=null;
		String fromStatus=null;
		String toStatus=null;
		if(docName.equals("WorkOrderOut")){
			docType = "7001";
			fromStatus="1100";
			toStatus="1400.20";
		}else{
			docType = "0003";
			fromStatus="1100";
			toStatus= "3780";
		}
		String enterpriseCode = inDoc.getDocumentElement().getAttribute(XMLConstants.ORGANIZATION_CODE);
		String itemIDStr = inDoc.getDocumentElement().getAttribute(XMLConstants.ITEM_ID);
		String uomStr = inDoc.getDocumentElement().getAttribute(XMLConstants.UNIT_OF_MEASURE);
		YFCDocument getOrderListIn = formInputForGetOrderList(docType, fromStatus, toStatus, itemIDStr, uomStr, enterpriseCode);
		YFCDocument getOrderListOut = SterlingUtil.callAPI(env,
				Constants.GET_ORDER_LIST, getOrderListIn, YFCDocument.parse(orderListTemplate));
		int taiwanOrders = countOrdersForTaiwan(getOrderListOut, itemIDStr, WorldwideInventorySnapshot.GOOD);

		int remainingOrders = getOrderListOut.getDocumentElement().getChildNodes().getLength() - taiwanOrders;
        		
        outDoc.getDocumentElement().setAttribute("Total", Integer.toString(remainingOrders));
        outDoc.getDocumentElement().setAttribute("Taiwan", Integer.toString(taiwanOrders));

		return outDoc;
	}

	private Map<String, Integer> callGetOrderList(YFSEnvironment env,
			YFCDocument inDoc, boolean isExchange) throws Exception {
		
		String orderListTemplate = "<OrderList><Order OrderNo=\"\" ><OrderLines><OrderLine ShipNode=\"\"><Item ItemID=\"\" ProductClass=\"\" />"
				+ "</OrderLine></OrderLines></Order></OrderList>";

		YFCDocument getOrderListIn = YFCDocument.createDocument(XMLConstants.ORDER);
		
		YFCElement orderEle = getOrderListIn.getDocumentElement();
		orderEle.setAttribute(
				XMLConstants.ENTERPRISE_CODE,
				inDoc.getDocumentElement().getAttribute(
						XMLConstants.ORGANIZATION_CODE));
		orderEle.setAttribute(XMLConstants.DOCUMENT_TYPE, "0001");
		orderEle.setAttribute(XMLConstants.MAXIMUM_RECORDS, "1000");
		orderEle.setAttribute(XMLConstants.DRAFT_ORDER_FLAG, Constants.NO);
		orderEle.setAttribute(XMLConstants.ORDER_DATE_QRY_TYPE, Constants.GE);
		orderEle.setAttribute(XMLConstants.ORDER_DATE,
				DateTimeUtil.getPreviousDate(Constants.YYYY_MM_DD, 270));
		if (isExchange) {
			orderEle.setAttribute(XMLConstants.EXCHANGE_TYPE, Constants.ADVANCED);
		}

		YFCElement orderLineEle = getOrderListIn
				.createElement(XMLConstants.ORDER_LINE);
		orderEle.appendChild(orderLineEle);
		orderLineEle.setAttribute(XMLConstants.ORDERING_UOM, Constants.EACH);

		YFCElement itemEle = getOrderListIn.createElement(XMLConstants.ITEM);
		orderLineEle.appendChild(itemEle);
		String itemIDStr = inDoc.getDocumentElement().getAttribute(
				XMLConstants.ITEM_ID);
		itemEle.setAttribute(XMLConstants.ITEM_ID, itemIDStr);
		itemEle.setAttribute(XMLConstants.PRODUCT_CLASS, WorldwideInventorySnapshot.GOOD);
		
		YFCDocument getOrderListGoodOut = SterlingUtil.callAPI(env,
				Constants.GET_ORDER_LIST, getOrderListIn, YFCDocument.parse(orderListTemplate));
		Map<String, Integer> ordersNodeMap = new HashMap<String, Integer>();
		countOrdersPerNode(getOrderListGoodOut, ordersNodeMap, itemIDStr,
				WorldwideInventorySnapshot.GOOD);

		itemEle.setAttribute(XMLConstants.PRODUCT_CLASS, WorldwideInventorySnapshot.BAD);
		YFCDocument getOrderListBadOut = SterlingUtil.callAPI(env, Constants.GET_ORDER_LIST,
				getOrderListIn, YFCDocument.parse(orderListTemplate));
		countOrdersPerNode(getOrderListBadOut, ordersNodeMap, itemIDStr, WorldwideInventorySnapshot.BAD);

		return ordersNodeMap;
	}

	private void countOrdersPerNode(YFCDocument inDoc,
			Map<String, Integer> ordersNodeMap, String itemIDStr,
			String productClassStr) throws Exception {
		YFCNodeList<YFCNode> ordersNL = inDoc.getDocumentElement().getChildNodes();

		for (int i = 0; i < ordersNL.getLength(); i++) {
			YFCNode orderNode =  ordersNL.item(i);
			
			YFCNodeList<YFCNode> itemNL = XPathUtil.getXpathNodeList(orderNode,
					"/Order/OrderLines/OrderLine/Item[@ItemID = \"" + itemIDStr
							+ "\" " + "and @ProductClass = \""
							+ productClassStr + "\"]");
			for(int j=0; j < itemNL.getLength(); j++){
				YFCNode itemEle = itemNL.item(j);
				String shipNodeStr = ((YFCElement) itemEle.getParentNode()).getAttribute(XMLConstants.SHIP_NODE);
				if (!StringUtil.isEmpty(shipNodeStr)) {
					if (ordersNodeMap.containsKey(shipNodeStr)) {
						ordersNodeMap.put(shipNodeStr,
								Integer.valueOf(ordersNodeMap.get(shipNodeStr)
										.intValue() + 1));
					} else {
						ordersNodeMap.put(shipNodeStr, Integer.parseInt("1"));
					}
				}
			}
				
		}

	}

	private int countOrdersForTaiwan(YFCDocument inDoc, String itemIDStr, String productClassStr) throws Exception {
		YFCNodeList<YFCNode> ordersNL = inDoc.getDocumentElement().getChildNodes();
		int count = 0;
		for (int i = 0; i < ordersNL.getLength(); i++) {
			YFCNode orderNode = ordersNL.item(i);
			YFCNodeList<YFCNode> itemNL = XPathUtil.getXpathNodeList(orderNode,
					"/Order/OrderLines/OrderLine/Item[@ItemID = \"" + itemIDStr
							+ "\" " + "and @ProductClass = \""
							+ productClassStr + "\"]");
			
			for(int j=0; j < itemNL.getLength(); j++){
				YFCNode itemEle =  itemNL.item(j);
				String shipNodeStr = ((YFCElement) itemEle.getParentNode())
						.getAttribute(XMLConstants.SHIP_NODE);
				if(shipNodeStr.equals(WorldwideInventorySnapshot.TAIWAN_NODE)){
					count++;
					break;
				}
			}
		}
		return count;
	}
}
