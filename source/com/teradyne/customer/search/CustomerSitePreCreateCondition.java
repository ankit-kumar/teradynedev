package com.teradyne.customer.search;

import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

import com.teradyne.utils.Constants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;


public class CustomerSitePreCreateCondition {

  public Document removeUnwantedInputElement(YFSEnvironment env, Document inputdoc) {

    YFCDocument document = YFCDocument.getDocumentFor(inputdoc);
    YFCElement inputEle = document.getDocumentElement();

    YFCElement extnEle = inputEle.getChildElement(Constants.EXTN);
    YFCElement listEle = extnEle.getChildElement("TerCustBillingOrgList");
    YFCNodeList<YFCNode> nodeList = listEle.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      YFCNode ele = nodeList.item(i);
      Map<String, String> map = ele.getAttributes();
      Set<String> attrs = map.keySet();
      if (attrs.size() == 3)
        for (String attr : attrs) {
          if ("TerDefaultFlag".equalsIgnoreCase(attr)) {
            ele.getParentNode().removeChild(ele);
            i--;
            break;
          }
        }
    }
    return document.getDocument();
  }
}
