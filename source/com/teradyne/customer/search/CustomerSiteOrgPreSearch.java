package com.teradyne.customer.search;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;

public class CustomerSiteOrgPreSearch implements YIFCustomApi {

	@Override
	public void setProperties(Properties p) throws Exception {}

	public Document formInputXMLForGetOrgList(YFSEnvironment env, Document inXML) {
		YFCDocument inputXML = YFCDocument.getDocumentFor(inXML);
		Map<String, YFCElement> addressDetails = fetchAddressDetails(inputXML);
		if (addressDetails != null) {
			if (addressDetails.containsKey("S")) {
				formXMLWithCorporatePersonInfo(inputXML, addressDetails.get("S"));
			} else if (addressDetails.containsKey("B")) {
				formXMLWithBillToPersonInfo(inputXML, addressDetails.get("B"));
			}
		}
		Document output = this.getOrganizationList(env, inputXML);
		if (XmlUtils.isVoid(output)) {
			return YFCDocument.createDocument("OrganizationList").getDocument();
		}
		setRoles(output);
		setExtraParams(inputXML, YFCDocument.getDocumentFor(output));
		return output;
	}

	private void setExtraParams(YFCDocument inputXML, YFCDocument output) {
		YFCElement organization = inputXML.getDocumentElement();
		YFCElement extraParams = organization.getChildElement("ExtraParams");
		Map<String, String> allExtraParams = null;
		if (extraParams != null) {
			allExtraParams = extraParams.getAttributes();
		} else {
			allExtraParams = new HashMap<String, String>();
			allExtraParams.put("ViewDetails","Y");
			allExtraParams.put("CreateCustomerOrg","Y");
			allExtraParams.put("CreateCustomerSiteOrg","Y");
			allExtraParams.put("ViewAudits","Y");
			allExtraParams.put("AddParticipantEnt","N");
			allExtraParams.put("AddChildOrg","N");
		}
		output.getDocumentElement().setAttributes(allExtraParams);
	}

	private void formXMLWithCorporatePersonInfo(YFCDocument inputXML, YFCElement address) {
		YFCElement organization = inputXML.getDocumentElement();
		if (address != null) {
			YFCElement corporatePersonInfo = organization.createChild(Constants.CORPORATE_PERSON_INFO);
			corporatePersonInfo.setAttributes(address.getAttributes());
		}
		organization.removeChild(organization.getChildElement(Constants.TEMP));
	}

	private Map<String, YFCElement> fetchAddressDetails(YFCDocument inputXML) {
		YFCElement organization = inputXML.getDocumentElement();
		YFCElement temp = organization.getChildElement(Constants.TEMP);
		if (temp == null) {
			return null;
		}
		String addressType = temp.getAttribute(Constants.ADDRESS_TYPE);
		YFCElement address = temp.getChildElement(Constants.ADDRESS);
		Map<String, YFCElement> m = new HashMap<String, YFCElement>();
		m.put(addressType, address);
		return m;
	}

	private void formXMLWithBillToPersonInfo(YFCDocument inputXML, YFCElement address) {
		YFCElement organization = inputXML.getDocumentElement();
		String isNode = organization.getAttribute(Constants.IS_NODE);
		if (isNode == null) {
			if (address != null) {
				Map<String, String> attr = address.getAttributes();
				YFCElement billingPersonInfo = organization.createChild(Constants.BILLING_PERSON_INFO);
				billingPersonInfo.setAttributes(attr);
			}
		} else {
			if (address != null) {
				Map<String, String> attr = address.getAttributes();
				YFCElement extn = organization.createChild(Constants.EXTN);
				YFCElement TerCustBillingOrgList = extn.createChild("TerCustBillingOrgList");
				YFCElement TerCustBillingOrg = TerCustBillingOrgList.createChild("TerCustBillingOrg");
				for (String key : attr.keySet()) {
					TerCustBillingOrg.setAttribute(Constants.PREFIX + key, address.getAttribute(key));
				}
			}
		}
		organization.removeChild(organization.getChildElement(Constants.TEMP));
	}

	private Document getOrganizationList(YFSEnvironment env, YFCDocument inputDoc) {
		Document output = null;
		YIFApi api;
		String template = 
				"<OrganizationList>" +
						"<Organization OrganizationCode=\"\" OrganizationName=\"\"" +
							" ParentOrganizationCode=\"\" CustomerSiteStatus=\"\">" +
							" <CorporatePersonInfo AddressID=\"\" AddressLine1=\"\" AddressLine2=\"\" AddressLine3=\"\"" +
								" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\" AlternateEmailID=\"\" Beeper=\"\"" +
								" City=\"\" Company=\"\" Country=\"\" DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\"" +
								" ErrorTxt=\"\" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\" HttpUrl=\"\" IsCommercialAddress=\"\"" +
								" JobTitle=\"\" LastName=\"\" Latitude=\"\" Longitude=\"\" MiddleName=\"\" MobilePhone=\"\" OtherPhone=\"\"" +
								" PersonID=\"\" PersonInfoKey=\"\" PreferredShipAddress=\"\" State=\"\" Suffix=\"\" TaxGeoCode=\"\" Title=\"\"" +
								" UseCount=\"\" VerificationStatus=\"\" ZipCode=\"\"/>" +
							"<OrgRoleList>" +
								"<OrgRole/>" +
							"</OrgRoleList>" +
							"<Extn CustomerSiteStatus=\"\" />" +
						"</Organization>" +
				"</OrganizationList>";
		YFCDocument templateDoc = YFCDocument.getDocumentFor(template);
		try {
			env.setApiTemplate(Constants.API_GET_ORGANIZATION_LIST, templateDoc.getDocument());
			api = YIFClientFactory.getInstance().getApi();
			output = api.invoke(env, Constants.API_GET_ORGANIZATION_LIST, inputDoc.getDocument());
			env.clearApiTemplate(Constants.API_GET_ORGANIZATION_LIST);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	private void setRoles(Document output) {
		YFCDocument yfcOutputDoc = YFCDocument.getDocumentFor(output);
		YFCElement yfcOutputEle = yfcOutputDoc.getDocumentElement();
		YFCNodeList<YFCElement> organizationList = yfcOutputEle.getElementsByTagName(Constants.ORGANIZATION);
		for (int org = 0; org < organizationList.getLength(); org++) {
			YFCElement orgEle = organizationList.item(org);
			YFCNodeList<YFCElement> yfcOrgRolesList = orgEle.getElementsByTagName(Constants.ORG_ROLE);
			StringBuilder role = new StringBuilder();
			for (int i = 0; i < yfcOrgRolesList.getLength(); i++) {
				YFCElement element = yfcOrgRolesList.item(i);
				String roleKey = element.getAttribute(Constants.ROLE_KEY);
				role.append(roleKey.charAt(0)).append(",");
			}
			if (role.length() > 0) {
				role.deleteCharAt(role.length() - 1);
			}
			orgEle.setAttribute("RoleList", role.toString());
		}
	}

}
