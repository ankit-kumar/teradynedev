package com.teradyne.customer.api;

import java.util.Properties;

import org.w3c.dom.Document;

import com.teradyne.utils.Constants;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class FetchCompleteCustomerDetails implements YIFCustomApi {

	@Override
	public void setProperties(Properties arg0) throws Exception {}
	
	public Document fetchCustomerDetails(YFSEnvironment env, Document inXML) throws Exception {
		YFCDocument inDocXML = YFCDocument.getDocumentFor(inXML);
		String id = inDocXML.getDocumentElement().getAttribute(Constants.ORGANIZATION_CODE);
		Document orgDetailsDoc = getOrganizationDetails(env, id);
		Document customerDetailsDoc = getCustomerDetails(env, id);
		Document outputDoc = mergeDetails(orgDetailsDoc, customerDetailsDoc);
		return outputDoc;
	}

	private Document mergeDetails(Document orgDetailsDoc, Document customerDetailsDoc) {
		YFCDocument outDoc = YFCDocument.createDocument(Constants.OUTPUT);
		YFCElement output = outDoc.getDocumentElement();
		if (orgDetailsDoc != null) {
			output.importNode(YFCDocument.getDocumentFor(orgDetailsDoc).getDocumentElement());
		}
		if (customerDetailsDoc != null) {
			output.importNode(YFCDocument.getDocumentFor(customerDetailsDoc).getDocumentElement());
		}
		return outDoc.getDocument();
	}

	private Document getCustomerDetails(YFSEnvironment env, String id) throws Exception {
		YFCDocument inputXML = YFCDocument.parse("<Customer CustomerID=\"" + id + "\"" +
				" OrganizationCode=\"CSO\"/>");
		YFCDocument outputTemplate = YFCDocument.parse(
				"<CustomerList>" +
					"<Customer CanConsumeSupplementalCapacity=\"\" CustomerID=\"\"" +
						" CustomerKey=\"\" CustomerType=\"\" MaxAssignRepsOrTeams=\"\"" +
						" OrganizationCode=\"\" SlotPreferenceType=\"\">" +
							"<BuyerOrganization AccountWithHub=\"\" AuthorityType=\"\"" +
								" BillingAddressKey=\"\" CatalogOrganizationCode=\"\"" +
								" ContactAddressKey=\"\" CorporateAddressKey=\"\"" +
								" CreatorOrganizationKey=\"\" DunsNumber=\"\"" +
								" InventoryOrganizationCode=\"\"" +
								" InventoryPublished=\"\" IsBuyer=\"\" IsHubOrganizationN=\"\"" +
								" IsLegalEntity=\"\" IsNode=\"\" IsSourcingKept=\"\"" +
								" IssuingAuthority=\"\" ItemXrefRule=\"\" LocaleCode=\"\"" +
								" OrganizationCode=\"\" OrganizationKey=\"\" OrganizationName=\"\"" +
								" ParentOrganizationCode=\"\" PaymentProcessingReqd=\"\"" +
								" PrimaryEnterpriseKey=\"\" PrimarySicCode=\"\" PrimaryUrl=\"\"" +
								" TaxExemptFlag=\"\" TaxExemptionCertificate=\"\" TaxJurisdiction=\"\"" +
								" TaxpayerId=\"\" XrefAliasType=\"\" XrefOrganizationCode=\"\">" +
									"<CorporatePersonInfo AddressLine1=\" \" AddressLine2=\"\"" +
										" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\"" +
										" AddressLine6=\"\" AlternateEmailID=\"\" Beeper=\"\"" +
										" City=\"\" Company=\"\" Country=\"\" DayFaxNo=\"\"" +
										" DayPhone=\"\" Department=\"\" EMailID=\"\" ErrorTxt=\"\"" +
										" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\"" +
										" HttpUrl=\"\" JobTitle=\"\" LastName=\"\" MiddleName=\"\"" +
										" MobilePhone=\"\" OtherPhone=\"\" PersonID=\"\"" +
										" PersonInfoKey=\"\" PreferredShipAddress=\"\" State=\"\"" +
										" Suffix=\"\" Title=\"\" UseCount=\"\" VerificationStatus=\"\"" +
										" ZipCode=\"\"/>" +
									"<BillingPersonInfo AddressLine1=\"\" AddressLine2=\"\"" +
										" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\"" +
										" AddressLine6=\"\" AlternateEmailID=\"\" Beeper=\"\"" +
										" City=\"\" Company=\"\" Country=\"\" DayFaxNo=\"\"" +
										" DayPhone=\"\" Department=\"\" EMailID=\"\" ErrorTxt=\"\"" +
										" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\"" +
										" HttpUrl=\"\" JobTitle=\"\" LastName=\"\" MiddleName=\"\"" +
										" MobilePhone=\"\" OtherPhone=\"\" PersonID=\"\"" +
										" PersonInfoKey=\"\" PreferredShipAddress=\"\" State=\"\"" +
										" Suffix=\"\" Title=\"\" UseCount=\"\" VerificationStatus=\"\"" +
										" ZipCode=\"\"/>" +
							"</BuyerOrganization>" +
							"<Extn RepairCenter=\"\" SpecialCustInstr=\"\" SpecialInstrVisible=\"\"" +
								" InternalCustomerFlag=\"\" IsInboundDirectShipAllowed=\"\"" +
								" IsOutboundDirectShipAllowed=\"\" IsCreditHold=\"\" IsServiceHold=\"\"" +
								" DefaultCurrency=\"\" PriceZone=\"\" PaymentTerms=\"\"" +
								" IsTransExceptionAllowed=\"\" DefaultOrderCenter=\"\"/>" +
					"</Customer>" +
				"</CustomerList>");
		return callAPI(env, Constants.API_GET_CUSTOMER_LIST, inputXML, outputTemplate);
	}

	private Document getOrganizationDetails(YFSEnvironment env, String id) throws Exception {
		YFCDocument inputXML = YFCDocument.parse(
				"<Organization OrganizationKey=\"" + id + "\">" +
						"<Extn>" +
							"<TerCustBillingOrg TerOrganizationCodeKey=\"" + id + "\"" +
									" TerDefaultFLag=\"Y\" />" +
						"</Extn>" +
				"</Organization>");
		YFCDocument outputTemplate = YFCDocument.parse(
				"<OrganizationList>" +
					"<Organization AuthorityType=\"\" CatalogOrganizationCode=\"\"" +
						" IgnoreOrdering=\"\" InventoryOrganizationCode=\"\" InventoryPublished=\"\"" +
						" InventoryKeptExternally=\"\" IssuingAuthority=\"\" LocaleCode=\"\"" +
						" OrganizationCode=\"\" OrganizationName=\"\" ParentOrganizationCode=\"\"" +
						" ParentOrganizationName=\"\" TaxpayerId=\"\" PrimaryEnterpriseKey=\"\"" +
						" TaxJurisdiction=\"\" TaxExemptionCertificate=\"\" TaxExemptFlag=\"\">" +
							"<CorporatePersonInfo AddressID=\"\" AddressLine1=\"\" AddressLine2=\"\"" +
								" AddressLine3=\"\" AddressLine4=\"\" AddressLine5=\"\" AddressLine6=\"\"" +
								" AlternateEmailID=\"\" Beeper=\"\" City=\"\" Company=\"\" Country=\"\"" +
								" DayFaxNo=\"\" DayPhone=\"\" Department=\"\" EMailID=\"\" ErrorTxt=\"\"" +
								" EveningFaxNo=\"\" EveningPhone=\"\" FirstName=\"\" HttpUrl=\"\"" +
								" JobTitle=\"\" LastName=\"\" Latitude=\"\" Longitude=\"\" MiddleName=\"\"" +
								" MobilePhone=\"\" OtherPhone=\"\" PersonID=\"\" PreferredShipAddress=\"\"" +
								" State=\"\" Suffix=\"\" Title=\"\" UseCount=\"\" VerificationStatus=\"\"" +
								" ZipCode=\"\" />" +
							"<Extn CustomerSiteStatus=\"\" CustomerSiteType=\"\" AffidavitNumber=\"\"" +
								" AffidavitFromDate=\"\" AffidavitToDate=\"\" Comments=\"\"" +
								" ExtnCreditHold=\"\" ExtnExemptionCert=\"\" ExtnInboundDirectShip=\"\"" +
								" ExtnInternalFlag=\"\" ExtnOutboundDirectShip=\"\"" +
								" ExtnPerTransExcepEntitled=\"\" ExtnServiceHold=\"\" " +
								" ExtnSpecialCustInstr=\"\" ExtnTaxExempt=\" \" ExtnTaxJurisdiction=\"\"" +
								" ExtnVisibleToCustomer=\"\" SHQLocal=\"\" SHQRegion=\"\"" +
								" TeradyneRevenueRegion=\"\" TeradyneServiceOffice=\"\">" +
									"<TerCustBillingOrgList/>" +
							"</Extn>" +
							"<OrgRoleList/>" +
							"<EnterpriseOrgList>" +
								"<OrgEnterprise OrgEnterpriseKey=\"\" EnterpriseOrganizationKey=\"\" />" +
							"</EnterpriseOrgList>" +
					"</Organization>" +
				"</OrganizationList>");
		Document output = callAPI(env, Constants.API_GET_ORGANIZATION_LIST, inputXML, outputTemplate);
		output = filterDefaultBillToAddress(output);
		output = setEnterpriseOrgName(env, output);
		output = setChildOrganizations(env, output);
		return output;
	}

	private Document setChildOrganizations(YFSEnvironment env, Document output)  throws Exception {
		YFCDocument doc = YFCDocument.getDocumentFor(output);
		YFCElement organizationList = doc.getDocumentElement();
		YFCElement organization = organizationList.getChildElement(Constants.ORGANIZATION);
		String organizationCode = organization.getAttribute(Constants.ORGANIZATION_CODE);
		Document getOrgHierarchyToGetSubOrgsOutput = getSubOrganizations(env, organizationCode);
		getOrgHierarchyToGetSubOrgsOutput = setAbbreviatedRoles(getOrgHierarchyToGetSubOrgsOutput);
		YFCDocument tempDoc = YFCDocument.getDocumentFor(getOrgHierarchyToGetSubOrgsOutput);
		YFCElement subOrganization = tempDoc.getDocumentElement().getChildElement(Constants.SUB_ORGANIZATION);
		if (subOrganization != null) {
			organization.importNode(subOrganization);
		}
		return doc.getDocument();
	}

	private Document getSubOrganizations(YFSEnvironment env, String organizationCode) throws Exception {
		YFCDocument inputXML = YFCDocument.parse(
				"<Organization OrganizationKey=\"" + organizationCode + "\" />");
		YFCDocument outputTemplate = YFCDocument.parse(
				"<Organization OrganizationCode=\"\" OrganizationKey=\"\" OrganizationName=\"\">" +
					"<OrgRoleList>" +
						"<OrgRole OrganizationKey=\"\" RoleKey=\"\"/>" +
					"</OrgRoleList>" +
					"<SubOrganization>" +
						"<Organization OrganizationCode=\"\" OrganizationKey=\"\" OrganizationName=\"\">" +
							"<OrgRoleList>" +
								"<OrgRole RoleKey=\"\" />" +
							"</OrgRoleList>" +
						"</Organization>" +
					"</SubOrganization>" +
				"</Organization>");
		return callAPI(env, Constants.API_GET_ORGANIZATION_HIERARCHY, inputXML, outputTemplate);
	}

	private Document setAbbreviatedRoles(Document output) {
		YFCDocument doc = YFCDocument.getDocumentFor(output);
		YFCElement organization = doc.getDocumentElement();
		YFCElement subOrganization = organization.getChildElement(Constants.SUB_ORGANIZATION);
		if (subOrganization != null) {
			 YFCIterable<YFCElement> allSubOrganizations = subOrganization.getChildren(Constants.ORGANIZATION);
			 if (allSubOrganizations != null) {
				 for (YFCElement subOrg : allSubOrganizations) {
					YFCElement orgRoleList = subOrg.getChildElement(Constants.ORG_ROLE_LIST);
					if (orgRoleList != null) {
						YFCIterable<YFCElement> allOrgRoles = orgRoleList.getChildren(Constants.ORG_ROLE);
						if (allOrgRoles != null) {
							StringBuilder abbrRoles = new StringBuilder();
							for (YFCElement orgRole : allOrgRoles) {
								String role = orgRole.getAttribute(Constants.ROLE_KEY);
								if (role != null && role.trim().length()>0) {
									abbrRoles.append(role.charAt(0)).append(",");
								}
							}
							abbrRoles.deleteCharAt(abbrRoles.length() - 1);
							orgRoleList.setAttribute(Constants.ROLES, abbrRoles.toString());
						}
					}
				}
			}
		}
		return doc.getDocument();
	}

	private Document setEnterpriseOrgName(YFSEnvironment env, Document output) throws Exception {
		YFCDocument doc = YFCDocument.getDocumentFor(output);
		YFCElement organizationList = doc.getDocumentElement();
		YFCElement organization = organizationList.getChildElement(Constants.ORGANIZATION);
		YFCElement enterpriseOrgList = organization.getChildElement(Constants.ENTERPRISE_ORG_LIST);
		if (enterpriseOrgList != null) {
			YFCIterable<YFCElement> allOrgEnterprises = enterpriseOrgList.getChildren(Constants.ORG_ENTERPRISE);
			if (allOrgEnterprises != null) {
				for (YFCElement orgEnterprise : allOrgEnterprises) {
					String enterpriseOrganizationKey = orgEnterprise.getAttribute(Constants.ENTERPRISE_ORGANIZATION_KEY);
					String enterpriseOrgName = getEnterpriseOrgName(env, enterpriseOrganizationKey);
					orgEnterprise.setAttribute(Constants.ENTERPRISE_ORGANIZATION_NAME, enterpriseOrgName);
				}
			}
		}
		return doc.getDocument();
	}

	private String getEnterpriseOrgName(YFSEnvironment env, String enterpriseOrganizationKey) throws Exception {
		YFCDocument inputXML = YFCDocument.parse("<Organization OrganizationKey=\"" + enterpriseOrganizationKey + "\" />");
		YFCDocument outputTemplate = YFCDocument.parse("<Organization OrganizationName=\"\" />");
		Document output = callAPI(env, Constants.API_GET_ORGANIZATION_HIERARCHY, inputXML, outputTemplate);
		YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
		return outputDoc.getDocumentElement().getAttribute(Constants.ORGANIZATION_NAME);
	}

	private Document filterDefaultBillToAddress(Document output) {
		YFCDocument doc = YFCDocument.getDocumentFor(output);
		YFCElement organizationList = doc.getDocumentElement();
		YFCElement organization = organizationList.getChildElement(Constants.ORGANIZATION);
		YFCElement extn = organization.getChildElement(Constants.EXTN);
		YFCElement terCustBillingOrgList = extn.getChildElement("TerCustBillingOrgList");
		if (terCustBillingOrgList != null) {
			YFCIterable<YFCElement> allTerCustBillingOrgs  = terCustBillingOrgList.getChildren("TerCustBillingOrg");
			if (allTerCustBillingOrgs != null) {
				for (YFCElement terCustBillingOrg : allTerCustBillingOrgs) {
					String defaultFlag = terCustBillingOrg.getAttribute("TerDefaultFLag");
					if (defaultFlag != null) {
						if (!defaultFlag.equalsIgnoreCase(Constants.Y)) {
							terCustBillingOrg.getParentElement().removeChild(terCustBillingOrg);
						}
					}
				}
			}
		}
		return doc.getDocument();
	}
	
	private Document callAPI(YFSEnvironment env, String apiName, YFCDocument inputXML, YFCDocument outputTemplate) throws Exception {
		env.setApiTemplate(apiName, outputTemplate.getDocument());
		Document output = YIFClientFactory.getInstance().getApi().invoke(env, apiName, inputXML.getDocument());
		env.clearApiTemplate(apiName);
		return output;
	}

}
