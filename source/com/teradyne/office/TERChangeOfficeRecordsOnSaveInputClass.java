package com.teradyne.office;
/**
 * @author Sourabh Goyal, Bridge Solutions Group
 */
import java.rmi.RemoteException;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class TERChangeOfficeRecordsOnSaveInputClass {
	private static YFCLogCategory log;
	static {
		log = YFCLogCategory.instance(com.teradyne.office.TERChangeOfficeRecordsOnSaveInputClass.class);
	}

	public Document terChangeOfficeInputMethod(final YFSEnvironment env, final Document inXML) throws YFSException, RemoteException,TransformerException, XPathExpressionException, YIFClientCreationException{
		/*Input to TERChangeOfficeRecordsOnSaveInputClass:
		<TerOfficeRecords Createprogid=" " Createts=" " Createuserid=" "
    		Lockid=" " Modifyprogid=" " Modifyts=" " Modifyuserid=" "
    		TerClarifyWorkgroup=" " TerGsoRevenueRegion=" "
    		TerModificationReasonCode=" " TerModificationReasonText=" "
    		TerOfficeCode=" " TerOfficeCountry=" " TerOfficeKey=" "
    		TerOfficeName=" " TerOfficeStatus=" " TerOfficeType=" "
    		TerOpsCoordinator=" " TerOrganizationCode=" " TerPriceZone=" "
    		TerServiceLocation=" " TerServiceOffice=" " TerServiceRegion=" " TerWorldwideRegion=" "/>
		 */
		log.info("Input XML for TERChangeOfficeRecordsOnSaveInputClass::::::  \n" + YFCDocument.getDocumentFor(inXML).toString());
		Document inXMLClone=inXML;
		YFCDocument inXmlYfcDoc = YFCDocument.getDocumentFor(inXMLClone);
		YFCElement rootElement=inXmlYfcDoc.getDocumentElement();
		String terModificationReasonText = rootElement.getAttribute("TerModificationReasonText");

		if("Office Record Created".equals(terModificationReasonText)){
			terModificationReasonText="Office Record Saved";
			rootElement.setAttribute("TerModificationReasonText",terModificationReasonText);
		}
		log.info("Output XML for TERChangeOfficeRecordsOnSaveInputClass inXMLClone::::::  \n" + YFCDocument.getDocumentFor(inXMLClone).toString());
		return inXMLClone;
	}
}
