package com.teradyne.om.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class SalesOrderUtils {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  
  public Document setTeradyneBusGrpAndSystemType(YFSEnvironment env, Document inDoc) {

    YFCDocument document = YFCDocument.getDocumentFor(inDoc);
    YFCElement element = document.getDocumentElement();

    YFCElement orderLines = element.getChildElement("OrderLines");

    if (!XmlUtils.isVoid(orderLines)) {
      YFCIterable<YFCElement> orderLineList = orderLines.getChildren("OrderLine");

      for (YFCElement orderLine : orderLineList) {
        if ("CREATE".equalsIgnoreCase(orderLine.getAttribute("Action"))) {
          YFCElement extnEle = orderLine.getChildElement(Constants.EXTN);
          String strBusinessGrp = "";
          String strSystemType = "";
          if (!XmlUtils.isVoid(extnEle)) {
            strBusinessGrp = extnEle.getAttribute("BusinessGrp");
            strSystemType = extnEle.getAttribute("SystemType");
          } else {
            extnEle = orderLine.createChild(Constants.EXTN);
          }

          if (XmlUtils.isVoid(strBusinessGrp) || XmlUtils.isVoid(strSystemType)) {
            log.debug("Business group and system type is setting");
            Map<String, String> outputMap = getTeradynBusGrpAndSystemType(env, orderLine);
            extnEle.setAttribute("BusinessGrp", outputMap.get(Constants.TD_BUS_GROUP));
            extnEle.setAttribute("SystemType", outputMap.get(Constants.SO_SYSTEM_TYPE));
          }
        }
      }
    }
    return document.getDocument();
  }

  public Document setAutoNoChargeCode(YFSEnvironment env, Document inDoc) {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    YFCElement orderLines = yfcInDocEle.getChildElement("OrderLines");
    if (!XmlUtils.isVoid(orderLines)) {

      YFCIterable<YFCElement> orderLineList = orderLines.getChildren("OrderLine");
      for (YFCElement orderLine : orderLineList) {
        if ("CREATE".equalsIgnoreCase(orderLine.getAttribute(XMLConstants.ACTION))
            && "0001".equalsIgnoreCase(yfcInDocEle.getAttribute(XMLConstants.DOCUMENT_TYPE))) {

          String strOrderType = yfcInDocEle.getAttribute(XMLConstants.ORDER_TYPE);
          YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
          if (!XmlUtils.isVoid(extnEle)) {

            String strHandlingNoChargeCode =
                extnEle.getAttribute(XMLConstants.HANDLING_NO_CHARGE_CODE);
            if ((Constants.Y).equalsIgnoreCase(strHandlingNoChargeCode)) {

              if ((XMLConstants.PIP_ORDER_TYPE).equalsIgnoreCase(strOrderType)) {
                extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "H");
              } else if (isWarrantyFailureOrderLine() && isOutOfBoxFailureOrderLine(orderLine)) {
                extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "K");
              } else {
                extnEle.setAttribute(XMLConstants.AUTOMATIC_NO_CHARGE_CODE, "J");
        }

      }
          }
        }
      }
    }
    return yfcInDoc.getDocument();
  }

  private boolean isWarrantyFailureOrderLine() {

    return false;
    }

  private boolean isOutOfBoxFailureOrderLine(YFCElement orderLine) {

    boolean flag = true;
    YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);

    if (!XmlUtils.isVoid(extnEle)) {

      String strOutOfBoxFailure = extnEle.getAttribute(XMLConstants.OUT_OF_BOX_FAILURE);
      if ((Constants.N).equalsIgnoreCase(strOutOfBoxFailure))
        flag = true;
    }
    return flag;
  }

  public Document updateQtyOrderedToday(YFSEnvironment env, Document inDoc) {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    String strRecvNode = yfcInEle.getAttribute("ReceivingNode");
    String strOrgCode = yfcInEle.getAttribute("EnterpriseCode");
    YFCElement yfcOrderLinesEle = yfcInEle.getChildElement("OrderLines");

    if (!XmlUtils.isVoid(yfcOrderLinesEle)) {

      YFCIterable<YFCElement> orderLineList = yfcOrderLinesEle.getChildren("OrderLine");

      for (YFCElement orderLine : orderLineList) {
        setQtyOrderedToday(env, orderLine, strRecvNode, strOrgCode);
      }
    }

    return yfcInDoc.getDocument();
  }

  private void setQtyOrderedToday(YFSEnvironment env, YFCElement orderLineEle, String strRecvNode,
      String strOrgCode) {

    YFCElement extnEle = orderLineEle.getChildElement(Constants.EXTN);
    YFCElement itemEle = orderLineEle.getChildElement("Item");

    if (!XmlUtils.isVoid(itemEle) && !XmlUtils.isVoid(itemEle.getAttribute("ItemID"))
        && !XmlUtils.isVoid(itemEle.getAttribute("ProductClass"))) {

      String strItemId = itemEle.getAttribute("ItemID");
      String strPC = itemEle.getAttribute("ProductClass");

      if (XmlUtils.isVoid(extnEle)) {
        extnEle = orderLineEle.createChild(Constants.EXTN);
      }

      extnEle.setAttribute("QTYOrderToday",
          calculateQtyOrderedToday(env, strRecvNode, strItemId, strPC, strOrgCode));
    }
  }

  private String calculateQtyOrderedToday(YFSEnvironment env, String strRecvNode, String itemId,
      String productClass, String strOrgCode) {

    double orderedToday = 0;
    String strInput =
        "<OrderLine ReceivingNode=\"" + strRecvNode + "\"><Item ItemID=\"" + itemId
            + "\" ProductClass=\"" + productClass + "\"/><Order EnterpriseCode=\"" + strOrgCode
            + "\" DocumentType=\"0001\"/></OrderLine>";
    String strTemp =
        "<OrderLineList><OrderLine OrderLineKey=\"\" OrderedQty=\"\" Status=\"\"><Order OrderHeaderKey=\"\" /></OrderLine></OrderLineList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);
    YFCDocument template = YFCDocument.getDocumentFor(strTemp);

    try {
      Document outputDoc =
          callApi(env, inputDoc.getDocument(), template.getDocument(), "getOrderLineList", true);

      YFCDocument yfcOutput = YFCDocument.getDocumentFor(outputDoc);
      YFCElement yfcOutputEle = yfcOutput.getDocumentElement();

      if (!XmlUtils.isVoid(yfcOutputEle)) {
        YFCNodeList<YFCElement> orderList = yfcOutputEle.getElementsByTagName("OrderLine");

        for (int i = 0; i < orderList.getLength(); i++) {
          YFCElement yfcOrderLine = orderList.item(i);
          
          if ("CANCELLED".equalsIgnoreCase(yfcOrderLine.getAttribute("Status"))) {
            continue;
          }
          
          String strOrderQty = yfcOrderLine.getAttribute("OrderedQty");

          orderedToday += Double.parseDouble(strOrderQty);
        }
      }

    } catch (Exception e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }
    log.debug("Ordered Quantity Today for customer-site is " + orderedToday);
    return Double.toString(orderedToday);
  }

  private Map<String, String> getTeradynBusGrpAndSystemType(YFSEnvironment env, YFCElement orderLine) {

    Map<String, String> outputMap = new HashMap<String, String>();

    try {
      Document output = callGetOrderListForGettingreqValues(env, orderLine);

      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement outputEle = outputDoc.getDocumentElement();

      YFCIterable<YFCElement> orderList = outputEle.getChildren("Order");
      for (YFCElement order : orderList) {
        YFCElement orderLinesEle = order.getChildElement("OrderLines");
        YFCIterable<YFCElement> outOrderLineList = orderLinesEle.getChildren("OrderLine");

        for (YFCElement outOrderLine : outOrderLineList) {
          // It always have only one order-line.
          YFCElement eleItemDetails = outOrderLine.getChildElement("ItemDetails");
          YFCElement eleExtnItem = eleItemDetails.getChildElement(Constants.EXTN);

          if (!XmlUtils.isVoid(eleItemDetails)) {
            outputMap.put(Constants.SO_SYSTEM_TYPE, eleItemDetails.getAttribute("ItemID"));
          } else {
            outputMap.put(Constants.SO_SYSTEM_TYPE, "");
          }

          if (!XmlUtils.isVoid(eleExtnItem)) {
            outputMap.put(Constants.TD_BUS_GROUP, eleExtnItem.getAttribute("SystemTesterGroup"));
          } else {
            outputMap.put(Constants.TD_BUS_GROUP, "");
          }
        }

      }
    } catch (Exception e) {
      outputMap.put(Constants.SO_SYSTEM_TYPE, "");
      outputMap.put(Constants.TD_BUS_GROUP, "");
      e.printStackTrace();
    }
    return outputMap;
  }

  private Document callGetOrderListForGettingreqValues(YFSEnvironment env, YFCElement orderLine)
      throws Exception {

    // TODO: I need to add status. Need to discuss with pranav

    YFCElement extnEle = orderLine.getChildElement(Constants.EXTN);
    String strInput =
        "<Order DocumentType=\"" + Constants.DOCUMENT_TYPE_IB + "\"><OrderLine>"
            + "<Extn SystemSerialNo=\"" + extnEle.getAttribute("SystemSerialNo") + "\" />"
            + "<Item ItemType=\"SYSTEM\" /></OrderLine></Order>";
    String template =
        "<OrderList><Order DocumentType=\"\"><OrderLines><OrderLine OrderedQty=\"\">"
            + "<ItemDetails ItemID=\"\"><Extn SystemTesterGroup=\"\" /></ItemDetails>"
            + "</OrderLine></OrderLines></Order></OrderList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(template);

    return callApi(env, inputDoc.getDocument(), templateDoc.getDocument(),
        Constants.GET_ORDER_LIST, true);
  }

  public Document setCurrency(YFSEnvironment env, Document inDoc) {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInEle = yfcInDoc.getDocumentElement();
    YFCElement yfcPriceInfo = yfcInEle.getChildElement("PriceInfo");

    String strCurrency = getCurrency(env, yfcInEle);
    if (XmlUtils.isVoid(yfcPriceInfo)) {
      yfcPriceInfo = yfcInEle.createChild("PriceInfo");
    }
    yfcPriceInfo.setAttribute("Currency", strCurrency);

    return yfcInDoc.getDocument();
  }

  private String getCurrency(YFSEnvironment env, YFCElement orderEle) {

    String currency = Constants.CUR_US; // Default

    YFCElement yfcPriceInfo = orderEle.getChildElement("PriceInfo");
    if (!XmlUtils.isVoid(yfcPriceInfo) && !XmlUtils.isVoid(yfcPriceInfo.getAttribute("Currency"))) {
      currency = yfcPriceInfo.getAttribute("Currency");;
    } else {
      String strReceivingCode = orderEle.getAttribute("ReceivingNode");

      String getLocaleCode = getLocaleCodeForOrganization(env, strReceivingCode);

      if ("zh_CN".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_CHINA;
      } else if ("ja_JP".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_JAPAN;
      } else if ("ko_KR".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_KOREA;
      } else if ("es_ES".equalsIgnoreCase(getLocaleCode)) {
        currency = Constants.CUR_SPANISH;
      }

    }

    return currency;

  }

  private String getLocaleCodeForOrganization(YFSEnvironment env, String strReceNode) {

    String localeCode = "";
    YFCDocument input =
        YFCDocument.getDocumentFor("<Organization OrganizationKey=\"" + strReceNode + "\" />");
    YFCDocument template =
        YFCDocument.getDocumentFor("<Organization OrganizationKey=\"\" LocaleCode=\"\" />");

    Document output;
    try {
      output =
          callApi(env, input.getDocument(), template.getDocument(), "getOrganizationHierarchy",
              true);
      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement outputEle = outputDoc.getDocumentElement();

      if (!XmlUtils.isVoid(outputEle)) {
        localeCode = outputEle.getAttribute("LocaleCode");
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    return (localeCode == null) ? "" : localeCode;
  }

  public Document checkOPPOSupport(YFSEnvironment env, Document inDoc) {

    return inDoc;
  }

  public Document setPlannerCode(YFSEnvironment env, Document inDoc) {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    String strEnterpriseCode = yfcInDocEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
    YFCElement orderlinesEle = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);
    if (!XmlUtils.isVoid(orderlinesEle)) {

      YFCNodeList<YFCElement> orderLineList =
          orderlinesEle.getElementsByTagName(XMLConstants.ORDER_LINE);
      for (int i = 0; i < orderLineList.getLength(); i++) {
        YFCElement orderline = orderLineList.item(i);
        YFCElement itemEle = orderline.getChildElement(XMLConstants.ITEM);
        if (!XmlUtils.isVoid(itemEle)
            && !XmlUtils.isVoid(itemEle.getAttribute(XMLConstants.ITEM_ID))) {
          String plannerCode = getPlannerCode(env, orderline, strEnterpriseCode);
          YFCElement extnEle = orderline.getChildElement(XMLConstants.EXTN);
          if (XmlUtils.isVoid(extnEle)) {
            extnEle = orderline.createChild(XMLConstants.EXTN);
          }
          extnEle.setAttribute(XMLConstants.PLANNER_CODE, plannerCode);
        }
      }
    }
    return yfcInDoc.getDocument();
  }


  public boolean validateUser(YFSEnvironment env, String strEnteredBy) {

    boolean isValidUser = false;
    String input = "<User Loginid=\"" + strEnteredBy + "\" />";
    String template = "<UserList> <User Loginid=\"\"/></UserList>";

    YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(template);

    try {
      Document output =
          callApi(env, inputDoc.getDocument(), templateDoc.getDocument(),
              Constants.GET_USER_LIST_API, Constants.TRUE);

      YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
      YFCElement outputEle = outputDoc.getDocumentElement();

      YFCNodeList<YFCElement> list = outputEle.getElementsByTagName(XMLConstants.USER);
      return (list.getLength() == 0);
    } catch (Exception e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }

    return isValidUser;
  }

  private String getPlannerCode(YFSEnvironment env, YFCElement orderLineEle, String orgCode) {

    String strPlannerCode = "";
    if (!XmlUtils.isVoid(orderLineEle)) {
      YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);
      YFCElement tranQty = orderLineEle.getChildElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
      String strItemId = "";
      String strUOM = "";
      if (!XmlUtils.isVoid(itemEle))
        strItemId = itemEle.getAttribute(XMLConstants.ITEM_ID);

      if (!XmlUtils.isVoid(tranQty))
        strUOM = tranQty.getAttribute(XMLConstants.TRANSACTIONAL_UOM);

      if (!XmlUtils.isVoid(strItemId) && !XmlUtils.isVoid(strUOM)) {
        YFCDocument input =
            YFCDocument.getDocumentFor("<Item UnitOfMeasure=\"" + strUOM + "\" ItemID=\""
                + strItemId + "\" OrganizationCode=\"" + orgCode + "\" />");
        YFCDocument template =
            YFCDocument.getDocumentFor("<Item ItemID=\"\"><Extn PlannerCode=\"\" /></Item>");

        try {
          Document output =
              callApi(env, input.getDocument(), template.getDocument(),
                  Constants.GET_ITEM_DETAILS_API, true);

          YFCDocument yfcDoc = YFCDocument.getDocumentFor(output);
          YFCElement yfcDocEle = yfcDoc.getDocumentElement();

          YFCElement extnEle = yfcDocEle.getChildElement(XMLConstants.EXTN);
          strPlannerCode = extnEle.getAttribute(XMLConstants.PLANNER_CODE);

        } catch (Exception e) {
          if (log.isVerboseEnabled()) {
            log.verbose(e);
          }
        }
      }
    }
    return strPlannerCode;

  }

  public Document setDueDate(YFSEnvironment env, Document inDoc) {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();
    
    String strEnterpriseCode = yfcInDocEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
    String strOrderDate = yfcInDocEle.getAttribute(XMLConstants.ORDER_DATE);
    YFCElement orderlinesEle = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);

    if (!XmlUtils.isVoid(orderlinesEle)) {
      YFCNodeList<YFCElement> orderLineList =
          orderlinesEle.getElementsByTagName(XMLConstants.ORDER_LINE);
      for (int i = 0; i < orderLineList.getLength(); i++) {
        String dueDate = "";
        YFCElement orderline = orderLineList.item(i);
        YFCElement itemEle = orderline.getChildElement(XMLConstants.ITEM);
        if (XMLConstants.CREATE.equalsIgnoreCase(orderline.getAttribute(XMLConstants.ACTION))
            && !XmlUtils.isVoid(itemEle)
            && !XmlUtils.isVoid(itemEle.getAttribute(XMLConstants.ITEM_ID))) {
          
          if (!XmlUtils.isVoid(strOrderDate)) {
            dueDate = calculateDueDate(env, orderline, strEnterpriseCode, strOrderDate);
          }
                    
          orderline.setAttribute(XMLConstants.REQUEST_SHIP_DATE, dueDate);
        }
      }
    }
    return yfcInDoc.getDocument();
  }

  private String calculateDueDate(YFSEnvironment env, YFCElement orderLine,
      String strEnterpriseCode, String strOrderDate) {

    String strServiceDays = getServiceDays(env, orderLine, strEnterpriseCode);
    try {
      SimpleDateFormat dateTimeFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
      SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
      SimpleDateFormat timeFormat = new SimpleDateFormat(Constants.TIME_FORMAT);

      String strDate = strOrderDate.substring(0, strOrderDate.indexOf('T'));
      String strTime = strOrderDate.substring(strOrderDate.indexOf('T') + 1, strOrderDate.length());

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(dateTimeFormat.parse(strDate + " " + strTime));

      calendar.add(Calendar.DATE, Integer.parseInt(strServiceDays));

      return dateFormat.format(calendar.getTime()) + "T" + timeFormat.format(calendar.getTime());
    } catch (ParseException e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }

    return "";

  }

  private String getServiceDays(YFSEnvironment env, YFCElement orderLineEle, String orgCode) {

    String strServiceDays = "";
    if (!XmlUtils.isVoid(orderLineEle)) {
      YFCElement itemEle = orderLineEle.getChildElement(XMLConstants.ITEM);
      YFCElement tranQty = orderLineEle.getChildElement(XMLConstants.ORDER_LINE_TRAN_QUANTITY);
      String strItemId = "";
      String strUOM = "";
      if (!XmlUtils.isVoid(itemEle))
        strItemId = itemEle.getAttribute(XMLConstants.ITEM_ID);

      if (!XmlUtils.isVoid(tranQty))
        strUOM = tranQty.getAttribute(XMLConstants.TRANSACTIONAL_UOM);

      if (!XmlUtils.isVoid(strItemId) && !XmlUtils.isVoid(strUOM)) {
        YFCDocument input =
            YFCDocument.getDocumentFor("<Item UnitOfMeasure=\"" + strUOM + "\" ItemID=\""
                + strItemId + "\" OrganizationCode=\"" + orgCode + "\" />");
        YFCDocument template =
            YFCDocument
                .getDocumentFor("<Item ItemID=\"\"><Extn PlannerCode=\"\" ServiceDays=\"\" /></Item>");

        try {
          Document output =
              callApi(env, input.getDocument(), template.getDocument(),
                  Constants.GET_ITEM_DETAILS_API, Constants.TRUE);

          YFCDocument yfcDoc = YFCDocument.getDocumentFor(output);
          YFCElement yfcDocEle = yfcDoc.getDocumentElement();

          YFCElement extnEle = yfcDocEle.getChildElement(XMLConstants.EXTN);
          strServiceDays = extnEle.getAttribute(XMLConstants.SERVICE_DAYS);

        } catch (Exception e) {
          if (log.isVerboseEnabled()) {
            log.verbose(e);
          }
        }
      }
    }

    if (!XmlUtils.isVoid(strServiceDays)) {
      strServiceDays = "0";
    }
    return strServiceDays;

  }

  public Document callApi(YFSEnvironment env, Document input, Document template, String strApiName,
      boolean isAPI) throws Exception {

    YIFApi api = YIFClientFactory.getInstance().getApi();

    if (template != null) {
      env.setApiTemplate(strApiName, template);
    }
    log.debug("Api Name:" + strApiName);
    log.debug("Is It Api? " + isAPI + " (true: API ; False : Service)");
    Document output = null;
    if (isAPI) {
       output = api.invoke(env, strApiName, input);
    } else {
      output = api.executeFlow(env, strApiName, input);
    }
    env.clearApiTemplate(strApiName);
    return output;
  }
    
  public String getProperty(YFSEnvironment env, String property) {
    
    YFCDocument inDoc =
        YFCDocument.getDocumentFor("<GetProperty PropertyName=\"" + property + "\" />");
    YFCDocument template = YFCDocument.getDocumentFor("<GetProperty PropertyValue=\"\" />");
    
    try {
      Document output =
          callApi(env, inDoc.getDocument(), template.getDocument(), Constants.GET_PROPERTY_API,
              Constants.TRUE);
      
      if (!XmlUtils.isVoid(output)) {
        
        YFCDocument outptDoc = YFCDocument.getDocumentFor(output);
        YFCElement outputEle = outptDoc.getDocumentElement();
        
        if (!XmlUtils.isVoid(outputEle)) {
          return outputEle.getAttribute(XMLConstants.PROPERTY_VALUE);
        }  
      }
      
    } catch (Exception e) {
      if (log.isVerboseEnabled()) {
        log.verbose(e);
      }
    }
    return "";
  }
}
