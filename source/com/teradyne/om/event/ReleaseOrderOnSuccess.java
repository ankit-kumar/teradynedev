package com.teradyne.om.event;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class ReleaseOrderOnSuccess {
  
  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  public Document onSuccess(YFSEnvironment env, Document inDoc) throws Exception {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement inEle = yfcInDoc.getDocumentElement();

    String strEnterpriseCode = inEle.getAttribute(XMLConstants.ENTERPRISE_CODE);
    YFCElement orderLinesELe = inEle.getChildElement(XMLConstants.ORDER_LINES);

    if (!XmlUtils.isVoid(orderLinesELe)) {

      YFCNodeList<YFCElement> list = orderLinesELe.getElementsByTagName(XMLConstants.ORDER_LINE);
      for (int i = 0; i < list.getLength(); i++) {

        YFCElement element = list.item(i);
        setShipLateFlag(env, element, strEnterpriseCode);
      }
    }
    orderUtils.callApi(env, yfcInDoc.getDocument(), null, Constants.CHANGE_ORDER_API, Constants.TRUE);
    return yfcInDoc.getDocument();
  }


  private void setShipLateFlag(YFSEnvironment env, YFCElement orderLine, String strEnterpriseCode) throws Exception {

    // TODO Doubt
    YFCElement schedulesElement = orderLine.getChildElement(XMLConstants.SCHEDULES);
    YFCElement scheduleElement = schedulesElement.getFirstChildElement();
    YFCElement ItemEle = orderLine.getChildElement(XMLConstants.ITEM);
    YFCElement extnEle = orderLine.createChild(XMLConstants.EXTN);

    String itemId = ItemEle.getAttribute(XMLConstants.ITEM_ID);
    String productClass = ItemEle.getAttribute(XMLConstants.PRODUCT_CLASS);
    String strUOM = ItemEle.getAttribute(XMLConstants.UNIT_OF_MEASURE);

    if (!XmlUtils.isVoid(orderLine.getAttribute(XMLConstants.SHIP_NODE))) {
      extnEle.setAttribute(XMLConstants.EXTN_SHIP_LATE, Constants.N);
    } else if (!XmlUtils.isVoid(orderLine.getAttribute(XMLConstants.DISTRIBUTION_RULE_ID))
        && !XmlUtils.isVoid(scheduleElement.getAttribute(XMLConstants.SHIP_NODE))) {
      String strDistribution = orderLine.getAttribute(XMLConstants.DISTRIBUTION_RULE_ID);
      String strShipNode = scheduleElement.getAttribute(XMLConstants.SHIP_NODE);

      boolean isPriorityNode =
          isPriority(env, strDistribution, strShipNode, itemId, productClass, strUOM,
              strEnterpriseCode);
      if(isPriorityNode){
        extnEle.setAttribute(XMLConstants.EXTN_SHIP_LATE, Constants.N);
      }else {
        extnEle.setAttribute(XMLConstants.EXTN_SHIP_LATE, Constants.Y);
      }
    } else {
      extnEle.setAttribute(XMLConstants.EXTN_SHIP_LATE, Constants.Y);
    }
    log.debug("Ship Late is flag setted");
  }

  private boolean isPriority(YFSEnvironment env, String strDistribution, String strShipNode,
      String strItemId, String pc, String uom, String orgcode) throws Exception {

    YFCDocument inputDoc =
        YFCDocument.getDocumentFor("<getDistributionSetup DistributionRuleId=\"" + strDistribution
            + "\" ItemID=\"" + strItemId + "\" ItemOrganizationCode=\"" + orgcode
            + "\" OrganizationCode=\"" + orgcode + "\" ProductClass=\"" + pc
            + "\" UnitOfMeasure=\"" + uom + "\"/>");
    YFCDocument template = YFCDocument.getDocumentFor("<Item ItemID=\"\"><ShipNodes><ShipNode Priority=\"\" ShipNode=\"\" Tracked=\"\"/></ShipNodes></Item>");
    
    Document output = orderUtils.callApi(env, inputDoc.getDocument(), template.getDocument(), "getDistributionSetup", true);
    
    YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
    YFCElement element = outputDoc.getDocumentElement();
    
    YFCNodeList<YFCElement> list = element.getElementsByTagName(XMLConstants.SHIP_NODE);
    if(list.getLength() != 0){
      YFCElement shipNodeEle = list.item(0);
      return (strShipNode.equalsIgnoreCase(shipNodeEle.getAttribute(XMLConstants.SHIP_NODE)));
    }
    
    return false;

  }
}
