package com.teradyne.om.event;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class SalesOrderOnSuccessEvent {

  private SalesOrderUtils orderUtils = new SalesOrderUtils();
  boolean isUpdate = false;

  public Document onSuccessHandle(YFSEnvironment env, Document inDoc) throws Exception {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();

    String strOrderNo = yfcInDocEle.getAttribute(XMLConstants.ORDER_NO);
    YFCElement yfcOrderLinesEle = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);
    if (!XmlUtils.isVoid(yfcOrderLinesEle)) {
      YFCIterable<YFCElement> orderLineList = yfcOrderLinesEle.getChildren(XMLConstants.ORDER_LINE);

      for (YFCElement orderLine : orderLineList) {
        stampControlNo(orderLine, strOrderNo);
      }

      if (isUpdate)
        callChangeOrder(env, yfcInDoc);
    }

    return yfcInDoc.getDocument();
  }

  private void stampControlNo(YFCElement orderLine, String strOrderNo) {

    String strControlNo = strOrderNo + orderLine.getAttribute(XMLConstants.PRIME_LINE_NO);
    YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);

    if (XmlUtils.isVoid(extnEle)) {
      extnEle = orderLine.createChild(XMLConstants.EXTN);
    }

    if (XmlUtils.isVoid(extnEle.getAttribute(XMLConstants.CONTROL_NO))) {
      isUpdate = true;
      extnEle.setAttribute(XMLConstants.CONTROL_NO, strControlNo);
    }
  }

  private Document callChangeOrder(YFSEnvironment env, YFCDocument inDoc) throws Exception {

    return orderUtils.callApi(env, inDoc.getDocument(), null,Constants.CHANGE_ORDER_API, Constants.TRUE);
  }

}
