package com.teradyne.om.api;

import java.text.*;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.*;

import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfs.japi.YFSEnvironment;
import com.teradyne.utils.Constants;

public class SystemSerialNoServiceType implements YIFCustomApi {
	@Override
	public void setProperties(Properties arg0) throws Exception {
	}
	public static Document formatInput(YFSEnvironment env, Document inXML) {
		YFCDocument indocXML = YFCDocument.getDocumentFor(inXML);

		DateFormat dateFormat = new SimpleDateFormat(Constants.YYYY_MM_DD);
		Date ExpirationDate = new Date();

		@SuppressWarnings("rawtypes")
		YFCNodeList nlOrderLine = indocXML.getElementsByTagName(Constants.ORDERLINE);
		for (int i=0; i< nlOrderLine.getLength(); i++){
			YFCElement eOrderLine = (YFCElement) nlOrderLine.item(i);
			YFCElement order = eOrderLine.createChild(Constants.ORDER);
			order.setAttribute(Constants.EXPIRATIONDATE, dateFormat.format(ExpirationDate).toString());
			order.setAttribute(Constants.EXPIRATIONDATEQRYTYPE, Constants.GE);
		}
		return indocXML.getDocument();
	}
}
