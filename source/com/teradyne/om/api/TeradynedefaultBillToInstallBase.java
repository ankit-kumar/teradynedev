package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class TeradynedefaultBillToInstallBase extends CustomCodeHelper implements
YFSBeforeCreateOrderUE {
private static YFCLogCategory _cat = YFCLogCategory
	.instance("com.yantra.CustomCode");

private static YIFApi _Api = null;

@Override
public Document beforeCreateOrder(YFSEnvironment arg0, Document arg1)
		throws YFSUserExitException {
	
	YFCDocument inDoc=YFCDocument.getDocumentFor(arg1);
	if (!(inDoc.getDocumentElement().hasAttribute("BillToID")
            && inDoc.getDocumentElement().getAttribute("BillToID").length()>0)) {
	setBilltoAddress(arg0,inDoc);
	}
  
	return inDoc.getDocument();
}

private void setBilltoAddress(YFSEnvironment env, YFCDocument inDoc) {

    YFCElement inDocEle = inDoc.getDocumentElement();
    try {
        if (inDocEle.hasAttribute("BuyerOrganizationCode")
               && inDocEle.hasAttribute("ReceivingNode")) {

           String receivingNode = inDocEle.getAttribute("ReceivingNode");

        
           Document teradyneCustsiteBillList = getCustomerSiteBillList(
                  env, receivingNode);
  if (_cat.isVerboseEnabled()) {
       		_cat.verbose("output from teradyne billing table:" + teradyneCustsiteBillList.toString());
       	}

           // Setting ReceivingNode also
           setBillToValues(inDocEle, teradyneCustsiteBillList);
        }

    } catch (Exception e) {
        // TODO Do we need to catch it or throw error
        if (_cat.isVerboseEnabled()) {
       		_cat.verbose("Default bill to address failed:");
       	}
    }

 }

 private Document getCustomerSiteBillList(YFSEnvironment env, String recvNode)
        throws Exception {

 _Api = YIFClientFactory.getInstance().getApi();
    YFCDocument inputdoc = YFCDocument
           .getDocumentFor("<TerCustBillingOrg TerOrganizationCodeKey=\""
                  + recvNode + "\" TerDefaultFLag=\"Y\" />");
    return _Api.executeFlow(env, "getTerCustBillingOrgList",
           inputdoc.getDocument());
 }

 private void setBillToValues(YFCElement inDocEle, Document billListDoc) throws Exception {

    YFCDocument yfcBillList = YFCDocument.getDocumentFor(billListDoc);
    		
    YFCElement element = yfcBillList.getDocumentElement();

    YFCElement terBillToEle = element.getFirstChildElement();
    try{
    if (!XmlUtils.isVoid(terBillToEle)) {
        YFCElement personBillTo = inDocEle.createChild("PersonInfoBillTo");

        personBillTo.setAttribute("AddressID",
               terBillToEle.getAttribute("TerBillingID"));
        personBillTo.setAttribute("AddressLine1",
               terBillToEle.getAttribute("TerAddressLine1"));
        personBillTo.setAttribute("AddressLine2",
               terBillToEle.getAttribute("TerAddressLine2"));
        personBillTo.setAttribute("AddressLine3",
               terBillToEle.getAttribute("TerAddressLine3"));
        personBillTo.setAttribute("AddressLine4",
               terBillToEle.getAttribute("TerAddressLine4"));
        personBillTo.setAttribute("AddressLine5",
               terBillToEle.getAttribute("TerAddressLine5"));
        personBillTo.setAttribute("AddressLine6",
               terBillToEle.getAttribute("TerAddressLine6"));
        personBillTo.setAttribute("AlternateEmailID",
               terBillToEle.getAttribute("TerAlternateEmailID"));
        personBillTo.setAttribute("Beeper",
               terBillToEle.getAttribute("TerBeeper"));
        personBillTo.setAttribute("City",
               terBillToEle.getAttribute("TerCity"));
        personBillTo.setAttribute("State",
               terBillToEle.getAttribute("TerState"));
        personBillTo.setAttribute("Country",
               terBillToEle.getAttribute("TerCountry"));
        personBillTo.setAttribute("DayFaxNo",
               terBillToEle.getAttribute("TerDayFaxNo"));
        personBillTo.setAttribute("DayPhone",
               terBillToEle.getAttribute("TerDayPhone"));
        personBillTo.setAttribute("EMailID",
               terBillToEle.getAttribute("TerEMailID"));
        personBillTo.setAttribute("EveningFaxNo",
               terBillToEle.getAttribute("TerEveningFaxNo"));
        personBillTo.setAttribute("EveningPhone",
               terBillToEle.getAttribute("TerEveningPhone"));
        personBillTo.setAttribute("MobilePhone",
               terBillToEle.getAttribute("TerMobilePhone"));
        personBillTo.setAttribute("OtherPhone",
               terBillToEle.getAttribute("TerOtherPhone"));
        personBillTo.setAttribute("ZipCode",
               terBillToEle.getAttribute("TerZipCode"));
    } else {
    	throw new Exception("No Default Bill To address found for the customer site");
        // TODO we need to throw error. or just logging is enough
    }
    }
    catch (Exception e) {
		//throw new YFSException(e.getMessage());
		throw new YFSException(
				  "mandatory Parameters Missing Or Invalid",
				  "EXTNTDYN0001",
				  e.getMessage()
			
				);
    }
 }


@Override
public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
		throws YFSUserExitException {
	// TODO Auto-generated method stub
	return null;
}


}
