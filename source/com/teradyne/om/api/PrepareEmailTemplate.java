package com.teradyne.om.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;

import org.w3c.dom.Document;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class PrepareEmailTemplate implements YIFCustomApi{
  
  private Properties properties = null;
  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  
  public void setProperties(Properties prop) throws Exception {
    properties = prop;
  }  

  private SalesOrderUtils salesOrderUtils = new SalesOrderUtils();

  public Document prepareEmailTemplate(YFSEnvironment env, Document inDoc) throws Exception {

    ArrayList<String> fileList = new ArrayList<String>();
    Document output = getOrderListForJasperReport(env, inDoc);

    String path = Constants.PATH;
    String termsFirstPage = Constants.TERMS_FIRST_PAGE;
    String termsSecPage = Constants.TERMS_SECOND_PAGE;
    
    log.debug("Quote file:"+path);
    log.debug("Terms file:"+termsFirstPage);
    log.debug("Terms1 file:"+termsSecPage);

    exportPdf(output, path, false, 0, fileList);
    exportPdf(output, termsFirstPage, true, 1, fileList);
    exportPdf(output, termsSecPage, true, 2, fileList);

    String filePath = doMerge(output,fileList);
    
    YFCDocument document = YFCDocument.getDocumentFor(output);
    YFCElement element = document.getDocumentElement();
    
    element.setAttribute("FilePath", filePath);
    return document.getDocument();
  }

  private String doMerge(Document output, ArrayList<String> fileNameList) throws IOException, DocumentException {

    String filename = getFilePath(output, -1);
    
    OutputStream out =
        new FileOutputStream(new File(filename));

    com.itextpdf.text.Document document = new com.itextpdf.text.Document();

    PdfCopy copy = new PdfCopy(document, out);

    document.open();

    PdfReader reader = null;

    for (String fileName : fileNameList) {
      reader = new PdfReader(fileName);
      int n = reader.getNumberOfPages();
      for (int page = 0; page < n;) {
        copy.addPage(copy.getImportedPage(reader, ++page));
      }
      copy.freeReader(reader);
      reader.close();
      File file = new File(fileName);
      if(file.exists()){
        file.delete();
      }
    }

    document.close();
    return filename;
  }

  private void exportPdf(Document output, String jrxmlpath, boolean isStatic, int count,
      ArrayList<String> list) throws JRException {

    JasperReport jasperReport =
        JasperCompileManager.compileReport(this.getClass().getClassLoader()
            .getResourceAsStream(jrxmlpath));
    String filepath = getFilePath(output, count);

    if (!isStatic) {
      JRXmlDataSource dataSource = new JRXmlDataSource(output);
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);
      JasperExportManager.exportReportToPdfFile(jasperPrint, filepath);
    } else {
      JREmptyDataSource dataSource = new JREmptyDataSource();
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);
      JasperExportManager.exportReportToPdfFile(jasperPrint, filepath);
    }

    list.add(filepath);

  }

  private String getFilePath(Document inDoc, int count) {

    YFCDocument document = YFCDocument.getDocumentFor(inDoc);
    YFCElement element = document.getDocumentElement();
    String filename = null;
    
    if (!XmlUtils.isVoid(element)) {
      String strOrderHeaderKey = element.getAttribute("OrderHeaderKey");

      if (count == 0) {
        filename = strOrderHeaderKey + "_temp.pdf";
      } else if (count == 1) {
        filename = strOrderHeaderKey + "_terms.pdf";
      } else if (count == 2) {
        filename = strOrderHeaderKey + "_terms1.pdf";
      } else {
        filename = "Quote_" + strOrderHeaderKey + ".pdf";
      }
    }
    
    return properties.getProperty("filePath") + filename;
  }

  private Document getOrderListForJasperReport(YFSEnvironment env, Document inDoc) throws Exception {

    YFCDocument template =
        YFCDocument
            .getDocumentFor("<OrderList><Order ShipToID=\"\" OrderDate=\"\" OrderHeaderKey=\"\" OrderNo=\"\"><PersonInfoShipTo AddressLine1=\"\" City=\"\" State=\"\" ZipCode=\"\" Country=\"\" EMailID=\"\"/>"
                + "<PersonInfoBillTo Company=\"\" AddressLine1=\"\" City=\"\" State=\"\" ZipCode=\"\" Country=\"\"/><Extn ContactAttnTo=\"\" ContactEMailID=\"\" ContactName=\"\" ContactPhone=\"\" />"
                + "<OrderLines><OrderLine PrimeLineNo=\"\" OrderedQty=\"\" Status=\"\" ><LinePriceInfo UnitPrice=\"\" LineTotal=\"\" /><Item ItemID=\"\" ItemShortDesc=\"\" UnitCost=\"\" />"
                + "<Extn ControlNo=\"\" PartExpiditingPrice=\"\" ServiceTypeCode=\"\"/></OrderLine></OrderLines></Order></OrderList>");

    Document output =
        salesOrderUtils.callApi(env, inDoc, template.getDocument(), "getOrderList", true);
    YFCDocument yfcOutputDoc = YFCDocument.getDocumentFor(output);

    String strOutput = "";
    YFCElement orderEle = yfcOutputDoc.getDocumentElement().getFirstChildElement();

    strOutput = prepareOrderForJasperTemplate(strOutput, orderEle);

    YFCDocument yfcOutput = YFCDocument.getDocumentFor(strOutput);
    return yfcOutput.getDocument();
  }

  private String prepareOrderForJasperTemplate(String strOutput, YFCElement orderEle) {
    if (!XmlUtils.isVoid(orderEle)) {

      YFCElement orderLinesEle = orderEle.getChildElement("OrderLines");
      for (YFCElement orderLine : orderLinesEle.getChildren("OrderLine")) {
        if ("Cancelled".equalsIgnoreCase(orderLine.getAttribute("Status"))) {
          orderLine.getParentElement().removeChild(orderLine);
        }
      }
      strOutput = orderEle.getString();
      strOutput = strOutput.replaceAll("&", "&amp;");
    }
    return strOutput;
  }
}
