package com.teradyne.om.api;

import java.rmi.RemoteException;
import java.util.Properties;

import org.w3c.dom.Document;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.interop.japi.YIFCustomApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class CustomerAddressInfo implements YIFCustomApi{

	public Document getCustomerAddressDetails(YFSEnvironment env, Document inXML)throws Exception {
		YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
		YFCElement rootElement = inDoc.getDocumentElement();
		String customerInAdditionalAddressID = rootElement.getAttribute("CustomerAdditionalAddressID");
		YIFApi oApi = null;
		Document docDetails = null;
		try {
			env.setApiTemplate("getCustomerList", getCustomerListTpl());
			oApi = YIFClientFactory.getInstance().getApi();
			docDetails = oApi.getCustomerList(env, inDoc.getDocument());
			env.clearApiTemplate("getCustomerList");

		} catch (YIFClientCreationException e) {
			e.printStackTrace();
			throw new YFSUserExitException(e.getMessage());
		} catch (YFSException e) {
			e.printStackTrace();
			throw new YFSUserExitException(e.getMessage());
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new YFSUserExitException(e.getMessage());
		}

		YFCDocument customerList = YFCDocument.getDocumentFor(docDetails);
		YFCElement customerRootEle = customerList.getDocumentElement();
		YFCElement customer = customerRootEle.getChildElement("Customer");
		YFCElement customerAdditionalAddressList = customer.getChildElement("CustomerAdditionalAddressList");

		for (YFCElement customerAdditionalAddress : customerAdditionalAddressList.getChildren("CustomerAdditionalAddress")) {
			String customerOutAdditionalAddressID = customerAdditionalAddress.getAttribute("CustomerAdditionalAddressID");
			if(!customerInAdditionalAddressID.equals(customerOutAdditionalAddressID)){
				customerAdditionalAddressList.removeChild((YFCNode)customerAdditionalAddress);
			}

		}
		return customerList.getDocument();
	}
	public Document getCustomerListTpl(){
		YFCDocument outTempDoc=null;
		outTempDoc = YFCDocument.createDocument("CustomerList");
		YFCElement customerListEle = outTempDoc.getDocumentElement();
		YFCElement customerEle = customerListEle.createChild("Customer");
		customerEle.setAttribute("CustomerID", "");
		YFCElement customerAdditionalAddressListEle = customerEle.createChild("CustomerAdditionalAddressList");
		YFCElement customerAdditionalAddressEle = customerAdditionalAddressListEle.createChild("CustomerAdditionalAddress");


		customerAdditionalAddressEle.setAttribute("AddressType","");
		customerAdditionalAddressEle.setAttribute("CustomerAdditionalAddressID","");
		customerAdditionalAddressEle.setAttribute("IsBillTo","");
		customerAdditionalAddressEle.setAttribute("IsDefaultBillTo","");
		customerAdditionalAddressEle.setAttribute("IsDefaultShipTo","");
		customerAdditionalAddressEle.setAttribute("IsDefaultSoldTo","");
		customerAdditionalAddressEle.setAttribute("IsInherited","");
		customerAdditionalAddressEle.setAttribute("IsShipTo","");
		customerAdditionalAddressEle.setAttribute("IsSoldTo","");

		YFCElement personInfo = customerAdditionalAddressEle.createChild("PersonInfo");
		personInfo.setAttribute("AddressID", "");
		personInfo.setAttribute("AddressLine1", "");
		personInfo.setAttribute("AddressLine2", "");
		personInfo.setAttribute("AddressLine3", "");
		personInfo.setAttribute("AddressLine4", "");
		personInfo.setAttribute("AddressLine5","");
		personInfo.setAttribute("AddressLine6", "");
		personInfo.setAttribute("AlternateEmailID", "");
		personInfo.setAttribute("Beeper", "");
		personInfo.setAttribute("City", "");
		personInfo.setAttribute("Company","");
		personInfo.setAttribute("Country","");
		personInfo.setAttribute("DayPhone", "");
		personInfo.setAttribute("Department", "");
		personInfo.setAttribute("EMailID", "");
		personInfo.setAttribute("ErrorTxt", "");
		personInfo.setAttribute("EMailID", "");
		personInfo.setAttribute("EveningFaxNo", "");
		personInfo.setAttribute("EveningPhone", "");
		personInfo.setAttribute("FirstName", "");
		personInfo.setAttribute("HttpUrl", "");
		personInfo.setAttribute("IsCommercialAddress", "");
		personInfo.setAttribute("LastName", "");
		personInfo.setAttribute("Latitude", "");
		personInfo.setAttribute("Longitude", "");
		personInfo.setAttribute("MiddleName", "");
		personInfo.setAttribute("MobilePhone", "");
		personInfo.setAttribute("OtherPhone", "");
		personInfo.setAttribute("PersonInfoKey","");
		personInfo.setAttribute("PreferredShipAddress", "");
		personInfo.setAttribute("State", "");
		personInfo.setAttribute("Suffix", "");
		personInfo.setAttribute("Title", "");
		personInfo.setAttribute("UseCount", "");
		personInfo.setAttribute("VerificationStatus", "");
		personInfo.setAttribute("ZipCode", "");

		return outTempDoc.getDocument();

	}
	@Override
	public void setProperties(Properties arg0) throws Exception {
		// TODO Auto-generated method stub

	}}