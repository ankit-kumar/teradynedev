package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

public class TeradyneSourcingEngineOnChange
extends CustomCodeHelper implements
YFSBeforeChangeOrderUE{
	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");

	@Override
	public Document beforeChangeOrder(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException 
			
			{
		      setEnv(arg0);
		  	YFCDocument inYFCXML = null;
		      inYFCXML = YFCDocument.getDocumentFor(inXML);
				
				
				 
				 
				 if(inYFCXML.getDocumentElement().hasAttribute("DocumentType")&&inYFCXML.getDocumentElement().getAttribute("DocumentType").equalsIgnoreCase("0001"))
				 {
					 inYFCXML=sourcingRuleDeteronChange(inYFCXML);
					 /*call for sourcing rule determination */
					
					 
				 }
	
			
			return inYFCXML.getDocument();
			}



	protected YFCDocument sourcingRuleDeteronChange(YFCDocument inXML)
			throws YFSException 
			{
		final YFCNodeList<YFCElement> OrderLineNodeList = inXML
				.getDocumentElement().getElementsByTagName("OrderLine");

		int OrderLineListLength = OrderLineNodeList.getLength();
		String sItemID=null;
		String sOdrHdrKey=null;
		String sEXTDOC=null;
		
		String sEXTNRPM = null;
	
		String sEXTNDIC=null;
		if(OrderLineListLength>0)
			
		{
		
			for (int i = 0; i < OrderLineListLength; i++)

			{
				
				final YFCNode cNode = OrderLineNodeList.item(i);

				if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
					if (cNode instanceof YFCElement) {
						final YFCElement OdrlineElem = (YFCElement) cNode;
						
						try {
							
							if(OdrlineElem.hasAttribute("Action")&&OdrlineElem.getAttribute("Action").equalsIgnoreCase("CREATE"))
							{
								sItemID=OdrlineElem.getChildElement("Item").getAttribute("ItemID");	
								if(OdrlineElem.hasAttribute("ShipNode")&&OdrlineElem.getAttribute("ShipNode").length()>0)
								{
									
									/*do nothing*/
								}
								else{
									
									
									
									if((OdrlineElem.getChildElement("Extn")!=null)&&OdrlineElem.getChildElement("Extn").hasAttribute("SystemSerialNo")&&
											OdrlineElem.getChildElement("Extn").getAttribute("SystemSerialNo").length()>0)
									{
										String sSysSerialNo=OdrlineElem.getChildElement("Extn").getAttribute("SystemSerialNo");
										
										/*get Rcenter from IB line with serial number and item id status combination*/
										final YFCDocument getIBLineerialInput = YFCDocument
												.getDocumentFor(getIBLineSerialIP());
										getIBLineerialInput.getDocumentElement().getElementsByTagName("Item").
										item(0).setAttribute("ItemID", sItemID);
										getIBLineerialInput.getDocumentElement().getElementsByTagName("Extn").
										item(0).setAttribute("SystemSerialNo", sSysSerialNo);
										String RapidSourceCenter=	getRSCfromIBLinee(getIBLineerialInput);
										
										
										
										
										if(RapidSourceCenter!=null&&(RapidSourceCenter.length()>0))
									{
										
										
										OdrlineElem.
												getChildElement("Extn").setAttribute("RapidSourceCenter",RapidSourceCenter);
										OdrlineElem.setAttribute("ShipNode", RapidSourceCenter);
										
										
									}
								
									else
										
									if (inXML.getDocumentElement().hasAttribute("OrderHeaderKey"))
										{
											sOdrHdrKey = inXML.getDocumentElement().getAttribute(
													"OrderHeaderKey");
											
											if (getCustomerId (sOdrHdrKey)!=null) {

												final YFCDocument getCustListInput = YFCDocument
														.getDocumentFor(getCustomerIP());
												getCustListInput.getDocumentElement().setAttribute(
														"CustomerID", getCustomerId (sOdrHdrKey));
												final YFCDocument CustomerListOutput = getCustomerListCall(getCustListInput);
												if (CustomerListOutput.getDocumentElement().getElementsByTagName("Extn").getLength()>0) {
													sEXTDOC = CustomerListOutput.getDocumentElement()
															.getChildElement("Customer")
															.getChildElement("Extn").getAttribute("DefaultOrderCenter");
													
												}
											} 
										
										
										if(sEXTDOC!=null)
										{

											
											
											//String sUOM=OdrlineElem.getChildElement("Item")
												//	.getAttribute("UnitOfMeasure");	
											final YFCDocument getItemListInput = YFCDocument
													.getDocumentFor(getItemListIP());
												
											getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
											//getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);
											
											final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);
											
											if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
											{
												sEXTNRPM = ItemListOutput.getDocumentElement()
														.getChildElement("Item")
														.getChildElement("Extn").getAttribute("RepairModel");
												
												sEXTNDIC=ItemListOutput.getDocumentElement()
														.getChildElement("Item")
														.getChildElement("Extn").getAttribute("DefaultInventoryCenter");
												
												
											}
											
											if((sEXTNRPM.length()!=0&&!sEXTNRPM.equalsIgnoreCase(" "))&&(sEXTNDIC.length()!=0&&!sEXTNDIC.equalsIgnoreCase(" ")))
											{
												
												if(isDistRule(sEXTDOC+sEXTNDIC+sEXTNRPM)){
											OdrlineElem.setAttribute("DistributionRuleId", sEXTDOC+sEXTNDIC+sEXTNRPM);
												}
												
											}
									
											
											
											
											
										}
										
										
										}
										
										
										
									
								
								
								
									
								}
									else
										
											if (inXML.getDocumentElement().hasAttribute("OrderHeaderKey"))
												{
													sOdrHdrKey = inXML.getDocumentElement().getAttribute(
															"OrderHeaderKey");
													
													if (getCustomerId (sOdrHdrKey)!=null) {

														final YFCDocument getCustListInput = YFCDocument
																.getDocumentFor(getCustomerIP());
														getCustListInput.getDocumentElement().setAttribute(
																"CustomerID", getCustomerId (sOdrHdrKey));
														final YFCDocument CustomerListOutput = getCustomerListCall(getCustListInput);
														if (CustomerListOutput.getDocumentElement().getElementsByTagName("Extn").getLength()>0) {
															sEXTDOC = CustomerListOutput.getDocumentElement()
																	.getChildElement("Customer")
																	.getChildElement("Extn").getAttribute("DefaultOrderCenter");
															
														}
													} 
												
												
												if(sEXTDOC!=null)
												{

													
													
													//String sUOM=OdrlineElem.getChildElement("Item")
															//.getAttribute("UnitOfMeasure");	
													final YFCDocument getItemListInput = YFCDocument
															.getDocumentFor(getItemListIP());
														
													getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
													//getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);
													
													final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);
													
													if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
													{
														sEXTNRPM = ItemListOutput.getDocumentElement()
																.getChildElement("Item")
																.getChildElement("Extn").getAttribute("RepairModel");
														
														sEXTNDIC=ItemListOutput.getDocumentElement()
																.getChildElement("Item")
																.getChildElement("Extn").getAttribute("DefaultInventoryCenter");
														
														
													}
													
													if((sEXTNRPM.length()!=0&&!sEXTNRPM.equalsIgnoreCase(" "))&&(sEXTNDIC.length()!=0&&!sEXTNDIC.equalsIgnoreCase(" ")))
													{
														
														

														if(isDistRule(sEXTDOC+sEXTNDIC+sEXTNRPM)){
													OdrlineElem.setAttribute("DistributionRuleId", sEXTDOC+sEXTNDIC+sEXTNRPM);
														}
														
														
														
													}
											
													
													
													
													
												}
												
												
												}
							}
							
						}
							
							else if((OdrlineElem.getChildElement("Extn")!=null)&&OdrlineElem.getChildElement("Extn").hasAttribute("SystemSerialNo")&&
									OdrlineElem.getChildElement("Extn").getAttribute("SystemSerialNo").length()>0)
							{
								
								
									
								String sSysSerialNo=OdrlineElem.getChildElement("Extn").getAttribute("SystemSerialNo");

								sItemID=	getItemID(OdrlineElem.getAttribute("OrderLineKey"));
								final YFCDocument getIBLineerialInput = YFCDocument
										.getDocumentFor(getIBLineSerialIP());
								getIBLineerialInput.getDocumentElement().getElementsByTagName("Item").
								item(0).setAttribute("ItemID", sItemID);
								getIBLineerialInput.getDocumentElement().getElementsByTagName("Extn").
								item(0).setAttribute("SystemSerialNo",sSysSerialNo);
								String RapidSourceCenter=	getRSCfromIBLinee(getIBLineerialInput);
								
								
								
								
								if(RapidSourceCenter!=null&&(RapidSourceCenter.length()>0))
							{
								
								
								OdrlineElem.
										getChildElement("Extn").setAttribute("RapidSourceCenter",RapidSourceCenter);
								OdrlineElem.setAttribute("ShipNode", RapidSourceCenter);
								
								
							}
						
							else
								
							if (inXML.getDocumentElement().hasAttribute("OrderHeaderKey")&&!(isDistrGroup(OdrlineElem.getAttribute("OrderLineKey"))))
								{
									sOdrHdrKey = inXML.getDocumentElement().getAttribute(
											"OrderHeaderKey");
									
									if (getCustomerId (sOdrHdrKey)!=null) {

										final YFCDocument getCustListInput = YFCDocument
												.getDocumentFor(getCustomerIP());
										getCustListInput.getDocumentElement().setAttribute(
												"CustomerID", getCustomerId (sOdrHdrKey));
										final YFCDocument CustomerListOutput = getCustomerListCall(getCustListInput);
										if (CustomerListOutput.getDocumentElement().getElementsByTagName("Extn").getLength()>0) {
											sEXTDOC = CustomerListOutput.getDocumentElement()
													.getChildElement("Customer")
													.getChildElement("Extn").getAttribute("DefaultOrderCenter");
											
										}
									} 
								
								
								if(sEXTDOC!=null)
								{

									
								//	String sUOM=OdrlineElem.getChildElement("Item")
											//.getAttribute("UnitOfMeasure");	
									final YFCDocument getItemListInput = YFCDocument
											.getDocumentFor(getItemListIP());
										
									getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
								//	getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);
									
									final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);
									
									if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
									{
										sEXTNRPM = ItemListOutput.getDocumentElement()
												.getChildElement("Item")
												.getChildElement("Extn").getAttribute("RepairModel");
										
										sEXTNDIC=ItemListOutput.getDocumentElement()
												.getChildElement("Item")
												.getChildElement("Extn").getAttribute("DefaultInventoryCenter");
										
										
									}
									
									if((sEXTNRPM.length()!=0&&!sEXTNRPM.equalsIgnoreCase(" "))&&(sEXTNDIC.length()!=0&&!sEXTNDIC.equalsIgnoreCase(" ")))
									{
										
										

										if(isDistRule(sEXTDOC+sEXTNDIC+sEXTNRPM)){
									OdrlineElem.setAttribute("DistributionRuleId", sEXTDOC+sEXTNDIC+sEXTNRPM);
										}
										
										
										
									}
							
									
									
									
									
								}
								
								
								}
								
								
								
							
						
						
						
							
						
								
								
								
							
							
							
							}
							
							
							
							
							
						}
						catch (Exception e)
						{
							//throw new YFSException(e.getMessage());
							throw new YFSException(
									  "in correct message processing",
									  "EXTNTDYN0010",
									  e.getMessage());
						}
					}
					
					}
				
				
			}
			
			
		}
		
		
		return inXML;
			}
	
	
	
	 
	 
	 private Document getIBLineSerialIP() throws YFSException {
			final YFCDocument getduplicateSerialIP = YFCDocument
					.getDocumentFor("<OrderLine StatusQryType=\"BETWEEN\" ToStatus=\"1100.30\" FromStatus=\"1000.10\"><Order DocumentType=\"0017.ex\" EnterpriseCode=\"CSO\"/><Item /><Extn "
							+ " /></OrderLine>");
			return getduplicateSerialIP.getDocument();

	}
		
	 private Document getIBLineRSCOPTemp() throws YFSException {
			final YFCDocument getorderLineListOPTemp = YFCDocument
					.getDocumentFor("<OrderLineList TotalLineList=\"\">"
							+ "<OrderLine OrderLineKey=\"\">"
							+ "<Extn SystemSerialNo=\"\" RapidSourceCenter=\"\"  /></OrderLine>"
									+ "</OrderLineList>");
			return getorderLineListOPTemp.getDocument();

	}
	 
	 protected String getRSCfromIBLinee(YFCDocument inXML)
				throws YFSException {
			String RSC=null;
			YFCDocument getorderLineListOP = null;
			
			final Document getorderLineListOPTemp = getIBLineRSCOPTemp();
			Document getorderLineListOp = null;
			getorderLineListOp = invokeApi("getOrderLineList", inXML.getDocument(),
					getorderLineListOPTemp);
			getorderLineListOP = YFCDocument.getDocumentFor(getorderLineListOp);

			if (_cat.isVerboseEnabled()) {
				_cat.verbose("1111output to getorderlinelist call is:"
						+ getorderLineListOP.toString());
			}
	if(getorderLineListOP.getDocumentElement().hasAttribute("TotalLineList"))
	{
			int TotalLineList=Integer.parseInt(getorderLineListOP.getDocumentElement().getAttribute("TotalLineList"));

			if(TotalLineList>0)
			{
				if(getorderLineListOP.getDocumentElement().getElementsByTagName("Extn").item(0).hasAttribute("RapidSourceCenter"))
				{
					
					RSC=getorderLineListOP.getDocumentElement().getElementsByTagName("Extn").item(0).getAttribute("RapidSourceCenter");
					
				}
				
				
			}
	}
			
		
	return RSC;
		

		}
	 
	 protected String getCustomerId (String oderheaderkey)
		{
			String sCustomerId=null;
			YFCDocument inputXML=YFCDocument
					.getDocumentFor("<Order OrderHeaderKey='"+oderheaderkey+"' />");
			Document getOdrlist=null;
			YFCDocument getItemListOPtemp=YFCDocument
					.getDocumentFor("<OrderList  TotalNumberOfRecords=\"\" ><Order ReceivingNode=\"\"/></OrderList>");
			getOdrlist = invokeApi("getOrderList", inputXML.getDocument(),
					getItemListOPtemp.getDocument());
			
			if(YFCDocument.getDocumentFor(getOdrlist).getDocumentElement().getElementsByTagName("Order").getLength()>0)
			
			{
				if(YFCDocument.getDocumentFor(getOdrlist).getDocumentElement().getElementsByTagName("Order").item(0).hasAttribute("ReceivingNode")&&
						
						
						YFCDocument.getDocumentFor(getOdrlist).getDocumentElement().getElementsByTagName("Order").item(0).getAttribute("ReceivingNode").length()>0)
				
				{
					sCustomerId=YFCDocument.getDocumentFor(getOdrlist).getDocumentElement().getElementsByTagName("Order").item(0).getAttribute("ReceivingNode");
					
				}
				
			}
			
			
			
			return sCustomerId;
		}
		
	 protected YFCDocument getCustomerListCall(YFCDocument inXML)
				throws YFSException {
			YFCDocument CustListOP = null;

			final Document CALDayDtlscallOPTemp = getCustomerOP();
			Document CALDayDtlscallOP = null;
			CALDayDtlscallOP = invokeApi("getCustomerList", inXML.getDocument(),
					CALDayDtlscallOPTemp);
			CustListOP = YFCDocument.getDocumentFor(CALDayDtlscallOP);

			if (_cat.isVerboseEnabled()) {
				_cat.verbose("output to getcalendardaydtls call is:"
						+ CustListOP.toString());
			}

			return CustListOP;

		}
	 private Document getCustomerIP() throws YFSException {
			final YFCDocument getCustomerIP = YFCDocument
					.getDocumentFor("<Customer  />");
			return getCustomerIP.getDocument();
		}

		private Document getCustomerOP() throws YFSException {
			final YFCDocument getCustomerOP = YFCDocument
					.getDocumentFor("<CustomerList>"
							+ "<Customer  CustomerKey=\"\">"
							+ "<Extn DefaultOrderCenter=\"\"/>"
							+ "<CustomerAdditionalAddressList TotalNumberOfRecords=\"\">"
							+ "<CustomerAdditionalAddress CustomerAdditionalAddressID=\"\" IsShipTo=\"\" IsDefaultShipTo=\"\">"
							+ "<PersonInfo  Country=\"\" />"
							+ "</CustomerAdditionalAddress>"
							+ "</CustomerAdditionalAddressList>"
							+ "</Customer></CustomerList>");
			return getCustomerOP.getDocument();
		}

		private Document getItemListIP() throws YFSException {
			final YFCDocument getItemListIP = YFCDocument
					.getDocumentFor("<Item ItemID=\"\" UnitOfMeasure=\"\"/>");
			return getItemListIP.getDocument();
		}

		protected YFCDocument getItemListCall(YFCDocument inXML)
				throws YFSException {
			YFCDocument ItemListOP = null;

			final Document getItemListOPtemp = getItemListOP();
			Document getItemListOP = null;
			getItemListOP = invokeApi("getItemList", inXML.getDocument(),
					getItemListOPtemp);
			ItemListOP = YFCDocument.getDocumentFor(getItemListOP);

			if (_cat.isVerboseEnabled()) {
				_cat.verbose("output to getcalendardaydtls call is:"
						+ ItemListOP.toString());
			}

			return ItemListOP;

		}

		
		
		private Document getItemListOP() throws YFSException {
			final YFCDocument getItemListOP = YFCDocument
					.getDocumentFor("<ItemList TotalItemList=\"\"><Item ItemID=\"\" "
							+ "ItemKey=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\"><Extn RepairModel=\"\" DefaultInventoryCenter=\"\"/>"
							+ "</Item></ItemList>");
			return getItemListOP.getDocument();
		}
		
		
		protected Boolean isDistrGroup(String sOrderLineKey)
		{
			
			Boolean isDistr=false;
			YFCDocument inputXML=YFCDocument
			.getDocumentFor("<OrderLineDetail OrderLineKey='"+sOrderLineKey+"'/>");
	Document getOdrDtls=null;
	YFCDocument getOdrLDtlsOPtemp=YFCDocument
			.getDocumentFor("<OrderLine DistributionRuleId=\"\"/>");
	getOdrDtls = invokeApi("getOrderLineDetails", inputXML.getDocument(),
			getOdrLDtlsOPtemp.getDocument());
			
	if(getOdrDtls.getDocumentElement().hasAttribute("DistributionRuleId")){
		isDistr=true;
	
	}
			
			return isDistr;
			
		}
		
		
		protected Boolean isDistRule(String sDistId)

		{
			Boolean isDistExits=false;
			Document getDistRuleList =null;
			YFCDocument getDistListOPtemp=YFCDocument
					.getDocumentFor("<ItemShipNodeList>"
							+ "<ItemShipNode DistributionRuleId=\"\" ActiveFlag=\"\"/>"
							+ "</ItemShipNodeList>");
			YFCDocument inputXMLDisList=YFCDocument
					.getDocumentFor("<ItemShipNode  ActiveFlag=\"Y\"  DistributionRuleId='" + sDistId
								+ "' />");
			getDistRuleList = invokeApi("getDistributionList", inputXMLDisList.getDocument(),
					getDistListOPtemp.getDocument());
			
			if(YFCDocument.getDocumentFor(getDistRuleList).getDocumentElement().hasChildNodes())
			{
				
				isDistExits=true;
				
			}
			return isDistExits;
		}


		protected String getItemID(String sOrderLineKey)
				throws YFSException {
			YFCDocument ItemTypeListOP = null;
			YFCDocument inputXML=YFCDocument
					.getDocumentFor("<OrderLine OrderLineKey='"+sOrderLineKey+"'/>");
			final Document getItemtypeOPTemp = getItemtypeOP();
			Document getItemtypeOP = null;
			getItemtypeOP = invokeApi("getOrderLineList", inputXML.getDocument(),
					getItemtypeOPTemp);
			ItemTypeListOP = YFCDocument.getDocumentFor(getItemtypeOP);

			if (_cat.isVerboseEnabled()) {
				_cat.verbose("----output to getorderlinelist call is:"
						+ ItemTypeListOP.toString());
			}

			return ItemTypeListOP.getDocumentElement().getElementsByTagName("ItemDetails").item(0).getAttribute("ItemID");

		}
		

		private Document getItemtypeOP() throws YFSException {
			final YFCDocument getItemtypeOP = YFCDocument
					.getDocumentFor("<OrderLineList TotalNumberOfRecords=\"\">"
							+ "<OrderLine ><ItemDetails ItemID=\"\" ><PrimaryInformation ItemType=\"\" />"
							+ "</ItemDetails ></OrderLine></OrderLineList>");
			return getItemtypeOP.getDocument();

	}

}





