package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeCreateOrderUE;

public class TeradyneSourcingEngineOnCreate extends CustomCodeHelper implements
		YFSBeforeCreateOrderUE {
	private static YFCLogCategory _cat = YFCLogCategory
			.instance("com.yantra.CustomCode");

	public Document beforeCreateOrder(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException {
		setEnv(arg0);

		int OrderLineListLength = 0;
		YFCDocument inYFCXML = null;
		String sBuyerOrg = null;
		String sEXTDOC = null;
		String sEXTNRPM = null;
		
		String sItemID=null;
		String sEXTNDIC=null;
		inYFCXML = YFCDocument.getDocumentFor(inXML);
	
	
		
		
		
		if(inYFCXML.getDocumentElement().getAttribute("DocumentType").equalsIgnoreCase("0001"))
				{

		if (inYFCXML.getDocumentElement().hasAttribute("ReceivingNode")&&inYFCXML.getDocumentElement().getAttribute("ReceivingNode").length()>0) {

			sBuyerOrg = inYFCXML.getDocumentElement().getAttribute(
					"ReceivingNode");

			if (sBuyerOrg.length() != 0) {

				final YFCDocument getCustListInput = YFCDocument
						.getDocumentFor(getCustomerIP());
				getCustListInput.getDocumentElement().setAttribute(
						"CustomerID", sBuyerOrg);
				final YFCDocument CustomerListOutput = getCustomerListCall(getCustListInput);
				if (CustomerListOutput.getDocumentElement().hasChildNodes()) {
					sEXTDOC = CustomerListOutput.getDocumentElement()
							.getChildElement("Customer")
							.getChildElement("Extn").getAttribute("DefaultOrderCenter");
					
				}
			}

		}

		final YFCNodeList<YFCElement> OrderLineNodeList = inYFCXML
				.getDocumentElement().getElementsByTagName("OrderLine");

		OrderLineListLength = OrderLineNodeList.getLength();
		if (OrderLineListLength > 0)

		{
			for (int i = 0; i < OrderLineListLength; i++)

			{

				final YFCNode cNode = OrderLineNodeList.item(i);

				if (cNode.getNodeName().equalsIgnoreCase("OrderLine")) {
					if (cNode instanceof YFCElement) {
						final YFCElement OdrlineElem = (YFCElement) cNode;
						try {
							
							if(OdrlineElem.getChildElement("Extn").hasAttribute("SystemSerialNo")&&
									OdrlineElem.getChildElement("Extn").getAttribute("SystemSerialNo").length()>0)
							{
								String sSysSerialNo=OdrlineElem.getChildElement("Extn").getAttribute("SystemSerialNo");
								sItemID=OdrlineElem.getChildElement("Item").getAttribute("ItemID");	
								/*get Rcenter from IB line with serial number and item id status combination*/
								final YFCDocument getIBLineerialInput = YFCDocument
										.getDocumentFor(getIBLineSerialIP());
								getIBLineerialInput.getDocumentElement().getElementsByTagName("Item").
								item(0).setAttribute("ItemID", sItemID);
								getIBLineerialInput.getDocumentElement().getElementsByTagName("Extn").
								item(0).setAttribute("SystemSerialNo", sSysSerialNo);
								String RapidSourceCenter=	getRSCfromIBLinee(getIBLineerialInput);
								
								
								
								
							if(RapidSourceCenter!=null&&(RapidSourceCenter.length()>0))
							{
								
								if(!(OdrlineElem.hasAttribute("ShipNode")&&OdrlineElem.getAttribute("ShipNode").length()>0))
								{
								OdrlineElem.
										getChildElement("Extn").setAttribute("RapidSourceCenter",RapidSourceCenter);
								OdrlineElem.setAttribute("ShipNode", RapidSourceCenter);
								}
								
							}
							
							else if(sEXTDOC!=null)
							{
							
							
								String sUOM=OdrlineElem.getChildElement("Item")
										.getAttribute("UnitOfMeasure");	
							final YFCDocument getItemListInput = YFCDocument
									.getDocumentFor(getItemListIP());
								
							getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
							getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);
							
							final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);
							
							if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
							{
								sEXTNRPM = ItemListOutput.getDocumentElement()
										.getChildElement("Item")
										.getChildElement("Extn").getAttribute("RepairModel");
								
								sEXTNDIC=ItemListOutput.getDocumentElement()
										.getChildElement("Item")
										.getChildElement("Extn").getAttribute("DefaultInventoryCenter");
								
								
							}
							
							if((sEXTNRPM.length()!=0&&!sEXTNRPM.equalsIgnoreCase(" "))&&(sEXTNDIC.length()!=0&&!sEXTNDIC.equalsIgnoreCase(" ")))
							{
								
								if(isDistRule(sEXTDOC+sEXTNDIC+sEXTNRPM)){
							OdrlineElem.setAttribute("DistributionRuleId", sEXTDOC+sEXTNDIC+sEXTNRPM);
								}
								
							}
					
							}
							
							}
							
							else if(sEXTDOC!=null)
							{
								
							sItemID=OdrlineElem.getChildElement("Item").getAttribute("ItemID");	
							String sUOM=OdrlineElem.getChildElement("Item")
									.getAttribute("UnitOfMeasure");	
							final YFCDocument getItemListInput = YFCDocument
									.getDocumentFor(getItemListIP());
								
							getItemListInput.getDocumentElement().setAttribute("ItemID", sItemID);
							getItemListInput.getDocumentElement().setAttribute("UnitOfMeasure", sUOM);
							
							final YFCDocument ItemListOutput 	=	getItemListCall(getItemListInput);
							
							if(!ItemListOutput.getDocumentElement().getAttribute("TotalItemList").equalsIgnoreCase("0"))
							{

								sEXTNRPM = ItemListOutput.getDocumentElement()
										.getChildElement("Item")
										.getChildElement("Extn").getAttribute("RepairModel");
								
								sEXTNDIC=ItemListOutput.getDocumentElement()
										.getChildElement("Item")
										.getChildElement("Extn").getAttribute("DefaultInventoryCenter");
							}
							
							if((sEXTNRPM.length()!=0&&!sEXTNRPM.equalsIgnoreCase(" "))&&(sEXTNDIC.length()!=0&&!sEXTNDIC.equalsIgnoreCase(" ")))
							{
								
								
								if(isDistRule(sEXTDOC+sEXTNDIC+sEXTNRPM)){
									OdrlineElem.setAttribute("DistributionRuleId", sEXTDOC+sEXTNDIC+sEXTNRPM);
										}
								
								
								
							}
					
							}
							
							
							
							

						} catch (final Exception e) {
							e.printStackTrace();
							try {
								throw e;
							} catch (final Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();

							}
						}
					}
				}
			}
		}
				}
		
		
		return inYFCXML.getDocument();
	}

	protected YFCDocument getCustomerListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument CustListOP = null;

		final Document CALDayDtlscallOPTemp = getCustomerOP();
		Document CALDayDtlscallOP = null;
		CALDayDtlscallOP = invokeApi("getCustomerList", inXML.getDocument(),
				CALDayDtlscallOPTemp);
		CustListOP = YFCDocument.getDocumentFor(CALDayDtlscallOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getcalendardaydtls call is:"
					+ CustListOP.toString());
		}

		return CustListOP;

	}
	protected YFCDocument getItemListCall(YFCDocument inXML)
			throws YFSException {
		YFCDocument ItemListOP = null;

		final Document getItemListOPtemp = getItemListOP();
		Document getItemListOP = null;
		getItemListOP = invokeApi("getItemList", inXML.getDocument(),
				getItemListOPtemp);
		ItemListOP = YFCDocument.getDocumentFor(getItemListOP);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("output to getcalendardaydtls call is:"
					+ ItemListOP.toString());
		}

		return ItemListOP;

	}

	private Document getCustomerIP() throws YFSException 
	{
		final YFCDocument getCustomerIP = YFCDocument
				.getDocumentFor("<Customer  />");
		return getCustomerIP.getDocument();
	}

	private Document getCustomerOP() throws YFSException {
		final YFCDocument getCustomerOP = YFCDocument
				.getDocumentFor("<CustomerList>"
						+ "<Customer  CustomerKey=\"\">"
						+ "<Extn DefaultOrderCenter=\"\"/>"
						+ "<CustomerAdditionalAddressList TotalNumberOfRecords=\"\">"
						+ "<CustomerAdditionalAddress CustomerAdditionalAddressID=\"\" IsShipTo=\"\" IsDefaultShipTo=\"\">"
						+ "<PersonInfo  Country=\"\" />"
						+ "</CustomerAdditionalAddress>"
						+ "</CustomerAdditionalAddressList>"
						+ "</Customer></CustomerList>");
		return getCustomerOP.getDocument();
	}
	private Document getItemListIP() throws YFSException {
		final YFCDocument getItemListIP = YFCDocument
				.getDocumentFor("<Item ItemID=\"\" UnitOfMeasure=\"\"/>");
		return getItemListIP.getDocument();
	}

	private Document getItemListOP() throws YFSException {
		final YFCDocument getItemListOP = YFCDocument
				.getDocumentFor("<ItemList TotalItemList=\"\"><Item ItemID=\"\" "
						+ "ItemKey=\"\" OrganizationCode=\"\" UnitOfMeasure=\"\"><Extn RepairModel=\"\" DefaultInventoryCenter=\"\"/>"
						+ "</Item></ItemList>");
		return getItemListOP.getDocument();
	}
	@Override
	public String beforeCreateOrder(YFSEnvironment arg0, String arg1)
			throws YFSUserExitException {
		// TODO Auto-generated method stub
		return null;
	}

	private Document getIBLineSerialIP() throws YFSException {
		final YFCDocument getduplicateSerialIP = YFCDocument
				.getDocumentFor("<OrderLine StatusQryType=\"BETWEEN\" ToStatus=\"1100.30\" FromStatus=\"1000.10\"><Order DocumentType=\"0017.ex\" EnterpriseCode=\"CSO\"/><Item /><Extn "
						+ " /></OrderLine>");
		return getduplicateSerialIP.getDocument();

}
	
	
	
	
	private Document getIBLineRSCOPTemp() throws YFSException {
		final YFCDocument getorderLineListOPTemp = YFCDocument
				.getDocumentFor("<OrderLineList TotalLineList=\"\">"
						+ "<OrderLine OrderLineKey=\"\">"
						+ "<Extn SystemSerialNo=\"\" RapidSourceCenter=\"\"  /></OrderLine>"
						
								+ "</OrderLineList>");
		return getorderLineListOPTemp.getDocument();

}

	protected String getRSCfromIBLinee(YFCDocument inXML)
			throws YFSException {
		String RSC=null;
		YFCDocument getorderLineListOP = null;
		
		final Document getorderLineListOPTemp = getIBLineRSCOPTemp();
		Document getorderLineListOp = null;
		getorderLineListOp = invokeApi("getOrderLineList", inXML.getDocument(),
				getorderLineListOPTemp);
		getorderLineListOP = YFCDocument.getDocumentFor(getorderLineListOp);

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("1111output to getorderlinelist call is:"
					+ getorderLineListOP.toString());
		}
if(getorderLineListOP.getDocumentElement().hasAttribute("TotalLineList"))
{
		int TotalLineList=Integer.parseInt(getorderLineListOP.getDocumentElement().getAttribute("TotalLineList"));

		if(TotalLineList>0)
		{
			if(getorderLineListOP.getDocumentElement().getElementsByTagName("Extn").item(0).hasAttribute("RapidSourceCenter"))
			{
				
				RSC=getorderLineListOP.getDocumentElement().getElementsByTagName("Extn").item(0).getAttribute("RapidSourceCenter");
				
			}
			
			
		}
}
		
	
return RSC;
	

	}
	

protected Boolean isDistRule(String sDistId)

{
	Boolean isDistExits=false;
	Document getDistRuleList =null;
	YFCDocument getDistListOPtemp=YFCDocument
			.getDocumentFor("<ItemShipNodeList>"
					+ "<ItemShipNode DistributionRuleId=\"\" ActiveFlag=\"\"/>"
					+ "</ItemShipNodeList>");
	YFCDocument inputXMLDisList=YFCDocument
			.getDocumentFor("<ItemShipNode  ActiveFlag=\"Y\"  DistributionRuleId='" + sDistId
						+ "' />");
	getDistRuleList = invokeApi("getDistributionList", inputXMLDisList.getDocument(),
			getDistListOPtemp.getDocument());
	
	if(YFCDocument.getDocumentFor(getDistRuleList).getDocumentElement().hasChildNodes())
	{
		
		isDistExits=true;
		
	}
	return isDistExits;
}


}
