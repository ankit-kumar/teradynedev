package com.teradyne.om.api;

import java.lang.String;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class CreateDiscountGroupClass {

	public CreateDiscountGroupClass(){}

	public Document resolveCustomerName(final YFSEnvironment env, final Document inDoc) throws YFSException {

		Document inputDG = inDoc;
		String orgName="";	
		Element elDiscountGroup = inputDG.getDocumentElement();
		String orgCode = elDiscountGroup.getAttribute("CustomerNo");
		YIFApi createDG;

		try
		{
			orgName = getOrganizationName(env, orgCode);

			elDiscountGroup.setAttribute("CustomerName", orgName);
			createDG =YIFClientFactory.getInstance().getApi();
			//call create createDiscountGroupService service to create new Discount Group.
			createDG.executeFlow(env, "createDiscountGroupService", inputDG); 

		}catch(YFSException e){
			String errorCode = e.getErrorCode();
			String errorDescription = e.getErrorDescription();
			throw new YFSException(errorCode, errorDescription,errorDescription);

		} catch (Exception e) {

			e.printStackTrace();
		}
		return inputDG;
	}

	public static String getOrganizationName(YFSEnvironment env, String orgCode) throws Exception {
		String orgName ="";
		Document outDoc = null;
		Document inXML = null;
		YIFApi oApi;        

		try {

			oApi = YIFClientFactory.getInstance().getLocalApi();
			inXML = SCXmlUtil.createDocument("Organization");
			inXML.getDocumentElement().setAttribute("OrganizationCode",orgCode);
			//call getOrganizationList API.
			outDoc = oApi.getOrganizationList(env, inXML);
			Element outElement=(Element)outDoc;
			orgName =SCXmlUtil.getXpathAttribute(outElement,"OrganizationList/Organization/@OrganizationName");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orgName;		
	}
}