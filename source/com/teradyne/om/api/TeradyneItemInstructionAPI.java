package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.yantra.shared.ycp.YFSContext;
import com.yantra.ycp.core.YCPEntityApi;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;

public class TeradyneItemInstructionAPI extends CustomCodeHelper 

{

	private static YFCLogCategory _cat = YFCLogCategory.instance("com.yantra.CustomCode");

	@SuppressWarnings("deprecation")
	public void manageItemInstruction(YFSEnvironment arg0, Document inXML)
			throws YFSUserExitException
			{          setEnv(arg0);

			if(getItemInstructionCodeCall(YFCDocument.getDocumentFor(inXML)))
			{
				final Document getItemInstructionCodeOPTemp = getItemInstructionCodeOPTemp();
				/*does not exit create new record*/
				arg0.setApiTemplate("getItemInstructionCode", getItemInstructionCodeOPTemp);
				YFSContext ctx = (YFSContext)arg0;
				//  YFCDocument inpDoc = YFCDocument.getDocumentFor(indoc);
				YFCDocument op=    YCPEntityApi.getInstance().invoke(ctx, "createItemInstructionCode", YFCDocument.getDocumentFor(inXML));
				if (_cat.isVerboseEnabled()) {
					_cat.verbose("instruction code create Output:" + op.toString());
				}
				arg0.clearApiTemplate("getItemInstructionCode");


			}


			else
			{
				/*exits do nothing*/   
				if (_cat.isVerboseEnabled()) {
					_cat.verbose("instruction code present in database:" + inXML.toString());
				}

			}


			}
	protected Boolean getItemInstructionCodeCall(YFCDocument inXML) throws YFSException {

		if (_cat.isVerboseEnabled()) {
			_cat.verbose("input to getItemInstructionCodeCall call is:" + inXML.toString());
		}
		Boolean hasInsCode=false;
		final Document getItemInstructionCodeOPTemp = getItemInstructionCodeOPTemp();
		Document itemInsCodeOP = null;
		YFCDocument ItemInstListOP=null;
		itemInsCodeOP=	invokeApi("getItemInstructionCodeList", inXML.getDocument(),getItemInstructionCodeOPTemp);
		ItemInstListOP = YFCDocument.getDocumentFor(itemInsCodeOP);
		if(	ItemInstListOP.getDocumentElement().getAttribute("TotalNumberOfRecords").equalsIgnoreCase("0"))
		{
			hasInsCode=true;

		}
		return hasInsCode;
	}


	private Document getItemInstructionCodeOPTemp() throws YFSException
	{
		final YFCDocument getItemInstructionCodeOPTemp = YFCDocument
				.getDocumentFor("<ItemInstructionCodeList TotalNumberOfRecords=\"\">"
						+ "<ItemInstructionCode OrganizationCode=\"\" ItemInstructionCodeKey=\"\" "
						+ "InstructionType=\"\" InstructionText=\"\" InstructionCode=\"\"/>"
						+ "</ItemInstructionCodeList>");
		return getItemInstructionCodeOPTemp.getDocument();
	}

}
