package com.teradyne.om.api;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class SOItemSearchList {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  public Document prepareInputForItemList(YFSEnvironment env, Document inDoc) {

    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInEle = yfcInDoc.getDocumentElement();

    YFCElement primaryInfo = yfcInEle.getChildElement(XMLConstants.PRIMARY_INFORMATION);

    if (XmlUtils.isVoid(primaryInfo) || XmlUtils.isVoid(primaryInfo.getAttribute(XMLConstants.ITEM_TYPE))) {
      log.debug("Attaching complex query for Sales Order related Item Types from Common Code");
      
      /* insert following XML to Item Element
      <ComplexQuery Operator="AND">
      <Or>
      <Exp Name="ItemType" VALUE="BOXID" />
      <Exp Name="ItemType" Value="SPAREPART"/>
      <Exp Name="ItemType" Value="COMPONENT"/>
      <Exp Name="ItemType" Value="FRU"/>
      </Or></ComplexQuery>*/
      YFCDocument output = getSOItemTypes(env);
      YFCElement outputEle = output.getDocumentElement();
      
      if(!XmlUtils.isVoid(outputEle)){
        
        YFCNodeList<YFCElement> list = outputEle.getElementsByTagName(XMLConstants.COMMON_CODE);
        log.debug("Commoncode List length:"+list.getLength());
        if(list.getLength() != 0){
          YFCElement complexEle = yfcInEle.createChild(XMLConstants.COMPLEX_QUERY);
          complexEle.setAttribute(XMLConstants.OPERATOR, XMLConstants.AND_OPERATOR);
          
          YFCElement orEle = complexEle.createChild(XMLConstants.OR_ELEMENT);
          for (int i = 0; i < list.getLength(); i++) {
            
            YFCElement commonCode = list.item(i);
            
            YFCElement expEle = orEle.createChild(XMLConstants.EXP_ELEMENT);
            expEle.setAttribute(XMLConstants.NAME, XMLConstants.ITEM_TYPE);
            expEle.setAttribute(XMLConstants.VALUE, commonCode.getAttribute(XMLConstants.CODE_VALUE));
          }
        }    
      } 
    }
    
    if(log.isVerboseEnabled()){
      log.verbose("Prepared Input:"+yfcInDoc.getString());
    }
    return yfcInDoc.getDocument();
  }
  
  private YFCDocument getSOItemTypes(YFSEnvironment env){
    
    String strInput = "<CommonCode CodeType=\""+XMLConstants.SO_ITEM_TYPE+"\" />";
    String strTemp  = "<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\"/></CommonCodeList>";
    
    YFCDocument inputDoc = YFCDocument.getDocumentFor(strInput);
    YFCDocument templateDoc = YFCDocument.getDocumentFor(strTemp);
    
    try {
      Document output = orderUtils.callApi(env, inputDoc.getDocument(), templateDoc.getDocument(), Constants.API_GET_COMMON_CODE_LIST, Constants.TRUE);
      if(!XmlUtils.isVoid(output)){
        return YFCDocument.getDocumentFor(output);
      }
    } catch (Exception e) {
      if(log.isVerboseEnabled()){
        log.verbose(e);
      }
    }
    return null;
  }
}
