package com.teradyne.om.api;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import com.sterlingcommerce.baseutil.SCXmlUtil;
import com.bridge.sterling.utils.DateTimeUtil;
import com.bridge.sterling.utils.SterlingUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfs.japi.YFSEnvironment;

public class ExportInfo {
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ExportInfo.class);

	/*
	 * (non-Javadoc)
	 * @see com.yantra.vas.japi.ue.VASBeforeCreateWorkOrderUE#beforeCreateWorkOrder(com.yantra.yfs.japi.YFSEnvironment, org.w3c.dom.Document)
	 * Fields to be exported:
	 * REPAIR_ORDER_NO, PROD_NO, PROD_SERIAL_NO, REFERENCE_NO
	 * ORDER_CENTER, DATE_TO_REPAIR, DOA_FLAG, OEM_SERIAL_NO
	 * CUST_NO, CUST_ADD_NO, SYSTEM_SERIAL_NO, SYSTEM_TYPE
	 * SPECIFIC_REVIEW_FLAG, RAW_REV_LEVEL, SERVICE_TYPE, RESP_PRODUCT_REP_DIVISION_CODE
	 * 
	 * 
	 */
	public String ExportWorkOrderInfo(YFSEnvironment env, Document inputXML) throws Exception {

		//Currently implemented at the User Exit: com.yantra.yfs.japi.ue.YFSBeforeReceiveOrderUE
		//The input xml that is received is <Receipt>
		Element eleInputXml = inputXML.getDocumentElement();
		Element eleReceiptOrderLine = (Element) eleInputXml.getElementsByTagName("OrderLine").item(0);

		//Obtain the return order's information
		Document returnDoc = SCXmlUtil.createDocument("Order");
		//Build return order template
		Document returnTemplateDoc = SCXmlUtil.createDocument("Order");
		Element rootReturnNode = returnTemplateDoc.getDocumentElement();

		Element orderExtnNode = returnTemplateDoc.createElement("Extn");
		rootReturnNode.appendChild(orderExtnNode);

		Element orderLinesNode = returnTemplateDoc.createElement("OrderLines");
		rootReturnNode.appendChild(orderLinesNode);

		Element orderLineNode = returnTemplateDoc.createElement("OrderLine");
		orderLinesNode.appendChild(orderLineNode);

		Element orderLineExtnNode = returnTemplateDoc.createElement("Extn");
		orderLineNode.appendChild(orderLineExtnNode);

		Element itemNode = returnTemplateDoc.createElement("Item");
		orderLineNode.appendChild(itemNode);

		Element orderItemExtnNode = returnTemplateDoc.createElement("Extn");
		itemNode.appendChild(orderItemExtnNode);

		Element itemDetailsNode = returnTemplateDoc.createElement("ItemDetails");
		orderLineNode.appendChild(itemDetailsNode);

		Element inventoryTagNode = returnTemplateDoc.createElement("InventoryTagAttributes");
		itemDetailsNode.appendChild(inventoryTagNode);

		//Build return order input
		Element returnXML = returnDoc.getDocumentElement();
		returnXML.setAttribute("OrderHeaderKey", eleReceiptOrderLine.getAttribute("OrderHeaderKey"));

		//Call api getOrderDetails
		YFCDocument returnOrderYFCDoc = SterlingUtil.callAPI(env,"getOrderDetails", YFCDocument.getDocumentFor(returnDoc), YFCDocument.getDocumentFor(returnTemplateDoc));

		Document returnOrderDoc=returnOrderYFCDoc.getDocument();
		Element eleReturnOrder = returnOrderDoc.getDocumentElement();


		Element eleInputOrderLine = (Element) eleReturnOrder.getElementsByTagName("OrderLine").item(0);
		//For work order no, create from the current date and time
		String strWorkOrderNo = DateTimeUtil.getCurrentDateAndTime();
		strWorkOrderNo= strWorkOrderNo.replaceAll("-", "");
		strWorkOrderNo= strWorkOrderNo.replaceAll(":", "");
		String strNodeKey = eleInputOrderLine.getAttribute("ShipNode"); //ShipNode on the OrderLIne
		String strEnterpriseCode = eleReturnOrder.getAttribute("EnterpriseCode");
		String strServiceItemGroupCode = "KIT";
		String strServiceItemID = "1234";
		String strPurpose = "ORDER";
		String strQuantityRequested = "1";

		Element eleInputItem = (Element) eleReturnOrder.getElementsByTagName("Item").item(0);
		String strItemID = eleInputItem.getAttribute("ItemID");
		String strProductClass = eleInputItem.getAttribute("ProductClass");
		String strUOM = eleInputItem.getAttribute("UnitOfMeasure");

		//Create Work Order
		Document createDoc = SCXmlUtil.createDocument("WorkOrder");
		Element createXML = createDoc.getDocumentElement();
		//create input xml
		createXML.setAttribute("WorkOrderNo", strWorkOrderNo);
		createXML.setAttribute("NodeKey", strNodeKey);
		createXML.setAttribute("EnterpriseCode", strEnterpriseCode);
		createXML.setAttribute("ServiceItemGroupCode", strServiceItemGroupCode);
		createXML.setAttribute("ServiceItemID", strServiceItemID);
		createXML.setAttribute("Purpose", strPurpose);
		createXML.setAttribute("QuantityRequested", strQuantityRequested);
		createXML.setAttribute("ItemID", strItemID);
		createXML.setAttribute("ProductClass", strProductClass);
		createXML.setAttribute("Uom", strUOM);

		//Create Work Order template
		Document createTemplateDoc = SCXmlUtil.createDocument("WorkOrder");

		//Call the api createWorkOrder
		YFCDocument workOrderYFCDoc = SterlingUtil.callAPI(env, "createWorkOrder",YFCDocument.getDocumentFor(createDoc), YFCDocument.getDocumentFor(createTemplateDoc));
		Document workOrderDoc=workOrderYFCDoc.getDocument();
		@SuppressWarnings("unused")
		Element eleWorkOrder = workOrderDoc.getDocumentElement();

		//ExportXMLNodeAsTextFile(eleWorkOrder, "WorkOrder", "WorkOrder-" + strWorkOrderNo);
		String resultFile = ExportWorkOrderAsTextFile(eleReturnOrder, strWorkOrderNo, "WorkOrder-" + strWorkOrderNo);
		//ExportXMLAsTextFile(inputXML);
		//return docPrintServiceOutput;

		sendMsgToQueue(resultFile);
		return resultFile;
	}

	public String ExportWorkOrderAsTextFile(Element returnXML, String workOrderNo, String filename)
	{
		String attributeList="";
		String attributeValueList="";
		BufferedWriter writer = null;
		try {
			//create a temporary file
			File workOrderFile = new File(filename + ".csv");

			// This will output the full path where the file will be written to...

			//Add Order Header attributes to the file

			//Add Repair Order No : YFS_ORDER_HEADER. ORDER_NO 
			attributeList = attributeList + "OrderNo" + ",";
			attributeValueList = attributeValueList + returnXML.getAttribute("OrderNo") + ",";

			//Add Reference No: YFS_ORDER_HEADER_CUSTOMER_PO_NO
			attributeList = attributeList + "CustomerPONo" + ",";
			attributeValueList = attributeValueList + returnXML.getAttribute("CustomerPONo") + ",";

			//Add Date To Repair: YFS_ORDER_HEADER. ORDER_DATE
			attributeList = attributeList + "DateToRepair" + ",";
			attributeValueList = attributeValueList + returnXML.getAttribute("OrderDate") + ",";

			//Add DOA Flag : YFS_ORDER_HEADER. ORDER_TYPE
			attributeList = attributeList + "DOAFlag" + ",";
			attributeValueList = attributeValueList + returnXML.getAttribute("OrderType") + ",";

			//Add Customer No: YFS_ORDER_HEADER. SHIP_TO_ID
			attributeList = attributeList + "CustNo" + ",";
			attributeValueList = attributeValueList + returnXML.getAttribute("ShipToID") + ",";            

			//Add Customer Site No: YFS_ORDER_HEADER. SHIP_TO_ID
			attributeList = attributeList + "CustSiteNo" + ",";
			attributeValueList = attributeValueList + returnXML.getAttribute("ShipToID") + ",";  

			//Add Order Header Extn attributes to file
			//Add System Serial No: YFS_ORDER_HEADER. EXTN_SYSTEM_SERIAL_NO

			Element eleReturnOrderExtn = (Element) returnXML.getElementsByTagName("Extn").item(0);
			if(eleReturnOrderExtn != null)
			{
				//Add System Serial No: YFS_ORDER_HEADER. EXTN_SYSTEM_SERIAL_NO
				attributeList = attributeList + "SystemSerialNumber" + ",";
				attributeValueList = attributeValueList + eleReturnOrderExtn.getAttribute("SystemSerialNumber") + ",";  

				//Add System Type: YFS_ORDER_HEADER. EXTN_SYSTEM_TYPE
				attributeList = attributeList + "SystemType" + ",";
				attributeValueList = attributeValueList + eleReturnOrderExtn.getAttribute("SystemType") + ",";

				//Add Specific Review Flag: YFS_ORDER_HEADER. EXTN_SPEC_REVEIW_FLAG
				attributeList = attributeList + "SpecReviewFlag" + ",";
				attributeValueList = attributeValueList + eleReturnOrderExtn.getAttribute("SpecReviewFlag") + ",";
			}
			else
			{
				//Add System Serial No: YFS_ORDER_HEADER. EXTN_SYSTEM_SERIAL_NO
				attributeList = attributeList + "SystemSerialNumber" + ",";
				attributeValueList = attributeValueList + ",";  

				//Add System Type: YFS_ORDER_HEADER. EXTN_SYSTEM_TYPE
				attributeList = attributeList + "SystemType" + ",";
				attributeValueList = attributeValueList + ",";

				//Add Specific Review Flag: YFS_ORDER_HEADER. EXTN_SPEC_REVEIW_FLAG
				attributeList = attributeList + "SpecReviewFlag" + ",";
				attributeValueList = attributeValueList + ",";
			}


			//Add Order Line extn attributes to file

			Element eleReturnOrderLine = (Element) returnXML.getElementsByTagName("OrderLine").item(0);
			Element eleReturnOrderLineExtn = (Element) eleReturnOrderLine.getElementsByTagName("Extn").item(0);

			if(eleReturnOrderLineExtn != null)
			{
				//Add Prod_Serial_No: YFS_ORDER_LINE. EXTN_PROD_SERIAL_NO
				attributeList = attributeList + "ProdSerialNumber" + ",";
				attributeValueList = attributeValueList + eleReturnOrderLineExtn.getAttribute("ProdSerialNumber") + ",";


				//Add Order Center : YFS_ORDER_LINE. REPAIR_CENTER_CODE (Doesn't exist oob)
				attributeList = attributeList + "RepairCenterCode" + ",";
				attributeValueList = attributeValueList + eleReturnOrderLineExtn.getAttribute("RepairCenterCode") + ",";


				//Add OEM Serial No : YFS_ORDER_LINE. EXTN_SERIAL_NBR
				attributeList = attributeList + "SerialNo" + ",";
				attributeValueList = attributeValueList + eleReturnOrderLineExtn.getAttribute("SerialNo") + ",";

				//Add Service Type: YFS_ORDER_LINE. EXTN_SERVICE_TYPE
				attributeList = attributeList + "ServiceType" + ",";
				attributeValueList = attributeValueList + eleReturnOrderLineExtn.getAttribute("ServiceType") + ",";
			}
			else
			{
				//Add Prod_Serial_No: YFS_ORDER_LINE. EXTN_PROD_SERIAL_NO
				attributeList = attributeList + "ProdSerialNumber" + ",";
				attributeValueList = attributeValueList + ",";


				//Add Order Center : YFS_ORDER_LINE. REPAIR_CENTER_CODE (Doesn't exist oob)
				attributeList = attributeList + "RepairCenterCode" + ",";
				attributeValueList = attributeValueList + ",";


				//Add OEM Serial No : YFS_ORDER_LINE. EXTN_SERIAL_NBR
				attributeList = attributeList + "SerialNo" + ",";
				attributeValueList = attributeValueList + ",";

				//Add Service Type: YFS_ORDER_LINE. EXTN_SERVICE_TYPE
				attributeList = attributeList + "ServiceType" + ",";
				attributeValueList = attributeValueList + ",";
			}

			//Add Item attributes to file                 

			//Add Prod_No (Item ID): YFS_ORDER_LINE. ITEM_ID
			Element eleReturnItem = (Element) returnXML.getElementsByTagName("Item").item(0);
			attributeList = attributeList + "ItemID" + ",";
			attributeValueList = attributeValueList + eleReturnItem.getAttribute("ItemID") + ",";

			//Add Item extn attributes to file
			Element eleReturnItemExtn = (Element) eleReturnItem.getElementsByTagName("Extn").item(0);

			if(eleReturnItemExtn != null)
			{
				//Add RESP_PRODUCT_REP_DIVISION_CODE: YFS_ITEM. EXTN.DESP_PRODUCT_REP_DIVISION_CODE
				attributeList = attributeList + "RespProdRepDivCode" + ",";
				attributeValueList = attributeValueList + eleReturnItemExtn.getAttribute("RespProdRepDivCode") + ",";
			}
			else
			{
				//Add RESP_PRODUCT_REP_DIVISION_CODE: YFS_ITEM. EXTN.DESP_PRODUCT_REP_DIVISION_CODE
				attributeList = attributeList + "RespProdRepDivCode" + ",";
				attributeValueList = attributeValueList + ",";	
			}

			//Add Inventory Tag attributes to file
			//Add Raw Rev Level: YFS_INVENTORY_TAG. REVISION_NO
			Element eleReturnInvTag = (Element) returnXML.getElementsByTagName("InventoryTagAttributes").item(0);
			if(eleReturnInvTag != null)
			{
				attributeList = attributeList + "RevisionNo" + ",";
				attributeValueList = attributeValueList + eleReturnInvTag.getAttribute("RevisionNo") + ",";
			}
			else
			{
				attributeList = attributeList + "RevisionNo" + ",";
				attributeValueList = attributeValueList + ",";
			}            
			writer = new BufferedWriter(new FileWriter(workOrderFile));
			writer.write(attributeList + "\n");
			writer.write(attributeValueList);

			return attributeList + "\n" + attributeValueList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				// Close the writer regardless of what happens...
				writer.close();
			} catch (Exception e) {
			}
		}
		return null;		

	}

	public void ExportXMLNodeAsTextFile(Element inputXML, String node, String fileName) throws Exception {
		String attributeList="";
		String attributeValueList="";
		String currentNode = inputXML.getNodeName();
		BufferedWriter writer = null;
		try {
			//create a temporary file
			File workOrderFile = new File(fileName + ".txt");

			// This will output the full path where the file will be written to...
			writer = new BufferedWriter(new FileWriter(workOrderFile));
			if(currentNode.equals(node))
			{
				// get a map containing the attributes of the node
				NamedNodeMap attributes = inputXML.getAttributes();

				// get the number of nodes in this map
				int numAttrs = attributes.getLength();

				for (int i = 0; i < numAttrs; i++) {
					Attr attr = (Attr) attributes.item(i);
					String attrName = attr.getNodeName();
					String attrValue = attr.getNodeValue();
					attributeList = attributeList + attrName + ",";
					attributeValueList = attributeValueList + attrValue + ",";
				}
			}
			else
			{
				NodeList nl = inputXML.getElementsByTagName(node);
				int num = nl.getLength();
				for (int i = 0; i < num; i++) 
				{
					Element nodeEle = (Element) nl.item(i); 

					// get a map containing the attributes of the node
					NamedNodeMap attributes = nodeEle.getAttributes();

					// get the number of nodes in this map
					int numAttrs = attributes.getLength();

					for (int j = 0; j < numAttrs; i++) {
						Attr attr = (Attr) attributes.item(i);
						String attrName = attr.getNodeName();
						String attrValue = attr.getNodeValue();
						attributeList = attributeList + attrName + ",";
						attributeValueList = attributeValueList + attrValue + ",";
					}

				}
			}
			writer.write(attributeList + "\n");
			writer.write(attributeValueList);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// Close the writer regardless of what happens...
				writer.close();
			} catch (Exception e) {
			}
		}		

	}

	public void sendMsgToQueue(String msgToQueue) throws Exception
	{
		String destinationName = "queue/A";

		Context ic = null;
		ConnectionFactory cf = null;
		Connection connection = null;

		try
		{

			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			env.put(Context.PROVIDER_URL, "jnp://localhost:1099");
			env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

			ic = new InitialContext(env);

			cf = (ConnectionFactory)ic.lookup("ConnectionFactory");
			javax.jms.Queue queue = (javax.jms.Queue)ic.lookup(destinationName);

			connection = cf.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer publisher = session.createProducer((Destination) queue);

			connection.start();
			TextMessage message = session.createTextMessage(msgToQueue);
			publisher.send(message);

			//Scanner keyIn = new Scanner(System.in);

			//keyIn.next();
		}
		finally
		{
			if(ic != null)
			{
				try
				{
					ic.close();
				}
				catch(Exception e)
				{
					throw e;
				}
			}

			try
			{
				if(connection != null)
				{
					connection.close();
				}
			}
			catch(JMSException jmse)
			{
			}
		}
	}
}
