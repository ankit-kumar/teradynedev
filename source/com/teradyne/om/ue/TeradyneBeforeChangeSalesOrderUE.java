package com.teradyne.om.ue;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.api.CustomCodeHelper;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;
import com.yantra.yfs.japi.YFSUserExitException;
import com.yantra.yfs.japi.ue.YFSBeforeChangeOrderUE;

public class TeradyneBeforeChangeSalesOrderUE extends CustomCodeHelper implements
    YFSBeforeChangeOrderUE {
  SalesOrderUtils salesOrderUtils = new SalesOrderUtils();
  
  @Override
  public Document beforeChangeOrder(YFSEnvironment env, Document inDoc) throws YFSUserExitException {
    
   // if(isRPDEligible(env, inDoc)){
    // Need to use BO service..
    IsVaildSystemSerial(env, inDoc);
      inDoc = salesOrderUtils.setPlannerCode(env, inDoc);  
      inDoc = salesOrderUtils.setDueDate(env, inDoc);
      inDoc = salesOrderUtils.setTeradyneBusGrpAndSystemType(env, inDoc);      
      inDoc = salesOrderUtils.updateQtyOrderedToday(env, inDoc);
    inDoc = salesOrderUtils.setAutoNoChargeCode(env, inDoc);
    // }
    
    return inDoc;
  }
  
  public void IsVaildSystemSerial(YFSEnvironment env, Document inDoc) {
    
    YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc);
    YFCElement yfcInDocEle = yfcInDoc.getDocumentElement();
    
    String strshipToId = yfcInDocEle.getAttribute(XMLConstants.SHIP_TO_ID);
    YFCElement orderLines = yfcInDocEle.getChildElement(XMLConstants.ORDER_LINES);
    
    if (!XmlUtils.isVoid(orderLines) && !XmlUtils.isVoid(strshipToId)) {
      
      YFCIterable<YFCElement> orderlineList = orderLines.getChildren(XMLConstants.ORDER_LINE);
      
      for (YFCElement orderLine : orderlineList) {
    
        YFCElement extnEle = orderLine.getChildElement(XMLConstants.EXTN);
    
        if (!XmlUtils.isVoid(extnEle)
            && !XmlUtils.isVoid(extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO))) {
          String input =
              "<Input xmlns=\"" + Constants.SPWP_IS_VAILD_SYSTEM_SERIAL_NAMESPACE + "\">"
                  + "<IsVaildSystemSerial SYSTEM_CUSTOMER_NO=\"" + strshipToId
                  + "\" SYSTEM_SERIAL_NO=\"" + extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO)
                  + "\"/>" + "</Input>";
          YFCDocument inputDoc = YFCDocument.getDocumentFor(input);
          Document output;
          try {
            output =
                salesOrderUtils.callApi(env, inputDoc.getDocument(), null,
                    Constants.SPWP_IS_VAILD_SYSTEM_SERIAL, Constants.FALSE);
            NodeList list = output.getElementsByTagName(XMLConstants.IS_VALID_SYSTEM_SERIAL);

            if (list.getLength() == 0) {

              YFSException ex = new YFSException();
              ex.setErrorCode("TD00004");
              ex.setErrorDescription(extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO)
                  + "is not Valid serial");
              throw ex;
            }
          } catch (YFSException ex) {
            throw ex;
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
  }

  /*
   * private boolean isRPDEligible(YFSEnvironment env, Document inDoc){
   * 
   * YFCDocument yfcInDoc = YFCDocument.getDocumentFor(inDoc); YFCElement yfcInEle =
   * yfcInDoc.getDocumentElement();
   * 
   * YFCElement orderLinesEle = yfcInEle.getChildElement("OrderLines");
   * if(!XmlUtils.isVoid(orderLinesEle)){
   * 
   * YFCNodeList<YFCElement> list = orderLinesEle.getElementsByTagName("OrderLine");
   * 
   * for (int i = 0; i < list.getLength(); i++) { YFCElement element = list.item(i);
   * 
   * //TODO code pending... What to do.. thr } } return true; }
  */
}





