package com.teradyne.om.condition;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import org.w3c.dom.Document;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.Constants;
import com.teradyne.utils.XMLConstants;
import com.yantra.ycp.japi.YCPDynamicCondition;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;

public class DuplicateOrderHold implements YCPDynamicCondition {

  private static YFCLogCategory log = YFCLogCategory.instance("com.yantra.CustomCode");
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String desc, Map arg2, String inXML) {

    if (!XmlUtils.isVoid(inXML)) {

      YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
      YFCElement inDocElement = inDoc.getDocumentElement();

      YFCElement order = inDocElement.getChildElement(XMLConstants.ORDER);
      YFCElement item = inDocElement.getChildElement(XMLConstants.ITEM);

      int noOfOrders = getNoOfOrders(env, order, item);

      return (noOfOrders != 0);
    }
    return false;
  }

  private int getNoOfOrders(YFSEnvironment env, YFCElement orderEle, YFCElement itemEle) {
    int count = 0;
    try {
      if (!XmlUtils.isVoid(orderEle) && !XmlUtils.isVoid(itemEle)) {
        
        String strSerialNo = "";
        YFCElement extnEle = itemEle.getChildElement(XMLConstants.EXTN);
        
        if(!XmlUtils.isVoid(extnEle)){
          strSerialNo = extnEle.getAttribute(XMLConstants.SYSTEM_SERIAL_NO);
        }
        
        String strToDate = getToDate(orderEle.getAttribute(XMLConstants.ORDER_DATE))+Constants.TO_TIME_STAMP;
        String strFromDate = getFromDate(strToDate)+Constants.FROM_TIME_STAMP;

        
        YFCDocument input =
            YFCDocument.getDocumentFor("<Order DocumentType=\""
                + orderEle.getAttribute("DocumentType") + "\" EnterpriseCode=\""
                + orderEle.getAttribute("EnterpriseCode") + "\" FromOrderDate=\"" + strFromDate
                + "\" OrderDateQryType=\"BETWEEN\" ToOrderDate=\"" + strToDate + "\" >" +
                		"<Item ItemID=\""+itemEle.getAttribute("ItemID")+"\" /><Extn SystemSerialNo=\""+strSerialNo+"\"/></Order>");
        YFCDocument template =
            YFCDocument.getDocumentFor("<OrderList><Order OrderNo=\"\" /></OrderList>");
        
        Document output = orderUtils.callApi(env, input.getDocument(), template.getDocument(), Constants.GET_ORDER_LIST,Constants.TRUE);
        YFCDocument outputDoc = YFCDocument.getDocumentFor(output);
        YFCElement element = outputDoc.getDocumentElement();
        
        YFCNodeList<YFCElement> list = element.getElementsByTagName(XMLConstants.ORDER);
        return list.getLength();
      }
    } catch (Exception e) {
      if(log.isVerboseEnabled()){
        log.verbose(e);
      }
      throw new YFCException(e);
    }

    return count;
  }

  private String getFromDate(String strOrderDate) throws ParseException {

    Date date = getDateFormString(strOrderDate);

    Calendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, Constants.NO_OF_DAYS_BEFORE);
    return getStringFromDate(calendar.getTime());
  }

  private String getToDate(String strOrderDate) {
    String orderDate = null;
    if (!XmlUtils.isVoid(strOrderDate)) {
      orderDate = strOrderDate.substring(0, strOrderDate.indexOf("T"));
    } else {
      Calendar now = Calendar.getInstance();
      orderDate = getStringFromDate(now.getTime());
    }
    return orderDate;
  }

  private Date getDateFormString(String strDate) throws ParseException {
    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    return format.parse(strDate);
  }

  private String getStringFromDate(Date date) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
    return dateFormat.format(date);
  }

}
