package com.teradyne.om.condition;

import java.util.Map;

import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.teradyne.om.util.SalesOrderUtils;
import com.teradyne.utils.XMLConstants;
import com.yantra.ycp.japi.YCPDynamicCondition;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSEnvironment;

public class InternationalAppovalPending implements YCPDynamicCondition {
  
  private SalesOrderUtils orderUtils = new SalesOrderUtils();

  @Override
  public boolean evaluateCondition(YFSEnvironment env, String arg1, Map arg2, String inXML) {
    
    if(!XmlUtils.isVoid(inXML)){
      YFCDocument inDoc = YFCDocument.getDocumentFor(inXML);
      YFCElement inEle = inDoc.getDocumentElement();
      
      String strEnteredBy = inEle.getAttribute(XMLConstants.ENTERED_BY);
      if(!XmlUtils.isVoid(strEnteredBy) && orderUtils.validateUser(env,strEnteredBy)){
        
        YFCElement personInfoShipTo = inEle.getChildElement(XMLConstants.PERSON_INFO_SHIP_TO);
        YFCElement personInfoBillTo = inEle.getChildElement(XMLConstants.PERSON_INFO_BILL_TO);
        
        if(!XmlUtils.isVoid(personInfoBillTo) && !XmlUtils.isVoid(personInfoShipTo) ){
          if(!personInfoBillTo.getAttribute(XMLConstants.COUNTRY).equalsIgnoreCase(personInfoShipTo.getAttribute(XMLConstants.COUNTRY))){
            return true;
          }
        }
      }
    }
    return false;
  }

}
