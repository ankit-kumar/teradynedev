<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0">
	<xsl:template match="/TerProductGroupList">
	<xsl:element name="TerProductGroup">
		<xsl:for-each select="TerProductGroup">
			<xsl:copy-of select="@*" />
		</xsl:for-each>
	</xsl:element>
	</xsl:template>
</xsl:stylesheet>
