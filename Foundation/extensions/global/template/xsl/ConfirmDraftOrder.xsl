<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml"/>	
<xsl:template match="/Order">
 	<xsl:element name="ConfirmDraftOrder">
  		<xsl:attribute name="DocumentType">
         			<xsl:value-of select="@DocumentType"/>
      		</xsl:attribute>
<xsl:attribute name="EnterpriseCode">
         			<xsl:value-of select="@EnterpriseCode"/>
      		</xsl:attribute>
<xsl:attribute name="OrderHeaderKey">
         			<xsl:value-of select="@OrderHeaderKey"/>
      		</xsl:attribute>
   </xsl:element>

</xsl:template>
</xsl:stylesheet>

