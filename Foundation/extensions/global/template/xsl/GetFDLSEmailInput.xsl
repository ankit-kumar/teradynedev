<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetFDLSEmail/SPWPGetFDLSEmail/input">
	<xsl:template match="/in:Input">
		<xsl:element name="OrderLine">
			<xsl:attribute name="DocumentType">
			<xsl:text>0017.ex</xsl:text>
		</xsl:attribute>
			<xsl:element name="Extn">
				<xsl:attribute name="SystemType">
					<xsl:value-of select="in:GetFDLSEmail/@SYSTEM_TYPE" />
				</xsl:attribute>
			
				<xsl:attribute name="SystemSerialNo">
					<xsl:value-of select="in:GetFDLSEmail/@SYSTEM_SERIAL_NO" />
				</xsl:attribute>

			</xsl:element>

		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
