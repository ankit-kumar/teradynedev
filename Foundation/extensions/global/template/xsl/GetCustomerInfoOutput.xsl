<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
	<xsl:for-each select="Customer">
		<xsl:if test="BuyerOrganization/Extn/@CustomerSiteStatus='A'">

						<xsl:element name="Customer">
								<xsl:attribute name="TimeZoneForCustomerSite">
									<xsl:value-of select="BuyerOrganization/@LocaleCode"/>
								</xsl:attribute>
								<xsl:attribute name="InvoicePrompt">
									<xsl:value-of select="Extn/@SpecialCustInstr"/>
								</xsl:attribute>
								<xsl:attribute name="InvoicePromptCustomerDisplayableFlag">
									<xsl:value-of select="Extn/@SpecialInstrVisible"/>
								</xsl:attribute>
								<xsl:attribute name="PTExceptionFlag">
									<xsl:value-of select="Extn/@IsTransExceptionAllowed"/>
								</xsl:attribute>
								<xsl:attribute name="CustomerName">
									<xsl:value-of select="BuyerOrganization/@OrganizationName"/>
								</xsl:attribute>
								<xsl:attribute name="InternalCustomerFlag">
									<xsl:value-of select="Extn/@InternalCustomerFlag"/>
								</xsl:attribute>
						 </xsl:element>
		</xsl:if> 

									</xsl:for-each>		
										</xsl:element>
  											</xsl:template>
												</xsl:stylesheet>
