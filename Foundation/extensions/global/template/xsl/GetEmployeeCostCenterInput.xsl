<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetEmployeeCostCenter/SPWPGetEmployeeCostCenter/input">
   <xsl:template match="/in:Input">
   <xsl:element name="User">  
      <xsl:attribute name="Loginid">
		   <xsl:value-of select="in:GetEmployeeCostCenter/@EmployeeID"/>
	</xsl:attribute>		
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>
