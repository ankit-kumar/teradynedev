<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
		<xsl:for-each select="OrderLineList/OrderLine">
			<xsl:if test="Order/OrderLines/OrderLine/ItemDetails/PrimaryInformation/@ItemType='FRU' or Order/OrderLines/OrderLine/ItemDetails/PrimaryInformation/@ItemType='SYSTEM'  ">
					<xsl:element name="GetOptionNumber">
								<xsl:attribute name="OptionSerialNumber">
									<xsl:value-of select="@SerialNo"/>
								</xsl:attribute>
								<xsl:attribute name="StatusFlag">
									<xsl:value-of select="Extn/@MktStatus"/>
								</xsl:attribute>
					</xsl:element>
			</xsl:if>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
