<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetPartNotes/SPWPGetPartNotes/input">
		<xsl:template match="/in:Input">
		<xsl:element name="MultiApi">
			<xsl:for-each select="in:GetPartNotes">
				<xsl:element name="API">
					<xsl:attribute name="Name">getItemList</xsl:attribute>
					<xsl:element name="Input">
						<xsl:element name="Item">
							<xsl:attribute name="ItemID">
								<xsl:value-of select="@PartNumber"/>
							</xsl:attribute>
							<xsl:attribute name="OrganizationCode">CSO</xsl:attribute>
						</xsl:element>
					</xsl:element>					
					<xsl:element name="Template">
						<xsl:element name="ItemList">
							<xsl:element name="Item">				
								<xsl:attribute name="ItemID"/>
								<xsl:element name="ItemInstructionList">
									<xsl:element name="ItemInstruction">
										<xsl:attribute name="InstructionType"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>								
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
