<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetAllSystemSerialNumbers/SPWPGetAllSystemSerialNumbers/input">
	<xsl:template match="/in:Input">
		<xsl:element name="Order">
			<!-- Change the doc type before delivery -->
			<xsl:attribute name="DocumentType">
				<xsl:text>0017.ex</xsl:text>
			</xsl:attribute>	  
			<xsl:attribute name ="ShipToIDQryType">FLIKE</xsl:attribute>
			<xsl:attribute name="ShipToID">
				<xsl:value-of select="in:GetAllSystemSerialNumbers/@SYSTEM_CUSTOMER_NO" />
			</xsl:attribute>
			<xsl:element name="OrderLine">
				<xsl:element name="Extn">
					<xsl:attribute name="MktStatus">
						<xsl:text>A</xsl:text>
					</xsl:attribute>
				</xsl:element>
				<xsl:element name="ItemDetails">
					<xsl:element name="PrimaryInformation">
						<xsl:attribute name="ItemType">
							<xsl:text>SYSTEM</xsl:text>
						</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
