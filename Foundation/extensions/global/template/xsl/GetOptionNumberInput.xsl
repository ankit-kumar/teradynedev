<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetOptionNumber/SPWPGetOptionNumber/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:for-each select="in:GetOptionNumber">
			<xsl:element name="OrderLine">  
				<xsl:attribute name="SerialNo">
					<xsl:value-of select="@PartSerialNumber"/>
				</xsl:attribute>
				<xsl:element name="Item">
					<xsl:attribute name="ItemID">
						<xsl:value-of select="@PartNumber"/>
					</xsl:attribute>
				</xsl:element>
				<xsl:element name="Order">
					<xsl:attribute name="DocumentType">
						<xsl:text>0017.ex</xsl:text>
					</xsl:attribute>
					<xsl:attribute name ="ShipToIDQryType">FLIKE</xsl:attribute>
					<xsl:attribute name="ShipToID">
					<xsl:value-of select="@CustomerNumber"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
