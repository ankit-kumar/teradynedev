<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="MultiApi/API/Output/OrderLineList/OrderLine">
			<xsl:if test=" Order/OrderLines/OrderLine/ItemDetails/PrimaryInformation/@ItemType = 'SYSTEM' or Order/OrderLines/OrderLine/ItemDetails/PrimaryInformation/@ItemType='FRU'">
				<xsl:element name="IsValidSystemSerial">
					<xsl:attribute name="SYSTEM_SERIAL_NO">
						<xsl:value-of select="Extn/@SystemSerialNo" />
					</xsl:attribute>
					<xsl:attribute name="SYSTEM_TYPE_CODE">
						<xsl:value-of select="Order/OrderLines/OrderLine/ItemDetails/@ItemID" />
					</xsl:attribute>
					<xsl:attribute name="TESTER_GROUP">
						<xsl:value-of select="Order/OrderLines/OrderLine/ItemDetails/Extn/@SystemTesterGroup" />
					</xsl:attribute>
					<xsl:attribute name="INSTALL_DATE">
						<xsl:value-of select="Extn/@ActualInsDate" />
					</xsl:attribute>
					<xsl:attribute name="PT_EXCEPTION_FLAG">
						<xsl:value-of select="Order/OrderLines/OrderLine/ItemDetails/Extn/@SystemPTExceptionFlag" />
					</xsl:attribute>
							
					
				</xsl:element>
				</xsl:if>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
