<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
		<xsl:for-each select="MultiApi/API/Output/OrderList/Order/OrderLines">
			
				<xsl:if test="OrderLine/Extn/@MktStatus='A'">
					<xsl:element name="SystemTypeTesterGroup">
					<xsl:attribute name="System_Type">
						<xsl:value-of select="OrderLine/ItemDetails/@ItemID"/>
					</xsl:attribute>
					<xsl:attribute name="Tester_Group">
						<xsl:value-of select="OrderLine/Extn/@TesterOwnerOrgID"/>
					</xsl:attribute>
					</xsl:element>
				</xsl:if>
			
		</xsl:for-each>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>
