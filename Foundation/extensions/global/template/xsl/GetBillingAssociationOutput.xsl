<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output indent="yes"/>
<xsl:template match="/">
<xsl:element name="Output">
<xsl:for-each select="MultiApi/API">
<xsl:element name="GetBillingAssociation">
<xsl:attribute name="CustomerNumber"><xsl:value-of select="substring-before(Output/Customer/@CustomerID,'-')"/></xsl:attribute>
<xsl:attribute name="CustomerAddressNumber"><xsl:value-of select="substring-after(Output/Customer/@CustomerID,'-')"/></xsl:attribute>
<xsl:attribute name="BilltoCustomerNumber"><xsl:value-of select="substring-before(Output/Customer/@BuyerOrganizationCode,'-')"/></xsl:attribute>
<xsl:attribute name="BilltoSiteId"><xsl:value-of select="substring-after(Output/Customer/@BuyerOrganizationCode,'-')"/></xsl:attribute>
<xsl:attribute name="BilltoCustomerName"><xsl:value-of select="Output/Customer/BuyerOrganization/@OrganizationName"/></xsl:attribute>
<xsl:attribute name="DefaultFlag"><xsl:value-of select="Output/Customer/@Status"/></xsl:attribute>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
