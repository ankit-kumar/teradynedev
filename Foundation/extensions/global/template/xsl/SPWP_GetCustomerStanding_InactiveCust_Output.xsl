<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:element name="GetCustomerStanding">
				<!--xsl:attribute name="CustomerID">
					<xsl:value-of select="Organization/@OrganizationCode" />
				</xsl:attribute>
				<xsl:attribute name="Status">
					<xsl:text>Inactive</xsl:text>
				</xsl:attribute -->
                <xsl:attribute name="CreditHoldFlag" />
                <xsl:attribute name="ServiceHoldFlag" />
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
