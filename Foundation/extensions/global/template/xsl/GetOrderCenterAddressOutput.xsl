<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="MultiApi/API/Output/Organization">
				<xsl:if test="@ActivateFlag='Y'">
					<xsl:if test="CorporatePersonInfo">
						<xsl:for-each select="CorporatePersonInfo"> 
							<xsl:element name="GetOrderBillingCenterAddress">
								<xsl:attribute name="Address-Line1">
									<xsl:value-of select="@AddressLine1" />
								</xsl:attribute>
								<xsl:attribute name="Address-Line2">
									<xsl:value-of select="@AddressLine2" />
								</xsl:attribute>
								<xsl:attribute name="Address-Line3">
									<xsl:value-of select="@AddressLine3" />
								</xsl:attribute>
								<xsl:attribute name="Address-Line4">
									<xsl:value-of select="@AddressLine4" />
								</xsl:attribute>
								<xsl:attribute name="Address-Line5">
									<xsl:value-of select="@AddressLine5" />
								</xsl:attribute>
								<xsl:attribute name="Address-City">
									<xsl:value-of select="@City" />
								</xsl:attribute>
								<xsl:attribute name="Address-State">
									<xsl:value-of select="@State" />
								</xsl:attribute>
								<xsl:attribute name="Address-Country">
									<xsl:value-of select="@Country" />
								</xsl:attribute>
								<xsl:attribute name="Address-ZipCode">
									<xsl:value-of select="@ZipCode" />
								</xsl:attribute>			
							</xsl:element>
						</xsl:for-each>	
					</xsl:if>
				</xsl:if>
			</xsl:for-each>		
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
