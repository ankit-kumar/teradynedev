<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output indent="yes"/>
	<xsl:template match="/Organization">
		<xsl:element name="Organization">
			<xsl:attribute name="OrganizationCode">
				<xsl:value-of select="@OrganizationCode"/>
			</xsl:attribute>
			<xsl:attribute name="ParentOrganizationCode">
				<xsl:value-of select="@CurrOrgCode"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>