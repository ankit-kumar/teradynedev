<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output indent="yes"/>
<xsl:template match="/">
	<xsl:element name="Output">
	<xsl:for-each select="MultiApi/API/Output/OrderLineList">
		<xsl:element name="CheckOPPOSupport">
			<xsl:attribute name="ServiceType"><xsl:value-of select="OrderLine/Extn/@ServiceType"/></xsl:attribute>
			<xsl:attribute name="SystemSerialNo"><xsl:value-of select="OrderLine/Extn/@SystemSerialNo"/></xsl:attribute>
			<xsl:attribute name="EntitlementTypeCode"><xsl:value-of select="OrderLine/Order/@OrderType"/></xsl:attribute>
		</xsl:element>
	</xsl:for-each>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>
