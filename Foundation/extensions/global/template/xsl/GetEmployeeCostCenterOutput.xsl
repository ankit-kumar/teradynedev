<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
	<xsl:element name="Output">
	<xsl:if test="User/@Activateflag='Y'">
       	<xsl:for-each select="User/ContactPersonInfo">
		<xsl:element name="GetEmployeeCostCenter">
			<xsl:attribute name="CostCenter">
				<xsl:value-of select="@AddressLine6" />
			</xsl:attribute>			
		</xsl:element>
	</xsl:for-each>
	</xsl:if>
	</xsl:element>
  </xsl:template>
</xsl:stylesheet>
