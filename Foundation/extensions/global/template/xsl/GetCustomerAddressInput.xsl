<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:in="http://www.sterlingcommerce.com/ws/SPWPGetCustomerAddress/SPWPGetCustomerAddress/input">
	<xsl:output indent="yes"/>
	<xsl:template match="/in:Input">
		<xsl:element name="Organization">
			<xsl:if test="in:GetCustomerAddress/@SiteID = ''">
				<xsl:attribute name="OrganizationCode">
					<xsl:value-of select="in:GetCustomerAddress/@CustomerNumber" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="in:GetCustomerAddress/@SiteID != ''">
				<xsl:attribute name="OrganizationCode">
				<xsl:value-of select="concat(in:GetCustomerAddress/@CustomerNumber,'-',in:GetCustomerAddress/@SiteID)"/>
				</xsl:attribute>
			</xsl:if>
					
			<xsl:element name="Extn">
			<xsl:attribute name="CustomerSiteType">
				<xsl:value-of select="in:GetCustomerAddress/@AddressType" /> 
			</xsl:attribute>
			</xsl:element>
			
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
