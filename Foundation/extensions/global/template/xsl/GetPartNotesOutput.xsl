<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<xsl:element name="Output">
			<xsl:for-each select="MultiApi/API/Output/ItemList/Item">
				<xsl:element name="GetPartNotes">
					<xsl:attribute name="PartNumber">
						<xsl:value-of select="@ItemID"/>
					</xsl:attribute>
					<xsl:attribute name="PartNotesCustomerDisplayableFlag">
						<xsl:value-of select="ItemInstructionList/ItemInstruction/@InstructionType"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
