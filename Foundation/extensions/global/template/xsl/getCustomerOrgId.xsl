<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output method="xml" encoding="utf-8"/>
<xsl:template match="@* | node()">
    <xsl:copy>
        <xsl:apply-templates select="@* | node()" />
    </xsl:copy>
</xsl:template>
<xsl:template match="OrgRoleList">
    <xsl:copy>
    
        <xsl:apply-templates select="@* | node()" />
		 <xsl:if  test="(OrgRole/@RoleKey='NODE')" >

                <OrgRole RoleKey="BUYER"/>
				</xsl:if>
                </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
