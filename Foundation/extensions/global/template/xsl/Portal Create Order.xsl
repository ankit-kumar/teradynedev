<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml"/>	
	<xsl:template match="/">
		<xsl:element name="Order">
			<xsl:attribute name="EnterpriseCode">
				<xsl:text>CSO</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="DocumentType">
				<xsl:text>0001</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="OrderNo">
				<xsl:value-of select="/Order/@requestId"/>
			</xsl:attribute>
			<xsl:attribute name="OrderDate">
				<xsl:value-of select="/Order/@createdDate"/>							
			</xsl:attribute>
			<xsl:attribute name="BuyerOrganizationCode">
				<xsl:value-of select="/Order/Customer/@billtoCustomerNumber"/>-<xsl:value-of select="/Order/Customer/@billtoCustomerSiteNumber"/>	
			</xsl:attribute>	
			<xsl:attribute name="DraftOrderFlag">
				<xsl:text>N</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="BillToID">
				<xsl:value-of select="/Order/Customer/@billtoCustomerNumber"/>-<xsl:value-of select="/Order/Customer/@billtoCustomerSiteNumber"/>	
			</xsl:attribute>
			<xsl:attribute name="EnteredBy">
				<xsl:value-of select="/Order/@lastUpdatedBy"/>
			</xsl:attribute>
			<xsl:attribute name="OrderType">
				<xsl:text>Normal</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="ReceivingNode">
				<xsl:value-of select="/Order/Customer/@billtoCustomerNumber"/>-<xsl:value-of select="/Order/Customer/@billtoCustomerSiteNumber"/>	
			</xsl:attribute>	
			<xsl:attribute name="ShipToID">
				<xsl:value-of select="/Order/Customer/@billtoCustomerNumber"/>-<xsl:value-of select="/Order/Customer/@billtoCustomerSiteNumber"/>	
			</xsl:attribute>
			<xsl:attribute name="SCAC">
				<xsl:value-of select="/Order/@shipViaCarrier"/>
			</xsl:attribute>
			<xsl:element name="Extn">
				<xsl:attribute name="ContactEMailID">
					<xsl:value-of select="/Order/Contact/@orderContactEmail"/>
				</xsl:attribute>
				<xsl:attribute name="ContactName">
					<xsl:value-of select="/Order/Contact/@orderContactName"/>
				</xsl:attribute>
				<xsl:attribute name="ContactPhone">
					<xsl:value-of select="/Order/Contact/@orderContactPhone"/>
				</xsl:attribute>
				<xsl:attribute name="ContactAttnTo">
					<xsl:value-of select="/Order/Contact/@orderAttentionToName"/>
				</xsl:attribute>
				<xsl:attribute name="CreditCardFlag">
					<xsl:value-of select="/Order/@pmFlag"/>
				</xsl:attribute>
			</xsl:element>

			<xsl:element name="OrderLines">

	<xsl:for-each select="/Order/OrderLines/OrderLine">
					<xsl:element name="OrderLine">

						<xsl:attribute name="OrderedQty">
							<xsl:value-of select="/Order/OrderLines/Item/@requestQuantity"/>
						</xsl:attribute>
						<xsl:attribute name="ReceivingNode">
							<xsl:value-of select="/Order/Customer/@shiptoCustomerNumber"/>-<xsl:value-of select="Order/Customer/@shiptoCustomerSiteNumber"/>	
						</xsl:attribute>
						<xsl:attribute name="FulfillmentType">
							<xsl:text>TER_FULFILLMENT</xsl:text>
						</xsl:attribute>	
						<xsl:attribute name="SerialNo">
							<xsl:value-of select="/Order/OrderLines/Item/@partSerialNumber"/>
						</xsl:attribute>						
					</xsl:element>

			<xsl:element name="EXTN">					
						<xsl:attribute name="ControlNo">
							<xsl:value-of select="/Order/@partRequestDetailId"/>
						</xsl:attribute>
						<xsl:attribute name="HandlingNoChargeCode">
							<xsl:value-of select="/Order/OrderLines/Item/@noChargeCode"/>
						</xsl:attribute>
						<xsl:attribute name="SystemSerialNo">
							<xsl:value-of select="/Order/OrderLines/Item/@systemSerialNumber"/>
						</xsl:attribute>
						<xsl:attribute name="ServiceType">
							<xsl:value-of select="/Order/OrderLines/Item/@serviceType"/>
						</xsl:attribute>
						<xsl:attribute name="WarrantyNo">
							<xsl:value-of select="/Order/@entitlementNumber"/>
						</xsl:attribute>
						<xsl:attribute name="ServiceType">
							<xsl:value-of select="/Order/OrderLines/Item/@serviceType"/>
						</xsl:attribute>						
						<xsl:attribute name="ImmediateShipFlag">
							<xsl:text>O</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="OEMSerialNo">
							<xsl:value-of select="/Order/OrderLines/Item/@oemPartSerialNumber"/>
						</xsl:attribute>	
						<xsl:attribute name="RepairNoChargeCode">
							<xsl:value-of select="/Order/OrderLines/Item/@noChargeCode"/>
						</xsl:attribute>							
			</xsl:element>	
			
			
					<xsl:element name="Item">					
						<xsl:attribute name="ItemID">
							<xsl:value-of select="/Order/OrderLines/Item/@partNumber"/>
						</xsl:attribute>
						<xsl:attribute name="ProductClass">
							<xsl:text>GOOD</xsl:text>
						</xsl:attribute>					
						<xsl:attribute name="PrimeLineNo">
							<xsl:text>1</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="SubLineNo">
							<xsl:text>1</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="UnitOfMeasure">
							<xsl:text>EA</xsl:text>
						</xsl:attribute>						
					</xsl:element>					
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
