<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader">
            <yfc:i18n>First_Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Last_Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>E-Mail</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Address_Line_1</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Address_Line_2</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>City</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>State</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Postal_Code</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Country</yfc:i18n>
        </td>
   </tr>
</thead>
    <tbody>
        <yfc:loopXML binding="xml:/PersonInfoList/@PersonInfo" id="PersonInfo">
            <tr>
				<td class="tablecolumn">
					<img class="icon" onClick="setLookupValue(this.value)"  value="<%=resolveValue("xml:/PersonInfo/@PersonInfoKey")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@FirstName"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@LastName"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@EMailID"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@AddressLine1"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@AddressLine2"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@City"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@State"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@ZipCode"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/PersonInfo/@Country"/>
                </td>
			</tr>
		</yfc:loopXML>
</tbody>
</table>