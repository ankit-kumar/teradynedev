<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%
//Get the document type that the lookup is for
String sDocType;
	sDocType = request.getParameter("DocType");

%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<%	
	//Setup default Organization Role
	String DefaultRole = "Node";
	String DefaultNodeType = "Customer-Site";
	String sLookupAttr = request.getParameter("LookupAttribute");
%>
<table class="view">
	<tr>
		<td>
			<input type="hidden" name="DocType" value="<%=HTMLEncode.htmlEscape(sDocType)%>"/>
			<input type="hidden" name="xml:/Order/@DocumentType" value="<%=sDocType%>"/>
			<input type="hidden" name="xml:/Organization/OrgRoleList/OrgRole/@RoleKey" value="<%=DefaultRole%>"/>
			<input type="hidden" name="xml:/Organization/Node/@NodeType" value="<%=DefaultNodeType%>"/>
			<input type="hidden" name="LookupAttribute" value="<%=HTMLEncode.htmlEscape(sLookupAttr)%>"/>

		</td>
	</tr>		 
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_ID</yfc:i18n>
        </td>
    </tr>
    <tr>
	<td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/Organization/@OrganizationCodeQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" 
		<%=getTextOptions("xml:/Organization/@OrganizationCode") %> />
    </td>      
    </tr>
	
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Customer_Site_Org_Name</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/Organization/@OrganizationNameQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/Organization/@OrganizationNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" 
		<%=getTextOptions("xml:/Organization/@OrganizationName") %> />
    </td> 
    </tr>		
</table>
