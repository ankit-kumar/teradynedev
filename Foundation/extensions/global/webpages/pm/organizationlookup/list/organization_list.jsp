<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script>
function setCustomerLookupValue(value)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = value;
			
		}
		window.close();
	}
</script>
<table class="table" width="100%" editable="false">
<thead>
    <tr> 
        <td class="lookupiconheader"><br /></td>
        <td class="tablecolumnheader">
            <yfc:i18n>Org_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Organization_Name</yfc:i18n>
        </td><td class="tablecolumnheader">
            <yfc:i18n>Org_Ship_To_Address</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>Roles</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>Parent_Org</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>Customer_Site_Status</yfc:i18n>
        </td>			
        
   </tr>
</thead>
    <tbody>
        <yfc:loopXML binding="xml:/OrganizationList/@Organization" id="Organization">
		<%
		String Address1 = resolveValue("xml:/Organization/CorporatePersonInfo/@AddressLine1");
		String Address2 = resolveValue("xml:/Organization/CorporatePersonInfo/@AddressLine2");
		String Address3 = resolveValue("xml:/Organization/CorporatePersonInfo/@AddressLine3");
		String City = resolveValue("xml:/Organization/CorporatePersonInfo/@City");
		String State = resolveValue("xml:/Organization/CorporatePersonInfo/@State");
		String ZipCode = resolveValue("xml:/Organization/CorporatePersonInfo/@ZipCode");
		String Country = resolveValue("xml:/Organization/CorporatePersonInfo/@Country");
		%>
            <tr>
				<td class="tablecolumn">
					<img class="icon" onClick="setCustomerLookupValue(this.value)"  value="<%=resolveValue("xml:/Organization/@OrganizationCode")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
				</td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@OrganizationCode"/>
                </td>
                <td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@OrganizationName"/>
                </td>
				<td class="tablecolumn">
					<%if(!isVoid(Address1)){%>
						<%=Address1%><br />
					<%}%>
					<%if(!isVoid(Address2)){%>
						<%=Address2%><br />
					<%}%>
					<%if(!isVoid(Address3)){%>
						<%=Address3%><br />
					<%}%>
					<%if(!isVoid(City)){%>
						<%=City%>, 
					<%}%>
					<%if(!isVoid(State)){%>
						<%=State%> 
					<%}%>
					<%if(!isVoid(ZipCode)){%>
						<%=ZipCode%><br />
					<%}%>
					<%if(!isVoid(Country)){%>
						<%=Country%><br />
					<%}%>
                </td>
				<td class="tablecolumn">
						<yfc:loopXML binding="xml:/Organization/OrgRoleList/@OrgRole" id="OrgRole">
						<yfc:getXMLValue binding="xml:/OrgRole/@RoleKey"/>
						</yfc:loopXML>
                </td>
				<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/@ParentOrganizationCode"/>
                </td>															
				<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Organization/Extn/@CustomerSiteStatus"/>
                </td>
			</tr>
		</yfc:loopXML>
</tbody>
</table>