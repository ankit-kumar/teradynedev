<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<table class="view" width="100%" >
  <td class="tablecolumn" nowrap="true">
 <yfc:i18n>Expected_Install</yfc:i18n></td>
           
                <td>
                   
							<input type="text"  class="dateinput"  OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ExpectedInsDate")%>" <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@ExpectedInsDate_YFCDATE", "xml:/OrderLine/Extn/@ExpectedInsDate_YFCDATE", "xml:/OrderLine/AllowedModifications")%>/>
							<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ExpectedInsDate", "xml:/OrderLine/AllowedModifications")%>/>
                   </td>
<tr>
 <td class="tablecolumn" nowrap="true"><yfc:i18n>Actual_Install</yfc:i18n></td>

					
				   <td class="tablecolumn" nowrap="true">
				   <input type="text" class="dateinput" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ActualInsDate")%>"  <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/AllowedModifications")%>/>
							<img class="lookupicon" onclick="invokeCalendar(this);return false" <%=yfsGetImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar", "xml:/OrderLine/Extn/@ActualInsDate", "xml:/OrderLine/AllowedModifications")%>/>
							
                    </td>   
					</tr>
</table>
