<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="orderline.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<%	if(equals("Y",resolveValue("xml:/OrderLineDetail/@IsBundleParent"))) {
	
		YFCElement OrderInput = YFCDocument.parse("<Order/>").getDocumentElement();		OrderInput.setAttribute("OrderHeaderKey",resolveValue("xml:/OrderLineDetail/@OrderHeaderKey"));
		YFCElement orderTemplate = YFCDocument.parse("<Order OrderHeaderKey=\"\"><OrderLines><OrderLine  OrderLineKey=\"\" KitCode=\"\"><Item ItemID=\"\"></Item><BundleParentLine OrderLineKey=\"\"></BundleParentLine></OrderLine></OrderLines></Order>").getDocumentElement();
		//System.out.println("orderTemplate :" + orderTemplate);
		%>		
			 <yfc:callAPI apiName="getOrderDetails" inputElement="<%=OrderInput%>"  outputNamespace="Order" /> 
		<% 
		YFCElement OrderDoc = (YFCElement)request.getAttribute("Order");
		//System.out.println("OrderDoc :" + OrderDoc);
		rearrangeBundleComponents(OrderDoc);
		request.setAttribute("Order",OrderDoc);
		} 
YFCElement orderlineDoc = (YFCElement)request.getAttribute("OrderLine");
String lineid = resolveValue("xml:/OrderLineDetail/@OriginalLineItemClicked");
Integer KitLineCounter2=0;
%>

<table class="view" width="100%">
<tr>
<td colspan="6">
	<table>
		<tr>
		    <td class="detaillabel" >
		        <yfc:i18n>Kit_Code</yfc:i18n>
		    </td>
		    <td class="protectedtext">
		    	<%=getComboText("xml:KitCodeList:/CommonCodeList/@CommonCode","CodeShortDescription","CodeValue","xml:/OrderLine/@KitCode", true)%>
		    </td>
		</tr>
	</table>
</tr>
</table>

<table class="table" width="100%" >
<thead>
<tr>
<% if(equals("Y",resolveValue("xml:/OrderLineDetail/@IsBundleParent"))) { %>
<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td> <%}%>

    <td class=tablecolumnheader style="width:<%= getUITableSize("xml:/OrderLine/KitLines/KitLine/@ItemID")%>"><yfc:i18n>Item_ID</yfc:i18n></td>
	 <td class=tablecolumnheader style="width:<%= getUITableSize("xml:/OrderLine/KitLines/KitLine/@ItemDesc")%>"><yfc:i18n>Description</yfc:i18n></td>
	<td class=tablecolumnheader style="width:<%= getUITableSize("xml:/OrderLine/KitLines/KitLine/@SerialNumber")%>"><yfc:i18n>Serial_#</yfc:i18n></td>
    <td class=tablecolumnheader style="width:<%= getUITableSize("xml:/OrderLine/KitLines/KitLine/@ISreSerial")%>"><yfc:i18n>Re_Serial</yfc:i18n></td>
  
      <td class=tablecolumnheader style="width:<%= getUITableSize("xml:/OrderLine/KitLines/KitLine/KitLineTranQuantity/@KitQty")%>"><yfc:i18n>Qty_Per_Kit</yfc:i18n></td>
    <td class=tablecolumnheader style="width:<%= getUITableSize("xml:/OrderLine/KitLines/KitLine/KitLineTranQuantity/@ComponentQty")%>"><yfc:i18n>Component_Qty</yfc:i18n></td>
</tr>
</thead>
<tbody>
        <yfc:loopXML binding="xml:/OrderLine/KitLines/@KitLine" id="KitLine">
            <yfc:makeXMLInput name="invItemKey">

                <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:KitLine:/KitLine/@ItemID" ></yfc:makeXMLKey>
                <yfc:makeXMLKey binding="xml:/InventoryItem/@UnitOfMeasure" value="xml:KitLine:/KitLine/KitLineTranQuantity/@TransactionalUOM" ></yfc:makeXMLKey>
                <yfc:makeXMLKey binding="xml:/InventoryItem/@OrganizationCode" value="xml:/OrderLine/Order/@SellerOrganizationCode" ></yfc:makeXMLKey>
				<% if(isShipNodeUser()) { %>
					<yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:CurrentUser:/User/@Node"/>
				<%} else {%>
	                <yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:/OrderLine/@ShipNode" ></yfc:makeXMLKey>
				<%}%>
            </yfc:makeXMLInput>
            <tr>
			<% if(equals("Y",resolveValue("xml:/OrderLineDetail/@IsBundleParent"))) { %>
<td nowrap="true" class="tablecolumn"></td> <%}%>
			 
                <td nowrap="true" class="tablecolumn">

                    <% 
                        String sNode = resolveValue("xml:/OrderLine/@ShipNode");
                        if(isVoid(sNode)) {%>
                            <a <%=getDetailHrefOptions("L01",getParameter("invItemKey"),"")%>><yfc:getXMLValue binding="xml:/KitLine/@ItemID"></yfc:getXMLValue></a>&nbsp;
                        <%} else {%>
                            <a <%=getDetailHrefOptions("L02",getParameter("invItemKey"),"")%>><yfc:getXMLValue binding="xml:/KitLine/@ItemID"></yfc:getXMLValue></a>&nbsp;
                        <% } %>
						<input type="hidden" name='OrderKitLineKey_<%=KitLineCounter%>' value='<%=resolveValue("xml:/KitLine/@OrderKitLineKey")%>' />
                </td>

				<input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine/KitLines/KitLine_" + KitLineCounter + "/@OrderKitLineKey", "xml:/KitLine/@OrderKitLineKey")%> />
				<td nowrap="true"class="tablecolumn">
                    <yfc:getXMLValue binding="xml:/KitLine/@ItemDesc"></yfc:getXMLValue>
                </td>
               

		 <td  nowrap="true"><input type="text" id='OrderLine_<%=KitLineCounter%>'class="Unprotectedinput"  <%=getTextOptions("xml:Order/OrderLines/OrderLine/KitLines/KitLine_"+KitLineCounter+"/Extn/@SystemSerialNo","xml:/KitLine/Extn/@SystemSerialNo")%>/>
		 </td>
               <td class="tablecolumn">
		<input class="checkbox" type="checkbox" <%=yfsGetCheckBoxOptions("xml:Order/OrderLines/OrderLine/KitLines/KitLine_"+KitLineCounter+"/Extn/@IsReSerial", "xml:/KitLine/Extn/@IsReSerial", "Y","xml:/OrderLine/AllowedModifications")%> yfcCheckedValue='Y' yfcUnCheckedValue='N' ></input>
	</td> 
			
                <td  nowrap="true" class="numerictablecolumn" sortValue="<%=getNumericValue("xml:KitLine:/KitLine/KitLineTranQuantity/@KitQty")%>">
                    <yfc:getXMLValue binding="xml:/KitLine/KitLineTranQuantity/@KitQty"></yfc:getXMLValue>
                </td>
                <td  nowrap="true" class="numerictablecolumn" sortValue="<%=getNumericValue("xml:KitLine:/KitLine/KitLineTranQuantity/@ComponentQty")%>">
                    <yfc:getXMLValue binding="xml:/KitLine/KitLineTranQuantity/@ComponentQty"></yfc:getXMLValue>
                </td>
            </tr>
			<%
		  KitLineCounter2=KitLineCounter;
		
		%>
        </yfc:loopXML>
		<input type="hidden" name="hiddenTemp1" id="hiddenTemp1" value="<%=KitLineCounter2%>">

	
		</tbody>
</table>

<script>
function validatekitserial()
	{
		
		var xyz =document.getElementById("hiddenTemp1").value;
		

for (var i = 1; i <= xyz; i++) 
	{ 
    if(!document.getElementById("OrderLine_"+i).value.length>0)
		{
	alert("Serial Number Mandatory for line #"+i);
	return false;
	}
}

		return true;
    }
	</script>
	
	