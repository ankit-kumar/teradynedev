<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>
<script language="javascript">
<%  
	YFCDate oEndDate = new YFCDate(); 
	oEndDate.setEndOfDay();

%>

	//------------------------------------------------------------------------------------------------------
	//Function Type: Private
	//------------------------------------------------------------------------------------------------------
	


</script>

<table class="view">
<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
  <jsp:param name="EnterpriseCodeBinding" value="xml:/TDYNSrvcOvrList/@OrganizationCode"/>
       <jsp:param name="EnterpriseCodeLabel" value="Enterprise"/>
		<jsp:param name="ShowDocumentType" value="false"/>
		<jsp:param name="RefreshOnEnterpriseCode" value="true"/>
		 <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include>
   
	<yfc:callAPI apiID="AP8"/>
   
           <td class="searchlabel" >
            <yfc:i18n>Program</yfc:i18n>
        </td>
   
    <tr>
        <td nowrap="True">
            <select name="xml:/TDYNSrvcOvr/@SystemProgram" class="combobox">
                <yfc:loopOptions binding="xml:/TDYNSrvcOvrList/@TDYNSrvcOvr" name="SystemProgram"
                value="SystemProgram" selected="xml:/TDYNSrvcOvr/@SystemProgram" isLocalized="Y"/>
            </select>
                   </td>
    </tr>
      
 <td class="searchlabel" >
            <yfc:i18n>Customer-Site Org ID</yfc:i18n>
        </td>
     <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/TDYNSrvcOvr/@CustomerNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/TDYNSrvcOvr/@CustomerNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@CustomerNo")%>/>
			<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>
<img class="lookupicon" onclick="callLookupForSBOrg('xml:/TDYNSrvcOvr/@CustomerNo','xml:/Order/Extn/@CustOrgName','NODE','<%=enterpriseCode%>','xml:/Order/@DocumentType')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Site_Org")%>/>
        </td>
    </tr>

	 <td class="searchlabel" >
            <yfc:i18n>Service Type Offering Item ID</yfc:i18n>
        </td>
     <tr >
	 
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TDYNSrvcOvr/@ServiceTypeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDYNSrvcOvr/@ServiceTypeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@ServiceType") %> />
		<% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode")); 
	
		%>
		
		
        <img class="lookupicon" onclick="callItemLookup1('xml:/TDYNSrvcOvr/@ServiceType','xml:/TDYNSrvcOvr/@ProductClass','xml:/TDYNSrvcOvr/@UnitOfMeasure','TDNitem','SERVICE',	'<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
    </td>
	</tr>
	
	 <td class="searchlabel" >
            <yfc:i18n>System Item ID</yfc:i18n>
        </td>
     <tr >
	 
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/TDYNSrvcOvr/@SystemTypeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDYNSrvcOvr/@SystemTypeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDYNSrvcOvr/@SystemType") %> />
		<% String extraParamss = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields","xml:/CommonFields/@EnterpriseCode")); 
	
		%>
		
		
        <img class="lookupicon" onclick="callItemLookup1('xml:/TDYNSrvcOvr/@SystemType','xml:/TDYNSrvcOvr/@ProductClass','xml:/TDYNSrvcOvr/@UnitOfMeasure','TDNitem','SYSTEM',	'<%=extraParamss%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
    </td>
	</tr>

	

	  </table>