<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script  language="javascript">
	function disableSearch(){
		var length = document.all("xml:/AttributeLength");
		if(length != null && length.value === "0"){
			document.all("btnSearch").setAttribute("disabled","disabled");
		}
	}
	window.attachEvent('onload', disableSearch);
</script>

<table class="view">
	<%
		YFCElement attributesEle = (YFCElement) request.getAttribute("Attributes");
		YFCElement status = (YFCElement) request.getAttribute("Statuses");
		YFCNodeList<YFCNode> nodeList = attributesEle.getChildNodes();
	
		%>
			<input type="hidden" name="xml:/AttributeLength" value="<%=nodeList.getLength()%>" />
		<%
		if(nodeList.getLength() == 0){
			out.println("Advanced Search is not enabled for this user");
		}else{
			String strFromStatus = null;
			String strToStatus = null;
			String statusName = null;
	%>
		<jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
			<jsp:param name="RefreshOnDocumentType" value="true"/>
			<jsp:param name="RefreshOnEnterpriseCode" value="true"/>
		</jsp:include>
		<yfc:callAPI apiID = 'AP2' />
		<yfc:loopXML name="Attributes" binding="xml:/Attributes/@Attribute" id="Attribute">
		<%
			String xpath = resolveValue("xml:/Attribute/@Xpath");
			String dropDownPath = xpath+"QryType";
			String strType = resolveValue("xml:/Attribute/@Type");
			boolean IsDropDownavailable=false;
			
			if(!YFCCommon.isVoid(xpath) && xpath.lastIndexOf("FromStatus") != -1){
				statusName = resolveValue("xml:/Attribute/@Name");
				strFromStatus = xpath;
			}
			if(!YFCCommon.isVoid(xpath) && xpath.lastIndexOf("ToStatus") != -1){
				statusName = resolveValue("xml:/Attribute/@Name");
				strToStatus = xpath;
			}
			if("String".equalsIgnoreCase(strType)){
				IsDropDownavailable=true;
			}else if("Number".equalsIgnoreCase(strType)){
				IsDropDownavailable=true;
			}else if("DATE".equalsIgnoreCase(strType)){
				IsDropDownavailable=true;
			}
		%>
		<tr>
			<%if(!IsDropDownavailable && !("DropDown".equalsIgnoreCase(strType))){%>
				<td><yfc:getXMLValue binding="xml:/Attribute/@Name" /></td>
				<td><input type="text" class="unprotectedinput" <%=getTextOptions(xpath) %> /></td>
			<%}%>
		</tr>
		<%if(IsDropDownavailable){%>
		<tr>
			<td><yfc:getXMLValue binding="xml:/Attribute/@Name" /></td>
		</tr>
		<tr>
			<td nowrap="true" class="searchcriteriacell">
			<%if("String".equalsIgnoreCase(strType)){%>
				<select name="<%=dropDownPath%>" class="combobox">
				<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" selected="<%=dropDownPath%>"/>
				</select>
				<input type="text" class="unprotectedinput" <%=getTextOptions(xpath) %> /> 
			<%}if("Number".equalsIgnoreCase(strType)){%>
				<select name="<%=dropDownPath%>" class="combobox">
				<yfc:loopOptions binding="xml:/QueryTypeList/NumericQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" selected="<%=dropDownPath%>"/>
				<input type="text" class="unprotectedinput" <%=getTextOptions(xpath) %> /> 
				</select>
			<%}if("DATE".equalsIgnoreCase(strType)){%>
				<select name="<%=dropDownPath%>" class="combobox">
				<yfc:loopOptions binding="xml:/QueryTypeList/DateQueryTypes/@QueryType" name="QueryTypeDesc" value="QueryType" selected="<%=dropDownPath%>"/>
				</select>
				<input type="text" class="unprotectedinput" <%=getTextOptions(xpath) %> /> 
				<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
				<input class="dateinput" type="hidden" <%=getTextOptions(xpath) %>/>
			<%}%>
			</td>			
		</tr>
		<%}%>
		</yfc:loopXML>
		<%if(!YFCCommon.isVoid(strFromStatus) && !YFCCommon.isVoid(strToStatus)){%>
		<tr>
			<input type="hidden" name="xml:/Order/@StatusQryType" value="BETWEEN" />
			<td><%=statusName%></td>
		</tr>
		<tr>
			<td nowrap="true">
				<select name="<%=strFromStatus%>" class="combobox">
					<yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
					value="Status" selected="<%=strFromStatus%>" isLocalized="Y"/>
				</select>
				<span class="searchlabel" ><yfc:i18n>To</yfc:i18n></span>
			</td>
		</tr>
		<tr>
			<td nowrap="true" class="searchcriteriacell">
				<select name="<%=strToStatus%>" class="combobox">
					<yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
					value="Status" selected="<%=strToStatus%>" isLocalized="Y"/>
				</select>
			</td>
		</tr>
		<%}else if(!YFCCommon.isVoid(strFromStatus)){%>
		<tr>
			<input type="hidden" name="xml:/Order/@StatusQryType" value="BETWEEN" />
			<td><%=statusName%></td>
		</tr>
		<tr>
			<td nowrap="true">
				<select name="<%=strFromStatus%>" class="combobox">
					<yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
					value="Status" selected="<%=strFromStatus%>" isLocalized="Y"/>
				</select>
			</td>
		</tr>
		<%}else if(!YFCCommon.isVoid(strToStatus)){%>
			<tr>
				<input type="hidden" name="xml:/Order/@StatusQryType" value="BETWEEN" />
				<td><%=statusName%></td>
			</tr>
			<tr>
				<td nowrap="true">
					<select name="<%=strToStatus%>" class="combobox">
						<yfc:loopOptions binding="xml:/StatusList/@Status" name="Description"
						value="Status" selected="<%=strToStatus%>" isLocalized="Y"/>
					</select>
				</td>
			</tr>
		<%}%>
	<%  }  %>
</table>