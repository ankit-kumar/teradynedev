<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/td.js"></script>

<%
	String Path = (String)request.getParameter("Path");
	if(Path==null){
	Path="xml:/Order";	%>
	
<%	}
%>

<table class="view" width ="90%" height="100%">


<tr>
	<td valign="top"  >
        
        <tr>
            <td class="detaillabel" ><yfc:i18n>Seller_Sales_order</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@SellerSalesOrdrNo")%> >
				<span style="color:red">*</span>&nbsp;
            </td>
			
			 <td class="detaillabel"><yfc:i18n>Re-Sales Right_to_use_License</yfc:i18n></td>
            <td><input class="unprotectedinput" <%=getTextOptions(Path+"/Extn/@RightToUseLicense")%>></td>
		
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Customer_#_On_Sales_Order</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" <%=getTextOptions(Path+"/Extn/@CustOnsalesOrdrNo")%>>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>US_List_At_Sales</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" <%=getTextOptions(Path+"/Extn/@UsListAtSale")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Booking_Year/Week</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" <%=getTextOptions(Path+"/Extn/@BookYW")%> >&nbsp;
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Ship_Date</yfc:i18n></td>
            <td>
                <input type="text" class="dateinput"  <%=getTextOptions("xml:/Order/Extn/@TdShipDate_YFCDATE")%>/>

            
            <img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> /><span style="color:red">*</span>
            </td>
        </tr>
        <tr>
            <td class="detaillabel" ><yfc:i18n>Ship_Year/Week</yfc:i18n></td>
            <td>
                <input type="text" class="unprotectedinput" <%=getTextOptions(Path+"/Extn/@ShipYW")%>>&nbsp;
            </td>
        </tr>
       
		</table>
