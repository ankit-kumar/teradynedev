<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<table class="view" width="100%">

	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n></td>
        <td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSCTID" /></td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_Name</yfc:i18n>:</td>
		<td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSCTName"/></td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Types</yfc:i18n>:</td>
		<td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSerContType"/></td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_Priority</yfc:i18n>:</td>
		<td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSCTPriority"/></td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TER_IB_ASSO_REQD</yfc:i18n></td>
        <td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerIBAssoReqd"/></td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_Status</yfc:i18n></td>
        <td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSCTStatus"/></td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_EFFECTIVE_DATE</yfc:i18n></td>
		<td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSCTEffectiveDate"/></td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_DURATION_DAYS</yfc:i18n></td>
		<td><yfc:getXMLValue name="TerSerContTemplate" binding="xml:/TerSerContTemplate/@TerSCTDurationDays"/></td>
	</tr>        
</table>
