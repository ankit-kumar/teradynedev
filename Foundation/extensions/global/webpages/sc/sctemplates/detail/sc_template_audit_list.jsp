<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/order.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="view" width="33%">
<tr>
    <td align="left" nowrap="true" ><yfc:i18n>Service Contract Template Audits</yfc:i18n></td>
</tr>
</table>
<table class="table" width="100%">
<thead>
    <tr> 
        <td class="tablecolumnheader">
            <yfc:i18n>Audit_#</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/AuditList/Audit/@Createts")%>">
            <yfc:i18n>Date</yfc:i18n>
        </td> 
        <td class="tablecolumnheader">
            <yfc:i18n>Modified_By</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Reason</yfc:i18n>
        </td>
		<td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Reason_Text</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Modification_Levels</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Modification_Types</yfc:i18n>
        </td> 
    </tr>
</thead>
<% String opBinding = "xml:/Audit/@Operation";
   HashMap opHM = new HashMap();
   opHM.put("Insert", "Create Template");
   opHM.put("Update", "Change Attributes");
   HashMap opChildHM = new HashMap();
   opChildHM.put("Insert", "Add Service Item");
   opChildHM.put("Update", "Change Attributes");
   opChildHM.put("Delete", "Remove Service Item");
%> 
<tbody>
<yfc:callAPI apiID="AP2" />
    <yfc:loopXML name="AuditList" binding="xml:/AuditList/@Audit" id="Audit" > 
		<% String tableName = resolveValue("xml:/Audit/@TableName"); 
		   String removeOp = resolveValue("xml:/Audit/@Operation");
		   boolean bRemove = false;
		   if(removeOp.equals("Delete")){
				bRemove = true;
		   }
		   boolean parent = false;
		   if(tableName.equals("TER_SERVICE_CONTRACT_TEMPLATE")){
				parent = true; } %>
        <tr>
			<yfc:makeXMLInput name="terSCTAuditKey">
				<yfc:makeXMLKey binding="xml:/Audit/@AuditKey" value="xml:/Audit/@AuditKey" />
			</yfc:makeXMLInput> 
            <td class="tablecolumn" sortValue="<%=AuditCounter%>"><%if(!bRemove){%>
                <a onclick="javascript:yfcShowDetailPopup('TDSCTAD','auditdetail','800','300','','servicecontractstemplat','<%=getParameter("terSCTAuditKey")%>');return false;" href=""><%}%><%=AuditCounter%><%if(!bRemove){%></a><%}%>
            </td>
            <td class="tablecolumn" sortValue="<%=getDateValue("xml:/Audit/@Createts")%>">
                <yfc:getXMLValue name="Audit" binding="xml:/Audit/@Createts" /> 
            </td>
            <td class="tablecolumn">
               <yfc:getXMLValue name="Audit" binding="xml:/Audit/@Reference3"/> 
            </td>
			<td class="tablecolumn">
			<% if(!bRemove && !resolveValue("xml:/Audit/@Reference1").equals("null")){%>
                <%=getComboText("xml:SCAuditReason:/CommonCodeList/@CommonCode", "CodeShortDescription", "CodeValue", "xml:/Audit/@Reference1",true)%>
			<%}%>
			</td>
			<td>
			<%if(!bRemove && !resolveValue("xml:/Audit/@Reference1").equals("null")){%>
				<yfc:getXMLValue name="Audit" binding="xml:/Audit/@Reference2"/>
			<%}%>
			</td>
			<td class="tablecolumn">
                <% if(parent) {%>
				<span>Service Contract Template</span>
				<% } else {%>
				<span>Service Item</span>
				<%}%>
            </td>
			<td class="tablecolumn">
				<% if(parent) {%>
				<%=opHM.get(resolveValue(opBinding))%>
				<% } else { %>
				<%=opChildHM.get(resolveValue(opBinding))%>
				<%}%>
            </td>
        </tr>
    </yfc:loopXML> 
</tbody>
</table>
