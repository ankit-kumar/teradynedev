<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<% String draftFlag = resolveValue("xml:/TerSerContTemplate/@TerSCTDraft");%>
<table class="view" width="100%">
	<tr>
		<input type="hidden" id="hiddenDraftFlag" value='<%=resolveValue("xml:/TerSerContTemplate/@TerSCTDraft")%>' />
		<input type="hidden" name="xml:/TerSerContTemplate/@TerSCTReasonCode" />
		<input type="hidden" name="xml:/TerSerContTemplate/@TerSCTReasonText" />
		<%if(draftFlag.equals("Y")){%>
		<input type="hidden" class="unprotectedinput" value="N" <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTDraft")%> />
		<%}%>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n>:</td>
		<td>
			<input type="text" class="protectedinput" readonly <%=resolveValue("xml:/TerSerContTemplate/@TerSCTID")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTID")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_Name</yfc:i18n></td>
        <td>
			<input type="text" maxLength="50" size="40" class="unprotectedinput" <%=resolveValue("xml:/TerSerContTemplate/@TerSCTName")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSCTName")%> />
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Types</yfc:i18n></td>
		<td>
			<input type="text" class="protectedinput" readonly <%=resolveValue("xml:/TerSerContTemplate/@TerSerContType")%> <%=getTextOptions("xml:/TerSerContTemplate/@TerSerContType")%>/>
        </td>
        </tr>
</table>
