<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<table class="view">
    <tr>
        <td>
            <input type="hidden" name="xml:/Order/OrderLine/Extn/@Percent_Cap_UsedQryType" value="BETWEEN"/>
			<input type="hidden" name="xml:/Order/@DocumentType" value="0018.ex"/>
			<input type="hidden" name="xml:/Order/@EnterpriseCode" value="CSO"/>
		</td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Document_Type</yfc:i18n>
        </td>
	</tr>
	<tr>
		<td class="searchcriteriacell">
			<input type="textbox" class="protectedinput" readonly value="Service Contracts" />
		</td>
	</tr>
	<tr>
		<td class="searchlabel" >
            <yfc:i18n>Enterprise_Code</yfc:i18n>
        </td>
	</tr>
	<tr>
		<td class="searchcriteriacell">
			<input type="textbox" class="protectedinput" readonly value="CSO" />
		</td>
	</tr>
	<% // Now call the APIs that are dependent on the common fields (Enterprise Code) %>
    <yfc:callAPI apiID="AP4"/>
    <yfc:callAPI apiID="AP5"/>
	
<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_Types</yfc:i18n>
        </td>
    </tr>
	<tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@OrderType" class="combobox" >
				<yfc:loopOptions binding="xml:SCTypes:/CommonCodeList/@CommonCode" 
					name="CodeShortDescription" value="CodeValue" selected="xml:/Order/@OrderType" />
			</select>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_ContractNo</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@OrderNoQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@OrderNoQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Buyer</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@BuyerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@BuyerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@BuyerOrganizationCode")%>/>
			<% String enterpriseCode = getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode");%>
            <img class="lookupicon" name="search" onclick="callLookupForOrder(this,'BUYER','<%=enterpriseCode%>','0018.ex')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization")%>/>
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Seller</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <select name="xml:/Order/@SellerOrganizationCodeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/@SellerOrganizationCodeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@SellerOrganizationCode")%>/>
            <img class="lookupicon" name="search" onclick="callLookupForOrder(this,'SELLER','<%=enterpriseCode%>','0018.ex')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Organization") %> />
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_EffDate</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@FromEffectiveDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			&nbsp;<yfc:i18n>To</yfc:i18n>&nbsp;
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@ToEffectiveDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_ExpDate</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
            <input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@FromExpirationDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			&nbsp;<yfc:i18n>To</yfc:i18n>&nbsp;
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@ToExpirationDate_YFCDATE")%> />
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
		</td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SI_ItemID</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true" >
            <select name="xml:/Order/OrderLine/Item/@ItemIDQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Item/@ItemIDQryType"/>
            </select>
            <input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/OrderLine/Item/@ItemID")%>/>
            <% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", "DEFAULT");
			extraParams = extraParams + "&xml:/Item/PrimaryInformation/@ItemType=SERVICE";	%>
            <img class="lookupicon" name="search" 
			onclick="callSerItemLookup('xml:/Order/OrderLine/Item/@ItemID','','','','xml:/Order/OrderLine/@OrderingUOM',
			'Seritem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
			<input type="hidden" <%=getTextOptions("xml:/Order/OrderLine/@OrderingUOM")%> />
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SC_CPF</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td class="searchcriteriacell" nowrap="true" >
            <select name="xml:/Order/OrderLine/Extn/@CoveredProductFamilyQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/Order/OrderLine/Extn/@CoveredProductFamilyQryType"/>
            </select>
			<input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/OrderLine/Extn/@CoveredProductFamily")%>/>
		</td>
	</tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SCT_NRPC</yfc:i18n>
        </td>
    </tr>
	<tr>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" name="xml:/Order/OrderLines/OrderLine/Extn/@NonrepairPartsCovered" >
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/Order/OrderLines/OrderLine/Extn/@NonrepairPartsCovered" />
			</select>
		</td>
	</tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>TD_SCT_RPC</yfc:i18n>
        </td>
    </tr>
	<tr>
		<td class="tablecolumn" nowrap="true">
			<select class="combobox" name="xml:/Order/OrderLine/Extn/@RepairablePartsCovered">
				<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/Order/OrderLines/OrderLine/Extn/@RepairablePartsCovered" />
			</select>
		</td>
	</tr>	
	<tr>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Order/OrderLine/Extn/@IsServiceCapGTZero" ,"Y", "xml:/Order/OrderLine/Extn/@IsServiceCapGTZero" )%> >
				<yfc:i18n>Service Cap Qty > 0</yfc:i18n>
			</input>
			<%boolean isServiceCapFlag = resolveValue("xml:/Order/OrderLine/Extn/@IsServiceCapGTZero")=="Y"?true: false;
			if(isServiceCapFlag){%>
				<input type="hidden" name="xml:/Order/OrderLine/Extn/@ServiceCapQty" value="0"/>
				<input type="hidden" name="xml:/Order/OrderLine/Extn/@ServiceCapQtyQryType" value="GT"/>
			<%}else{%>
				<input type="hidden" name="xml:/Order/OrderLine/Extn/@ServiceCapQty" value="0"/>
				<input type="hidden" name="xml:/Order/OrderLine/Extn/@ServiceCapQtyQryType" value="LE"/>
			<%}%>
		</td>
	</tr>
	<tr>
		<td class="searchlabel" >
			<yfc:i18n>TD_SC_SCP</yfc:i18n>
		</td>
	</tr>
	<tr>
		<td class="tablecolumn" nowrap="true">
			<input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/OrderLine/Extn/@FromPercent_Cap_Used")%>/>
			<yfc:i18n>To</yfc:i18n>
			<input class="unprotectedinput" type="text" <%=getTextOptions("xml:/Order/OrderLine/Extn/@ToPercent_Cap_Used")%>/>
		</td>
	</tr>
	
</table>
