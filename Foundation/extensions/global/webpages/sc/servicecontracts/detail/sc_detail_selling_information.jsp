<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>


<table class="view" width="100%"  >
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_DivisionSalesOrder</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput"  <%=yfsGetTextOptions("xml:/Order/Extn/@SellerSalesOrderNo", "xml:/Order/AllowedModifications")%>/>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_BookYrWk</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" class="unprotectedinput"  <%=yfsGetTextOptions("xml:/Order/Extn/@BookYW","xml:/Order/AllowedModifications")%>/>
        </td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_SC_DivisionName</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" class="unprotectedinput"  <%=yfsGetTextOptions("xml:/Order/Extn/@DivisionName","xml:/Order/AllowedModifications")%>/>
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SC_ForecastQtr</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=yfsGetTextOptions("xml:/Order/Extn/@ForecastQuarter","xml:/Order/AllowedModifications")%>/>
		</td>
	
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Salesman</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput"  <%=yfsGetTextOptions("xml:/Order/Extn/@Salesman","xml:/Order/AllowedModifications")%>/>
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_CustomerRep</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" class="unprotectedinput"  <%=yfsGetTextOptions("xml:/Order/Extn/@CustomerRep","xml:/Order/AllowedModifications")%>/>
        </td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_SC_PriorContract</yfc:i18n></td>
		<yfc:makeXMLInput name="priorContractKey">
            <yfc:makeXMLKey binding="xml:/Order/@OrderNo" value="xml:/Order/Extn/@PriorContractNo" />
			<yfc:makeXMLKey binding="xml:/Order/@EnterpriseCode" value="xml:/Order/@EnterpriseCode" />
			<yfc:makeXMLKey binding="xml:/Order/@DocumentType" value="xml:/Order/@DocumentType" />
        </yfc:makeXMLInput> 
        <td nowrap="true">
			<a href="javascript:showDetailFor('<%=getParameter("priorContractKey")%>');">
                <yfc:getXMLValue binding="xml:/Order/Extn/@PriorContractNo"/>
            </a>
			<!-- <input type="text" class="unprotectedinput"  yfsGetTextOptions("xml:/Order/Extn/@PriorContractNo","xml:/Order/AllowedModifications")/> -->
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SC_RenewalContract</yfc:i18n></td>
		<yfc:makeXMLInput name="renewedContractKey">
            <yfc:makeXMLKey binding="xml:/Order/@OrderNo" value="xml:/Order/Extn/@RenewalContractNo" />
			<yfc:makeXMLKey binding="xml:/Order/@EnterpriseCode" value="xml:/Order/@EnterpriseCode" />
			<yfc:makeXMLKey binding="xml:/Order/@DocumentType" value="xml:/Order/@DocumentType" />
        </yfc:makeXMLInput>
        <td>
			<a href="javascript:showDetailFor('<%=getParameter("renewedContractKey")%>');">
                <yfc:getXMLValue binding="xml:/Order/Extn/@RenewalContractNo"/>
            </a>
			<!--<input type="text" class="unprotectedinput"  yfsGetTextOptions("xml:/Order/Extn/@RenewalContractNo","xml:/Order/AllowedModifications")/> -->
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_LinkToQuote</yfc:i18n></td>
        <td colspan="3">
			<input type="text" size="50" class="unprotectedinput" <%=yfsGetTextOptions("xml:/Order/Extn/@LinkToQuote","xml:/Order/AllowedModifications")%>/>
		</td>
	</tr>
</table>
