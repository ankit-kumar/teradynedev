<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">
<%
	
	String ohKey = resolveValue("xml:Order/@OrderHeaderKey");
	if (!isVoid(ohKey)) {
		YFCDocument orderDoc = YFCDocument.createDocument("Order");
		YFCElement orderEle = orderDoc.getDocumentElement();
		orderEle.setAttribute("OrderHeaderKey",ohKey);
		
%>
		function showDetail() {
			showDetailFor('<%=orderDoc.getDocumentElement().getString(false)%>');
		}
		window.attachEvent("onload", showDetail);
	<%
	}
%>
</script>
<table class="view" width="100%"  >
	<tr>
		<td>
			<%String enterpriseCode = "CSO";
			String enterpriseName=resolveValue("xml:EnterpriseName:/OrganizationList/Organization/@OrganizationName");%>
			<input type="hidden" name="xml:/Order/@DraftOrderFlag" value="Y"/>
			<input type="hidden" <%=getTextOptions("xml:/Order/@OrderType")%> >
            <input type="hidden" name="xml:/Order/@EnteredBy" value="<%=resolveValue("xml:CurrentUser:/User/@Loginid")%>"/>
			<input type="hidden" name="xml:/Order/@DocumentType" value="0018.ex" />
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SCT_ID</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@SCTemplateID")%> /><img class="lookupicon" onclick="callSCTLookup('xml:/Order/Extn/@TerSCTKey','xml:/Order/Extn/@SCTemplateID','xml:/Order/Extn/@SCTemplateName','xml:/Order/@OrderType','servicecontractstemplat','xml:/TerSCTVw/@CallForLookup=Y')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_servicecontractstemplat") %> />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SCT_Name</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="40" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@SCTemplateName")%> />
			<input type="hidden" <%=getTextOptions("xml:/Order/Extn/@TerSCTKey")%> />
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>Enterprise</yfc:i18n></td>
        <td>
			<input type="text" class="protectedinput" readonly value="<%=enterpriseCode%>" <%=getTextOptions("xml:/Order/@EnterpriseCode")%> />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_Enterprise_Name</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="40" class="protectedinput" readonly value="<%=enterpriseName%>" />
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>Buyer</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@BuyerOrganizationCode")%> />
			<% String buyerExtraParams = "role=BUYER&forOrder=Y&enterprise=CSO&docType=0018.ex"; %>
			<img class="lookupicon" onclick="callSCOrgLookup('xml:/Order/@BuyerOrganizationCode','xml:/Order/@BuyerOrganizationName','TDORGL','<%=buyerExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDORGL")%> />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_Buyer_Name</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="40" class="unprotectedinput" name="xml:/Order/@BuyerOrganizationName" />
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>Seller</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@SellerOrganizationCode")%> />
			<% String sellerExtraParams = "role=SELLER&forOrder=Y&enterprise=CSO&docType=0018.ex"; %>
			<img class="lookupicon" onclick="callSCOrgLookup('xml:/Order/@SellerOrganizationCode','xml:/Order/@SellerOrganizationName','TDORGL','<%=sellerExtraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TDORGL")%> />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_Seller_Name</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="40" class="unprotectedinput" name="xml:/Order/@SellerOrganizationName" />
        </td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_ContractNo</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@OrderNo")%> />
		</td>
		<td class="detaillabel"><yfc:i18n>TD_SC_EffDate</yfc:i18n></td>
        <td nowrap="true">
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@EffectiveDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<input class="dateinput" type="text" <%=getTextOptions("xml:/Order/Extn/@EffectiveDate_YFCTIME")%>/>
			<img class="lookupicon" name="search" onclick="invokeTimeLookup(this);return false" <%=getImageOptions(YFSUIBackendConsts.TIME_LOOKUP_ICON, "Time_Lookup") %>/>
        </td>
	</tr>
</table>
