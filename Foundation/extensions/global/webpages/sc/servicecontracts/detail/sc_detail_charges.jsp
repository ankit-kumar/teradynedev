<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>


<table class="view" width="100%"  >
	<yfc:callAPI apiID="AP1" />
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SC_Agreement_Discount</yfc:i18n></td>
        <td>
			<yfc:getXMLValue binding="xml:/Order/Extn/@AgreementDiscount" />%
		</td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>TD_SC_Selling_Price</yfc:i18n></td>
        <td nowrap="true">
			<yfc:getXMLValue binding="xml:/Order/Extn/@TotalSellingPrice" />
		</td>
	</tr>
	<tr>
		<td class="detaillabel"><yfc:i18n>Currency</yfc:i18n></td>
        <td nowrap="true">
			<select  name="xml:/Order/PriceInfo/@Currency" class="combobox" >
				<yfc:loopOptions binding="xml:/CurrencyList/@Currency" 
					name="Currency" value="Currency" selected="xml:/Order/PriceInfo/@Currency"/>
			</select>
        </td>
	</tr>
</table>
