<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<table class="view" width="100%"  >
	<thead>
		<tr>
			<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemID")%>"><yfc:i18n>TD_SI_ItemID</yfc:i18n></td>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemDesc")%>"><yfc:i18n>Description</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredProductFamily")%>"><yfc:i18n>TD_SC_CPF</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredInstallBaseStatus")%>"><yfc:i18n>TD_SC_IBS</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@NonrepairPartsCovered")%>"><yfc:i18n>TD_SCT_NRPC</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RepairablePartsCovered")%>"><yfc:i18n>TD_SCT_RPC</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STORepairListDiscount")%>"><yfc:i18n>TD_SCT_RLD</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOExpediteListDiscount")%>"><yfc:i18n>TD_SCT_SED</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SCT_AD</yfc:i18n></td>
	</tr>
	</thead>
	<tbody>
		<tr>
			<yfc:makeXMLInput name="orderLineKey">
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCOLKey" value="xml:/OrderLineDetail/@OrderLineKey" />
				<yfc:makeXMLKey binding="xml:/TerSCIBMapping/@TerSCOHKey" value="xml:/OrderLineDetail/@OrderHeaderKey" />
			</yfc:makeXMLInput>
			<input type="hidden" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey"  />
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemDesc" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@CoveredProductFamily" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@CoveredInstallBaseStatus" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@NonrepairPartsCovered" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RepairablePartsCovered" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@STORepairListDiscount" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@STOExpediteListDiscount" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@STOAdditionalDiscount" />
			</td>
		</tr>
	</tbody>
</table>
