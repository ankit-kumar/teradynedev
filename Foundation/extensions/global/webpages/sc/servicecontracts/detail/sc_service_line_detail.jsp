<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>

<yfc:callAPI apiID="AP1" />
<yfc:callAPI apiID="AP2" />
<table class="table" width="100%"  >
	<thead>
		<tr>
			<td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemID")%>"><yfc:i18n>STO/STOG Item ID</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/Item/@ItemDesc")%>"><yfc:i18n>Description</yfc:i18n></td>
			<td class="tablecolumnheader" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredProductFamily")%>"><yfc:i18n>TD_SC_CPF</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@CoveredInstallBaseStatus")%>"><yfc:i18n>TD_SC_IBS</yfc:i18n></td>
            <td class="tablecolumnheader"  sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@NonrepairPartsCovered")%>"><yfc:i18n>TD_SCT_NRPC</yfc:i18n></td>
			<td class="tablecolumnheader"  sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@RepairablePartsCovered")%>"><yfc:i18n>TD_SCT_RPC</yfc:i18n></td>
			<td class="tablecolumnheader" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STORepairListDiscount")%>"><yfc:i18n>TD_SCT_RLD</yfc:i18n></td>
			<td class="tablecolumnheader" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOExpediteListDiscount")%>"><yfc:i18n>TD_SCT_SED</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no" style="width:<%=getUITableSize("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"><yfc:i18n>TD_SCT_AD</yfc:i18n></td>
	</tr>
	</thead>
	<tbody>
		<tr>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemDesc" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<yfc:getXMLValue binding="xml:/OrderLine/Extn/@CoveredProductFamily" />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<input type="text"  OldValue="<%=resolveValue("xml:/OrderLine/Extn/@CoveredInstallBaseStatus")%>" <%=yfsGetTextOptions("xml:/OrderLine/Extn/@CoveredInstallBaseStatus", "xml:/OrderLine/Extn/@CoveredInstallBaseStatus", "xml:/OrderLine/AllowedModifications")%> />
			</td>
			<td class="tablecolumn" nowrap="true" >
				<select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@NonrepairPartsCovered")%>" <%=yfsGetComboOptions("xml:/OrderLine/Extn/@NonrepairPartsCovered","xml:/OrderLine/Extn/@NonrepairPartsCovered", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
					<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@NonrepairPartsCovered"  />
				</select>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<select OldValue="<%=resolveValue("xml:/OrderLine/Extn/@RepairablePartsCovered")%>" <%=yfsGetComboOptions("xml:/OrderLine/Extn/@RepairablePartsCovered","xml:/OrderLine/Extn/@RepairablePartsCovered", "xml:/OrderLine/AllowedModifications")%> class="combobox"  >
					<yfc:loopOptions binding="xml:YesNo:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@RepairablePartsCovered"  />
				</select>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<select class="combobox" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@STORepairListDiscount")%>" <%=yfsGetComboOptions("xml:/OrderLine/Extn/@STORepairListDiscount","xml:/OrderLine/Extn/@STORepairListDiscount", "xml:/OrderLine/AllowedModifications")%> >
					<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@STORepairListDiscount"  />
				</select>
			</td>
			<td class="tablecolumn" nowrap="true" >
				<select class="combobox" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@STOExpediteListDiscount")%>" <%=yfsGetComboOptions("xml:/OrderLine/Extn/@STOExpediteListDiscount","xml:/OrderLine/Extn/@STOExpediteListDiscount", "xml:/OrderLine/AllowedModifications")%> >
					<yfc:loopOptions binding="xml:OneZero:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue"  selected="xml:/OrderLine/Extn/@STOExpediteListDiscount" />
				</select>
			</td>
			
			<td class="tablecolumn" nowrap="true" >
				<input type="text" OldValue="<%=resolveValue("xml:/OrderLine/Extn/@STOAdditionalDiscount")%>"  <%=yfsGetTextOptions("xml:/OrderLine/Extn/@STOAdditionalDiscount","xml:/OrderLine/Extn/@STOAdditionalDiscount","xml:/OrderLine/AllowedModifications")%> />
			</td>
		</tr>
	</tbody>
</table>
