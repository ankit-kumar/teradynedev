<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">
function callLookupforserial(entity,field1,field2,field3,field4){
		var obj = new Object();
		obj.field1 = document.all(field1);
		obj.field2 = document.all(field2);
		obj.field3 = document.all(field3);
		obj.field4 = document.all(field4);
		obj.lookup = true;
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
}
</script>
<table class="view" width="100%"  >
	<tr>
		<td class="detaillabel" ><yfc:i18n>Serial #</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSCIBMapping/@SerialNo")%> />
			<img class="lookupicon" onclick="callLookupforserial('INBorder','xml:/TerSCIBMapping/@SerialNo','xml:/TerSCIBMapping/@ItemID',
			'xml:/TerSCIBMapping/@TerIBOLKey','xml:/TerSCIBMapping/@TerIBOHKey')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Serial")%> />
		</td>
		<td class="detaillabel" ><yfc:i18n>Item ID</yfc:i18n></td>
        <td>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerSCIBMapping/@ItemID")%> />
		</td>
		<td>
			<input type="hidden" class="unprotectedinput" <%=getTextOptions("xml:/TerSCIBMapping/@TerIBOLKey")%> />
			<input type="hidden" class="unprotectedinput" <%=getTextOptions("xml:/TerSCIBMapping/@TerIBOHKey")%> />
			<input type="hidden" name="xml:/TerSCIBMapping/@TerSCOLKey" value="<%=resolveValue("xml:/TerSCIBMapping/@TerSCOLKey")%>"/>
			<input type="hidden" name="xml:/TerSCIBMapping/@TerSCOHKey" value="<%=resolveValue("xml:/TerSCIBMapping/@TerSCOHKey")%>"/>
		</td>
	</tr>
</table>
