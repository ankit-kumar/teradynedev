<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
                <input type="hidden" name="xml:/Order/@ModificationReasonCode" />
                <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
                <input type="hidden" name="xml:/Order/@Override" value="N"/>
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>TD_SC_ContractNo</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_SC_Types</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>TD_SC_Status</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Seller</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Buyer</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>TD_SC_EffDate</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>TD_SC_ExpDate</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Total_Amount</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Currency</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/OrderList/@Order" id="Order">
            <tr>
                <yfc:makeXMLInput name="orderKey">
                    <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey" />
                </yfc:makeXMLInput>                
                <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("orderKey")%>' name="EntityKey" isHistory='<%=getValue("Order","xml:/Order/@isHistory")%>' 	/>
                </td>
                <td class="tablecolumn">
                    <a href="javascript:showDetailFor('<%=getParameter("orderKey")%>');">
                        <yfc:getXMLValue binding="xml:/Order/@OrderNo"/>
                    </a>               
                </td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@OrderType"/></td>
                <td class="tablecolumn">
                    <% if (isVoid(getValue("Order", "xml:/Order/@Status"))) { %>
                        [<yfc:i18n>Draft</yfc:i18n>]
                    <% } else { %>
                       <%=displayOrderStatus(getValue("Order","xml:/Order/@MultipleStatusesExist"),getValue("Order","xml:/Order/@MaxOrderStatusDesc"),true)%>
                    <% } %>
                    <% if (equals("Y", getValue("Order", "xml:/Order/@HoldFlag"))) { %>
                        <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held")%>/>
                    <% } %>
                    <% if(equals("Y", getValue("Order","xml:/Order/@isHistory") )){ %>
                        <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HISTORY_ORDER, "This_is_an_archived_order")%>/>
                    <% } %>
                </td>
                <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@SellerOrganizationCode"/></td>
                <td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@BuyerOrganizationCode"/></td>
                <td class="tablecolumn" sortValue="<%=getDateValue("xml:/Order/Extn/@EffectiveDate")%>"><yfc:getXMLValue binding="xml:/Order/Extn/@EffectiveDate"/></td>
				<td class="tablecolumn" sortValue="<%=getDateValue("xml:/Order/Extn/@ExpirationDate")%>"><yfc:getXMLValue binding="xml:/Order/Extn/@ExpirationDate"/></td>
                <td class="numerictablecolumn" sortValue="<%=getNumericValue("xml:/Order/Extn/@TotalSellingPrice")%>">
                <yfc:getXMLValue binding="xml:/Order/Extn/@TotalSellingPrice"/>
                </td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/PriceInfo/@Currency"/></td>
            </tr>
        </yfc:loopXML>
   </tbody>
</table>
