<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>

<table class="view" width="100%">
	<tr>
		<td class="detaillabel" ><yfc:i18n>TD_SR_ID</yfc:i18n>:</td>
		<td>
			<input type="text" class="protectedinput" readonly <%=resolveValue("xml:/TerSCTSelectionRule/@TerSRID")%> 
			<%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRID")%> />
        </td>
		<td class="detaillabel" ><yfc:i18n>TD_SR_Name</yfc:i18n></td>
        <td nowrap="true">
			<input type="text" size="40" maxLength="50" class="unprotectedinput" <%=resolveValue("xml:/TerSCTSelectionRule/@TerSRName")%> 
			<%=getTextOptions("xml:/TerSCTSelectionRule/@TerSRName") %> />
        </td>
	</tr>
</table>
