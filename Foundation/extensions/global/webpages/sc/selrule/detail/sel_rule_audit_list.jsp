<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ include file="/console/jsp/order.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="view" width="100%">
<tr>
   <td class="tablecolumn" nowrap="true" ><yfc:i18n>Service Contract Template Selection Rule Audits</yfc:i18n></td>
</tr>
</table>
<table class="table" width="100%">
<thead>
    <tr> 
        <td class="tablecolumnheader">
            <yfc:i18n>Audit_#</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" style="width:<%=getUITableSize("xml:/AuditList/Audit/@Createts")%>">
            <yfc:i18n>Date</yfc:i18n>
        </td> 
        <td class="tablecolumnheader">
            <yfc:i18n>Modified_By</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Reason</yfc:i18n>
        </td>
		<td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Reason_Text</yfc:i18n>
        </td>
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Modification_Levels</yfc:i18n>
        </td> 
        <td class="tablecolumnheader" sortable="no">
            <yfc:i18n>Modification_Types</yfc:i18n>
        </td> 
    </tr>
</thead>
<% String opBinding = "xml:/Audit/@Operation";
   HashMap opHM = new HashMap();
   opHM.put("Insert", "Create Selection Rule");
   opHM.put("Update", "Change Attributes");
   HashMap opChildHM = new HashMap();
   opChildHM.put("Insert", "Add Service Contract Template");
   opChildHM.put("Delete", "Remove Service Contract Template");
%> 
<tbody>
<yfc:callAPI apiID="AP2" />
    <yfc:loopXML name="AuditList" binding="xml:/AuditList/@Audit" id="Audit" > 
		<% String tableName = resolveValue("xml:/Audit/@TableName"); 
		   String removeOp = resolveValue("xml:/Audit/@Operation");
		   boolean bRemove = false;
		   if(removeOp.equals("Delete")){
				bRemove = true;
		   }
		   boolean parent = false;
		   if(tableName.equals("TER_SCT_SELECTION_RULE")){
				parent = true; } %>
        <tr>
			<yfc:makeXMLInput name="terSRAuditKey">
				<yfc:makeXMLKey binding="xml:/Audit/@AuditKey" value="xml:/Audit/@AuditKey" />
			</yfc:makeXMLInput> 
            <td class="tablecolumn" sortValue="<%=AuditCounter%>"><%if(!bRemove){%>
                <a onclick="javascript:yfcShowDetailPopup('SCTSRA','auditdetail','800','300','','SCTSR','<%=getParameter("terSRAuditKey")%>');return false;" href=""><%}%><%=AuditCounter%><%if(!bRemove){%></a><%}%>
            </td>
            <td class="tablecolumn" sortValue="<%=getDateValue("xml:/Audit/@Createts")%>">
                <yfc:getXMLValue name="Audit" binding="xml:/Audit/@Createts" />
            </td>
            <td class="tablecolumn">
               <yfc:getXMLValue name="Audit" binding="xml:/Audit/@Reference3"/> 
            </td>
            <td class="tablecolumn">
             <% if(!resolveValue("xml:/Audit/@Reference1").equals("null")){%>
                <%=getComboText("xml:SRAuditReason:/CommonCodeList/@CommonCode", "CodeShortDescription", "CodeValue", "xml:/Audit/@Reference1",true)%>
			<%}%>
			</td>
			<td>
			<%if(!resolveValue("xml:/Audit/@Reference1").equals("null") && !resolveValue("xml:/Audit/@Reference2").equals("null")){%>
				<yfc:getXMLValue name="Audit" binding="xml:/Audit/@Reference2"/>
			<%}%>
			</td>
            <td class="tablecolumn">
                <% if(parent) {%>
				<span>SC Selection Rule</span>
				<% } else {%>
				<span>Service Contract Template</span>
				<%}%>
            </td>
			<td class="tablecolumn">
				<% if(parent) {%>
				<%=opHM.get(resolveValue(opBinding))%>
				<% } else { %>
				<%=opChildHM.get(resolveValue(opBinding))%>
				<%}%>
            </td>
        </tr>
    </yfc:loopXML> 
</tbody>
</table>
