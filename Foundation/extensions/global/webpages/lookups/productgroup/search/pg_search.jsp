<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>

<table width="100%" class="view">
    <% // Now call the APIs that are dependent on the common fields. %>
    <yfc:callAPI apiID="AP1"/>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_RESP_ID</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
		
        <select name="xml:/TerPG/@TerResponsibleIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerPG/@TerResponsibleIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerPG/@TerResponsibleID")%>/>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_RESP_Name</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerPG/@TerResponsibleNameQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerPG/@TerResponsibleNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerPG/@TerResponsibleName")%> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>TD_DIV_CODE</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
		<select name="xml:/TerPG/@TerDivisionCodeQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TerPG/@TerDivisionCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerPG/@TerDivisionCode")%>/>
    </td>
</tr>
</tr>
</table>
