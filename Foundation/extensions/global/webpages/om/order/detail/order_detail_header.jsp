<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_header.jspf" %>
 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script language="javascript">
	function validateOrderType(orderType){
		var inputType = "<%=resolveValue("xml:/Order/@OrderType")%>";
		
		if(trim(orderType) == ""){
			if(trim(inputType).toLowerCase() === "Offline".toLowerCase()){
				alert('For Offline orders, please Choose Offline Shipment button.');
				return false;
			}
		}else{  /*Offline*/
			if(trim(inputType).toLowerCase() != trim(orderType).toLowerCase()){
				alert('This button only for offline Orders');
				return false;
			}
		}
		return true;
	}
</Script>
<%  

	String sRequestDOM = request.getParameter("getRequestDOM");
    String modifyView = request.getParameter("ModifyView");
    modifyView = modifyView == null ? "" : modifyView;

	String sHiddenDraftOrderFlag = getValue("Order", "xml:/Order/@DraftOrderFlag");
    String driverDate = getValue("Order", "xml:/Order/@DriverDate");
	String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
	extraParams += "&" + getExtraParamsForTargetBinding("xml:/Order/@OrderHeaderKey", resolveValue("xml:/Order/@OrderHeaderKey"));
	extraParams += "&" + getExtraParamsForTargetBinding("IsStandaloneService", "Y");
	extraParams += "&" + getExtraParamsForTargetBinding("hiddenDraftOrderFlag", sHiddenDraftOrderFlag);
%>

<script language="javascript">
	// this method is used by 'Add Service Request' action on order header detail innerpanel
	function callPSItemLookup()	{
		yfcShowSearchPopupWithParams('','itemlookup',900,550,new Object(), 'psItemLookup', '<%=extraParams%>');
	}
</script>
	
<%
	String uploadPath = (String) session.getAttribute("FilePath");
	System.out.println(uploadPath);
	if(isVoid(uploadPath)){
		YFCDocument inputDoc = YFCDocument.getDocumentFor("<GetProperty PropertyName=\"pdf.dump.folder\" />");
		YFCDocument template = YFCDocument.getDocumentFor("<GetProperty PropertyValue=\"\" />");
		System.out.println("Null or white Space");
%>
		<yfc:callAPI apiName='getProperty' inputElement='<%=inputDoc.getDocumentElement()%>' templateElement='<%=template.getDocumentElement()%>' outputNamespace='FolderPath'/>
<%
		YFCElement outputEle = getElement("FolderPath");
		
		if(isVoid(outputEle)){
			session.setAttribute("FilePath","");
			System.out.println("empty");
		}else{
			session.setAttribute("FilePath",outputEle.getAttribute("PropertyValue"));
			System.out.println("folder:"+outputEle.getAttribute("PropertyValue"));
		}
	}
%>
	
<table class="view" width="100%">
    <yfc:makeXMLInput name="orderKey">
        <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
    </yfc:makeXMLInput>
    <tr>
        <td>
            <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
            <input type="hidden" name="xml:/Order/@ModificationReasonCode" />
            <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
            <input type="hidden" name="xml:/Order/@Override" value="N"/>
            <input type="hidden" name="hiddenDraftOrderFlag" value='<%=sHiddenDraftOrderFlag%>'/>
            <input type="hidden" name="chkWOEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>'/>
            <input type="hidden" name="chkCopyOrderEntityKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="chkOrderHeaderKey" value='<%=HTMLEncode.htmlEscape(getParameter("orderKey"))%>' />
			<input type="hidden" name="xml:/Order/@EnterpriseCode" value='<%=getValue("Order", "xml:/Order/@EnterpriseCode")%>'/>
			<input type="hidden" name="xml:/Order/@DocumentType" value='<%=getValue("Order", "xml:/Order/@DocumentType")%>'/>
        </td>
    </tr>
    <tr>
        <td class="detaillabel" ><yfc:i18n>Order#</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/@OrderNo"/></td>
        <td class="detaillabel" ><yfc:i18n>Order Type</yfc:i18n></td>
		<td class="">
			<select class="combobox" id="TDOrderType" <%=getComboOptions("xml:/Order/@OrderType")%>>
				<yfc:loopOptions binding="xml:OrderTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" isLocalized="Y" selected="xml:/Order/@OrderType"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>Status</yfc:i18n></td>
        <td class="protectedtext">
            <% if (isVoid(getValue("Order", "xml:/Order/@Status"))) {%>
                [<yfc:i18n>Draft</yfc:i18n>]
            <% } else { %>
                <a <%=getDetailHrefOptions("L01", getParameter("orderKey"), "ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("Order","xml:/Order/@MultipleStatusesExist"),getValue("Order","xml:/Order/@MaxOrderStatusDesc"),true)%></a>
            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@HoldFlag"))) { %>

	            <% if (isVoid(modifyView) || isTrue("xml:/Rules/@RuleSetValue")) {%>
					<img onmouseover="this.style.cursor='default'" class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held")%>>
				<%	}	else	{	%>
					<a <%=getDetailHrefOptions("L05", getParameter("orderKey"), "")%>><img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.HELD_ORDER, "This_order_is_held\nclick_to_add/remove_hold")%>></a>
				<%	}	%>

            <% } %>
            <% if (equals("Y", getValue("Order", "xml:/Order/@SaleVoided"))) { %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.SALE_VOIDED, "This_sale_is_voided")%>/>
            <% } %>
            <% if (equals("Y", getValue("Order","xml:/Order/@isHistory") )){ %>
                <img class="icon" onmouseover="this.style.cursor='default'" <%=getImageOptions(YFSUIBackendConsts.HISTORY_ORDER, "This_is_an_archived_order")%>/>
            <% } %>
        </td>
		<td class="detaillabel">Created by</td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/User/@Username" /></td>
		<td class="detaillabel">Email</td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/User/ContactPersonInfo/@EMailID" /></td>
		<td class="detaillabel">Phone</td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/User/ContactPersonInfo/@DayPhone"/></td>
    </tr>
	<tr>
		<td class="detaillabel">Created Date</td>
		<td class="protectedtext">
			<yfc:getXMLValue binding="xml:/Order/@OrderDate"/>
			<input type="hidden" <%=getTextOptions("xml:/Order/@OrderDate")%> />
			<input type="hidden" <%=getTextOptions("xml:/Order/@ReceivingNode")%> />
		</td>
		<td class="detaillabel">Cost Center</td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:/Order/Extn/@CostCenter"/></td>
		<td class="detaillabel">Attn To</td>
		<td class="protectedtext"><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactAttnTo")%>/></td>
	</tr>
    <tr>
		<td class="detaillabel"><yfc:i18n>Customer Reference</yfc:i18n></td>
		<td> <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/@CustomerPONo")%> /></td>
        <td class="detaillabel" ><yfc:i18n>Contact</yfc:i18n></td>
        <td><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactName")%> /></td>
		<td class="detaillabel" ><yfc:i18n>Phone</yfc:i18n></td>
        <td><input type="text" class="unprotectedinput"  <%=getTextOptions("xml:/Order/Extn/@ContactPhone")%> /></td>
		<td class="detaillabel" ><yfc:i18n>Email</yfc:i18n></td>
        <td><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/Order/Extn/@ContactEMailID")%> /></td>
    </tr>
</table>
