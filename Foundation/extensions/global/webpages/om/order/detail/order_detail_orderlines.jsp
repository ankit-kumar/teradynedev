<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/console/scripts/ajaxItemServiceType.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/css/scripts/editabletbl.js"></script>

<script language="javascript">
    document.body.attachEvent("onunload", processSaveRecordsForChildNode);
	
	function templateRowCallItemLookup_td(obj,ItemID,pc,uom,entity,extraParams,id){
	
		var itemEle = document.getElementById(id);
		extraParams = extraParams+"&itemField="+ itemEle.value;
		templateRowCallItemLookup(obj,ItemID,pc,uom,entity,extraParams);
	}
	
	function callLookupforserial(entity,field1,field2,recvNode){
		var obj = new Object();
		obj.field1 = document.all(field1);
		obj.field2 = document.all(field2);
		obj.recvNode = recvNode;
		obj.lookup = true;
		yfcShowSearchPopupWithParams('','lookup',900,550,obj,entity,'');
	}
	
	function validateFile(id,hname,hVal,keyId, keyVal){	
		if(Validate(document.getElementById(id).value)){
			submitForm(document.getElementById(id).value,hname,hVal,keyId, keyVal);
			return true;	
		}else{
			return false;
		}
	}

	function submitForm(val,hName,hVal,keyId, keyVal){
	
		var hiddenElement = document.all(hName);
	    hiddenElement.value = hVal;
		var hiddenKeyElement = document.all(keyId);
	    hiddenKeyElement.value = keyVal;	
		//alert(hiddenKeyElement.value);
		//alert(hiddenKeyElement.name);
		
		var containerForm= document.all("containerform")
		window.document.forms.containerform.setAttribute("method","post");
		window.document.forms.containerform.setAttribute("encoding","multipart/form-data");
		window.document.forms.containerform.setAttribute("enctype","multipart/form-data");
		window.document.forms.containerform.action=contextPath+'/extn/pdf';
		window.document.forms.containerform.submit();
		
	}
	
	function loadFile(){
		var btn = document.getElementsByClassName("button");
		var reqBtn = null;
		if(document.getElementsByClassName("button")[0].value == 'Save'){
			reqBtn = document.getElementsByClassName("button")[0];
		}else{
			reqBtn = document.getElementsByClassName("button")[1];
		}
		
		var url = document.URL;
		var orderLineKey = '<%=request.getAttribute("orderLineKey")%>';
		var quoteId = '<%=request.getAttribute("quoteOrderLineId")%>';
		var confirmId = '<%=request.getAttribute("confirmOrderLineId")%>';
		//alert(quoteId);
		//alert(confirmId);
		if(quoteId != 'null'){
			var filename = '<%=request.getAttribute("quote")%>';
			var id = orderLineKey+"_q";
			document.getElementById(id).value='<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'';
			reqBtn.click();
			/*document.getElementById(quoteId).innerHTML = '<a href="<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'" >'+filename+'</a>';*/
		}
		
		if(confirmId != 'null'){
			var filename = '<%=request.getAttribute("confirm")%>';
			var id = orderLineKey+"_c";
			document.getElementById(id).value='<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'';
			reqBtn.click();
			/*document.getElementById(confirmId).innerHTML = '<a href="<%=request.getContextPath()%>/extn/pdfDownload?file='+filename+'" >'+filename+'</a>';*/
		}
	}
	
	/*function addOnclick(){
		var buttons = document.getElementsByClassName("button");
		for(i=0;i<buttons.length;i++){
			var button = buttons[i];
			if((button.value).toLowerCase() === "save" ){
				if(button.attachEvent){
					button.attachEvent('onmouseover', removeForm);
				}else if(button.addEvenetListener){
					button.addEvenetListener('mouseover', removeForm);
				}
				break;
			}
		}
	}*/
		
	function Validate(sFileName) {
		var sExtension = ".pdf";
		if (sFileName.length > 0) {
            if (sFileName.substr(sFileName.length - sExtension.length, sExtension.length).toLowerCase() == sExtension.toLowerCase()) {
				return true;
			}else{	
				alert("You can only upload PDF files");
				return false;	
			}
		}
	}
	//window.attachEvent("onload", IgnoreChangeNames);
	window.attachEvent("onload", loadFile);
	//window.attachEvent('onload', addOnclick);
</script>

<%
	String strKey = (request.getAttribute("orderHeaderKey")).toString(); 
	session.setAttribute("EKey",strKey);	
%>
<%
	appendBundleRootParent((YFCElement)request.getAttribute("Order"));
	boolean bAppendOldValue = false;
	if(!isVoid(errors) || equals(sOperation,"Y") || equals(sOperation,"DELETE")) 
		bAppendOldValue = true;
	String modifyView = request.getParameter("ModifyView");
    modifyView = modifyView == null ? "" : modifyView;

	String status = getValue("Order","xml:/Order/@Status");
    String driverDate = getValue("Order", "xml:/Order/@DriverDate");
	String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
%>

<table class="table" ID="OrderLines" cellspacing="0" width="100%" yfcMaxSortingRecords="1000" >
    <thead>
        <tr>
            <td class="checkboxheader" sortable="no">
                <input type="hidden" id="userOperation" name="userOperation" value="" />
                <input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
                <input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
            </td>
			<td class="tablecolumnheader" nowrap="true" style="width:30px">&nbsp;</td>
            <td class="tablecolumnheader" nowrap="true"><yfc:i18n>Line</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true"><yfc:i18n>Cust Line</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true"><yfc:i18n>Cust RMA</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Customer Reference#</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Control#</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Customer Purchase Order#</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" ><yfc:i18n>System Serial#</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" ><yfc:i18n>System_Type</yfc:i18n></td>
            <td class="tablecolumnheader" nowrap="true" sortable="no" ><yfc:i18n>Teradyne Business Group</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Agreement/ Waranty#</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Out of Box failure?</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Part#</yfc:i18n></td>
			<td class="tablecolumnheader" ><yfc:i18n>Description</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Part Serial#</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>OEM Serial#</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Product Class</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>ROHS</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Service Type</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Ship From</yfc:i18n></td>
			<td class="tablecolumnheader" ><yfc:i18n>Ship To</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>UOM</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Qry Ordered Today</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>QTY</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Lead Time</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Quote</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Confirmation</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Due Date</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Expected Delivery</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Ship Late</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Handling Chge/Applied</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Repair no chrg Code</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Expedite no chrg Code</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Auto no charges codes</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Payment</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Open Blanket PO Balance</yfc:i18n></td>
            <td class="tablecolumnheader" ><yfc:i18n>Status</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML name="Order" binding="xml:/Order/OrderLines/@OrderLine" id="OrderLine">
        	<%	
			//Set variables to indicate the orderlines dependency situation. 
			boolean isDependentParent = false;
			boolean isDependentChild = false;
			if (equals(getValue("OrderLine","xml:/OrderLine/@ParentOfDependentGroup"),"Y")) {  
				isDependentParent = true;
			}
			if (!isVoid(getValue("OrderLine","xml:/OrderLine/@DependentOnLineKey"))) {
				isDependentChild = true;
			}
		
			if(!isVoid(resolveValue("xml:OrderLine:/OrderLine/@Status"))) {  
				if (equals(getValue("OrderLine","xml:/OrderLine/@ItemGroupCode"),"PROD") )
					//display line in this inner panel only if item has ItemGroupCode = PROD
				{
					if(bAppendOldValue) {
						String sOrderLineKey = resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey");
						if(oMap.containsKey(sOrderLineKey))
							request.setAttribute("OrigAPIOrderLine",(YFCElement)oMap.get(sOrderLineKey));
				} else 
						request.setAttribute("OrigAPIOrderLine",(YFCElement)pageContext.getAttribute("OrderLine"));
					%>
				<tr>
					<yfc:makeXMLInput name="orderLineKey">
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
					</yfc:makeXMLInput>
					<yfc:makeXMLInput name="bundleRootParentKey">
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@BundleRootParentKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OriginalLineItemClicked" value="xml:/OrderLine/@PrimeLineNo"/>
					</yfc:makeXMLInput>
					<td class="checkboxcolumn" >
						<input type="checkbox" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey" 
						<% if (isDependentParent || isDependentChild) {%> inExistingDependency="true" <%}%>/>
						<%/*This hidden input is required by yfc to match up each line attribute that is editable in this row against the appropriate order line # on the server side once you save.  */%>
						<input type="hidden" name='OrderLineKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>' />
						<input type="hidden" name='OrderHeaderKey_<%=OrderLineCounter%>' value='<%=resolveValue("xml:/Order/@OrderHeaderKey")%>' />
                        <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@OrderLineKey", "xml:/OrderLine/@OrderLineKey")%> />
						<input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@PrimeLineNo", "xml:/OrderLine/@PrimeLineNo")%> />
						<input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@SubLineNo", "xml:/OrderLine/@SubLineNo")%> />
					</td>
					<td class="tablecolumn" nowrap="true">
						<% if (equals(getValue("OrderLine","xml:/OrderLine/@GiftFlag"),"Y") ){ %>
							<img <%=getImageOptions(request.getContextPath() + "/console/icons/gift.gif", "This_is_a_Gift_Line")%>/>
						<% } %>
						<yfc:hasXMLNode binding="xml:/OrderLine/Instructions/Instruction">
							<a <%=getDetailHrefOptions("L01", getParameter("orderLineKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.INSTRUCTIONS_COLUMN, "Instructions")%>></a>
						</yfc:hasXMLNode>
						<yfc:hasXMLNode binding="xml:/OrderLine/KitLines/KitLine">
							<a <%=getDetailHrefOptions("L02", getParameter("orderLineKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Kit_Components")%>></a>
						</yfc:hasXMLNode>

						<% if (equals(getValue("OrderLine","xml:/OrderLine/@IsBundleParent"),"Y")) { %>
						<a <%=getDetailHrefOptions("L02", getParameter("bundleRootParentKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Bundle_Components")%>></a>
								<%}%>


						<yfc:makeXMLInput name="orderLineKey">
							<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
							<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						</yfc:makeXMLInput>
					<%	if(!equals("false", getParameter("CanAddServiceLines") ) )
						{
							if (equals(getValue("OrderLine","xml:/OrderLine/@HasServiceLines"),"Y")) { %>
								<a <%=getDetailHrefOptions("L12", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/providedservicecol.gif", "Line_Has_Associated_Service_Requests")%> ></a>

						<%	}	else	if (equals(getValue("OrderLine","xml:/OrderLine/@CanAddServiceLines"),"Y") && ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>  
								<a <%=getDetailHrefOptions("L10", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/addprovidedservice.gif", "Line_Has_Service_Requests_That_Can_Be_Added")%> ></a>
						<%	}	%>
						<%}%>
						<%	if (isDependentParent) { %>    
								<yfc:makeXMLInput name="dependentParentKey">
									<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
									<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
								</yfc:makeXMLInput>
								<a <%=getDetailHrefOptions("L05", getParameter("dependentParentKey"), "")%>><img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.PARENT_DEPENDENCY_COMPONENTS_COLUMN, "Dependent_Parent")%> ></a>
						<% } %>
						<% if (isDependentChild) { %>
							<yfc:makeXMLInput name="dependentChildKey">
								<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@DependentOnLineKey"/>
								<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
							</yfc:makeXMLInput>
							<a <%=getDetailHrefOptions("L06", getParameter("dependentChildKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.CHILD_DEPENDENCY_COMPONENTS_COLUMN, "Dependent_Child")%>></a>
						<% }%>
						<% if (equals(getValue("OrderLine","xml:/OrderLine/@HasChainedLines"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@ChainedFromOrderLineKey"))) || equals(getValue("OrderLine","xml:/OrderLine/@HasDerivedChild"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@DerivedFromOrderLineKey")))) { %>                        
							<a <%=getDetailHrefOptions("L09", getParameter("orderLineKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.CHAINED_ORDERLINES_COLUMN, "Related_Lines")%>></a>
						<% }%>
						<% if (equals(getValue("OrderLine","xml:/OrderLine/@AwaitingDeliveryRequest"),"Y") && ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>
							<yfc:makeXMLInput name="orderKey">
								<yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
							</yfc:makeXMLInput>
							<a <%=getDetailHrefOptions("L13", getParameter("orderKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/deliveryitem.gif", "Delivery_Request_needs_to_be_added")%>></a>
						<%	}
                            // If the order line has a CurrentWorkOrderKey, the icon will link directly to work order details
                            String linkForWorkOrder = "";
                            String keyNameForLink = "";
							String currentWorkOrderKey = getValue("OrderLine", "xml:/OrderLine/@CurrentWorkOrderKey");
                            if (!isVoid(currentWorkOrderKey)) {
                                linkForWorkOrder = "L15";
                                keyNameForLink = "workOrderKey";
                        %>
                                <yfc:makeXMLInput name="workOrderKey">
                                    <yfc:makeXMLKey binding="xml:/WorkOrder/@WorkOrderKey" value="xml:/OrderLine/@CurrentWorkOrderKey"/>
                                </yfc:makeXMLInput>
                        <%
                            } else {
                                // If the order line has other work orders (without current work order key), the icon will
                                // link to the line-level work order list screen.
                                String numberOfWorkOrders = getValue("OrderLine","xml:/OrderLine/WorkOrders/@NumberOfWorkOrders");
							    if (!isVoid(numberOfWorkOrders)) {
                                    int numberOfWorkOrdersInt = (new Integer(numberOfWorkOrders)).intValue();	
                                    if (numberOfWorkOrdersInt > 0) {
                                        linkForWorkOrder = "L14";
                                        keyNameForLink = "orderLineKey";
                                    }
                                }
                            }
                            if (!isVoid(linkForWorkOrder) && !equals(getValue("Order","xml:/Order/@isHistory"),"Y")) {
                        %>
                                <a <%=getDetailHrefOptions(linkForWorkOrder, getParameter(keyNameForLink), "")%>>
                                    <img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/workorders.gif", "View_Work_Orders")%>>
                                </a>
						<%  } %>
					</td>
					<td class="tablecolumn" sortValue="<%=getNumericValue("xml:OrderLine:/OrderLine/@PrimeLineNo")%>">
						<% if(showOrderLineNo("Order","Order")) {%>
							<a <%=getDetailHrefOptions("L03", getParameter("orderLineKey"), "")%>>
								<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/></a>
						<%} else {%>
							<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
						<%}%>
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/@CustomerLinePONo" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RMA" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@CutomerRef" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ControlNo" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/@CustomerPONo" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemSerialNo" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemType" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@BusinessGrp" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@AgreementNo" />
					</td>
					<td class="tablecolumn">
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@OutOfBoxFailure")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OutOfBoxFailure","xml:/OrderLine/Extn/@OutOfBoxFailure", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="xml:/OrderLine/Extn/@OutOfBoxFailure" />
                        </select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" />
                    </td>
					<td class="tablecolumn"><%=getLocalizedOrderLineDescription("OrderLine")%></td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/@SerialNo" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@OEMSerialNo" />
					</td>
					<td class="tablecolumn">
						 <select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Item/@ProductClass")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","xml:/OrderLine/Item/@ProductClass", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="xml:/OrderLine/Item/@ProductClass"/>
                        </select>
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/PrimaryInformation/@IsHazmat" />
					</td><!-- Service Type -->
					<td class="tablecolumn" >
						<%
							String lineStatus = resolveValue("xml:/OrderLine/@MaxLineStatusDesc");
							if("Draft Order Created".equalsIgnoreCase(lineStatus)){
								
								YFCDocument input = YFCDocument.getDocumentFor("<CommonCode CodeType=\""+resolveValue("xml:OrderLine:/OrderLine/ItemDetails/Extn/@SupportStatusCode")+"\" />");
								YFCDocument template = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" /></CommonCodeList>");
						%>
							<yfc:callAPI apiName='getCommonCodeList' inputElement='<%=input.getDocumentElement()%>'
								templateElement='<%=template.getDocumentElement()%>' outputNamespace='ServiceTypes'/>
							<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ServiceTypeCode","xml:/OrderLine/Extn/@ServiceTypeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
								<yfc:loopOptions binding="xml:ServiceTypes:/CommonCodeList/@CommonCode" name="CodeValue"
		                     value="CodeValue" selected="xml:/OrderLine/Extn/@ServiceTypeCode"/>
							</select>
						<%}else{%>
							<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ServiceTypeCode" />
						<%}%>
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@ShipNode")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ShipNode", "xml:/OrderLine/@ShipNode", "xml:/OrderLine/AllowedModifications")%>/>
						<img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=yfsGetImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node", "xml:/OrderLine/@ShipNode", "xml:/OrderLine/AllowedModifications")%>/>
					</td> 
					<td class="tablecolumn" nowrap="true"><yfc:getXMLValue binding="xml:/Order/@ReceivingNode" /></td>
					<td class="tablecolumn">
		                <select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                           <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
		                    value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
                        </select>
                    </td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@QTYOrderToday" />
					</td>
					<td class="tablecolumn">
						<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/OrderLineTranQuantity/@OrderedQty")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/AllowedModifications")%>/>
						
					</td>
					<!-- Lead time. doubt? -->
					<td class="tablecolumn">
						<input type="text" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@LeadTime")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@LeadTime","xml:/OrderLine/Extn/@LeadTime","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<input type="hidden" id="hidKey_<%=OrderLineCounter%>" name="xml:/Order/@RequiredKey" value="" />
					<%if("Draft Order Created".equalsIgnoreCase(status)){
						String quote = resolveValue("xml:/OrderLine/Extn/@Quote");
						if(!isVoid(quote)){
							String strQuoteFileName = quote.substring((quote.indexOf("file=")+5),quote.indexOf(".pdf"));
					%>
							<td class="tablecolumn" id="quote_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>">
								<a href="<%=resolveValue("xml:/OrderLine/Extn/@Quote")%>"><%=strQuoteFileName%>.pdf</a>
								&nbsp;<%if("Draft Order Created".equalsIgnoreCase(status)){%><a href="#" onclick="return edit('quote_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>','quote');">Edit</a><%}%>
							</td>
						<%}else{%>
							<input type="hidden" id="<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_q" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@Quote" oldValue="" value="" />
							<td class="tablecolumn" id='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_quote'>
								<input type="hidden" id="hidQuote_<%=OrderLineCounter%>" name="xml:/Order/@TempQuote" value="" />
								<input type="file" id="quote_<%=OrderLineCounter%>" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@Quote" oldValue=" " value=""/><input type="submit" value="upload" onclick="return validateFile('quote_<%=OrderLineCounter%>','hidQuote_<%=OrderLineCounter%>','quote','hidKey_<%=OrderLineCounter%>','<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>');" />
							</td>
						<%}%>
					<%}else{%>
						<td class="tablecolumn"></td>
					<%}%>
					<%if("Created".equalsIgnoreCase(status)){
						String confirm = resolveValue("xml:/OrderLine/Extn/@Confirmation");
						if(!isVoid(confirm)){
							String strConfirmFileName = confirm.substring((confirm.indexOf("file=")+5),confirm.indexOf(".pdf"));
					%>
							<td class="tablecolumn" id="confirm_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>">
								<a href="<%=resolveValue("xml:/OrderLine/Extn/@Confirmation")%>"><%=strConfirmFileName%>.pdf</a>
								&nbsp;<%if("Created".equalsIgnoreCase(status)){%><a href="#" onclick="return edit('confirm_Td_<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>','confirm');">Edit</a><%}%>
							</td>
						<%}else if("Created".equalsIgnoreCase(status)){%>
							<input type="hidden" id="<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_c" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@Confirmation" value="" />
							<td class="tablecolumn" id='<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>_confirm'>
								<input type="hidden" id="hidconfirm_<%=OrderLineCounter%>" name="xml:/Order/@TempConfirm" value="" />
								<input type="file" id="confirm_<%=OrderLineCounter%>" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@Confirmation" oldValue=" " value=""/><input type="button" value="upload" onclick="return validateFile('confirm_<%=OrderLineCounter%>','hidconfirm_<%=OrderLineCounter%>','confirm','hidKey_<%=OrderLineCounter%>','<%=resolveValue("xml:/OrderLine/@OrderLineKey")%>');" />
							</td>
						<%}%>
					<%}else{%>
						<td class="tablecolumn"></td>	
					<%}%>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/@ReqShipDate" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/OrderLine/@EarliestDeliveryDate" />
					</td>
					<td class="tablecolumn">
						<yfc:getXMLValue binding="xml:/Order/OrderLines/OrderLine/Extn/@ExtnShipLate" />
					</td>
					<td class="tablecolumn">
						<%if("Draft Order Created".equalsIgnoreCase(lineStatus)){%>
							<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@HandlingNoChargeCode")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@HandlingNoChargeCode","xml:/OrderLine/Extn/@HandlingNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
								<yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
								value="CodeValue" selected="xml:/OrderLine/Extn/@HandlingNoChargeCode"/>
							</select>
						<%}else{%>
							<yfc:getXMLValue binding="xml:/OrderLine/Extn/@HandlingNoChargeCode" />	
						<%}%>
					</td>
					<td class="tablecolumn">
						<%if("Draft Order Created".equalsIgnoreCase(lineStatus)){%>
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@RepairNoChargeCode")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RepairNoChargeCode","xml:/OrderLine/Extn/@RepairNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="xml:/OrderLine/Extn/@RepairNoChargeCode"/>
                        </select>
						<%}else{%>
							<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RepairNoChargeCode" />
						<%}%>
					</td>
					<td class="tablecolumn">
						<%if("Draft Order Created".equalsIgnoreCase(lineStatus)){%>
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ExpeditedNoChargeCode")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExpeditedNoChargeCode","xml:/OrderLine/Extn/@ExpeditedNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="xml:/OrderLine/Extn/@ExpeditedNoChargeCode"/>
                        </select>
						<%}else{%>
							<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ExpeditedNoChargeCode" />
						<%}%>
					</td><!-- AUTO NO CHARGE CODE -->
					<td class="tablecolumn">
						
					</td>
					<td class="tablecolumn">
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ExtnPayment")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExtnPayment","xml:/OrderLine/Extn/@ExtnPayment", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:Payment:/PaymentTypeList/@PaymentType" name="PaymentType" 
		                    value="PaymentType" selected="xml:/OrderLine/Extn/@ExtnPayment"/>
                        </select>
					</td><!-- blanket -->
					<td class="tablecolumn">
						
					</td>
					<td class="tablecolumn">
						<a <%=getDetailHrefOptions("L04", getParameter("orderLineKey"),"ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"),true)%></a>
					</td>
				</tr>
                <%}
            } else if(isVoid(resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey"))) {%>
				<tr DeleteRowIndex="<%=OrderLineCounter%>">
					<td class="checkboxcolumn"> 
						<img class="icon" onclick="setDeleteOperationForRow(this,'xml:/Order/OrderLines/OrderLine')" <%=getImageOptions(YFSUIBackendConsts.DELETE_ICON, "Remove_Row")%>/>
					</td>
                    <td class="tablecolumn">
						<input type="hidden" OldValue="" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "CREATE")%> />
						<input type="hidden"  <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@DeleteRow",  "")%> />
                    </td>
                    <td class="tablecolumn">&nbsp;</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@CustomerLinePONo","xml:/OrderLine/@CustomerLinePONo","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RMA","xml:/OrderLine/Extn/@RMA","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@CutomerRef","xml:/OrderLine/Extn/@CutomerRef","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						
					</td>
					<!--Editable-->
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@CustomerPONo","xml:/OrderLine/@CustomerPONo","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@SystemSerialNo","xml:/OrderLine/Extn/@SystemSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> /><img class="lookupicon" name="search" onclick="callLookupforserial('INBorder','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemSerialNo','xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType','xml:/Order/@ReceivingNode');" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_serial")%>/>
					</td>
					<td class="tablecolumn">
						<input type="text" class="protectedinput" name="xml:/Order/OrderLines/OrderLine_<%=OrderLineCounter%>/Extn/@SystemType" value="" />
					</td>
					<td class="tablecolumn">
					</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@AgreementNo","xml:/OrderLine/Extn/@AgreementNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OutOfBoxFailure","xml:/OrderLine/Extn/@OutOfBoxFailure", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" />
                        </select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text" id='tdPartNo_<%=OrderLineCounter%>' OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ItemID","xml:/OrderLine/Item/@ItemID","xml:/Order/AllowedModifications","ADD_LINE","text")%> onblur="getItemService('tdPartNo_<%=OrderLineCounter%>' ,'tdDesc_<%=OrderLineCounter%>');"/>		
						<img class="lookupicon" id='tdPartNo_<%=OrderLineCounter%>_img' onclick="templateRowCallItemLookup_td(this,'ItemID','ProductClass','TransactionalUOM','TDSitem','<%=extraParams%>','tdPartNo_<%=OrderLineCounter%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%> />
                    </td>
					<td class="tablecolumn" id='tdDesc_<%=OrderLineCounter%>' >&nbsp;</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@SerialNo","xml:/OrderLine/@SerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OEMSerialNo","xml:/OrderLine/Extn/@OEMSerialNo","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<td class="tablecolumn">
						 <select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","xml:/OrderLine/Item/@ProductClass", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="xml:/OrderLine/Item/@ProductClass"/>
                        </select>
					</td>
					<td class="tablecolumn" id='ROHS'>
						<yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/PrimaryInformation/@IsHazmat" />
					</td>
					<td class="tablecolumn" >
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ServiceTypeCode","xml:/OrderLine/Extn/@ServiceTypeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            
                        </select>
					</td>
					<td class="tablecolumn" nowrap="true">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@ShipNode","xml:/OrderLine/@ShipNode","xml:/Order/AllowedModifications","ADD_LINE","text")%>/>
                        <img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node")%>/>
                    </td> 
					<td class="tablecolumn" nowrap="true">&nbsp;</td>
					<td class="tablecolumn">
		                <select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                           <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
		                    value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
                        </select>
                    </td>
					<td class="tablecolumn"> <!-- we have to deal it in code -->
						
					</td>
					<td class="numerictablecolumn">
						<input type="text" OldValue="" 	<%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@OrderedQty","xml:/OrderLine/OrderLineTranQuantity/@OrderedQty","xml:/Order/AllowedModifications","ADD_LINE","text")%> style='width:40px'/>
                    </td>
					<td class="tablecolumn">
						<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@LeadTime","xml:/OrderLine/Extn/@LeadTime","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
					</td>
					<!-- Quote -->
					<td class="tablecolumn">
						&nbsp;
					</td>
					<!-- confirmation -->
					<td class="tablecolumn">
						&nbsp;
					</td>
					<!-- Due date-->
					<td class="tablecolumn">
						<!-- Calculated value -->
					</td>
					<!-- Expected deivery -->
					<td class="tablecolumn">
						<!--Calculated value -->
					</td>
					<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/OrderLines/OrderLine/Extn/@ExtnShipLate" /></td>
					<td class="tablecolumn">
						<select OldValue="N" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@HandlingNoChargeCode","xml:/OrderLine/Extn/@HandlingNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" selected="N"/>
                        </select>
					</td><!-- Dropdown -- Repair no chrg code -->
					<td class="tablecolumn">
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RepairNoChargeCode","xml:/OrderLine/Extn/@RepairNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" />
                        </select>
					</td><!-- Dropdown -- expediate no chrg code -->
					<td class="tablecolumn">
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExpeditedNoChargeCode","xml:/OrderLine/Extn/@ExpeditedNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
		                    value="CodeValue" />
                        </select>
					</td><!-- Dropdown -- auto  no chrg code -->
					<td class="tablecolumn">
						
					</td>
					<td class="tablecolumn">
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExtnPayment","xml:/OrderLine/Extn/@ExtnPayment", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                            <yfc:loopOptions binding="xml:Payment:/PaymentTypeList/@PaymentType" name="PaymentType" 
		                    value="PaymentType" />
                        </select>
					</td><!-- blanket -->
					<td class="tablecolumn">&nbsp;</td>
					<td class="tablecolumn">&nbsp;</td>
                </tr>
            <%}%>
        </yfc:loopXML>
    </tbody>
    <tfoot>        
		<%if (isModificationAllowed("xml:/@AddLine","xml:/Order/AllowedModifications") &&
		       ("Draft Order Created".equalsIgnoreCase(status) || isVoid(status)) ) { %>
        <tr>
        	<td nowrap="true" colspan="40">
        		<jsp:include page="/common/editabletbl.jsp" flush="true">
                <jsp:param name="ReloadOnAddLine" value="Y"/>
        		</jsp:include>
        	</td>
        </tr>
        <%}%>
    </tfoot>
</table>
