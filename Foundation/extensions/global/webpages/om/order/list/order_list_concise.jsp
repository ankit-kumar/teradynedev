<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="hidden" name="userHasOverridePermissions" value='<%=userHasOverridePermissions()%>'/>
                <input type="hidden" name="xml:/Order/@ModificationReasonCode" />
                <input type="hidden" name="xml:/Order/@ModificationReasonText"/>
                <input type="hidden" name="xml:/Order/@Override" value="N"/>
                <input type="hidden" name="ResetDetailPageDocumentType" value="Y"/>	<%-- cr 35413 --%>
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Order_#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Purchase_Order</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Customer_Number</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Order_Date</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Expected_Ship_Date</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Total_Amount</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/OrderList/@Order" id="Order">
            <tr>
                <yfc:makeXMLInput name="orderKey">
                    <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey" />
                </yfc:makeXMLInput>                
                <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("orderKey")%>' name="EntityKey" isHistory='<%=getValue("Order","xml:/Order/@isHistory")%>' 	/>
                </td>
                <td class="tablecolumn">
                    <a href="javascript:showDetailFor('<%=getParameter("orderKey")%>');">
                        <yfc:getXMLValue binding="xml:/Order/@OrderNo"/>
                    </a>               
                </td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@CustomerPONo"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/Order/@ShipToID"/></td>

                <td class="tablecolumn" sortValue="<%=getDateValue("xml:/Order/@OrderDate")%>"><yfc:getXMLValue binding="xml:/Order/@OrderDate"/></td>
				<td class="tablecolumn" sortValue="<%=getDateValue("xml:/Order/@ReqShipDate")%>"><yfc:getXMLValue binding="xml:/Order/@ReqShipDate"/></td>
                <td class="numerictablecolumn" sortValue="<%=getNumericValue("xml:/Order/PriceInfo/@TotalAmount")%>">
                    <%=displayAmount(getValue("Order", "xml:/Order/PriceInfo/@TotalAmount"), (YFCElement) request.getAttribute("CurrencyList"), getValue("Order", "xml:/Order/PriceInfo/@Currency"))%>
                </td>
            </tr>
        </yfc:loopXML>
   </tbody>
</table>