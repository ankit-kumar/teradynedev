<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script>

function showOfficeRecordCommonCodeLookupPopup(CodeValue, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(CodeValue);
		
		//Pass common code type to the lookup screen
		var extraParams = "CodeType=" + extraParams;

		
		yfcShowSearchPopupWithParams('TDorlookupS020','officelookup',900,550,oObj,entityname, extraParams);
		
	}
	
function showOfficeRecordLookupPopup(Value, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(Value);
		//Pass TDOffice attribute to the lookup screen
		var extraParams = "LookupAttribute=" + extraParams;

		
		yfcShowSearchPopupWithParams('TDofficeS020','officelookup',900,550,oObj,entityname, extraParams);
		
	}
</script>
<table width="100%" class="view">
<!-- <Dropdown lists fed by Common Codes>-->
<tr>
<td class="searchlabel" ><yfc:i18n>Office_Type</yfc:i18n></td>
<td class="searchcriteriacell">
            <select name="xml:/TDOfficeRecords/@OfficeType" class="combobox">
                <yfc:loopOptions binding="xml:OfficeTypeList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/TDOfficeRecords/@OfficeType" />
            </select>
        </td>
		
<td class="searchlabel" ><yfc:i18n>Status</yfc:i18n></td>
<td class="searchcriteriacell">
            <select name="xml:/TDOfficeRecords/@OfficeStatus" class="combobox">
                <yfc:loopOptions binding="xml:OfficeStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription"
                value="CodeValue" selected="xml:/TDOfficeRecords/@OfficeStatus" />
            </select>
        </td>

</tr>
<!-- </Dropdown lists fed by Common Codes>-->
<tr>
<td class="searchlabel" ><yfc:i18n>Office_Code</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@OfficeCodeQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@OfficeCodeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@OfficeCode") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TDOfficeRecords/@OfficeCode', 'TERoffice', 'OfficeCode') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Code") %> />
    </td>
</tr>

<tr >
<td class="searchlabel" ><yfc:i18n>Office_Name</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@OfficeNameQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@OfficeNameQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@OfficeName") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TDOfficeRecords/@OfficeName', 'TERoffice', 'OfficeName') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Code") %> />		
    </td>
</tr>

<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Office_Country</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@OfficeCountryQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@OfficeCountryQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@OfficeCountry") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TDOfficeRecords/@OfficeCountry', 'TDofficelookup', 'Office_Country') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Country") %> />
    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> -->
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Price_Zone</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@PriceZoneQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@PriceZoneQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@PriceZone") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TDOfficeRecords/@PriceZone', 'TDofficelookup', 'Price_Zone') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Price_Zone") %> />
    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> -->
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>GSO_Revenue_Region</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@GSORevenueRegionQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@GSORevenueRegionQryType"/>
        </select>
        <input type="text" size="20" maxlength="40" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@GSORevenueRegion") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TDOfficeRecords/@GSORevenueRegion', 'TDofficelookup', 'GSO_Revenue_Region') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_GSO_Revenue_Region") %> />

    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> --> 
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Ops_Coordinator</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@OPSCoordinatorQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@OPSCoordinatorQryType"/>
        </select>
        <input type="text" size="20" maxlength="30" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@OPSCoordinator") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TDOfficeRecords/@OPSCoordinator', 'TDofficelookup', 'Regional_Ops_Coord') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Regional_Ops_Coordinator") %> />

    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> --> 
<!-- <EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Clarify_Workgroup</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@ClarifyWorkgroupQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@ClarifyWorkgroupQryType"/>
        </select>
        <input type="text" size="20" maxlength="40" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@ClarifyWorkgroup") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TDOfficeRecords/@ClarifyWorkgroup', 'TDofficelookup', 'Clarify_Workgroup') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Clarify_Workgroup") %> />

    </td>
</tr>
<!-- </EXTRACTED FROM COMMON CODES> -->
<tr >
<td class="searchlabel" ><yfc:i18n>Service_Office</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@ServiceOfficeQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@ServiceOfficeQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@ServiceOffice") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TDOfficeRecords/@ServiceOffice', 'TERoffice', 'ServiceOffice') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Code") %> />
    </td>
</tr>

<tr >
<td class="searchlabel" ><yfc:i18n>Service_Location</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@ServiceLocationQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@ServiceLocationQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@ServiceLocation") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TDOfficeRecords/@ServiceLocation', 'TERoffice', 'ServiceLocation') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Code") %> />		
    </td>
</tr> 
<tr >
<td class="searchlabel" ><yfc:i18n>Service_Region</yfc:i18n></td>
    <td class="searchcriteriacell" nowrap="true">
        <select class="combobox"  <%=getComboOptions("xml:/TDOfficeRecords/@ServiceRegionQryType") %> >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/TDOfficeRecords/@ServiceRegionQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TDOfficeRecords/@ServiceRegion") %> />
		<img class="lookupicon" name="search" 
			onclick="showOfficeRecordLookupPopup('xml:/TDOfficeRecords/@ServiceRegion', 'TERoffice', 'ServiceRegion') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Office_Code") %> />		
    </td>
</tr>
</table>