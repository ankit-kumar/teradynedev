<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/order.jspf" %>
<%@ page import="com.yantra.shared.inv.*" %>
<%@ include file="/yfsjspcommon/editable_util_lines.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<script language="javascript">
    document.body.attachEvent("onunload", processSaveRecordsForChildNode);
</script>

<%
	appendBundleRootParent((YFCElement)request.getAttribute("Order"));
	boolean bAppendOldValue = false;
	if(!isVoid(errors) || equals(sOperation,"Y") || equals(sOperation,"DELETE")) 
		bAppendOldValue = true;
   String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("Order", "xml:/Order/@EnterpriseCode"));
%>
<table class="table" ID="OrderLines" cellspacing="0" width="100%" >
    <thead>
        <tr>
            <td class="checkboxheader" sortable="no">
                <input type="checkbox" value="checkbox" name="checkbox" onclick="doCheckAll(this);"/>
                <input type="hidden" id="userOperation" name="userOperation" value="" />
                <input type="hidden" id="numRowsToAdd" name="numRowsToAdd" value="" />
            </td>
            <td class="tablecolumnheader" nowrap="true" style="width:30px">&nbsp;</td>
            <td class="tablecolumnheader" nowrap="true" style="width:<%=getUITableSize("xml:/OrderLine/@PrimeLineNo")%>"><yfc:i18n>Line</yfc:i18n></td>
			<!--
			<!%if (isTrue("xml:/Order/@HasDerivedParent")) {%>
	            <td class="tablecolumnheader" nowrap="true" style="width:<!%=getUITableSize("xml:/OrderLine/DerivedFromOrderHeader/@OrderNo")%>"><yfc:i18n>Order_#_Line_#</yfc:i18n></td>
			<!%}%> -->
			<td class="tablecolumnheader"><yfc:i18n>Cust_Line</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Cust_RMA</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Customer_Reference_#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Control#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Customer_Purchase_Order#</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no"><yfc:i18n>System_Serial#</yfc:i18n></td>
			<td class="tablecolumnheader" nowrap="true" sortable="no"><yfc:i18n>System_Type</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Agreement_Warranty#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Out_of_Box_failure?</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Item#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Serial#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>OEM_Serial#</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Description</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Product_Class</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>ROHS</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service_Type</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Ship From</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>UOM</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Qty_Ordered_Today</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Qty</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Lead Time</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Confirmation</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>FDLS</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Due_Date</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Expected_Delivery</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Ship_Late</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Handling_Chrge/Applied</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Repair_No_Chrge_Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Expedite_No_Chrge_Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Auto_No_Charges_Codes</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Payment</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Open_Blanket_PO_Balance</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Status</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Specific_Review_Contact</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML name="Order" binding="xml:/Order/OrderLines/@OrderLine" id="OrderLine">
        <%if(!isVoid(resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey"))) {
            if (equals(getValue("OrderLine","xml:/OrderLine/@ItemGroupCode"), INVConstants.ITEM_GROUP_CODE_SHIPPING) ) {
				if(bAppendOldValue) 
				{
					String sOrderLineKey = resolveValue("xml:OrderLine:/OrderLine/@OrderLineKey");
					if(oMap.containsKey(sOrderLineKey))
						request.setAttribute("OrigAPIOrderLine",(YFCElement)oMap.get(sOrderLineKey));
				} 
				else 
					request.setAttribute("OrigAPIOrderLine",(YFCElement)pageContext.getAttribute("OrderLine"));		
				%>
            <tr>
                <yfc:makeXMLInput name="orderLineKey">
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@OrderLineKey"/>
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>                
                </yfc:makeXMLInput>

				<yfc:makeXMLInput name="derivedFromLineKey">
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@DerivedFromOrderLineKey"/>
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@PrimeLineNo" value="xml:/OrderLine/DerivedFromOrderLine/@PrimeLineNo"/>
                    <yfc:makeXMLKey binding="xml:/OrderLineDetail/@SubLineNo" value="xml:/OrderLine/DerivedFromOrderLine/@SubLineNo"/>
                </yfc:makeXMLInput>
                <yfc:makeXMLInput name="derivedFromOrderKey">
                    <yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/OrderLine/@DerivedFromOrderHeaderKey"/>
                </yfc:makeXMLInput>

					<yfc:makeXMLInput name="bundleRootParentKey">
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderLineKey" value="xml:/OrderLine/@BundleRootParentKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@IsBundleParent" value="xml:/OrderLine/@IsBundleParent"/>
						<yfc:makeXMLKey binding="xml:/OrderLineDetail/@OriginalLineItemClicked" value="xml:/OrderLine/@PrimeLineNo"/>
					</yfc:makeXMLInput>
                <td class="checkboxcolumn" >
                    <input type="checkbox" value='<%=getParameter("orderLineKey")%>' name="chkEntityKey"
				<% 
					if( !showOrderLineNo("Order","Order") ){%> disabled="true" <%}%>
					/>
                    <%/*This hidden input is required by yfc to match up each line attribute that is editable in this row 
                        against the appropriate order line # on the server side once you save.  */%>
                    <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@PrimeLineNo", "xml:/OrderLine/@PrimeLineNo")%> />
                    <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@SubLineNo", "xml:/OrderLine/@SubLineNo")%> />
                    <input type="hidden" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@OrderLineKey", "xml:/OrderLine/@OrderLineKey")%> />
                </td>
                <td class="tablecolumn" nowrap="true">
                    <% if( ! equals("Y", getValue("Order","xml:/OrderLine/@isHistory")) ) { %>
                        <yfc:hasXMLNode binding="xml:/OrderLine/Exceptions/Exception">
                            <a <%=getDetailHrefOptions("L05", getParameter("orderLineKey"), "")%>>
                                <img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.YANTRA_TITLE_ALERT_EXCEPTIONS, "Exceptions")%>></a>
                        </yfc:hasXMLNode>
                    <% } %>
                    <yfc:hasXMLNode binding="xml:/OrderLine/KitLines/KitLine">
                        <a <%=getDetailHrefOptions("L06", getParameter("orderLineKey"), "")%>>
                            <img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Kit_Components")%>></a>
                    </yfc:hasXMLNode>
						<% if (equals(getValue("OrderLine","xml:/OrderLine/@IsBundleParent"),"Y")) { %>
						<a <%=getDetailHrefOptions("L06", getParameter("bundleRootParentKey"), "")%>>
								<img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.KIT_COMPONENTS_COLUMN, "Bundle_Components")%>></a>
								<%}%>

                    <% if (equals(getValue("OrderLine","xml:/OrderLine/@HasDerivedChild"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@DerivedFromOrderLineKey"))) || equals(getValue("OrderLine","xml:/OrderLine/@HasChainedChild"),"Y") || (!isVoid(getValue("OrderLine","xml:/OrderLine/@ChainedFromOrderLineKey")))) { %>                        
                        <a <%=getDetailHrefOptions("L09", getParameter("orderLineKey"), "")%>>
                            <img class="columnicon" <%=getImageOptions(YFSUIBackendConsts.CHAINED_ORDERLINES_COLUMN, "Related_Order_Lines")%>></a>
                    <% }%>

					<%	if (equals(getValue("OrderLine","xml:/OrderLine/@HasServiceLines"),"Y")) { %>
								<a <%=getDetailHrefOptions("L13", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/providedservicecol.gif", "Line_Has_Associated_Service_Requests")%> ></a>

						<%	}	else	if (equals(getValue("OrderLine","xml:/OrderLine/@CanAddServiceLines"),"Y") && ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>  
								<a <%=getDetailHrefOptions("L12", getParameter("orderLineKey"), "")%>><img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/addprovidedservice.gif", "Line_Has_Service_Requests_That_Can_Be_Added")%> ></a>
						<%	}	%>
					
					<% if (isTrue("xml:/OrderLine/@AwaitingDeliveryRequest") 
							&& ! equals(getValue("Order","xml:/Order/@isHistory"),"Y")) { %>
						<yfc:makeXMLInput name="orderKey">
							<yfc:makeXMLKey binding="xml:/Order/@OrderHeaderKey" value="xml:/Order/@OrderHeaderKey"/>
						</yfc:makeXMLInput>
						<a <%=getDetailHrefOptions("L10", getParameter("orderKey"), "")%>>
							<img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/deliveryitem.gif", "Delivery_Request_needs_to_be_added")%>></a>
					<% } %>
                    <% String currentWorkOrderKey = getValue("OrderLine", "xml:/OrderLine/@CurrentWorkOrderKey");
                       if (!isVoid(currentWorkOrderKey)) { %>
                            <yfc:makeXMLInput name="workOrderKey">
                                <yfc:makeXMLKey binding="xml:/WorkOrder/@WorkOrderKey" value="xml:/OrderLine/@CurrentWorkOrderKey"/>
                            </yfc:makeXMLInput>
                            <a <%=getDetailHrefOptions("L11", getParameter("workOrderKey"), "")%>>
                                <img class="columnicon" <%=getImageOptions(request.getContextPath() + "/console/icons/workorders.gif", "View_Work_Order")%>>
                            </a>
                    <% } %>
                </td>
                <td class="tablecolumn" sortValue="<%=getNumericValue("xml:OrderLine:/OrderLine/@PrimeLineNo")%>">
					<% if(showOrderLineNo("Order","Order")) {%>
	                    <a <%=getDetailHrefOptions("L01", getParameter("orderLineKey"), "")%>>
		                    <yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
						</a>
					<%} else {%>
						<yfc:getXMLValue binding="xml:/OrderLine/@PrimeLineNo"/>
					<%}%>
                </td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@CustomerLinePONo" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RMA" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@CutomerRef" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ControlNo" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@CustomerPONo" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemSerialNo" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@SystemType" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@AgreementNo" />
				</td>
				<td class="tablecolumn">
					<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@OutOfBoxFailure")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@OutOfBoxFailure","xml:/OrderLine/Extn/@OutOfBoxFailure", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Extn/@OutOfBoxFailure" />
					</select>
				</td>
				<td class="tablecolumn" nowrap="true">
					<yfc:getXMLValue binding="xml:/OrderLine/Item/@ItemID" />
                </td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@SerialNo" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@OEMSerialNo" />
				</td>
				<td class="tablecolumn"><%=getLocalizedOrderLineDescription("OrderLine")%></td>
				<td class="tablecolumn">
					 <select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Item/@ProductClass")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","xml:/OrderLine/Item/@ProductClass", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Item/@ProductClass"/>
					</select>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/ItemDetails/PrimaryInformation/@IsHazmat" />
				</td>
				<td class="tablecolumn" >
					<%
						String lineStatus = resolveValue("xml:/OrderLine/@MaxLineStatusDesc");
						if("Draft Order Created".equalsIgnoreCase(lineStatus)){
							
							YFCDocument input = YFCDocument.getDocumentFor("<CommonCode CodeType=\""+resolveValue("xml:OrderLine:/OrderLine/ItemDetails/Extn/@SupportStatusCode")+"\" />");
							YFCDocument template = YFCDocument.getDocumentFor("<CommonCodeList><CommonCode CodeType=\"\" CodeValue=\"\" /></CommonCodeList>");
					%>
						<yfc:callAPI apiName='getCommonCodeList' inputElement='<%=input.getDocumentElement()%>'
							templateElement='<%=template.getDocumentElement()%>' outputNamespace='ServiceTypes'/>
						<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ServiceTypeCode","xml:/OrderLine/Extn/@ServiceTypeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
							<yfc:loopOptions binding="xml:ServiceTypes:/CommonCodeList/@CommonCode" name="CodeValue"
						 value="CodeValue" selected="xml:/OrderLine/Extn/@ServiceTypeCode"/>
						</select>
					<%}else{%>
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ServiceTypeCode" />
					<%}%>
				</td>
				<td class="tablecolumn" nowrap="true">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/@ShipNode")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/@ShipNode", "xml:/OrderLine/@ShipNode", "xml:/OrderLine/AllowedModifications")%>/>
					<img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=yfsGetImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node", "xml:/OrderLine/@ShipNode", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
					   <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
						value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
					</select>
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/Extn/@QTYOrderToday" />
				</td>
				<td class="tablecolumn">
					<input type="text"   <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:OrderLine:/OrderLine/OrderLineTranQuantity/@OrderedQty")%>"  <%}%> <%=yfsGetTextOptions("xml:/Order/OrderLines/OrderLine_" + OrderLineCounter + "/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/AllowedModifications")%>/>
				</td>
				<td class="tablecolumn">
					<input type="text" <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@LeadTime")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@LeadTime","xml:/OrderLine/Extn/@LeadTime","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@ReqShipDate" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/OrderLine/@EarliestDeliveryDate" />
				</td>
				<td class="tablecolumn">
					<yfc:getXMLValue binding="xml:/Order/OrderLines/OrderLine/Extn/@ExtnShipLate" />
				</td>
				<td class="tablecolumn">
					<%if("Draft Order Created".equalsIgnoreCase(lineStatus)){%>
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@HandlingNoChargeCode")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@HandlingNoChargeCode","xml:/OrderLine/Extn/@HandlingNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
							<yfc:loopOptions binding="xml:Flag:/CommonCodeList/@CommonCode" name="CodeValue" 
							value="CodeValue" selected="xml:/OrderLine/Extn/@HandlingNoChargeCode"/>
						</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@HandlingNoChargeCode" />	
					<%}%>
				</td>
				<td class="tablecolumn">
					<%if("Draft Order Created".equalsIgnoreCase(lineStatus)){%>
						<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@RepairNoChargeCode")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@RepairNoChargeCode","xml:/OrderLine/Extn/@RepairNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
							<yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" value="CodeValue" selected="xml:/OrderLine/Extn/@RepairNoChargeCode"/>
						</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@RepairNoChargeCode" />
					<%}%>
				</td>
				<td class="tablecolumn">
					<%if("Draft Order Created".equalsIgnoreCase(lineStatus)){%>
					<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ExpeditedNoChargeCode")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExpeditedNoChargeCode","xml:/OrderLine/Extn/@ExpeditedNoChargeCode", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:chrgCode:/CommonCodeList/@CommonCode" name="CodeValue" 
						value="CodeValue" selected="xml:/OrderLine/Extn/@ExpeditedNoChargeCode"/>
					</select>
					<%}else{%>
						<yfc:getXMLValue binding="xml:/OrderLine/Extn/@ExpeditedNoChargeCode" />
					<%}%>
				</td>
				<td>&nbsp;</td>
				<td class="tablecolumn">
					<select <%if(bAppendOldValue) { %>OldValue="<%=resolveValue("xml:/OrderLine/Extn/@ExtnPayment")%>"<%}%> <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Extn/@ExtnPayment","xml:/OrderLine/Extn/@ExtnPayment", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
						<yfc:loopOptions binding="xml:Payment:/PaymentTypeList/@PaymentType" name="PaymentType" 
						value="PaymentType" selected="xml:/OrderLine/Extn/@ExtnPayment"/>
					</select>
				</td>
				<td>&nbsp;</td>
				<td class="tablecolumn">
					<a <%=getDetailHrefOptions("L04", getParameter("orderLineKey"),"ShowReleaseNo=Y")%>><%=displayOrderStatus(getValue("OrderLine","xml:/OrderLine/@MultipleStatusesExist"),getValue("OrderLine","xml:/OrderLine/@MaxLineStatusDesc"),true)%></a>
				</td>
				<td class="tablecolumn" nowrap="true">
					<yfc:getXMLValue binding="xml:/OrderLine/Item/Extn/@SPRAttentionTo" />
                </td>
			<!--
			<!%if (isTrue("xml:/Order/@HasDerivedParent")) {%>
                    <td class="tablecolumn">
					<!%if (! isVoid(getValue("OrderLine", "xml:/OrderLine/DerivedFromOrder/@OrderNo"))) {%>
						<!% if(showOrderNo("OrderLine","DerivedFromOrder")) {%>
		                    <a <!%=getDetailHrefOptions("L02", getParameter("derivedFromOrderKey"), "")%>>
								<yfc:getXMLValue binding="xml:/OrderLine/DerivedFromOrder/@OrderNo"/>
							</a>
						<!%} else {%>
							<yfc:getXMLValue binding="xml:/OrderLine/DerivedFromOrder/@OrderNo"/>
						<!%}%>
					<!%}%>
                    </td>
               <!%}%> -->
            <%}
		  } else {%>
             <tr>
                <td class="checkboxcolumn"> 
                    <img class="icon" onclick="setDeleteOperationForRow(this,'xml:/Order/OrderLines/OrderLine')" <%=getImageOptions(YFSUIBackendConsts.DELETE_ICON, "Remove_Row")%>/>
                </td>
                <%if (isTrue("xml:/Order/@HasDerivedParent")) {%>
                    <td class="tablecolumn">&nbsp;</td>
                <%}%>
               <td class="tablecolumn">
					<input type="hidden" OldValue="" <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@Action", "CREATE")%> />
                    <input type="hidden"  <%=getTextOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@DeleteRow",  "")%> />
                </td>
                <td class="tablecolumn">&nbsp;</td>
                <td class="tablecolumn" nowrap="true">
					<input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ItemID","xml:/OrderLine/Item/@ItemID","xml:/Order/AllowedModifications","ADD_LINE","text")%> />
                    <img class="lookupicon" onclick="templateRowCallItemLookup(this,'ItemID','ProductClass','TransactionalUOM','item','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item")%>/>
                </td>
                <td class="tablecolumn">
					<select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/Item/@ProductClass","xml:/OrderLine/Item/@ProductClass", "xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                       <yfc:loopOptions binding="xml:ProductClassList:/CommonCodeList/@CommonCode" name="CodeValue"
                       value="CodeValue" selected="xml:/OrderLine/Item/@ProductClass"/>
                   </select>
               </td>
               <td class="tablecolumn">      
				    <select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@TransactionalUOM","xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM","xml:/Order/AllowedModifications","ADD_LINE","combo")%>>
                       <yfc:loopOptions binding="xml:UnitOfMeasureList:/ItemUOMMasterList/@ItemUOMMaster" name="UnitOfMeasure"
                       value="UnitOfMeasure" selected="xml:/OrderLine/OrderLineTranQuantity/@TransactionalUOM"/>
                   </select>
               </td>
               <td class="tablecolumn">&nbsp;</td>
               <td class="tablecolumn">
		            <select  OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@ReturnReason", "xml:/OrderLine/@ReturnReason","xml:/Order/AllowedModifications", "ADD_LINE", "combo")%>>
                       <yfc:loopOptions binding="xml:ReasonCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/OrderLine/@ReturnReason" isLocalized="Y"/>
                   </select>
               </td>
               <td class="tablecolumn">
	                <select OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@LineType", "xml:/OrderLine/@LineType","xml:/Order/AllowedModifications", "ADD_LINE", "combo")%>>
                       <yfc:loopOptions binding="xml:LineTypeList:/OrderLineTypeList/@OrderLineType" name="LineTypeDesc"
                       value="LineType" selected="xml:/OrderLine/@LineType" isLocalized="Y"/>
                   </select>
               </td>
               <td class="tablecolumn" nowrap="true">
	                <input type="text" OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/@ShipNode","xml:/OrderLine/@ShipNode", "xml:/Order/AllowedModifications", "ADD_LINE", "text")%>/>
                    <img class="lookupicon" onclick="callLookup(this,'shipnode')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Ship_Node")%>/>
                </td>
               <td class="numerictablecolumn">
				    <input type="text" style='width:40px' OldValue="" <%=yfsGetTemplateRowOptions("xml:/Order/OrderLines/OrderLine_"+OrderLineCounter+"/OrderLineTranQuantity/@OrderedQty", "xml:/OrderLine/OrderLineTranQuantity/@OrderedQty","xml:/Order/AllowedModifications", "ADD_LINE", "text")%>>
               </td>
               <td class="tablecolumn">&nbsp;</td>
               <td class="tablecolumn">&nbsp;</td>
           </tr>
           <%}%>
        </yfc:loopXML>
    </tbody>
    <tfoot>        
		<%if (! isTrue("xml:/Order/@HasDerivedParent")) {%>
			<%if (isModificationAllowed("xml:/@AddLine","xml:/Order/AllowedModifications")) { %>
			<tr>
				<td nowrap="true" colspan="38">
					<jsp:include page="/common/editabletbl.jsp" flush="true">
                    <jsp:param name="ReloadOnAddLine" value="Y"/>
					</jsp:include>
				</td>
			</tr>
			<%} 
		}%>
    </tfoot>
</table>
