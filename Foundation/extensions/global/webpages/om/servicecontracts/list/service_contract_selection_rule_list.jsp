<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" style="width:<%= getUITableSize("xml:/InventoryItem/@ItemID")%>">
            <yfc:i18n>Rule_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryItem/@ProductClass")%>">
            <yfc:i18n>Template_Selection_Rule_Name</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/InventoryItem/@UnitOfMeasure")%>">
            <yfc:i18n>MFG_Div_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryItem/@UnitOfMeasure")%>">
            <yfc:i18n>Install_Base_Item_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryItem/@UnitOfMeasure")%>">
            <yfc:i18n>Country_Code</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="InventoryList" binding="xml:/InventoryList/@InventoryItem" id="InventoryItem"  keyName="InventoryItemKey" > 
    <tr> 
        <yfc:makeXMLInput name="inventoryItemKey">
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:/InventoryItem/@ItemID" />
        </yfc:makeXMLInput>
        <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("OfficeKey")%>' name="EntityKey"/>
            </td>
		<!-- ID -->
        <td class="tablecolumn">
            <a onclick="javascript:showDetailFor('<%=getParameter("inventoryItemKey")%>');return false;" href=""><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@ItemID"/></a>
        </td>
		<!-- Name -->
        <td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@ProductClass"/></td>
		<!-- DIV ID -->
        <td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@UnitOfMeasure"/></td>
		<!-- Install Base Item ID-->
        <td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/Item/PrimaryInformation/@Description"/></td>
		<!-- Country -->
		<td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@ProductClass"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>