<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@include file="/console/jsp/order.jspf" %>
<%@ include file="/console/jsp/paymentutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script>

function showECCNvalueLookupPopup(ECCN, entityname)
	{
		var oObj = new Object();
		oObj.field1 = document.all(ECCN);
	
		yfcShowSearchPopup('TDexportS020','ECCNlookup',900,550,oObj,entityname);
		
	}
</script>

<table class="view">
	<tr>
		<td>
		<input type="hidden" name="xml:/CommonCode/CodeValueQryType_1" value="FLIKE" />
		<input type="hidden" name="xml:/CommonCode/CodeValueQryType_2" value="LIKE" />
		<%
		String codeType = resolveValue("xml:CommonCode/@CodeType");
		ArrayList ECCNList = getLoopingElementList("xml:ECCNList:/CommonCodeList/@CommonCode");

			for(int counter = 0; counter < ECCNList.size(); counter++)
			{
				YFCElement ECCN = (YFCElement)ECCNList.get(counter);
				String codeValue = ECCN.getAttribute("CodeValue");
				%>
				<input type="hidden" name="xml:/CommonCode/ComplexQuery/Or/Exp_<%=counter%>/@Name" value="CodeType" />
				<input type="hidden" name="xml:/CommonCode/ComplexQuery/Or/Exp_<%=counter%>/@Value" value="<%=codeValue%>" />
				<%
			}
		
		%>
		</td>
	</tr>
	<% // Now call the APIs that will populate the values for  liscense exception code%>
    <yfc:callAPI apiID="AP1"/>
	<tr>

        <td class="searchlabel" >
            <yfc:i18n>ECCN</yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">
		
            <select name="xml:/CommonCode/@CodeTypeQryType" class="combobox">
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/CommonCode/@CodeTypeQryType"/>
            </select>
            <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/CommonCode/@CodeType")%>/>
			<img class="lookupicon" name="search" 
				onclick="showECCNvalueLookupPopup('xml:/CommonCode/@CodeType', 'TDexportliscense') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Valid_ECCN") %> />
        </td>
    </tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_To_Country</yfc:i18n>
        </td>
    </tr>
    <tr>

		    <td>
				<select name="xml:/CommonCode/@CodeValue_1" class="combobox">
					<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/CommonCode/@CodeValue_1" isLocalized="Y"/>
				</select>			
	        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>Ship_From_Country</yfc:i18n>
        </td>
    </tr>
    <tr>
		    <td>
				<select name="xml:/CommonCode/@CodeValue_2" class="combobox">
					<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/CommonCode/@CodeValue_2" isLocalized="Y"/>
				</select>			
	        </td>
    </tr>
	<tr>
        <td class="searchlabel" >
            <yfc:i18n>License_Exception_Code</yfc:i18n>
        </td>
    </tr>
    <tr>
		    <td>
				<select name="xml:/CommonCode/@CodeShortDescription" class="combobox" >
					<yfc:loopOptions binding="xml:LiscenseExceptionCode:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/CommonCode/@CodeShortDescription" isLocalized="Y"/>
				</select>			
	        </td>
    </tr>
</table>
