<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/currencyutils.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/modificationreason.js"></script> 
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<table class="table" editable="false" width="100%" cellspacing="0">
    <thead> 
        <tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
            <td class="tablecolumnheader"><yfc:i18n>Ship_To_Country</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Item_ID</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>ECCN</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>HTS</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>HTS_Description</yfc:i18n></td>
        </tr>
    </thead>
    <tbody>
        <yfc:loopXML binding="xml:/TerIntItemCodesList/@TerIntItemCodes" id="TerIntItemCodes">
            <tr>
                <yfc:makeXMLInput name="itemCodesKey">
                    <yfc:makeXMLKey binding="xml:/TerIntItemCodes/@IntItemCodesKey" value="xml:/TerIntItemCodes/@IntItemCodesKey" />
                </yfc:makeXMLInput>                
                <td class="checkboxcolumn">                     
                    <input type="checkbox" value='<%=getParameter("itemCodesKey")%>' name="EntityKey"/>
                </td>

				<td class="tablecolumn">
					<a href="javascript:showDetailFor('<%=getParameter("itemCodesKey")%>');"><yfc:getXMLValue binding="xml:/TerIntItemCodes/@TerShipToCountryCode"/></a>
				</td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerIntItemCodes/@TerItemID"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerIntItemCodes/@TerECCNCode"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerIntItemCodes/@TerHTSCode"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerIntItemCodes/@TerHTSDescription"/></td>
            </tr>
        </yfc:loopXML>
   </tbody>
</table>