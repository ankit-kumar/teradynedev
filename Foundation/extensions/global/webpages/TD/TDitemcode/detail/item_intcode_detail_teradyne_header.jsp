<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script language="javascript">
function inputValidation()
	{
		var inputElem = document.getElementById("ShipToCountry");
		var inputElemVal = inputElem.options[inputElem.selectedIndex].text;
		
		var errorLabel = document.getElementById("ErrorLabel");
		var errorMessage="";
		var result = true;

		//check ShipToCountry dropdown box or the Ship To Country selected
		if(inputElemVal.length < 1)
		{
			result = false;
		}
		
		
		//check TerItemID text box
		inputElem = document.getElementById("TerItemID");
		inputElemVal = inputElem.value;
		
		if(inputElemVal.length < 1)
		{
			result = false;
		}
		
		//check ECCN text box
		inputElem = document.getElementById("ECCN");
		inputElemVal = inputElem.options[inputElem.selectedIndex].text;;
		
		if(inputElemVal.length < 1)
		{
			result = false;
		}
		
		//check HTCCode text box
		inputElem = document.getElementById("HTCCode");
		inputElemVal = inputElem.value;
		
		if(inputElemVal.length < 1)
		{
			result = false;
		}
			
		//check HTCCode text box
		inputElem = document.getElementById("HTCDescription");
		inputElemVal = inputElem.value;
		
		if(inputElemVal.length < 1)
		{
			result = false;
		}
		
		if(result == false)
		{
			errorMessage = "All fields are required. <br/><br/>" + errorMessage;
			errorLabel.innerHTML = errorMessage;
		}
		return (result);
	}
</script>
<table class="view" width="100%">
	<tr>
 		<td>
 			<div id="ErrorLabel" style="color:#ff0000"></div>
 		</td>
 	</tr>
	<tr>
	
		<td class="detaillabel" ><yfc:i18n>Ship_To_Country</yfc:i18n></td>

        <td>			
			<select name="xml:/TerIntItemCodes/@TerShipToCountryCode" id="ShipToCountry" class="combobox">
				<yfc:loopOptions binding="xml:CommonCountryCodeList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:TerIntItemCodes/@TerShipToCountryCode" isLocalized="Y"/>
			</select>			
        </td>
		<td class="detaillabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
		<td>
			<input type="text" id="TerItemID" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerItemID")%>/>
            <img class="lookupicon" name="search" 
			onclick="callItemLookup('xml:/TerIntItemCodes/@TerItemID',' ',' ',
			'item',' ')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Item") %> />
		</td>
		
		<td class="detaillabel" ><yfc:i18n>ECCN</yfc:i18n></td>
		<td>
			<select name="xml:/TerIntItemCodes/@TerECCNCode" id="ECCN" class="combobox" >
				<yfc:loopOptions binding="xml:ECCNList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/TerIntItemCodes/@TerECCNCode" isLocalized="Y"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" ><yfc:i18n>HTC</yfc:i18n></td>
		<td>
			<input type="text" size="10" maxlength="40" id="HTCCode" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerHTSCode")%>/>

		</td>
		<td class="detaillabel" ><yfc:i18n>HTC_Description</yfc:i18n></td>
		<td>
			<input type="text" size="20" maxlength="40" id="HTCDescription" class="unprotectedinput" <%=getTextOptions("xml:/TerIntItemCodes/@TerHTSDescription")%>/>

		</td>
	</tr>


</table>
