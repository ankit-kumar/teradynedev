<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script>
function setServiceTypeLookupValue(value)
	{
		var Obj = window.dialogArguments
		if(Obj != null)
		{
			Obj.field1.value = value;
			
		}
		window.close();
	}
</script>


<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td class="lookupiconheader" sortable="no"><br /></td>
		<td class="tablecolumnheader"><yfc:i18n>Service_Type</yfc:i18n></td>   
		<td class="tablecolumnheader"><yfc:i18n>Description</yfc:i18n></td>						
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/CommonCodeList/@CommonCode" id="CommonCode"> 
    <tr> 
        <td class="tablecolumn">
			<img class="icon" onClick="setServiceTypeLookupValue(this.value)"  value="<%=resolveValue("xml:/CommonCode/@CodeValue")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click_to_Select")%> />
        </td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeValue"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeShortDescription"/></td>

    </tr>
    </yfc:loopXML> 
</tbody>
</table>


