<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
//Get the document type that the lookup is for
String sCodeType;
	sCodeType = request.getParameter("CodeType");
%>

<table class="view">
	<tr>
		<td>
			<input type="hidden" name="CodeType" value="<%=HTMLEncode.htmlEscape(sCodeType)%>"/>
			<input type="hidden" name="xml:/CommonCode/@CodeType" value="<%=sCodeType%>"/>
		</td>
	</tr>
    <tr>
        <td class="searchlabel" >
            <yfc:i18n><%=sCodeType%></yfc:i18n>
        </td>
    </tr>
    <tr>
        <td nowrap="true" class="searchcriteriacell">		
            <select class="combobox" <%=getComboOptions("xml:/CommonCode/@CodeValueQryType") %> >
                <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" name="QueryTypeDesc"
                value="QueryType" selected="xml:/CommonCode/@CodeValueQryType"/>
            </select>
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/CommonCode/@CodeValue")%>/>	
		</td>
            	
    </tr>
</table>
