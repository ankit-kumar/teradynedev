<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script>
function setOfficeRecordLookupValue(value){
		var Obj = window.dialogArguments
		if(Obj != null)	{
			Obj.field1.value = value;			
		}
		window.close();
	}
</script>

<%
	String sCodeType = resolveValue("xml:/CommonCodeList/CommonCode/@CodeType");
	String actualSearchLabel = resolveValue("xml:/TempSearchLabel/@ActualSearchLabel");
%>
<table class="table" width="100%" editable="false">
<thead>
   <tr> 
        <td class="lookupiconheader" sortable="no"><br /></td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/CommonCodeList/CommonCode/@CodeValue")%>">
            <yfc:i18n><%=actualSearchLabel%></yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/CommonCodeList/CommonCode/@CodeShortDescription")%>">
            <yfc:i18n>Short Description</yfc:i18n>
        </td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/CommonCodeList/@CommonCode" id="CommonCode"> 
    <tr> 
        <td class="tablecolumn">
			<%	
				if(equals(actualSearchLabel, "Regional Manager")){%>
				<img class="icon" onClick="setOfficeRecordLookupValue(this.value)"  value="<%=resolveValue("xml:/CommonCode/@CodeShortDescription")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click to Select")%> />
				<% } else {%>	
				<img class="icon" onClick="setOfficeRecordLookupValue(this.value)"  value="<%=resolveValue("xml:/CommonCode/@CodeValue")%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click to Select")%> />
			<% } %>
		</td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeValue"/></td>
        <td class="tablecolumn"><yfc:getXMLValue binding="xml:/CommonCode/@CodeShortDescription"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>