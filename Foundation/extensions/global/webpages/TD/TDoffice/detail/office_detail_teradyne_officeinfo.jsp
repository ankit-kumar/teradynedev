<!--@Author: SourabhG-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>
<script>

function showOfficeRecordCommonCodeLookupPopup(CodeValue, entityname, extraParamCodeType, extraParamActualSearchLabel){
		var oObj = new Object();
		oObj.field1 = document.all(CodeValue);
		var extraParams = "&CodeType=" + extraParamCodeType + "&ActualSearchLabel="+extraParamActualSearchLabel;
		yfcShowSearchPopupWithParams('TDorlookupS020',' ',850,550,oObj,entityname, extraParams);		
	}

function showOfficeRecordLookupPopup(Value, entityname, extraParamsLookUpAttribute, extraParamOfficeTypeValue){
		var oObj = new Object();
		oObj.field1 = document.all(Value);
		var extraParams = "&LookupAttribute=" + extraParamsLookUpAttribute + "&OfficeTypeValue="+extraParamOfficeTypeValue;		
		yfcShowSearchPopupWithParams('TDofficeS030',' ',900,550,oObj,entityname, extraParams);
	}

function enterActionOfficeRecordModificationReason(modReasonViewID, modReasonCodeBinding, modReasonTextBinding){
		var prizeZoneValue=document.getElementById('priceZoneID').value;
		var prizeZoneValueLength=prizeZoneValue.length;
		if(prizeZoneValueLength <= 0){
			alert("Price Zone is Required");   
			return false;  
		}

		var modReasonText = (document.all(modReasonTextBinding)).value;
		if(!('Office Record Created'== modReasonText)){
		var myObject = new Object();
		myObject.currentWindow = window;
		myObject.reasonCodeInput = document.all(modReasonCodeBinding);
		myObject.reasonTextInput = document.all(modReasonTextBinding);

		yfcShowDetailPopup(modReasonViewID, "", "550", "255", myObject);

		if (getOKClickedAttribute() == "true") {
			window.document.documentElement.setAttribute("OKClicked", "false");
			return (true);
		}
		else {
			window.document.documentElement.setAttribute("OKClicked", "false");
			return (false);
		}
	} else
		{
		return(true);
	}
}

function getOKClickedAttribute() {
    return (window.document.documentElement.getAttribute("OKClicked"));
}
</script>

<table class="view" width="100%">
	<tr>
		<td>
			<input type="hidden" name="xml:/TerOfficeRecords/@TerModificationReasonCode" value="<%=resolveValue("xml:/TerOfficeRecords/@TerModificationReasonCode")%>"/>
			<input type="hidden" name="xml:/TerOfficeRecords/@TerModificationReasonText" value="<%=resolveValue("xml:/TerOfficeRecords/@TerModificationReasonText")%>"/>
			<input type="hidden" name="xml:/TerOfficeRecords/@TerOrganizationCode" value="<%=resolveValue("xml:CurrentUser:/User/@OrganizationKey")%>"/>
		</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

    <tr>
        <td><yfc:i18n>Office Name</yfc:i18n></td>		
        <td><yfc:i18n>Office Country</yfc:i18n></td>		
		<td><yfc:i18n>Office Status</yfc:i18n></td>
		<td></td>
	</tr>
	<tr>
	    <td nowrap="true"><input type="text" size="20" maxlength="50" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeName")%>/></td>
		
		<td nowrap="true">
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOfficeCountry")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerOfficeCountry', 'TDofficelookup', 'Office_Country','Office Country') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Office Country") %> />
		</td>

        <td nowrap="true">
				<%	
					String officeStatusDB=resolveValue("xml:/TerOfficeRecords/@TerOfficeStatus");
					String officeStatusSelected=officeStatusDB;
					if(equals(officeStatusDB," ")){
					officeStatusSelected="A";
					}
				%>
			<select name="xml:/TerOfficeRecords/@TerOfficeStatus" class="combobox" >
				<yfc:loopOptions binding="xml:OfficeStatusList:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="<%=officeStatusSelected%>" isLocalized="Y"/>
			</select>

		</td>
		<td></td>
	</tr>
	<tr>
		<td><br/></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
        <td><yfc:i18n>Price Zone</yfc:i18n></td>		
        <td><yfc:i18n>GSO Revenue Region</yfc:i18n></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
        <td nowrap="true">
			<input type="text" class="unprotectedinput" id="priceZoneID" <%=getTextOptions("xml:/TerOfficeRecords/@TerPriceZone")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerPriceZone', 'TDofficelookup', 'Price_Zone', 'Price Zone') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Price Zone") %> />
		</td>

        <td nowrap="true">
			<input type="text" size="30" maxlength="30" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerGsoRevenueRegion")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerGsoRevenueRegion', 'TDofficelookup', 'GSO_Revenue_Region', 'GSO Revenue Region') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for GSO Revenue Region") %> />

		</td>
		<td></td>
		<td></td>

	</tr>
	<tr>
		<td><br/></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
        <td><yfc:i18n>Worldwide Region</yfc:i18n></td>		
        <td><yfc:i18n>Service Region</yfc:i18n></td>
		<td><yfc:i18n>Service Sub-Region</yfc:i18n></td>		
		<td><yfc:i18n>Service Location</yfc:i18n></td>		
	</tr>
	<tr>
        <td nowrap="true">
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerWorldwideRegion")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerWorldwideRegion', 'TDoffice','TerWorldwideRegion', 'W') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Worldwide Region") %> />
		</td>

        <td nowrap="true">
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceRegion")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerServiceRegion', 'TDoffice', 'TerServiceRegion', 'C')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Service Region") %> />
		</td>

        <td nowrap="true">
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceOffice")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerServiceOffice', 'TDoffice', 'TerServiceOffice', 'R') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Service Sub-Region") %> />
		</td>

        <td nowrap="true">
			<input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerServiceLocation")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordLookupPopup('xml:/TerOfficeRecords/@TerServiceLocation', 'TDoffice', 'TerServiceLocation', 'L') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Service Location") %> />
		</td>

	</tr>
	<tr>
		<td><br/></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	
	<tr>
        <td><yfc:i18n>Worldwide Region Name</yfc:i18n></td>		
        <td><yfc:i18n>Service Region Name</yfc:i18n></td>
		<td><yfc:i18n>Service Sub-Region Name</yfc:i18n></td>		
		<td><yfc:i18n>Service Location Name</yfc:i18n></td>	
	</tr>
	<tr> 
		<td class="protectedtext"><yfc:getXMLValue binding="xml:WROfficeNameSpace:/TerOfficeRecordsList/TerOfficeRecords/@TerOfficeName"/></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:SROfficeNameSpace:/TerOfficeRecordsList/TerOfficeRecords/@TerOfficeName"/></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:SRROfficeNameSpace:/TerOfficeRecordsList/TerOfficeRecords/@TerOfficeName"/></td>
		<td class="protectedtext"><yfc:getXMLValue binding="xml:SLOfficeNameSpace:/TerOfficeRecordsList/TerOfficeRecords/@TerOfficeName"/></td>
	</tr>	
	<tr>
		<td><br/></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>

	<tr>
        <td><yfc:i18n>Regional Manager</yfc:i18n></td>
        <td><yfc:i18n>Regional Ops Coordinator</yfc:i18n></td>
		<td><yfc:i18n>Engineer Labor Rate</yfc:i18n></td>
        <td><yfc:i18n>Clarify Workgroup</yfc:i18n></td>
		<td></td>
		<td></td>
	</tr>
	<tr>

		<td nowrap="true">
			<input type="text" size="20" maxlength="30" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerRegionalManager")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerRegionalManager', 'TDofficelookup', 'Regional_Manager','Regional Manager') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Regional Manager") %> />		
		</td>

        <td nowrap="true">
			<input type="text" size="20" maxlength="30" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerOpsCoordinator")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerOpsCoordinator', 'TDofficelookup', 'Regional_Ops_Coord','Regional Ops Coord') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Regional Ops Coordinator") %> />
		</td>

	    <td nowrap="true"><input type="text" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerEngineerLaborRate")%>/></td>

        <td nowrap="true">
			<input type="text" size="20" maxlength="40" class="unprotectedinput" <%=getTextOptions("xml:/TerOfficeRecords/@TerClarifyWorkgroup")%>/>
			<img class="lookupicon" name="search" 
				onclick="showOfficeRecordCommonCodeLookupPopup('xml:/TerOfficeRecords/@TerClarifyWorkgroup', 'TDofficelookup', 'Clarify_Workgroup','Clarify Workgroup') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search for Clarify Workgroup") %> />		
		</td>

        <td></td>
        <td></td>
	</tr>
</table>
