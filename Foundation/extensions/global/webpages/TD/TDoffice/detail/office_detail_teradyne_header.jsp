<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/om.js"></script>

<table class="view" width="40%">

	<tr>
		<td><input type="hidden" name="xml:/TerOfficeRecords/@TerOfficeCode" value="<%=resolveValue("xml:/TerOfficeRecords/@TerOfficeCode")%>"/></td>
		<td><input type="hidden" name="xml:/TerOfficeRecords/@TerOfficeType" value="<%=resolveValue("xml:/TerOfficeRecords/@TerOfficeType")%>"/></td>		
		<td/><td/><td/><td/><td/><td/>
	</tr>

    <tr>
        <td nowrap="true"><yfc:i18n>Office Code</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeCode"/></td>
		<td/><td/><td/>
        <td nowrap="true"><yfc:i18n>Office Type</yfc:i18n></td>
        <td class="protectedtext"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeType"/></td>
	</tr>
</table>
