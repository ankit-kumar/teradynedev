<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf" %>

<script>
/*******************************************************************************
 * (C) Copyright  2009 Sterling Commerce, Inc.
 *******************************************************************************/
// modificationreasonpopup.js file consists of all the javascript methods used
// in the modification reason popup JSP.
var myObject = new Object();
myObject = window.dialogArguments
var parentWindow = myObject.currentWindow;
var parentReasonCodeInput = myObject.reasonCodeInput;
var parentReasonTextInput = myObject.reasonTextInput;
var TerModificationReasonCode="";
var TerModificationReasonText="";

function setOKClickedAttribute() {

    if(validateControlValues()) {
        parentWindow.document.documentElement.setAttribute("OKClicked", "true");
        var reasonCodeInput = document.all("xml:/TerOfficeRecords/@TerModificationReasonCode");
        parentReasonCodeInput.value = reasonCodeInput.value;

        var reasonTextInput = document.all("xml:/TerOfficeRecords/@TerModificationReasonText");
        parentReasonTextInput.value = reasonTextInput.value;

        window.close();
        }
}

function setReasonCode(value){
		TerModificationReasonCode = value;
	}
function setReasonText(value){
		TerModificationReasonText = value;
	}
		

</script>

<table width="100%" class="view">
    <tr>
        <td>
            <yfc:i18n>Reason Code</yfc:i18n>
        </td>
        <td>
            <select name="xml:/TerOfficeRecords/@TerModificationReasonCode" class="combobox">
                <yfc:loopOptions binding="xml:ReasonCodeList:/CommonCodeList/@CommonCode" 
                    name="CodeShortDescription" value="CodeValue" isLocalized="Y"/>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <yfc:i18n>Reason Text</yfc:i18n>
        </td>
        <td>
            <textarea class="unprotectedtextareainput" rows="3" cols="50" <%=getTextAreaOptions("xml:/TerOfficeRecords/@TerModificationReasonText")%>></textarea>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <input type="button" class="button" value='<%=getI18N("__OK__")%>' onclick="setOKClickedAttribute();return false;"/>
            <input type="button" class="button" value='<%=getI18N("Cancel")%>' onclick="window.close();"/>
        <td>
    <tr>
</table>