<!--@Author-->

<%@ include file="/yfsjspcommon/yfsutil.jspf" %>
<%@ include file="/console/jsp/modificationutils.jspf" %>
<%@ include file="/console/jsp/order.jspf" %>


<table cellSpacing=0 class="anchor" cellpadding="7px">
<tr>
    <td>
        <yfc:makeXMLInput name="commonCodeKey">
            <yfc:makeXMLKey binding="xml:/CommonCode/@CommonCodeKey" value="xml:/CommonCode/@CommonCodeKey"/>
        </yfc:makeXMLInput>
        <input type="hidden" value='<%=getParameter("commonCodeKey")%>' name="CommonCodeEntitykey"/>
        <input type="hidden" <%=getTextOptions("xml:/CommonCode/@CommonCodeKey")%>/>

		<%	
		String worldWideRegion=resolveValue("xml:/TerOfficeRecords/@TerWorldwideRegion");
		String serviceRegion=resolveValue("xml:/TerOfficeRecords/@TerServiceRegion");
		String serviceSubRegion=resolveValue("xml:/TerOfficeRecords/@TerServiceOffice");
		String serviceLocation=resolveValue("xml:/TerOfficeRecords/@TerServiceLocation");
		if(!(equals(worldWideRegion,"") || equals(worldWideRegion," "))){%>
			<yfc:callAPI apiID='AP1'/>
		<%}%>

		<%
		if(!(equals(serviceRegion,"") || equals(serviceRegion," "))){%>
			<yfc:callAPI apiID='AP2'/>
		<%}%>

		<%
		if(!(equals(serviceSubRegion,"") || equals(serviceSubRegion," "))){%>
			<yfc:callAPI apiID='AP3'/>
		<%}%>

		<%
		if(!(equals(serviceLocation,"") || equals(serviceLocation," "))){%>
			<yfc:callAPI apiID='AP4'/>
		<%}%>

    </td>
</tr>
<tr>
    <td colspan="4">
	    <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I01"/>
            <jsp:param name="ModifyView" value="true"/>
		    <jsp:param name="getRequestDOM" value="Y"/>
        </jsp:include>
    </td>
</tr>
<tr>
<td colspan="4">
	    <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I02"/>
            <jsp:param name="ModifyView" value="true"/>
		    <jsp:param name="getRequestDOM" value="Y"/>
        </jsp:include>
    </td>
</tr>

</table>
