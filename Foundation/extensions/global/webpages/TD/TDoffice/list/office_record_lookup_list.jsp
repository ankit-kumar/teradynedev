<!--@Author-->

<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@page import="com.yantra.yfs.ui.backend.*" %>

<script>
function setOfficeRecordLookupValue(value){
		var Obj = window.dialogArguments;
		if(Obj != null){
			Obj.field1.value = value;			
		}
		window.close();
	}
</script>

<%
String sLookupAttr = request.getParameter("LookupAttribute");
%>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
        <td class="lookupiconheader" sortable="no"><br /></td>
            <td class="tablecolumnheader"><yfc:i18n>Office Code</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Office Name</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Office Type</yfc:i18n></td>
            <td class="tablecolumnheader"><yfc:i18n>Office Status</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Worldwide Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service Sub-Region</yfc:i18n></td>
			<td class="tablecolumnheader"><yfc:i18n>Service Location</yfc:i18n></td>
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TerOfficeRecordsList/@TerOfficeRecords" id="TerOfficeRecords"> 
    <tr> 
        <td class="tablecolumn">
			<img class="icon" onClick="setOfficeRecordLookupValue(this.value)"  value="<%=resolveValue("xml:/TerOfficeRecords/@" + sLookupAttr)%>" <%=getImageOptions(YFSUIBackendConsts.GO_ICON,"Click to Select")%> />
        </td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeCode"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeName"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeType"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerOfficeStatus"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerWorldwideRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerServiceRegion"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerServiceOffice"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TerOfficeRecords/@TerServiceLocation"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
