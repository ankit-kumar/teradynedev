<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<script language="javascript">

function showDiscountGroupCustomerNoPopup(Value, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(Value);
		var extraParams = "CustomerNo=" + extraParams;		
		yfcShowSearchPopupWithParams('TDDGLkpCusS030','DisGrpLupCustomer',900,550,oObj,entityname, extraParams);
		
	}
	
	function showDiscountGroupServiceTypePopup(CodeValue, entityname, extraParams)
	{
		var oObj = new Object();
		oObj.field1 = document.all(CodeValue);
		//Pass Common code type to the lookup screen
		var extraParams = "CodeType=" + extraParams;
		yfcShowSearchPopupWithParams('TDDGlookupS020','Discoutgrouplookup',900,550,oObj,entityname,extraParams);
		
	}
</script>

<table class="view">
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Discount_Group</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TDDiscountgroup/@DiscountGroupIdQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TDDiscountgroup/@DiscountGroupIdQryType"/>
		</select>        
    </td>
	<td class="searchcriteriacell">
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TDDiscountgroup/@TerDiscountGroupId")%>/>
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Customer_#</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
        <select class="combobox"  <%=getComboOptions("xml:/TDDiscountgroup/@CustomerNoQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TDDiscountgroup/@CustomerNoQryType"/>
		</select>			
    </td>
	<td class="searchcriteriacell">
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TDDiscountgroup/@TerCustomerNo")%>/>
		<img class="lookupicon" name="search" 
		onclick="showDiscountGroupCustomerNoPopup('xml:/TDDiscountgroup/@TerCustomerNo', 'TDDisGrpLupCustomer', 'CustomerNo') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Customer_Number") %> />
	</td>
	</tr>
	
	<tr>
		<td class="searchlabel" ><yfc:i18n>Customer_Name</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TDDiscountgroup/@CustomerNameQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TDDiscountgroup/@CustomerNameQryType"/>
		</select>
	</td>
	<td class="searchcriteriacell">
		<input type="text" size="30" maxlength="20" class="unprotectedinput" <%=getTextOptions("xml:/TDDiscountgroup/@TerCustomerName")%>/>
	</td>
	</tr>
	
	<tr>
	<td class="searchlabel" ><yfc:i18n>Service_Type</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TDDiscountgroup/@ServiceTypeQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TDDiscountgroup/@ServiceTypeQryType"/>
		</select>
	</td>
	<td class="searchcriteriacell">	
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TDDiscountgroup/@TerServiceType")%>/>
		<img class="lookupicon" name="search" 
		onclick="showDiscountGroupServiceTypePopup('xml:/TDDiscountgroup/@TerServiceType', 'TDDiscoutgrouplookup', 'Service_Type') " <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_Service_Type") %> />
	</td>
	</tr>
	
	<tr>
	<td class="searchlabel" ><yfc:i18n>System_Type</yfc:i18n></td>
	</tr>
	<tr></tr>
	
	<tr>
	<td class="searchcriteriacell">
		<select class="combobox"  <%=getComboOptions("xml:/TDDiscountgroup/@SystemTypeQryType") %> >
			<yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
			name="QueryTypeDesc" value="QueryType" selected="xml:/TDDiscountgroup/@SystemTypeQryType"/>
		</select>
	</td>
	<td class="searchcriteriacell">
		<input type="text" size="30" class="unprotectedinput" <%=getTextOptions("xml:/TDDiscountgroup/@TerSystemType")%>/>
	</td>
	</tr>

</table>