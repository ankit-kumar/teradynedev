<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<table class="table" width="100%" editable="false">
<thead>
   <tr> 
		<td sortable="no" class="checkboxheader">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
		<td class="tablecolumnheader"><yfc:i18n>Discount_Group</yfc:i18n></td>    
		<td class="tablecolumnheader"><yfc:i18n>Customer_No</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Customer_Name</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Discount_Percentage</yfc:i18n></td>
		<td class="tablecolumnheader"><yfc:i18n>Service_Type</yfc:i18n></td>	
		<td class="tablecolumnheader"><yfc:i18n>System_Type</yfc:i18n></td>	
   </tr>
</thead>
<tbody>
    <yfc:loopXML binding="xml:/TDDiscountgroupList/@TDDiscountgroup" id="TDDiscountgroup">	
    <tr> 
		<yfc:makeXMLInput name="TERDiscountGroupKey">
            <yfc:makeXMLKey binding="xml:/TDDiscountgroup/@TERDiscountGroupKey" value="xml:/TDDiscountgroup/@TERDiscountGroupKey" />
        </yfc:makeXMLInput>
		<td class="checkboxcolumn">
		<input type="checkbox" value="<%=getParameter("TERDiscountGroupKey")%>" name="EntityKey"/>
		</td>
			<td class="tablecolumn">
                <a href="javascript:showDetailFor('<%=getParameter("TERDiscountGroupKey")%>');">
                    <yfc:getXMLValue binding="xml:/TDDiscountgroup/@TerDiscountGroupId"/>
                </a>
            </td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDDiscountgroup/@TerCustomerNo"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDDiscountgroup/@TerCustomerName"/></td>
			<td class="numerictablecolumn"><yfc:getXMLValue binding="xml:/TDDiscountgroup/@TerDiscountPercentage"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDDiscountgroup/@TerServiceType"/></td>
			<td class="tablecolumn"><yfc:getXMLValue binding="xml:/TDDiscountgroup/@TerSystemType"/></td>			
    </tr>
    </yfc:loopXML> 
</tbody>
</table>