<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.shared.ycp.*" %>
<%  YFCElement nodeElement= (YFCElement) request.getAttribute("LocationInventoryAudit");
	if(nodeElement!=null){ 
		nodeElement.setDateTimeAttribute("FromCreatets", new YFCDate(YCPConstants.YCP_LOW_DATE));
		nodeElement.setAttribute("CreatetsQryType","BETWEEN" );
	}
%> 

<table class="view" width="100%">
<tr>
    <yfc:makeXMLInput name="locationKey">
            <yfc:makeXMLKey binding="xml:/Location/@Node" value="xml:/LocationInventoryAudit/@Node" />
            <yfc:makeXMLKey binding="xml:/Location/@LocationId" value="xml:/LocationInventoryAudit/@LocationId" />
    </yfc:makeXMLInput>
	<yfc:makeXMLInput name="caseKey">
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@CaseId" value="xml:/LocationInventoryAudit/@CaseId" />
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@EnterpriseCode" value="xml:/LocationInventoryAudit/@EnterpriseCode" />
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@FromCreatets" value="xml:/LocationInventoryAudit/@FromCreatets" />
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@CreatetsQryType" value="xml:/LocationInventoryAudit/@CreatetsQryType" />    
	</yfc:makeXMLInput>
		<yfc:makeXMLInput name="palletKey">
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@EnterpriseCode" value="xml:/LocationInventoryAudit/@EnterpriseCode" />
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@PalletId" value="xml:/LocationInventoryAudit/@PalletId" />
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@FromCreatets" value="xml:/LocationInventoryAudit/@FromCreatets" />
			 <yfc:makeXMLKey binding="xml:/LocationInventoryAudit/@CreatetsQryType" value="xml:/LocationInventoryAudit/@CreatetsQryType" />    
	</yfc:makeXMLInput>
	<td class="detaillabel" ><yfc:i18n>Location</yfc:i18n></td>
    <td class="protectedtext">
	 <a <%=getDetailHrefOptions("L01", getParameter("locationKey"),"")%> >
		<yfc:getXMLValue binding="xml:/LocationInventoryAudit/@LocationId" name="LocationInventoryAudit" />
	 </a>
	</td>
	<td class="detaillabel" ><yfc:i18n>Date</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@Createts" name="LocationInventoryAudit" /></td>
   
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>User_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@Modifyuserid" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Task_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@TaskType" name="LocationInventoryAudit" /></td>
</tr>
<tr>
	<td class="detaillabel" ><yfc:i18n>Pallet_ID</yfc:i18n></td>
    <td class="protectedtext">
		<a <%=getDetailHrefOptions("L02", getParameter("palletKey"),"")%> >
			<yfc:getXMLValue binding="xml:/LocationInventoryAudit/@PalletId" name="LocationInventoryAudit" />
		</a>
	</td>
	<td class="detaillabel" ><yfc:i18n>Case_ID</yfc:i18n></td>
    <td class="protectedtext">
		<a <%=getDetailHrefOptions("L02", getParameter("caseKey"),"")%> >
			<yfc:getXMLValue binding="xml:/LocationInventoryAudit/@CaseId" name="LocationInventoryAudit" />
		</a>
	</td>
</tr>
<tr>
	<td class="detaillabel" ><yfc:i18n>Adjustment_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:i18n>
		<yfc:getXMLValue binding="xml:LocationInventoryAudit:/LocationInventoryAudit/@AdjustmentType"/></yfc:i18n>
	</td>
	<td class="detaillabel" ><yfc:i18n>Count_Request_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/CountRequest/@CountRequestNo" name="LocationInventoryAudit" /></td>
</tr>
<tr>
<td class="detaillabel" ><yfc:i18n>Request_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/CountRequest/@RequestType" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Count_Program_Name</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/CountRequest/@CountProgramName" name="LocationInventoryAudit" /></td>
</tr>
</table> 