<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="view" width="100%">
<% String quantity = "0";
YFCElement extnLocnInvAudVwEle = (YFCElement)request.getAttribute("EXTNLocnInvAuditVwList");
YFCElement locnInvAuditEle = (YFCElement) request.getAttribute("LocationInventoryAudit");
boolean abc = locnInvAuditEle.getChildElement("TagDetail").hasAttribute("TotalOnhandSupply");
if(abc){
	 quantity = resolveValue("xml:/LocationInventoryAudit/TagDetail/@TotalOnhandSupply");
	}else {
	quantity = Integer.valueOf(extnLocnInvAudVwEle.getChildNodes().getLength()).toString(); 
}
%>
<tr>
	<yfc:makeXMLInput name="itemKey">
		<yfc:makeXMLKey binding="xml:/Item/@UserOrganizationCode" value="xml:/LocationInventoryAudit/@EnterpriseCode" />
		<yfc:makeXMLKey binding="xml:/Item/@ItemID" value="xml:/LocationInventoryAudit/InventoryItem/@ItemID" />
	    <yfc:makeXMLKey binding="xml:/Item/@ProductClass" value="xml:/LocationInventoryAudit/InventoryItem/@ProductClass" />
	    <yfc:makeXMLKey binding="xml:/Item/@UnitOfMeasure" value="xml:/LocationInventoryAudit/InventoryItem/@UnitOfMeasure" />
    </yfc:makeXMLInput>
	<td class="detaillabel" ><yfc:i18n>Enterprise</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@EnterpriseCode" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
	<td class="protectedtext">
	 <a <%=getDetailHrefOptions("L01", getParameter("itemKey"),"")%> >
		 <yfc:getXMLValue binding="xml:/LocationInventoryAudit/InventoryItem/@ItemID" name="LocationInventoryAudit" />
	 </a>
    </td>
	<td class="detaillabel" ><yfc:i18n>Product_Class</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/InventoryItem/@ProductClass" name="LocationInventoryAudit" /></td>

</tr>
<tr>
	<td class="detaillabel" ><yfc:i18n>Unit_Of_Measure</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/InventoryItem/@UnitOfMeasure" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Item_Description</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/InventoryItem/Item/PrimaryInformation/@Description" name="LocationInventoryAudit" /></td>
</tr>
<tr>
	<td class="detaillabel" ><yfc:i18n>Quantity</yfc:i18n></td>
    <td class="protectednumber"><%=quantity%>		
		<%if (equals("+",resolveValue("xml:/LocationInventoryAudit/@AuditOperation"))){%>
		    <yfc:i18n>+</yfc:i18n>
			<%}else{%>
			<yfc:i18n>-</yfc:i18n>
			<%}%>
	</td>
	<td class="detaillabel" ><yfc:i18n>Manufacturer_Item</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/InventoryItem/Item/PrimaryInformation/@ManufacturerItem" name="LocationInventoryAudit" /></td>
</tr>
</table>
