<%@include file="/yfsjspcommon/yfsutil.jspf"%>

<table class="view" width="100%">
<tr>
    <td class="detaillabel" ><yfc:i18n>FIFO_#</yfc:i18n></td>
    <td class="protectedtext">
	<%=(int)getNumericValue("xml:/LocationInventoryAudit/@FifoNo")%>
	</td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Segment_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@Segment" name="LocationInventoryAudit" /></td>
    <td class="detaillabel" ><yfc:i18n>Segment_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@SegmentType" name="LocationInventoryAudit" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Ship_By_Date</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@ShipByDate" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Receipt_#</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/Receipt/@ReceiptNo" name="LocationInventoryAudit" /></td>
</tr>
<tr>
	<td class="detaillabel" ><yfc:i18n>Inventory_Status</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@InventoryStatus" name="LocationInventoryAudit" /></td>
	<td class="detaillabel" ><yfc:i18n>Country_Of_Origin</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/LocationInventoryAudit/@CountryOfOrigin" name="LocationInventoryAudit" /></td>
</tr>
</table>