<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<table class="view" border=0>
	<tr>
		<td class="detaillabel">
			<yfc:i18n>Status</yfc:i18n>
		</td>
		<td>
			<select  class="combobox"<%=getComboOptions("xml:/Organization/Extn/@CustomerSiteStatus")%>>
					<yfc:loopOptions binding="xml:ORGANIZATION_STATUS:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"   selected="xml:/Output/OrganizationList/Organization/Extn/@CustomerSiteStatus" />
			</select>		
		</td>
		<td nowrap="true" class="detaillabel">
			<yfc:i18n>Payment_Terms</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@PaymentTerms")%>>
					<yfc:loopOptions binding="xml:PAYMENT_TERM:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"  selected="xml:/Output/CustomerList/Customer/Extn/@PaymentTerms"/>
			</select>
		</td>
	</tr>
	<tr>
		<td class="detaillabel">
			<yfc:i18n>Credit_Hold</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@IsCreditHold")%>>
					<yfc:loopOptions binding="xml:CREDIT_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"  selected="xml:/Output/CustomerList/Customer/Extn/@IsCreditHold"/>
			</select>
		</td>
		<td class="detaillabel">
			<yfc:i18n>Service_Hold</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@IsServiceHold")%> >
					<yfc:loopOptions binding="xml:SERVICE_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription"
					value="CodeValue"   selected="xml:/Output/CustomerList/Customer/Extn/@IsServiceHold"/>
			</select>
		</td>
	</tr>
</table>	
	