<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@include file="/console/jsp/modificationutils.jspf"%>

<table class="view" width="100%" border="0">
	<tr>
		<td class="detaillabel">
			<yfc:i18n>Affidavit_No</yfc:i18n>
		</td>
		<td>
			<input type="text" size="20" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@AffidavitNumber")%>" <%=getTextOptions("xml:/Organization/Extn/@AffidavitNumber")%>/>
		</td>
		<td class="detaillabel">
			<yfc:i18n>Affidavit_Validity_Dates</yfc:i18n>
		</td>
		<td nowrap="true">
			<input class="dateinput" type="text" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@AffidavitFromDate")%>" <%=getTextOptions("xml:/Organization/Extn/@AffidavitFromDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
			<yfc:i18n>To</yfc:i18n>	
			<input class="dateinput" type="text" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@AffidavitToDate")%>" <%=getTextOptions("xml:/Organization/Extn/@AffidavitToDate_YFCDATE")%>/>
			<img class="lookupicon" name="search" onclick="invokeCalendar(this);return false" <%=getImageOptions(YFSUIBackendConsts.DATE_LOOKUP_ICON, "Calendar") %> />
		</td>
		<td class="detaillabel">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<yfc:i18n>Visible_To_Customer</yfc:i18n>
		</td>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@SpecialInstrVisible")%>>
				<yfc:loopOptions binding="xml:SERVICE_HOLD:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" isLocalized="Y" selected="xml:/Output/CustomerList/Customer/Extn/@SpecialInstrVisible"/>
			</select>
		</td>
		<td class="detaillabel">
			<yfc:i18n>Special_Customer_Instructions</yfc:i18n>
		</td>
		<td>
			<textarea class="unprotectedtextareainput" rows="3" cols="50" <%=getTextAreaOptions("xml:/Organization/ForCustomer/@SpecialCustInstr")%>><yfc:getXMLValue binding="xml:/Output/CustomerList/Customer/Extn/@SpecialCustInstr" /></textarea>
		</td>
	</tr>
</table>	
	