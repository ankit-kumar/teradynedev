<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@include file="/console/jsp/orderentry.jspf" %>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>
<%@ page import="com.bridge.sterling.utils.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/commonutil.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>
<table class="view" width="100%" border="0" >
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Default_Currency</yfc:i18n>
		</td>
		<%
		String defCurrency = resolveValue("xml:/Output/CustomerList/Customer/Extn/@DefaultCurrency");
		if (isVoid(defCurrency)) {
			String orgLocale = resolveValue("xml:/Output/OrganizationList/Organization/@LocaleCode");
			YFCElement localeList = getElement("LocaleList");
			defCurrency = XPathUtil.getXpathAttribute(localeList, "/LocaleList/Locale[@Localecode='" + orgLocale + "']/@Currency");
		}		
		%>
		<td>
			<select class="combobox" <%=getComboOptions("xml:/Organization/ForCustomer/@DefaultCurrency")%> >
				<yfc:loopOptions binding="xml:Currency:/CurrencyList/@Currency" name="CurrencyDescription" value="CurrencyKey" selected="<%=defCurrency%>"/>
			</select>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Is Tax Exempt ?</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/@TaxExemptFlag", "xml:/Output/OrganizationList/Organization/@TaxExemptFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' '>
			</input>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Expemtion Certificate</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@TaxExemptionCertificate")%>" <%=getTextOptions("xml:/Organization/@TaxExemptionCertificate")%>/>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Price_Zone</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/CustomerList/Customer/Extn/@PriceZone")%>" <%=getTextOptions("xml:/Organization/ForCustomer/@PriceZone")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Taxer_Payer_ID</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@TaxpayerId")%>" <%=getTextOptions("xml:/Organization/@TaxpayerId")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Tax_Jurisdiction</yfc:i18n>&nbsp;
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@TaxJurisdiction")%>" <%=getTextOptions("xml:/Organization/@TaxJurisdiction")%>/>
		</td>	
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Is Internal Customer-Site ?</yfc:i18n>
		</td>
		<td>
			<input type="checkbox" <%=getCheckBoxOptions("xml:/Organization/ForCustomer/@InternalCustomerFlag", "xml:/Output/CustomerList/Customer/Extn/@InternalCustomerFlag", "Y")%> yfcCheckedValue='Y' yfcUnCheckedValue=' ' >
			</input>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Issuing_Authority</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@IssuingAuthority")%>" <%=getTextOptions("xml:/Organization/@IssuingAuthority")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Service_Office</yfc:i18n>
		</td>
		<td>
			<input type="text" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@TeradyneServiceOffice")%>" <%=getTextOptions("xml:/Organization/Extn/@TeradyneServiceOffice")%>/>
			<img class="lookupicon" onclick="callOrgLookupForCustomerMaster('xml:/Organization/Extn/@TeradyneServiceOffice', 'xml:/Organization/Extn/@TeradyneServiceOffice','organization','')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_For_Service_Office")%>/>
		</td>
	</tr>
	<tr>
		<td class="detaillabel" nowrap="true" colspan="2" style="text-align:left">
			<yfc:i18n>Customer_Site_Type</yfc:i18n>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Authority_Type</yfc:i18n>
		</td>
		<td>
			<input type="text" size="15" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/@AuthorityType")%>" <%=getTextOptions("xml:/Organization/@AuthorityType")%>/>
		</td>
		<td class="detaillabel" nowrap="true">
			<yfc:i18n>Revenue_Office</yfc:i18n>
		</td>
		<td>
			<input type="text" class="unprotectedinput" value="<%=resolveValue("xml:/Output/OrganizationList/Organization/Extn/@TeradyneRevenueRegion")%>" <%=getTextOptions("xml:/Organization/Extn/@TeradyneRevenueRegion")%>/>
			<img class="lookupicon" onclick="callOrgLookupForCustomerMaster('xml:/Organization/Extn/@TeradyneRevenueRegion', 'xml:/Organization/Extn/@TeradyneRevenueRegion','organization','')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_For_Revenue_Office")%>/>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<select class="combobox" <%=getComboOptions("xml:/Organization/Extn/@CustomerSiteType")%> >
				<yfc:loopOptions binding="xml:CUSTOMER_SITE_TYPES:/CommonCodeList/@CommonCode" name="CodeShortDescription" value="CodeValue" selected="xml:/Output/OrganizationList/Organization/Extn/@CustomerSiteType"/>
			</select>
		</td>
	</tr>
</table>
	