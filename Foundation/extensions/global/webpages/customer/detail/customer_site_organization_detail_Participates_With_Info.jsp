<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<%@ page import="com.yantra.yfc.dom.*" %>

<script language="javascript" src="<%=request.getContextPath()%>/yfcscripts/yfc.js"></script>

<script langugae="javascript">

function loadEnterprises() {
	var obj = new Object();
	obj.createCustomerOrg = "N";
	obj.createCustomerSiteOrg = "N";
	obj.viewDetails = "N";
	obj.viewAudits = "N";
	obj.addParticipantEnt = "Y";
	obj.setEnt = "Y";
	obj.orgCode = "<%=resolveValue("xml:/Output/OrganizationList/Organization/@OrganizationCode")%>";
	yfcShowSearchPopupWithParams('','lookup',900,550,obj,'TDCustOrg','');
}

</script>

<table class="table" editable="false" width="100%" cellspacing="0">
	<thead>
		<tr>
			<td class="tablecolumnheader" colspan=3 style="text-align:center"><b>Enterprises</b></td>
		</tr>
		<tr>
            <td sortable="no" class="checkboxheader">
                <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
            </td>
			<td class="tablecolumnheader">Org. ID</td>
			<td class="tablecolumnheader">Org. Name</td>
		</tr>
	</thead>
	<tbody>
		<yfc:loopXML binding="xml:/Output/OrganizationList/Organization/EnterpriseOrgList/@OrgEnterprise" id="Organization">
			<tr>
				<yfc:makeXMLInput name="Organization1">
					<yfc:makeXMLKey binding="xml:/Organization/@EnterpriseOrganizationKey" value="xml:Organization:/OrgEnterprise/@EnterpriseOrganizationKey" />
                    <yfc:makeXMLKey binding="xml:/Organization/@OrganizationCode" value="xml:/Output/OrganizationList/Organization/@OrganizationCode" />
                </yfc:makeXMLInput>                
				<td class="checkboxcolumn" id="entList" style="display:block;">
					<input type="checkbox" value='<%=getParameter("Organization1")%>' name="chkEntityKey" />
				</td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:Organization:/OrgEnterprise/@EnterpriseOrganizationKey"/></td>
				<td class="tablecolumn"><yfc:getXMLValue binding="xml:Organization:/OrgEnterprise/@EnterpriseOrganizationName"/></td>
			</tr>
		</yfc:loopXML>
	</tbody>
</table>