	function insertRecord(num, bodyEle, count,max){
		for(i=count;i<max;i++){
			var row = document.createElement("tr");
			
			if(count % 2 == 0){
				row.setAttribute("class","evenrow");
			}else{
				row.setAttribute("class","oddrow");
			}
			row.setAttribute("id","tr123");			
			
			var img1 = document.getElementById("img1");
			var addr1 = document.getElementById("TerAddressLine11");
			var addr2 = document.getElementById("TerAddressLine21");
			var addr3 = document.getElementById("TerAddressLine31");
			var addr4 = document.getElementById("TerAddressLine41");
			var addr5 = document.getElementById("TerAddressLine51");
			var addr6 = document.getElementById("TerAddressLine61");
			var city = document.getElementById("TerCity1");
			var state = document.getElementById("TerState1");
			var country = document.getElementById("TerCountry1");
			var zip = document.getElementById("TerZipCode1");
			var dayPhone = document.getElementById("TerDayPhone1");
			var evePhone = document.getElementById("TerEveningPhone1");
			var mobPhone = document.getElementById("TerMobilePhone1");
			var beeper = document.getElementById("TerBeeper1");
			var othPhone = document.getElementById("TerOtherPhone1");
			var dayFax = document.getElementById("TerDayFaxNo1");
			var eveFax = document.getElementById("TerEveningFaxNo1");
			var emailId = document.getElementById("TerEMailID1");
			var altEmailId = document.getElementById("TerAlternateEmailID1");
			var billingId = document.getElementById("TerBillingId1");
			var flag = document.getElementById("flag1");
			
			setValues(row,'TerBillingId'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerBillingID',billingId,i);
			setValues(row,'TerAddressLine1'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAddressLine1',addr1,i);
			setValues(row,'TerAddressLine2'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAddressLine2',addr2,i);
			setValues(row,'TerAddressLine3'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAddressLine3',addr3,i);
			setValues(row,'TerAddressLine4'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAddressLine4',addr4,i);
			setValues(row,'TerAddressLine5'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAddressLine5',addr5,i);
			setValues(row,'TerAddressLine6'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAddressLine6',addr6,i);
			setValues(row,'TerCity'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerCity',city,i);
			setValues(row,'TerState'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerState',state,i);
			setValues(row,'TerCountry'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerCountry',country,i);
			setValues(row,'TerZipCode'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerZipCode',zip,i);
			setValues(row,'TerDayPhone'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerDayPhone',dayPhone,i);
			setValues(row,'TerEveningPhone'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerEveningPhone',evePhone,i);
			setValues(row,'TerMobilePhone'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerMobilePhone',mobPhone,i);
			setValues(row,'TerBeeper'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerBeeper',beeper,i);
			setValues(row,'TerOtherPhone'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerOtherPhone',othPhone,i);
			setValues(row,'TerDayFaxNo'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerDayFaxNo',dayFax,i);
			setValues(row,'TerEveningFaxNo'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerEveningFaxNo',eveFax,i);
			setValues(row,'TerEMailID'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerEMailID',emailId,i);
		    setValues(row,'TerAlternateEmailID'+i,'xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_'+i+'/@TerAlternateEmailID',altEmailId,i);
			
			var td = document.createElement("td");
			td.setAttribute("class","tablecolumn");
			td.innerHTML = "<INPUT id='flag"+i+"' class='checkbox' onclick='checkCurrentBoxOnly(this);' name='xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerDefaultFLag' value='Y' type='checkbox' OldValue='N' oldChecked='false' originalName='xml:/Organization/Extn/TerCustBillingOrgList/TerCustBillingOrg_"+i+"/@TerDefaultFLag'><INPUT value='N' type='hidden'>";
			
			row.appendChild(td);
			bodyEle.appendChild(row);	
		}
	}
	
	function setValues(row, idvalue,nameValue,element,count){
	
		var td = document.createElement("td");
		td.setAttribute("class","tablecolumn");
		var datatype = element.getAttribute("dataType");
		var max = element.getAttribute("maxLength");
		var size = element.getAttribute("size");
		if(idvalue.indexOf("TerBillingId") != "-1"){
			td.innerHTML = '<input type="text" name="'+nameValue+'" id="'+idvalue+'" value="" dataType="'+datatype+'" size="'+size+'" OldValue="" maxLength="'+max+'"/><img id="img'+count+'" name="search" alt="Search for BillToID" src="/smcfs/console/icons/lookup.gif" class="lookupicon" onclick="callLookupforBill(\'TerBillingId'+count+'\',\'TDBillTo\',\'TerAddressLine1'+count+'\',\'TerAddressLine2'+count+'\',\'TerAddressLine3'+count+'\',\'TerAddressLine4'+count+'\',\'TerAddressLine5'+count+'\',\'TerAddressLine6'+count+'\',\'TerCity'+count+'\',\'TerState'+count+'\',\'TerCountry'+count+'\',\'TerZipCode'+count+'\',\'TerDayPhone'+count+'\',\'TerEveningPhone'+count+'\',\'TerMobilePhone'+count+'\',\'TerBeeper'+count+'\',\'TerOtherPhone'+count+'\',\'TerDayFaxNo'+count+'\',\'TerEveningFaxNo'+count+'\',\'TerEMailID'+count+'\',\'TerAlternateEmailID'+count+'\');" />';
		}else{
			td.innerHTML = "<input type='text' name='"+nameValue+"' id='"+idvalue+"' value='' dataType='"+datatype+"' size='"+size+"' OldValue='' maxLength='"+max+"'/>";
		}
		
		row.appendChild(td);
	}

	function addRow(numCopy,body,record){
		var numcopyEle = document.all("numCopyAdd");
		var bodyEle = document.all(body);
		var recordEle = document.getElementById(record);
		var max = parseInt(numcopyEle.value)+parseInt(recordEle.value);
		insertRecord(numcopyEle.value,bodyEle,recordEle.value,max);
		recordEle.value=(max-1);
	}