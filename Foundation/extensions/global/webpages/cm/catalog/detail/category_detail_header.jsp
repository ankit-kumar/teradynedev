<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<% 
    String sFullPath = resolveValue("xml:Category:/Category/@CategoryPath") ;
    int iLastIndex = sFullPath.lastIndexOf("/");
    String sActualPath  = sFullPath.substring(0,iLastIndex+1);
    String sCurrentCategory  = sFullPath.substring(iLastIndex+1);
%>
<table class="view" width="100%">
<tr>
    <td class="detaillabel" ><yfc:i18n>Category_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/Category/@CategoryID" name="Category" /></td>
    <td class="detaillabel" ><yfc:i18n>Description</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValueI18NDB binding="xml:/Category/@Description" name="Category" /></td>
</tr>
<tr>
    <yfc:makeXMLInput name="parentCategoryKey">
        <yfc:makeXMLKey binding="xml:/Category/@CategoryKey" value="xml:/Category/@ParentCategoryKey" />
        <yfc:makeXMLKey binding="xml:/Category/@OrganizationCode" value="xml:/Category/@OrganizationCode" />
    </yfc:makeXMLInput>
    <td class="detaillabel" ><yfc:i18n>Category_Path</yfc:i18n></td>
    <td class="protectedtext">
        
        <%if(isVoid(resolveValue("xml:Category:/Category/@ParentCategoryKey"))) {%>
            <%=sActualPath%><%=sCurrentCategory%>
        <%} else {%>
            <a href="javascript:showDetailFor('<%=getParameter("parentCategoryKey")%>');">
                <%=sActualPath%></a><%=sCurrentCategory%>
            </a>
        <%}%>
    </td>
</tr>
</table>
