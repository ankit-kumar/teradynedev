<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>

<%
   	String tagControlFlag = getValue("ItemDetails","xml:/Item/InventoryParameters/@TagControlFlag");
%>

<table class="view" width="100%">
<tr>
    <td class="detaillabel" ><yfc:i18n>Activity_Date</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Modifyts" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Modified_By</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Modifyuserid" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Transaction_Type</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@TransactionType" name="InventoryAudits" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Supply_Type</yfc:i18n></td>
    <td class="protectedtext"><%=getComboText("xml:SupplyTypeList:/InventorySupplyTypeList/@InventorySupplyType","Description","SupplyType",resolveValue("xml:/InventoryAudits/InventoryAudit/@SupplyType"),true)%></td>
    <td class="detaillabel" ><yfc:i18n>Quantity</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Quantity" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Unit_Cost</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@UnitCost" name="InventoryAudits" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Ship_Node</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@ShipNode" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Ship_By_Date</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@ShipByDate" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Segment_Type</yfc:i18n></td>
	<% 
	  if(!isVoid(resolveValue("xml:/InventoryAudits/InventoryAudit/@SegmentType"))){	
       YFCElement commCodeElem = YFCDocument.createDocument("CommonCode").getDocumentElement();
	   commCodeElem.setAttribute("CodeType","SEGMENT_TYPE");
	   commCodeElem.setAttribute("CodeValue",resolveValue("xml:/InventoryAudits/InventoryAudit/@SegmentType"));
	   YFCElement templateElem = YFCDocument.parse("<CommonCode CodeName=\"\" CodeShortDescription=\"\" CodeType=\"\" CodeValue=\"\" CommonCodeKey=\"\" />").getDocumentElement();
	%>
		<yfc:callAPI apiName="getCommonCodeList" inputElement="<%=commCodeElem%>" 
													templateElement="<%=templateElem%>" outputNamespace=""/>
		<td class="protectedtext">
			<yfc:getXMLValueI18NDB binding="xml:/CommonCodeList/CommonCode/@CodeShortDescription" />
		</td>
	<% } else { %>
		<td class="tablecolumn">&nbsp;</td>
	<% } %>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Segment</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Segment" name="InventoryAudits" /></td>    
    <td class="detaillabel" ><yfc:i18n>Reference_1</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Reference1" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Reference_2</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Reference2" name="InventoryAudits" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Reference_3</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Reference3" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Reference_4</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Reference4" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Reference_5</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@Reference5" name="InventoryAudits" /></td>
</tr>

<% if ((equals(tagControlFlag, "Y")) || (equals(tagControlFlag, "S"))) { %>
    <tr>
		<td colspan="6">
			<jsp:include page="/im/inventory/detail/inventory_detail_tagattributes.jsp" flush="true">
		        <jsp:param name="NoOfColumns" value='3'/>
				<jsp:param name="TagContainer" value="ItemDetails"/>
				<jsp:param name="TagElement" value="InventoryTagAttributes"/>
		        <jsp:param name="Modifiable" value='true'/>
		        <jsp:param name="LabelTDClass" value='detaillabel'/>
		        <jsp:param name="InputTDClass" value='protectedtext'/>
		        <jsp:param name="BindingPrefix" value='xml:/InventoryAudits/InventoryAudit'/>
				<jsp:param name="HideDescriptorAttributes" value='true'/>
	    	</jsp:include>
		</td>
    </tr>
<% } %>
</table>