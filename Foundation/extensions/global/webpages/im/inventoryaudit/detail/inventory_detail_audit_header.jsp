<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<table class="view" width="100%">
<tr>
    <td class="detaillabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@ItemID" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Product_Class</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@ProductClass" name="InventoryAudits" /></td>
    <td class="detaillabel" ><yfc:i18n>Unit_Of_Measure</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/@UnitOfMeasure" name="InventoryAudits" /></td>
</tr>
<tr>
    <td class="detaillabel" ><yfc:i18n>Description</yfc:i18n></td>
    <td class="protectedtext"><yfc:getXMLValue binding="xml:/InventoryAudits/InventoryAudit/Item/PrimaryInformation/@Description" name="InventoryAudits" /></td>
</tr>
</table>
