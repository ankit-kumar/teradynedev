<%@include file="/im/inventory/detail/inventory_detail_graphinclude.jspf" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<%
    String sSupplyTitle = "Supply";
	String sSupplyQty = ": " + resolveValue("xml:/InventoryInformation/Item/InventoryTotals/Supplies/@TotalSupply");
    
	String sDemandTitle = "Demand" ;
	String sDemandQty = ": " + resolveValue("xml:/InventoryInformation/Item/InventoryTotals/Demands/@TotalDemand");

	String sAvailable = "Available";
	String sAvailableQty = null;
    if(equals(resolveValue("xml:/InventoryInformation/Item/@TrackedEverywhere"),"N")) 
        sAvailableQty = "INFINITE" ;
    else
        sAvailableQty = ": " +resolveValue("xml:/InventoryInformation/Item/@AvailableToSell");
%>

<%
    String itemID = resolveValue("xml:/InventoryInformation/Item/@ItemID");
    String productClass = resolveValue("xml:/InventoryInformation/Item/@ProductClass");
    String unitOfMeasure = resolveValue("xml:/InventoryInformation/Item/@UnitOfMeasure");
    String orgCode = resolveValue("xml:/InventoryInformation/Item/@OrganizationCode");
    String shipNode = resolveValue("xml:/InventoryInformation/Item/@ShipNode");

    YFCElement inputElem = YFCDocument.parse("<InventoryNodeControl ItemID=\"" + itemID + "\" ProductClass=\"" + productClass + "\" UnitOfMeasure=\"" + unitOfMeasure + "\" OrganizationCode=\"" + orgCode + "\" Node=\"" + shipNode + "\" />").getDocumentElement();

    YFCElement templateElem = YFCDocument.parse("<InventoryNodeControl ItemID=\"\" ProductClass=\"\" UnitOfMeasure=\"\" OrganizationCode=\"\" Node=\"\" InvPictureIncorrectNow=\"\" InvPictureIncorrectTillDate=\"\" />").getDocumentElement();
%>

    <yfc:callAPI apiName="getInventoryNodeControlList" inputElement="<%=inputElem%>" templateElement="<%=templateElem%>" outputNamespace="InventoryNodeControlList"/>

    <% String showEnableSourcing = "N"; %>
    <% String showUntilDate = ""; %>
    <yfc:loopXML binding="xml:/InventoryNodeControlList/@InventoryNodeControl" id="InventoryNodeControl">
        <%
            String invPicIncorrectNow = resolveValue("xml:/InventoryNodeControl/@InvPictureIncorrectNow");
            
            if ((equals(showEnableSourcing,"N")) && (equals(invPicIncorrectNow,"Y"))) {
                // Check to see if the input criteria matches output criteria.
                if ((equals(itemID,resolveValue("xml:/InventoryNodeControl/@ItemID")))
                 && (equals(productClass,resolveValue("xml:/InventoryNodeControl/@ProductClass")))
                 && (equals(unitOfMeasure,resolveValue("xml:/InventoryNodeControl/@UnitOfMeasure")))
                 && (equals(orgCode,resolveValue("xml:/InventoryNodeControl/@OrganizationCode")))
                 && (equals(shipNode,resolveValue("xml:/InventoryNodeControl/@Node")))) {

                    showEnableSourcing = "Y";
                    showUntilDate = resolveValue("xml:/InventoryNodeControl/@InvPictureIncorrectTillDate");
                }
            }
        %>
    </yfc:loopXML>
    <%
        YFCElement invInfo = (YFCElement) request.getAttribute("InventoryInformation");
		if(invInfo!=null){
        invInfo.setAttribute("ShowEnableSourcing", showEnableSourcing);
        invInfo.setAttribute("ShowUntilDate", showUntilDate);
		}
    %>

<table class="anchor" cellpadding="7px"  cellSpacing="0" >
<tr>
    <td colspan="2" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I01"/>
            <jsp:param name="ShowShipNode" value="true"/>
        </jsp:include>
    </td>
</tr>
<tr>
    <td width="50%" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I02"/>
            <jsp:param name="requestID" value="<%=sRequestID%>" />
            <jsp:param name="totalSupply" value="<%=String.valueOf(iTotalSupply)%>" />
            <jsp:param name="Title" value="<%=sSupplyTitle%>" />
			<jsp:param name="TitleQty" value="<%=sSupplyQty%>" />
            <jsp:param name="IPHeight" value="150px" />
        </jsp:include>
    </td>
    <td width="50%" >
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I03"/>
            <jsp:param name="requestID" value="<%=sRequestID%>" />
            <jsp:param name="totalDemand" value="<%=String.valueOf(iTotalDemand)%>" />
            <jsp:param name="Title" value="<%=sDemandTitle%>" />
			<jsp:param name="TitleQty" value="<%=sDemandQty%>" />
            <jsp:param name="IPHeight" value="150px" />
        </jsp:include>
    </td>
</tr>
<tr>
    <td colspan="2">
        <jsp:include page="/yfc/innerpanel.jsp" flush="true" >
            <jsp:param name="CurrentInnerPanelID" value="I04"/>
            <jsp:param name="requestID" value="<%=sRequestID%>" />
            <jsp:param name="Title" value="<%=sAvailable%>" />
			<jsp:param name="TitleQty" value="<%=sAvailableQty%>" />
		</jsp:include>
    </td>
</tr>
</table>
