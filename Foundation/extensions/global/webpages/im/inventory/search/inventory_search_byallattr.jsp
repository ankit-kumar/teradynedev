<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<%@ page import="com.yantra.yfs.ui.backend.*" %>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/tools.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/extn/scripts/serviceContracts.js"></script>
<table width="100%" class="view">

    <jsp:include page="/yfsjspcommon/common_fields.jsp" flush="true">
        <jsp:param name="EnterpriseCodeBinding" value="xml:/InventoryItem/@OrganizationCode"/>
        <jsp:param name="EnterpriseCodeLabel" value="Organization"/>
        <jsp:param name="RefreshOnEnterpriseCode" value="true"/>
        <jsp:param name="ShowDocumentType" value="false"/>
        <jsp:param name="OrganizationListForInventory" value="true"/>
    </jsp:include>
    <% // Now call the APIs that are dependent on the common fields (Organization Code) %>
    <yfc:callAPI apiID="AP2"/>
    <yfc:callAPI apiID="AP3"/>
	<yfc:callAPI apiID="AP4"/>
	
<tr>
    <td class="searchlabel" ><yfc:i18n>Item_ID</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@ItemIDQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/InventoryItem/@ItemIDQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/InventoryItem/@ItemID")%> />
		<% String extraParams = getExtraParamsForTargetBinding("xml:/Item/@CallingOrganizationCode", getValue("CommonFields", "xml:/CommonFields/@EnterpriseCode"));%>
        <img class="lookupicon" 
		onclick="callTRItemLookup('xml:/InventoryItem/@ItemID','xml:/InventoryItem/@ProductClass','xml:/InventoryItem/@UnitOfMeasure','TRitem','<%=extraParams%>')" <%=getImageOptions(YFSUIBackendConsts.LOOKUP_ICON, "Search_for_TRitem") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Product_Class</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@ProductClass" class="combobox" >
            <yfc:loopOptions binding="xml:ProductClass:/CommonCodeList/@CommonCode" 
                name="CodeValue" value="CodeValue" selected="xml:/InventoryItem/@ProductClass"/>
        </select>
    </td>
</tr>
<tr>
    <td  class="searchlabel" ><yfc:i18n>Unit_Of_Measure</yfc:i18n></td>
</tr>
<tr>
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@UnitOfMeasure" class="combobox" >
            <yfc:loopOptions binding="xml:UnitOfMeasure:/ItemUOMMasterList/@ItemUOMMaster" 
                name="UnitOfMeasure" value="UnitOfMeasure" selected="xml:/InventoryItem/@UnitOfMeasure"/>
        </select>
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Serial_No</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@SerialNoQryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/InventoryItem/@SerialNoQryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/InventoryItem/@SerialNo") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>OEM_SerialNo</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@SecondarySerial1QryType" class="combobox" >
            <yfc:loopOptions binding="xml:/QueryTypeList/StringQueryTypes/@QueryType" 
                name="QueryTypeDesc" value="QueryType" selected="xml:/InventoryItem/@SecondarySerial1QryType"/>
        </select>
        <input type="text" class="unprotectedinput" <%=getTextOptions("xml:/InventoryItem/@SecondarySerial1") %> />
    </td>
</tr>
<tr>
    <td class="searchlabel" ><yfc:i18n>Country_Of_Origin</yfc:i18n></td>
</tr>
<tr >
    <td class="searchcriteriacell" nowrap="true">
        <select name="xml:/InventoryItem/@CountryOfOrigin" class="combobox" >
            <yfc:loopOptions binding="xml:Country:/CommonCodeList/@CommonCode" 
                name="CodeValue" value="CodeValue" selected="xml:/InventoryItem/@CountryOfOrigin"/>
        </select>
    </td>
</tr>

</table>
