<%@include file="/yfsjspcommon/yfsutil.jspf"%>
<script language="javascript" src="<%=request.getContextPath()%>/console/scripts/im.js"></script>
<%
YFCElement inputEle = (YFCElement)request.getAttribute("InventoryItem");
String serialNoStr = inputEle.getAttribute("SerialNo");
String secSerial1Str = inputEle.getAttribute("SecondarySerial1");
//System.out.println("SerialNo:"+serialNoStr+" -"+secSerial1Str);
if(serialNoStr == "" && secSerial1Str == ""){%>
<yfc:callAPI apiID="AP2" />
<%YFCElement root = (YFCElement)request.getAttribute("InventoryList");
int invItemsCount = countChildElements(root);%>
<script language="javascript">setRetrievedRecordCount(<%=invItemsCount%>);</script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" style="width:<%= getUITableSize("xml:/InventoryItem/@ItemID")%>">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/InventoryItem/@ProductClass")%>">
            <yfc:i18n>PC</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/InventoryItem/@UnitOfMeasure")%>">
            <yfc:i18n>UOM</yfc:i18n>
        </td>
        <td class="tablecolumnheader">
            <yfc:i18n>Description</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="InventoryList" binding="xml:/InventoryList/@InventoryItem" id="InventoryItem"  keyName="InventoryItemKey" > 
    <tr> 
        <yfc:makeXMLInput name="inventoryItemKey">
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:/InventoryItem/@ItemID" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@UnitOfMeasure" value="xml:/InventoryItem/@UnitOfMeasure" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ProductClass" value="xml:/InventoryItem/@ProductClass" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@OrganizationCode" value="xml:InventoryList:/InventoryList/@OrganizationCode" />
			<% if(isShipNodeUser()) { %>
				<yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:CurrentUser:/User/@Node" />
			<%}%>
        </yfc:makeXMLInput>
        <td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("inventoryItemKey")%>' name="EntityKey"/>
			<input type="hidden" name='ItemID_<%=InventoryItemCounter%>' value='<%=resolveValue("xml:/InventoryItem/@ItemID")%>' />
			<input type="hidden" name='UOM_<%=InventoryItemCounter%>' value='<%=resolveValue("xml:/InventoryItem/@UnitOfMeasure")%>' />
			<input type="hidden" name='PC_<%=InventoryItemCounter%>' value='<%=resolveValue("xml:/InventoryItem/@ProductClass")%>' />
			<input type="hidden" name='OrgCode_<%=InventoryItemCounter%>' value='<%=resolveValue("xml:InventoryList:/InventoryList/@OrganizationCode")%>' />
        </td>
        <td class="tablecolumn">
            <a onclick="javascript:showDetailFor('<%=getParameter("inventoryItemKey")%>');return false;" href=""><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@ItemID"/></a>
        </td>
        <td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@ProductClass"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/@UnitOfMeasure"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="InventoryItem" binding="xml:/InventoryItem/Item/PrimaryInformation/@Description"/></td>
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
<%}
else{%>
<yfc:callAPI apiID="AP1" />
<%//System.out.println("Else entered");
YFCElement root = (YFCElement)request.getAttribute("EXTNItemSerVwList");
int countElem = countChildElements(root); %>
<script language="javascript">setRetrievedRecordCount(<%=countElem%>);</script>
<table class="table" border="0" cellspacing="0" width="100%">
<thead>
    <tr> 
        <td class="checkboxheader" sortable="no">
            <input type="checkbox" name="checkbox" value="checkbox" onclick="doCheckAll(this);"/>
        </td>
        <td class="tablecolumnheader" nowrap="true" style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@ItemID")%>">
            <yfc:i18n>Item_ID</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@SerialNo")%>">
            <yfc:i18n>Ter S/N</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@SecondarySerial1")%>">
            <yfc:i18n>OEM S/N</yfc:i18n>
        </td>
        <td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@ProductClass")%>">
            <yfc:i18n>PC</yfc:i18n>
        </td>
		<td class="tablecolumnheader" style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@Description")%>" >
            <yfc:i18n>Description</yfc:i18n>
        </td>
        <td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@ShipNode")%>">
            <yfc:i18n>Node</yfc:i18n>
        </td>
		<td class="tablecolumnheader"  nowrap="true"  style="width:<%= getUITableSize("xml:/EXTNItemSerVwList/EXTNItemSerVw/@ShipNode")%>">
            <yfc:i18n>Calibration Date</yfc:i18n>
        </td>
		<td class="tablecolumnheader">
            <yfc:i18n>COO</yfc:i18n>
        </td>
    </tr>
</thead>
<tbody>
    <yfc:loopXML name="EXTNItemSerVwList" binding="xml:/EXTNItemSerVwList/@EXTNItemSerVw" id="EXTNItemSerVw"  keyName="ItemSerVwKey" > 
    <tr> 
        <yfc:makeXMLInput name="inventoryItemKey">
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ItemID" value="xml:/EXTNItemSerVw/@ItemID" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@UnitOfMeasure" value="xml:/EXTNItemSerVw/@UnitOfMeasure" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@ProductClass" value="xml:/EXTNItemSerVw/@ProductClass" />
            <yfc:makeXMLKey binding="xml:/InventoryItem/@OrganizationCode" value="xml:/EXTNItemSerVw/@OrganizationCode" />
			<% if(isShipNodeUser()) { %>
				<yfc:makeXMLKey binding="xml:/InventoryItem/@ShipNode" value="xml:CurrentUser:/User/@Node" />
			<%}%>
        </yfc:makeXMLInput>
        <td class="checkboxcolumn">
            <input type="checkbox" value='<%=getParameter("inventoryItemKey")%>' name="EntityKey"/>
			<input type="hidden" name='ItemID_<%=EXTNItemSerVwCounter%>' value='<%=resolveValue("xml:/EXTNItemSerVw/@ItemID")%>' />
			<input type="hidden" name='UOM_<%=EXTNItemSerVwCounter%>' value='<%=resolveValue("xml:/EXTNItemSerVw/@UnitOfMeasure")%>' />
			<input type="hidden" name='PC_<%=EXTNItemSerVwCounter%>' value='<%=resolveValue("xml:/EXTNItemSerVw/@ProductClass")%>' />
			<input type="hidden" name='OrgCode_<%=EXTNItemSerVwCounter%>' value='<%=resolveValue("xml:/EXTNItemSerVw/@OrganizationCode")%>' />
        </td>
        <td class="tablecolumn">
            <a onclick="javascript:showDetailFor('<%=getParameter("inventoryItemKey") %>');return false;" href=""><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@ItemID"/></a>
        </td>
		<td class="tablecolumn"><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@SerialNo"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@SecondarySerial1"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@ProductClass"/></td>
        <td class="tablecolumn"><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@Description"/></td>
		<td class="tablecolumn"><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@ShipNode"/></td>
		<td class="tablecolumn">&nbsp;</td>
		<td class="tablecolumn"><yfc:getXMLValue name="EXTNItemSerVw" binding="xml:/EXTNItemSerVw/@CountryOfOrigin"/></td>
        
    </tr>
    </yfc:loopXML> 
</tbody>
</table>
<%}%>